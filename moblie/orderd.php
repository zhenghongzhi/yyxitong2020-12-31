<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "订单详情";
  $order = $db->get_one("select o.*,k.kehu_number,s.subject,s.kehu_number as skehu_number,s.bankNo,s.openName from yasa_order as o 
  	left join yasa_kehu as k on o.pid1 = k.id 
  	left join yasa_shangjia as s on s.id = o.pid2
  	where o.id = $id");
  $shangjiasql = $db->query("select * from yasa_shangjia where is_delete = 1");
  $kehusql = $db->query("select * from yasa_kehu where is_delete = 1");

  if ($k) {
  	// echo $order['pid1'];die;
  	$kehu = $db->get_one("select * from yasa_kehu where id = $order[pid1]");
  	$sj = $db->get_one("select * from yasa_shangjia where id = $order[pid2]");
  }
  // print_r($order);die;
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>订单详情</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <!-- <link rel="stylesheet" href="css/weui.min.css"/> -->
	    <!-- <link rel="stylesheet" href="css/jquery-weui.min.css"/> -->
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      <link href="css/select2.min.css" rel="stylesheet" />

	    <script src="js/jquery-1.8.3.min.js"></script>
      	<script type="text/javascript" src="js/layer/layer.js"></script>
      <script src="js/select2.min.js"></script>

		
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
  			
  			*{
  				cursor:pointer;
  			}
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    .content{
		    	width: 90%;
		    	margin: 0px auto;
		    	padding: 15px 8px;
		    	border-radius: 20px;
		    	background: rgba(204, 204, 204, 0.21);
		    	margin-top: 20px;
		    	border: 1px solid rgb(204, 204, 204);
		    }
			.content table tr td{
				width: 25%;
				text-align: left;
				height: 40px;
				line-height: 40px;
			}
			.radio{
				margin-top: 30px;
				padding: 10px;
				margin-left: 10px;
			}
			.radio .checkbox{
				width: 20px;
				height: 20px;
				-webkit-appearance:checkbox;
			}
			#shi,#fou{
				margin-left: 40px;
			}
			.qianzi{
				margin: 10px;
				margin-top: 20px;
				height: 200px;
				border: 1px solid rgb(204, 204, 204)
			}
			.submit{
				display: flex;
  				justify-content: center;
  				margin-top: 20px;
  				margin-bottom: 100px;
			}
			.button{
				width: 74px;
    			height: 35px;
    			background-color: rgb(0, 153, 255);
    			border: 1px solid rgb(102, 102, 102);
    			color: #fff;
    			border-radius: 10px;
			}
			.quxiao{
				margin-left: 50px;
				text-align: center;
				line-height: 35px;
			}
			table input,table select{
				width: 46%;
			}
			.xinxi{
				width: 100%;
			}
			.xinxi tr td{
				width: 50%;
				height: 30px;
			}
			.xinxi input{
				vertical-align: middle;
			}
		</style>
		<div class="search">
		</div>
		<div class="shaixuan">
	    	签收信息
	    </div>
	    <? if($k != 1){?>
	    <form id="form">
		    <div class="content">
				<table>
					<tr>
						<td>订单编号：<?=$order['order_id']; ?></td>
						
					</tr>
					<tr>
						<td>客户编号：
							<select name="pid1" id="" class='select'>
								<?php while ($kehu = $db->fetch_array($kehusql)) { ?>
									<option value="<?=$kehu['id']; ?>" <?=$kehu['id']==$order['pid1']?'selected':''; ?>><?=$kehu['kehu_number']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>总&nbsp;&nbsp;货&nbsp;&nbsp;款：<input type="text" name="type1" value="<?=$order['type1']; ?>" id="zong"></td>
						
					</tr>
					<tr>
						<td>客户付款： <input type="text" name="type5" value="<?=$order['type5']; ?>" id="kehu"></td>
					</tr>
					<tr>
						<td>佣&nbsp;&nbsp;金&nbsp;&nbsp;比： <input type="text" name="yongjin" value="<?=$order['yongjin']; ?>">%</td>
					</tr>
					<tr>
						<td>尾&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款：<span id="wei"><?=$order['type1']-$order['type5']?></span></td>
					</tr>
					<tr>
						<td>供&nbsp;&nbsp;应&nbsp;&nbsp;商：
							<select name="pid2" id="shangjia" style="width: 250px;" class="select">
								<?php while ($shangjia = $db->fetch_array($shangjiasql)) { ?>
									<option value="<?=$shangjia['id']; ?>" <?=$shangjia['id']==$order['pid2']?'selected':''; ?>><?=$shangjia['subject']; ?></option>
								<?php } ?>
							</select></td>
					</tr>
					<tr>
						<td>会&nbsp;&nbsp;员&nbsp;&nbsp;号：<span id="huiyuan"><?=$order['skehu_number']; ?></span></td>

					</tr>
					<tr>
						<td>账&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号：<span id="bankNo"><?=$order['bankNo']; ?></span></td>
					</tr>
					<tr>
						<td>户&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名：<span id="openName"><?=$order['openName']; ?></span></td>

					</tr>
					<tr>
						<td>原始订单：<img src="../<?=$order['images_original']; ?>" alt="" width="100%"></td>
					</tr>
				</table>
		    </div>
			<div class="radio">
		    	信息反馈：
		    	<br>
		    	<table class="xinxi">
					<tr>
						<td>
		    				<input type="checkbox" class="checkbox" name="problemmoney" id="1" value="1" <?=$order['problemmoney']==1?'checked':''; ?>><label for="1">总货款错误</label>	
						</td>
						<td>
		    				<input type="checkbox" class="checkbox" name="problemkehu" id="2" value="1" <?=$order['problemkehu']==1?'checked':''; ?>><label for="2">客户错误</label>
						</td>
					</tr>
					<tr>
						<td>
		    				<input type="checkbox" class="checkbox" name="problemkehufukuan" id="3" value="1" <?=$order['problemkehufukuan']==1?'checked':''; ?>><label for="3">客户付款错误</label>
						</td>
						<td>
		    				<input type="checkbox" class="checkbox" name="problemshangjia" id="4" value="1" <?=$order['problemshangjia']==1?'checked':''; ?>><label for="4">会员号错误</label>
						</td>
					</tr>
					<tr>
						<td colspan="2">
		    				<input type="checkbox" class="checkbox" name="problemother" id="5" value="1" <?=$order['problemother']==1?'checked':''; ?>><label for="5">其他</label>
		    				<input type="text" name="other" value="<?=$order['other']; ?>">
						</td>
					</tr>
		    	</table>
			</div>
			<input type="hidden" value="<?=$order['id']; ?>" name="id">
	    </form>
		
		<div class="submit">
			<input type="button" value="提交" class="button queding">
			<a type="button" href="orderprocessing.php" value="" class="button quxiao">取消</a>
		</div>
		<? }else{ ?>
			<div class="content">
				<table>
					<tr>
						<td>订单编号：<?=$order['order_id']; ?></td>
						
					</tr>
					<tr>
						<td>客户编号：
						<?=$kehu['kehu_number']; ?>
							
						</td>
					</tr>
					<tr>
						<td>总&nbsp;&nbsp;货&nbsp;&nbsp;款：<?=$order['type1']; ?></td>
						
					</tr>
					<tr>
						<td>客户付款： <?=$order['type5']; ?></td>
					</tr>
					<tr>
						<td>尾&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;款：<?=$order['type1']-$order['type5']?></td>
					</tr>
					<tr>
						<td>供&nbsp;&nbsp;应&nbsp;&nbsp;商：
							<?=$sj['subject']; ?>
						</td>
					</tr>
					<tr>
						<td>会&nbsp;&nbsp;员&nbsp;&nbsp;号：<span id="huiyuan"><?=$order['skehu_number']; ?></span></td>

					</tr>
					<tr>
						<td>账&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号：<span id="bankNo"><?=$order['bankNo']; ?></span></td>
					</tr>
					<tr>
						<td>户&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名：<span id="openName"><?=$order['openName']; ?></span></td>

					</tr>
					<tr>
						<td>原始订单：<img src="../<?=$order['images_original']; ?>" alt="" width="100%"></td>
					</tr>
				</table>
				<div class="radio">
		    	信息反馈：<?=$order['problemmoney']==1?"总货款错误":""; ?><?=$order['problemkehu']==1?"客户错误":""; ?><?=$order['problemkehufukuan']==1?"客户付款错误":""; ?><?=$order['problemshangjia']==1?"会员号错误":""; ?><?=$order['other']; ?>
		    	</div>
		    </div>
		<? } ?>
		<div style="height: 100px;width: 100%;"></div>
    	<?php include_once("include/footer.php"); ?>
		
		<script>
		$(document).ready(function() {

		      $('.select').select2();

		  });
			function payment(){
				var zong = $("#zong").val();
				var kehu = $("#kehu").val();
				var money = Number(zong) - Number(kehu);
				$('#wei').html(money);
			}
			$('#zong').change(function(){
				payment();
			})
			$('#kehu').change(function(){
				payment();
			})
			$('#shangjia').change(function(){
				var id = $(this).val();
				$.ajax({
				    'url': 'shangjiaapi.php',
				    'type': 'POST',
				    'dataType': 'json',
				    'data': {id:id},
				    'success': function(res){
				        $("#bankNo").html(res.bankNo);
				        $("#openName").html(res.openName);
				        $("#huiyuan").html(res.kehu_number);
				    },
				    'error': function(){
				          
				    }
				})
			})

			$('.queding').click(function(){
				var str = "<?=$_SESSION['uid']==99999?'是否确定订单，确定后无法修改':'是否编辑订单' ?>"
				layer.msg(str, {
					time: 0 //不自动关闭
					,btn: ['是', '否']
					,yes: function(a){
						
						var form = $('#form').serialize();
						$.post('orderapi.php',form,
					      	// 调用服务端成功后的回调函数
						    function(data){
						    	layer.msg('成功',function(){
						    		// window.location.href="orderprocessing.php";
						    	});
						    },'text'
						)
				  	}
				});
			})
		</script>
  	</body>
</html>