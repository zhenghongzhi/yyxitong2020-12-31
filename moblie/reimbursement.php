<?php 
  header("Content-Type: text/html;charset=utf-8");
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "提交审批";

  $shenqingren = "ZhengHongZhi";
  // if ($_SESSION['wxid'] || $_GET['wxid']) {
  //  $shenqingren = $_SESSION['wxid']?$_SESSION['wxid']:$_GET['wxid'];
  // }else{
  //  echo '<script type="text/javascript">alert("未登录或账号异常请联系管理员");window.history.back();</script>';
  //  die;
  // }
  // 数据库名
  $yasa_order = " yasa_order ";
  $yasa_kehu  =" yasa_kehu ";
  $yasa_shangjia =" yasa_shangjia ";
  $yasa_user =" yasa_user ";
  $yasa_module =" yasa_module ";
  
  //获取token 有效期2小时
  $corpid = 'ww36bc4d6ec9858223';//企业微信号
  $secret = 'gDbD1b8zqdeap_c3kQrkMOwhgx47GdhGswRi_p4fzo0';
  $template_id = '3TmB9ZVQsdQ2A8Ho3kPW4RtGHY2j3d4uLFUAAUxW';//模板id
  

  $res = getToken($corpid,$secret);

  $approvalcontent = getApproval($res[access_token],$template_id);
  $template_content = $approvalcontent->template_content->controls;
  // print_r($template_content);die;
  $modulesql = "select * from $yasa_module where template_id = '$template_id'";
  $module = $db->get_one($modulesql);
  // print_r($template_content);die;
  $guize = unserialize($module[content]);




?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
      <title>申请记录</title>
      <link rel="stylesheet" href="fonts/iconfont.css"/>
      <link rel="stylesheet" href="css/font.css"/>
      <link rel="stylesheet" href="css/weui.min.css"/>
      <link rel="stylesheet" href="css/jquery-weui.min.css"/>
      <link rel="stylesheet" href="css/mui.css"/>
      <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      <script src="js/jquery-1.8.3.min.js"></script>
      <script type="text/javascript" src="js/layer/layer.js"></script>
      <script src="js/cupload.js"></script>
      <script>(function (doc, win) {
        var docEl = doc.documentElement,
          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
          recalc = function () {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
          };

        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
      })(document, window);</script>
  </head>
  <style>
    body{
      background-color: rgba(204, 204, 204, 0.33);
    }
    .clear {
      clear: both;
    }
    .content {
        padding-top: 44px;
        margin-bottom: 50px;
    }
    .list{
      border: 1px solid rgb(204, 204, 204);
      margin-top: 10px;
      background: #fff;
    }
    .text{
      float: left;
      width: 30%;
      height: 39px;
      line-height: 39px;
      text-align: left;
      text-indent: 10px;
    }
    .input {
      width: 50%;
      float: left;
      height: 39px;
      line-height: 39px;
    }
    .input input,.input select,.input textarea{
      height: 30px;
      border: 0px;
      width: 100%;
    }
    .tijiao{
      border: 1px solid rgb(204, 204, 204);
      margin-top: 10px;
      background: #fff;
      height: 39px;
      line-height: 39px;
      text-align: center;
      background: #0099ff;
      color: #fff;
    }
    .cupload-image-preview{
      width: 100% !important;
      height: 100% !important;
    }
    .zz{
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0px;
      left: 0px;
      background: rgba(0,0,0,0.5);
      z-index: 9999999;
      display: none;
      align-items: center
    }
    .small{
      z-index: 1000;
    }
    .big{
            align-items: center
        }
    /*.input select{}*/
  </style>
  <body>
    <?php include_once("include/header.php"); ?>
    <form id="form">
      <input type="hidden" name="moudle" value="baoxiao" />
      <input type="hidden" name="shenqing" value="<?=$_SESSION['wxid']; ?>" />
      <div class="content">
        <? foreach($template_content as $key=>$value) { ?>
          <? if ($value->property->control == 'Text' || $value->property->control == 'Number' || $value->property->control == 'Money'){ ?>
            <div class="list">
              <div class="text">
               <?=$value->property->title[0]->text;?><span style="color: red;"><?=$value->property->require == 1?'*':''; ?></span>
              </div>
              <div class="input">
                <input type="text" class="<?=$value->property->require == 1?'most':''; ?>" name="content[<?=$value->property->id; ?>]" value="<?=$duiyingshuju[$value->property->id]; ?>"; id="<?=$value->property->id; ?>" placeholder="<?=$value->property->placeholder[0]->text; ?>" <?=in_array($value->property->id,$only)?'readonly':'' ?>>
                <input type="hidden"  name="text[<?=$value->property->id; ?>]" value="<?=$value->property->title[0]->text; ?>" />
              </div>
              <div class="clear"></div>
            </div>
          <? }elseif ($value->property->control == 'Selector') { ?>
            <div class="list">
              <div class="text">
                <?=$value->property->title[0]->text;?><y style="color: red;"><?=$value->property->require == 1?'*':''; ?></y>
              </div>
              <div class="input">
                <select id="<?=$value->property->id; ?>" name="<?=$value->property->id; ?>" class="<?=$value->property->require == 1?'most':''; ?>">
                    <option value="">请选择</option>
                    <? foreach($value->config->selector->options as $ke=>$val) { ?>
                      <option value="<?=$val->key?>" v="<?=$val->value[0]->text?>"><?=$val->value[0]->text?></option>
                    <? } ?>
                  </select>
                  <input type="hidden"  name="text[<?=$value->property->id; ?>]" value="<?=$value->property->title[0]->text; ?>" />
                  <input type="hidden" name="content[<?=$value->property->id; ?>]" class="<?=$value->property->id; ?> <?=$value->property->require == 1?'content':''; ?>" />
              </div>
              <div class="clear"></div>
            </div>
          <? }elseif ($value->property->control == 'Textarea') { ?>
            <div class="list">
              <div class="text">
                <?=$value->property->title[0]->text;?><y style="color: red;"><?=$value->property->require == 1?'*':''; ?></y>
              </div>
              <div class="input">
                <input type="text" class="<?=$value->property->require == 1?'most':''; ?>" name="content[<?=$value->property->id; ?>]" value="<?=$duiyingshuju[$value->property->id]; ?>"; id="<?=$value->property->id; ?>" placeholder="<?=$value->property->placeholder[0]->text; ?>" <?=in_array($value->property->id,$only)?'readonly':'' ?>>
                <input type="hidden"  name="text[<?=$value->property->id; ?>]" value="<?=$value->property->title[0]->text; ?>" />
              </div>
              <div class="clear"></div>
            </div>
          <? }elseif ($value->property->control == "Date") { ?>
            <div class="list">
              <div class="text">
                <?=$value->property->title[0]->text;?><y style="color: red;"><?=$value->property->require == 1?'*':''; ?></y>
              </div>
              <div class="input">
                <input type="date" class="<?=$value->property->require == 1?'most':''; ?>"  name="content[<?=$value->property->id; ?>]" value="<?=$duiyingshuju[$value->property->id]; ?>"; id="<?=$value->property->id; ?>" placeholder="<?=$value->property->placeholder[0]->text; ?>" />
                <input type="hidden"  name="text[<?=$value->property->id; ?>]" value="<?=$value->property->title[0]->text; ?>" />
              </div>
              <div class="clear"></div>
            </div>
          <? } ?>
        <? } ?>
        <div class="list">
          <div class="text" style="height: 100px;">
            附件
          </div>
          <div class="input">
            <div id="cupload-create">
            
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
        <div class="tijiao queding">提交</div>
    </form>

    <div class="zz">
      <img src="" alt="" class="big" style="width: 100%;">
    </div>
    <script type="text/javascript">
      var cuploadCreate = new Cupload ({
        ele: '#cupload-create',
        num: 6,
      });
      $(document).on("click",".small",function(){
      $('.zz').css('display','flex');
      var src = $(this).attr('src');
      $('.big').attr('src',src);
      $(".zz").show();
    })
    $('.zz').click(function(){
      $('.zz').hide();
    })
    $('.queding').click(function(){
        var a = 1;
        $('.most').each(function() {

            if ($(this).val() != "") {
                // console.log(1);
            }else{
              a+=1;
              layer.msg('请填写完整数据');
              return false;
            }
        });
        if (a == 1) {
          layer.msg('是否确定提交审批？', {
            time: 0 //不自动关闭
            ,btn: ['是', '否']
            ,yes: function(a){
              layer.load(1);
              var form = $('#form').serialize();
              $('.layui-layer-btn1').click();
              $.post('rembusementapi.php',form,
                    // 调用服务端成功后的回调函数
                  function(data){
                    // console.log(data);
                     if (data.status == 0) {
                        // layer.msg(data.msg,function(){
                        //   // parent.layer.close(index);      
                        //   window.location.href="payapply.php";
                        // });
                        layer.closeAll('loading');
                        layer.msg(data.msg,{time: 2000},function(){
                           window.location.href="reimbursementlist.php";
                         });

                     }else{
                        layer.closeAll('loading');
                        layer.msg(data.msg,function(){

                        });
                     }
                  },'json')
              }
          });
        }
      });
      $('#Selector-1599788888663').change(function(){
        var khlx = $("#Selector-1599788888663").find("option:selected").text();
        
        $('.Selector-1599788888663').val(khlx);
      })
       $('#item-1494248811896').change(function(){
        var khlx = $("#item-1494248811896").find("option:selected").text();
        
        $('.item-1494248811896').val(khlx);
      })
       $('#Selector-1599790003226').change(function(){
        var khlx = $("#Selector-1599790003226").find("option:selected").text();
        
        $('.Selector-1599790003226').val(khlx);
      })
    </script>
  </body>
</html>