<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once("checkuser.php");
	
	if ($act == 'upload') {

		$path = "../upfiles/";
	
		$base = base64_image_content($img,$path);
		$base = str_replace("../","/",$base);
		$resimg = str_replace("//","/",$base);
		$res = $db->query("UPDATE yasa_order SET images = '$resimg' WHERE id = $id ");
		
		echo 1;die;
		// print_r($_POST);die;
	}
	$order = $db->get_one("select o.*,k.kehu_number from yasa_order as o 
	left join yasa_kehu as k on k.id = o.pid1 where o.id = $orderid");

	$productsql = $db->query("select * from yasa_product where pid1 = $orderid and is_delete = 1");

	$caiwuurl1 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$order[order_id];
  	$caiwu1 = curl_get_https($caiwuurl1);
  	$caiwu1 = json_decode($caiwu1);
  	$c = $l?$l:'cn';


  	//语言包
  	$lang[cn][xght] = '销购合同';
  	$lang[cn][order] = '订单号：';
  	$lang[cn][date] = '日期：';
  	$lang[cn][jiafang] = '供货单位（协议称甲方）：广州越洋进出口贸易有限公司';
  	$lang[cn][yifang] = '购货单位（协议称乙方）：';
  	$lang[cn][desc]  ='经买卖双方充分沟通与确认，本着诚实和公平交易原则签订本购销合同，甲方同意销售，乙 方愿意购买以下货物出口：';
  	$lang[cn][product] = '一、产品名称、规格、数量价格';
  	$lang[cn][xuhao] = '序号';
  	$lang[cn][pinming] = '品名';
  	$lang[cn][tupian] = '图片';
  	$lang[cn][kuanhao] = '款号';
  	$lang[cn][shuliang] = '数量';
  	$lang[cn][danjia] = '单价';
  	$lang[cn][zonge] = '总额';
  	$lang[cn][beizhu] = '备注';
  	$lang[cn][jineheji] = '金额合计：';
  	$lang[cn][yifukuan] = '已付款：';
  	$lang[cn][yukuan] = '余款：';
  	$lang[cn][bzfs] = '包装方式';
  	$lang[cn][bzfs1] = '纸箱+双层编织袋+“＃”打包条';
  	$lang[cn][p1] = '请认真阅读以下协议条款内容：';
  	$lang[cn][p2] = '甲方作为销售方，乙方作为购买方，经买卖双方充分沟通与确认，本着诚实和公平交易原则签订本购销合同，并共同遵守和履行本协议规定的双方的权利和义务。';
  	$lang[cn][p3] = '一、 商品信息确认';
  	$lang[cn][p4] = '乙方认同并确认该订单合同的相关产品信息';
  	$lang[cn][p5] = '二、商品价格';
  	$lang[cn][p6] = '1.	商品的价格，按买卖双方协定好的价格执行';
  	$lang[cn][p7] = '特别说明：逾期交货，遇价格上涨时，按原价执行；遇价格下降时，按原价执行；逾期提货或者付款的，遇价格上涨时，按新价执行；遇价格下降时，按原价执行';
  	$lang[cn][p8] = '2.	以上报价为税前产品价格，若购货单位需要开具发票，则需支付  5   %的税点';
  	$lang[cn][p9] = '三、商品质量';
  	$lang[cn][p10] = '1.甲方按乙方指定的商品规格、型号等要求供货，因商品本身质量的问题引发的纠纷，由甲方承当经济赔偿责任。';
  	$lang[cn][p11] = '2.乙方在收到商品后对质量有异议的，需要在收到货物7天内跟甲方联系并提供信息反馈，甲方在收到异议后，应在10天内负责处理，否则，即默认乙方提出的质量异议。';
  	$lang[cn][p12] = '3.因乙方人为原因，使用、保管不当等原因造成的货物质量问题，不得提出异议。';
  	$lang[cn][p13] = '四、交货方式';
  	$lang[cn][p14] = '1. 境内同城交货，甲方按照乙方或者乙方的委托方指定的地址免费送货上门，超出送货范围的，送货费由乙方自行承担，甲方应实报实销该费用；自提货物（包括快递/物流），途中导致的产品破损、毁坏、丢失及因其他不可抗力因素导致的经济损失均由乙方自行承担。';
  	$lang[cn][p15] = '2. 需要境外交货，须另行签订物流运输条款';
  	$lang[cn][p16] = '五、货款及费用结算';
  	$lang[cn][p17] = '1.从乙方确认该电子订单合同起，两天内乙方需要向甲方支付总货款的30%作为合同定金，待甲方将乙方所购买的产品全部送至乙方指定地点并验收合格后，乙方须当即一次性付清甲方货款，钱货两讫。';
  	$lang[cn][p18] = '2.超出送货范围的，货物送至乙方指定的仓库后乙方须当即一次性付清额外的送货费。';
  	$lang[cn][p19] = '六、违约责任';
  	$lang[cn][p20] = '1.甲方未能按时向乙方交付符合要求的商品，应第一时间通知到乙方，经双方协商延长送货时间，协商不妥的，乙方有权解除合同，甲方须如数退还乙方支付的定金。';
  	$lang[cn][p21] = '2.乙方逾期不提货，甲方有权对商品进行销售处理，乙方未按时向甲方支付货款及相关费用，甲方有权解除合同，所付的定金不予退回，乙方自行承担全部经济损失。';



 ?>

 <html>
	<head>
		<meta charset="UTF-8">
	      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	      <title>申请记录</title>
	      <link rel="stylesheet" href="fonts/iconfont.css"/>
	      <link rel="stylesheet" href="css/font.css"/>
	      <link rel="stylesheet" href="css/weui.min.css"/>
	      <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	      <link rel="stylesheet" href="css/mui.css"/>
	      <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	      <script src="js/jquery-1.8.3.min.js"></script>
	      <script type="text/javascript" src="js/layer/layer.js"></script>
		<!-- <script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script> -->
		<script type="text/javascript" src="js/layer/layer.js"></script>
		<script type="text/javascript" src="js/html2canvas.min.js"></script>
	</head>
	<body>
		<span><a href="ordercontract.php?l=cn&orderid=<?=$orderid; ?>">中文</a></span>
		<span><a href="ordercontract.php?l=en&orderid=<?=$orderid; ?>">英文</a></span>
		<span><a href="ordercontract.php?l=fa&orderid=<?=$orderid; ?>">波斯语</a></span>
		<div class="content" id="printtable">
			<style>
				.content{
					width: 900px;
					margin: 0px auto;
					background: #fff;
				}
				.clear{
					clear:both;
				}
				.f18{
					font-size:18px;
				}
				table{
					width: 100%;
				}
				table tr td{
					height: 30px;
					text-align: center;
				}
			</style>
			
			<h1 style="text-align: center;"><?=$lang[$c][xght]?></h1>
			<p><span style="float: left;" class="f18"><?=$lang[$c][order]?><?=$order[order_id]; ?></span> <span class="f18" style="float: right;"><?=$lang[$c][date]?><?=date("Y-m-d",$order[addtime]); ?></span></p>
			<div class="clear"></div>
			<p><?=$lang[$c][jiafang]?> </p>
			<p><?=$lang[$c][yifang]?><?=$order[kehu_number]?></p>
			<p><?=$lang[$c][desc]?> </p>
			<p><?=$lang[$c][product]?></p>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<th><?=$lang[$c][xuhao]?></th>
					<th><?=$lang[$c][pinming]?></th>
					<th><?=$lang[$c][tupian]?></th>
					<th><?=$lang[$c][kuanhao]?></th>
					<th><?=$lang[$c][shuliang]?></th>
					<th><?=$lang[$c][danjia]?></th>
					<th><?=$lang[$c][zonge]?></th>
					<th><?=$lang[$c][beizhu]?></th>
				</tr>
				<?php $i = 1;
					while ($product = $db->fetch_array($productsql)) { 
						// print_r($product);die;
					?>
					<tr>
						<td><?=$i;?></td>
						<td><?=$product[subject]?></td>
						<td><img src="../<?=$product[images]?$product[images]:'/cahce_images/system/200x200/20201221/20201221102940347344805.jpg'; ?>" alt="" width="50"></td>
						<td><?=$order[iscai]==1?$product[type1]:$product[type2]; ?></td>
						<td><?=$product[type4]; ?></td>
						<td><?=$product[type5]; ?></td>
						<td><?=$product[type4]*$product[type5]; ?></td>
						<td><?=$product[remake]; ?></td>
					</tr>
				<? 
					$i++; 
					$money += $product[type4]*$product[type5];
				} ?>
				
				<tr>
					<td colspan="8" style="text-align: left;">
						<span><?=$lang[$c][jineheji]; ?> ￥<?=$money; ?></span>
						<span style="margin-left: 20px;"><?=$lang[$c][yifukuan]; ?> ￥<?=$order[type5]+$caiwu1->deposit+$caiwu1->balance; ?></span>
						<span style="margin-left: 20px;"><?=$lang[$c][yukuan]; ?>￥<?=$money-$order[type5]-$caiwu1->deposit-$caiwu1->balance?></span>
					</td>
					</tr>
				<tr>
					<td><?=$lang[$c][bzfs]?></td>
					<td colspan="7"><?=$lang[$c][bzfs1]?></td>
				</tr>
			</table>
			<div>
				<p><?=$lang[$c][p1]?></p>
				<p><?=$lang[$c][p2]?></p>
				<p><?=$lang[$c][p3]?></p>
				<p><?=$lang[$c][p4]?></p>
				<p><?=$lang[$c][p5]?></p>
				<p><?=$lang[$c][p6]?></p>
				<p><?=$lang[$c][p7]?></p>
				<p><?=$lang[$c][p8]?></p>
				<p><?=$lang[$c][p9]?></p>
				<p><?=$lang[$c][p10]?></p>
				<p><?=$lang[$c][p11]?></p>
				<p><?=$lang[$c][p12]?></p>
				<p><?=$lang[$c][p13]?></p>
				<p><?=$lang[$c][p14]?></p>
				<p><?=$lang[$c][p15]?></p>
				<p><?=$lang[$c][p16]?></p>
				<p><?=$lang[$c][p17]?></p>
				<p><?=$lang[$c][p18]?></p>
				<p><?=$lang[$c][p19]?></p>
				<p><?=$lang[$c][p20]?></p>
				<p><?=$lang[$c][p21]?></p>
			</div>
		</div>


		<input type="button" style="width: auto;" value="打印预览" class="dayin">
		<?php if ($order[iscai] == 2) { ?>
			<button id="saveimg_btn">生成图片</button>
		<? } ?>
		<?php if ($order[images]) { ?>
			<button onclick="window.open('../<?=$order['images']; ?>')">查看图片</button>
		<? } ?>
		<script>
			$(function () {
				$("#saveimg_btn").on("click",function (event) {
					if (confirm("确定生成图片吗")) {
						event.preventDefault();
						html2canvas($("#printtable"), {
							allowTaint: true,
							taintTest: false,
							onrendered: function (canvas) {
								canvas.id = "mycanvas";
								//document.body.appendChild(canvas);
								//生成base64图片数据
								var dataUrl = canvas.toDataURL("image/jpeg");
								// var newImg = document.createElement("img");
								// newImg.src = dataUrl;
								// document.body.appendChild(newImg);
								$.post('ordercontract.php?act=upload',{id:<?=$orderid; ?>,img:dataUrl},
				                    // 调用服务端成功后的回调函数
				                  function(data){
				                    // console.log(data);
				                     alert("成功");
				                     $(window.parent.document).find("iframe")[0].contentWindow.location.reload(true);
				                  },'json')
								// $(".no_print").hide();
								// $("#printtable").hide();
							}
						});
					}else{
						
					}
					
				});
			});
			$('.dayin').click(function(){
			    var printWindow = window.open("打印窗口","_blank");
				//将明细页面拷贝到新的窗口，这样新的窗口就有了明细页面的样式
				var htmlbox = $(".content").html();
				
				printWindow.document.write(htmlbox);
				//获得要打印的内容
				var printbox = $(".content").html();
				//将要打印的内容替换到新窗口的body中
				printWindow.document.body.innerHTML = printbox;
				//脚本向窗口(不管是本窗口或其他窗口)写完内容后，必须关闭输出流。在延时脚本的最后一个document.write()方法后面，必须确保含有document.close()方法，不这样做就不能显示图像和表单。
				//并且，任何后面调用的document.write()方法只会把内容追加到页面后，而不会清除现有内容来写入新值。
				printWindow.document.close();
				//打印
				//chrome浏览器使用jqprint插件打印时偶尔空白页问题
				//解决方案：有时候页面总是加载不出来，打印出现空白页，可以设置延迟时间来等待页面的渲染，但是渲染时间的长短跟页面的大小有关，是不固定的。所以这里使用事件监听器。
				// print.portrait   =  false    ;//横向打印 
				printWindow.addEventListener('load', function(){
					printWindow.portrait = false;
					printWindow.print();
					//关闭窗口
					printWindow.close(
						$('.show').show()
					);
				});


			})
		</script>
	</body>
 </html>