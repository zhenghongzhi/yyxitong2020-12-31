<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  if ($_SESSION['uid'] != 99999 && $_SESSION['uid']) {
  		$wwhere = " and o.pid3 = '".$_SESSION['uid']."'";
  	}
  $warehousehai = $db->query("select w.* from yasa_warehouse as w 
  	left join yasa_order as o on o.order_id = w.orderid 
  	where o.is_delete = 1 and w.is_del = 1 and w.spstatus > 1 and w.orderid != 0 and w.jincangdanhao = '$id' and w.fahuo = '海运' ".$wwhere);
  $warehousekong = $db->query("select w.* from yasa_warehouse as w
  	left join yasa_order as o on o.order_id = w.orderid 
  	where o.is_delete = 1 and w.is_del = 1 and w.spstatus > 1 and w.orderid != 0 and w.jincangdanhao = '$id' and w.fahuo = '空运' ".$wwhere);

  $all = $db->get_one("select sum(w.count) as count,sum(w.volume) as volume,sum(w.weight) as weight,w.userid,w.jincangdanhao,sum(w.money) as money,w.rukutime,GROUP_CONCAT(DISTINCT(w.orderid)) as orderid,GROUP_CONCAT(DISTINCT(w.product)) as product,w.bztype from yasa_warehouse as w
  	left join yasa_order as o on o.order_id = w.orderid 
  	where w.jincangdanhao = '$id' and w.is_del = 1  and o.is_delete = 1 ".$wwhere);
  // print_r($all);die;
  $title = "签收信息";

  $shouqi = $db->get_one("select count(*) as count from yasa_warehouse as w
  	left join yasa_order as o on o.order_id = w.orderid 
  	where w.jincangdanhao = '$id' and w.is_del = 1 and w.spstatus > 1 and w.orderid != 0 and w.is_finish = 2 and o.is_delete = 1 ".$wwhere);
  // echo $all[orderid];die;
  $order = $db->get_one("select * from yasa_order where is_delete = 1 and order_id = '".$all[orderid]."'");
  // print_r("select * from yasa_order where is_delete = 1 and order_id = '".$id."'");die;
  $wazong = $db->get_one("select sum(money) as money from yasa_warehouse where is_del = 1 and orderid = '".$all[orderid]."'");
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>付款申请</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <!-- <link rel="stylesheet" href="css/weui.min.css"/> -->
	    <!-- <link rel="stylesheet" href="css/jquery-weui.min.css"/> -->
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      	<script src="js/jquery-1.8.3.min.js"></script>
	    <script src="js/jq-signature.js"></script>
     	<script type="text/javascript" src="js/layer/layer.js"></script>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
  			*{
  				cursor:pointer;
  			}
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    .content{
		    	width: 90%;
		    	margin: 0px auto;
		    	padding: 15px 8px;
		    	border-radius: 20px;
		    	background: rgba(204, 204, 204, 0.21);
		    	margin-top: 20px;
		    	border: 1px solid rgb(204, 204, 204);
		    }
			.content table tr td{
				width: 25%;
				text-align: left;
				height: 40px;
				line-height: 40px;
			}
			.radio{
				margin-top: 30px;
				padding: 10px;
				margin-left: 10px;
			}
			.radio input{
				width: 20px;
				height: 20px;
				-webkit-appearance:checkbox;
			}
			#shi,#fou{
				margin-left: 40px;
			}
			.js-signature{
				margin: 10px;
				margin-top: 20px;
				height: 200px;
				border: 1px solid rgb(204, 204, 204)
			}
			.submit{
				display: flex;
  				justify-content: center;
  				margin-top: 40px;
  				margin-bottom: 100px;
			}
			.button{
				width: 74px;
    			height: 35px;
    			background-color: rgb(0, 153, 255);
    			border: 1px solid rgb(102, 102, 102);
    			color: #fff;
    			border-radius: 10px;
			}
			.quxiao{
				margin-left: 50px;
			}
			.btn{
				padding: 10px;
				width: 100px;
				text-align: center;
				color: #fff;
				background: rgb(0, 153, 255);
				border-radius: 10px;
				float: left;
				margin-left: 10px;
			}
		</style>
		<div class="search">
		</div>
		<div class="shaixuan">
	    	 签收信息
	    </div>
	    <form id="form">
		    <div class="content">
				<table>
					<tr>
						<td colspan="2">入仓日期：<?=$all['rukutime']; ?></td>
						<td colspan="2">入仓号：<?=$all['jincangdanhao']; ?></td>
					</tr>
					<tr>
						<td colspan="2">订单号：<?=$all['orderid']; ?></td>
						<td colspan="2">客户编号：<?=$all['userid']; ?></td>
					</tr>
					<tr>
						<td colspan="2">入仓货物：<?=$all['product']; ?></td>
						<td colspan="2">入仓数量：<?=round($all['count']); ?>CTNS</td>
					</tr>
					<tr>
						<td colspan="2">体积：<?=round($all['volume'],2); ?>m³</td>
						<td colspan="2">重量： <?=round($all['weight'],2); ?>kg</td>

					</tr>
					<tr>
						<td colspan="2">外包装：<?=$all['bztype']; ?></td>
						<td colspan="2">目的港：迪拜??</td>
					</tr>
					<tr>
						<td colspan="2">订单金额：<?=$order['type1']; ?></td>
						<td colspan="2">入仓金额：<?=$all[money]?></td>
					</tr>
					<tr>
						<td colspan="4">是否收齐货：<?=$shouqi==1?'是':'否'?></td>
					</tr>
					<tr style="border:1px dashed #000;" >
						<td  colspan="4" style="border:1px dashed #000;height: 0px;"></td>
					</tr>
					<tr>
						<td colspan="4">发货方式：海运</td>
					</tr>
					<tr>
						<td>货物类型</td>
						<td>数量</td>
						<td>重量</td>
						<td>体积</td>
					</tr>
					<tr>
						<?php while ($k = $db->fetch_array($warehousehai)) { ?>
							<tr>
								<td><?=$k[protype]; ?></td>
								<td><?=$k[count]; ?>CTNS</td>
								<td><?=$k[weight]; ?>kg</td>
								<td><?=$k[volume]; ?>m³</td>
							</tr>
						<? } ?>

					</tr>
					<tr>
						<td colspan="4">发货方式：空运</td>
					</tr>
					<tr>
						<td>货物类型</td>
						<td>数量</td>
						<td>重量</td>
						<td>体积</td>
					</tr>
					<tr>
						<?php while ($k = $db->fetch_array($warehousekong)) { ?>
							<tr>
								<td><?=$k[protype]; ?></td>
								<td><?=$k[count]; ?>CTNS</td>
								<td><?=$k[weight]; ?>kg</td>
								<td><?=$k[volume]; ?>m³</td>
							</tr>
						<? } ?>

					</tr>
				</table>
		    </div>
		    <? if(!$yi){ ?>

			    <div class="submit">
					<a href="sign.php?id=<?=$id;?>" style="display: inline-block;padding: 10px 40px; background:rgb(0, 153, 255); color:#fff;border-radius: 20px;font-size: 20px;">签字确认</a>
				</div>
		    <?	} ?>
			
		</form>
		<div style="height: 100px;width: 100%;"></div>
    	<?php include_once("include/footer.php"); ?>	
  	</body>
  	<script type="text/javascript">

		$(document).on('ready', function() {
			if ($('.js-signature').length) {
				$('.js-signature').jqSignature();
			}
		});

		function clearCanvas() {
			// $('#signature').html('<p><em>Your signature will appear here when you click "Save Signature"</em></p>');
			$('.js-signature').jqSignature('clearCanvas');
			$('#saveBtn').attr('disabled', true);
			$('#sign').val("");
		}

		function saveSignature() {
			var dataUrl = $('.js-signature').jqSignature('getDataURL');
			$('#sign').val(dataUrl);
	        
			
		}

		$('.js-signature').on('jq.signature.changed', function() {
			$('#saveBtn').attr('disabled', false);
		});

		$('.queding').click(function(){
	        var a = 1;
	        
	        var val=$('input:radio[name="finish"]:checked').val();
	        var form = $('#form').serialize();
            if(val==null){
                layer.msg('请完成信息',function(){

                });
                return false;
            }
            var sign = $("#sign").val();
            if (!sign) {
            	layer.msg('请确定签名',function(){

                });
                return false;
            }

	        if (a == 1) {

	          	layer.msg('是否确定提交审批？', {
		            time: 0 //不自动关闭
		            ,btn: ['是', '否']
		            ,yes: function(a){
		              	$('.layui-layer-btn1').click();
		             	$.post('warehouseapi.php',form,
		                    // 调用服务端成功后的回调函数
		                  	function(data){
		                    	// console.log(data);
		                    	if (data.code == 1) {
			                        // layer.msg(data.msg,function(){
			                        //   // parent.layer.close(index);      
			                        //   window.location.href="payapply.php";
			                        // });
			                        layer.msg(data.msg,{time: 2000},function(){
			                           window.location.href="receipt.php";
			                        });
		                    }else{
		                        layer.msg(data.msg,function(){

		                        });
		                    }
		                },'json'
		              	)
		            }
	          	});
	        }
      	});
	</script>
</html>