<?php     
 	header("Content-Type: text/html;charset=utf-8");
	include_once("../include/common.ini.php");
 	include_once("error.inc.php");
 	include_once("include/common.php");
 	$title="入仓签收";
 	$wwhere = "1 and w.is_del = 1 and o.is_delete = 1";
 	if ($_SESSION['uid'] != 99999 && $_SESSION['uid']) {
  		$wwhere .= " and o.pid3 = '".$_SESSION['uid']."'";
  	}

  	$type = $type?$type:0;
   	if ($search) {
	  $wwhere .= " and (w.jincangdanhao like '%".$search."%' or k.kehu_number like '%".$search."%') ";
	}
	if ($type) {
	  	$order = ' rukutime desc';
	  }else{
	  	$order = ' rukutime asc';
	  }
	 //  echo "select w.*,o.pid3,k.kehu_number,sum(w.count) as wcount from yasa_warehouse as w 
  // 		left join yasa_order as o on o.order_id = w.orderid 
		// left join yasa_kehu as k on k.id = o.pid1 
  // 		where $wwhere group by w.jincangdanhao order by $order";die;
  	$daisql = $db->query("select w.*,o.pid3,k.kehu_number,sum(w.count) as wcount from yasa_warehouse as w 
  		left join yasa_order as o on o.order_id = w.orderid 
		left join yasa_kehu as k on k.id = o.pid1 
  		where $wwhere group by w.jincangdanhao order by $order");
 	while ($dai = $db->fetch_array($daisql)) {
 		$check = $db->get_one("select * from yasa_ckqianshou where jincangdanhao = '$dai[jincangdanhao]'");
 		if ($check) {
 			$qq[] = $dai;
 		}else{
 			$dd[] = $dai;
 		}
 	}
 	
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>入仓签收</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script src="js/jquery-1.8.3.min.js"></script>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
			.content{
				padding: 10px; 
			}
			.content .top{
				border-bottom: 1px solid #f0f0f0;
			}	
			.content .top a{
				display: inline-block;
				padding: 5px 15px;
				float: left;
			}
			.dangqian{
				background: #0099ff;
				color: #fff;
			}
			table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 10px;
			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}
		</style>
  		<div class="search">
	        <form action="" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="hidden" name="type" value="<?=$type; ?>">
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	入仓签收
	    </div>
	    <div class="content">
			<div class="top">
				<a href="receipt.php?type=0" class="<?=$type==0?'dangqian':''; ?>">待签收</a>
				<a href="receipt.php?type=1" class="<?=$type==1?'dangqian':''; ?>">已签收</a>
				<div class="clear"></div>
			</div>
			<div class="middle">
				<? if (!$type) { ?>
					<table border="1" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th width="22%">入仓日期</th>
								<th width="21%">入仓号</th>
								<th width="21%">客户编号</th>
								<th width="21%">审批状态</th>
								<th width="15%">操作</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($dd as $key => $dai) { ?>
								<tr>
									<td><?=$dai['rukutime']; ?></td>
									<td><?=$dai['jincangdanhao']; ?></td>
									<td><?=$dai['userid']; ?></td>
									<td><? if($dai['spstatus'] == 1){echo '待审批';}elseif ($dai['spstatus'] == 2) {echo '已审批';}else{echo '可发货';} ?></td>
									<td><a href="receiptd.php?id=<?=$dai['jincangdanhao']; ?>">查看</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<? }else{ ?>
					<table border="1" cellpadding="0" cellspacing="0">
						<thead>
							<tr>
								<th width="22%">入仓日期</th>
								<th width="21%">入仓号</th>
								<th width="21%">客户编号</th>
								<th width="21%">数量</th>
								<th width="15%">操作</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($qq as $key => $qing) { ?>
								<tr>
									<td><?=$qing['rukutime']; ?></td>
									<td><a href="receiptcx.php?id=<?=$qing['jincangdanhao']; ?>"><?=$qing['jincangdanhao']; ?></a></td>
									<td><?=$qing['userid']; ?></td>
									<td><?=$qing['wcount'];?>CTNS</td>
									<!-- <td><a href="receiptd.php?id=<?=$qing['id']; ?>">查看</a></td> -->
									<td><a href="receiptd.php?id=<?=$qing['jincangdanhao']; ?>&yi=1">查看</a></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<? } ?>
			</div>
	    </div>
		<div style="height: 100px;width: 100%;"></div>

    	<?php include_once("include/footer.php"); ?>
	    <script>
	  		// $('.top a').click(function(){
	  		// 	var index = $(this).index();
	  		// 	$(".top a").removeClass("dangqian");
	  		// 	$(this).addClass("dangqian");
	  		// 	$('table').hide();
	  		// 	$('table').eq(index).show();
	  		// })
	  	</script>
  	</body>
</html>