<?php     
  	include_once("../include/common.ini.php");
  	include_once("error.inc.php");
  	include_once("include/common.php");
  	$title = "装箱单查询";
  	if ($shuaixuan) {
  		$where = 'pxh.is_delete = 1 and pr.is_delete = 1 and o.is_delete = 1';
  		
	  	if ($shuaixuan == 'user_id') {
	  		$where .= ' and k.kehu_number = "'.$search.'"';
	  	}elseif ($shuaixuan == 'order_id') {
	  		$where .= ' and o.order_id = "'.$search.'"';
	  	}

	  	$sql = $db->query("select pr.id,k.kehu_number,o.order_id,pr.type1 as xianghao,pxh.subject,p.subject as subject1,p.type1 as kuanhao,pxh.type1 as jian,p.type5 as danjia,pr.type2 as zongshuliang,pr.type3 as xiangshu,pr.type4 as danwei,pr.type5 as tiji,pr.type6 as zhongliang 
			from yasa_product_xiangzi_huowu as pxh 
			left join yasa_product_ruku as pr on pr.only_id = pxh.pid1
			left join yasa_order as o on pr.pid1 = o.id 
		  	left join yasa_kehu as k on o.pid1 = k.id
		  	left join yasa_product as p on p.id = pxh.pid3 
		  	where ".$where." order by o.addtime desc");

	  // 	echo "select pr.id,k.kehu_number,o.order_id,pr.type1 as xianghao,pxh.subject,p.subject as subject1,p.type1 as kuanhao,pxh.type1 as jian,p.type5 as danjia,pr.type2 as zongshuliang,pr.type3 as xiangshu,pr.type4 as danwei,pr.type5 as tiji,pr.type6 as zhongliang 
			// from yasa_product_xiangzi_huowu as pxh 
			// left join yasa_product_ruku as pr on pr.only_id = pxh.pid1
			// left join yasa_order as o on pr.pid1 = o.id 
		 //  	left join yasa_kehu as k on o.pid1 = k.id
		 //  	left join yasa_product as p on p.id = pxh.pid3 
		 //  	where ".$where." order by o.addtime desc";die;
	  	while ($sql1 = $db->fetch_array($sql)) {
			$arr[$sql1[id]][] = $sql1;
		}
  	}
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>装箱单查询</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <!-- <link rel="stylesheet" href="css/weui.min.css"/> -->
	    <!-- <link rel="stylesheet" href="css/jquery-weui.min.css"/> -->
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<style>
	    .clear{
	      	clear: both;
	    }
	    .searchbutton{
	      	width: 70%;
	      	border-radius: 10px;
	    }
	    .shuaixuan{
			height: 30px;
			border:1;
		}
	    .search {
	        padding-top: 44px;
	    }
	    .search input{
	      	height: 30px;
	    }
	    .shaixuan{
      		padding: 10px;
	      	background-color:rgba(102, 102, 102, 0.06);
	    }
	    .content{
			padding: 10px; 
		}
		.content .top{
			border-bottom: 1px solid #f0f0f0;
		}
		.content .top a{
			display: inline-block;
			padding: 5px 15px;
			float: left;
		}
		.dangqian{
			background: #0099ff;
			color: #fff;
		}
		table{
			margin: 0 auto;
			width: 95%;
			text-align: center;
			margin-top: 10px;

		}
		table{table-layout: fixed;}
		td{word-break: break-all; word-wrap:break-word;}
		table thead{
			background-color: rgba(204, 204, 204, 0.23);
		}
	</style>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<div class="search">
	        <form action="" style="position: relative;">
	        	<select name="shuaixuan" id="shuaixuan" id="shuaixuan">
					<option value="user_id">客户编号</option>
					<option value="order_id">订单号</option>
	          	</select>
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	装箱单查询 
	    </div>
	    <div class="content">
			<div class="middle">
				<table border="1" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th>客户编号</th>
							<th>订单号</th>
							<th>箱号</th>
							<th>品名</th>
							<th>数量</th>
							<th>单价</th>
							<th>总价</th>
							<th>体积</th>
							<th>重量</th>
							<th>箱数</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($arr as $key => $value) { 
						$count = count($value);
						foreach ($value as $ke => $val) { 
								$allzong += $val[danjia]*$val[jian];
								if ($ke == 0) { 
								$alltiji += $val[tiji];
								$allzhongliang += $val[zhongliang];
								$allxiangshu += $val[xiangshu];?>
									<tr>
										<td rowspan="<?=$count; ?>"><?=$val[kehu_number]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[order_id]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[xianghao]; ?></td>
										<td><?=$val[subject]; ?></td>
										<td><?=$val[jian]; ?></td>
										<td><?=$val[danjia]; ?></td>
										<td><?=round($val[danjia]*$val[jian]); ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[tiji]; ?></td>
										<td rowspan="<?=$count; ?>"><?=round($val[zhongliang],1); ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[xiangshu]; ?></td>
									</tr>
								<? }else{ ?>
									<tr>
										
										<td><?=$val[subject]; ?></td>
										<td><?=$val[jian]; ?></td>
										<td><?=$val[danjia]; ?></td>
										<td><?=round($val[danjia]*$val[jian]); ?></td>
										
									</tr>
								<?}
							?>	
					<?php }
					} ?>
						
					</tbody>
					<thead>
						<tr>
							<td>合计</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><?=$allzong; ?></td>
							<td><?=$alltiji; ?></td>
							<td><?=round($allzhongliang,1); ?></td>
							<td><?=$allxiangshu; ?></td>
						</tr>
					</thead>
				</table>
			</div>
	    </div>
	    <div style="height: 100px;width: 100%;"></div>

    	<?php include_once("include/footer.php"); ?>

  	</body>
</html>