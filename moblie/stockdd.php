<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php"); 
  $title = "库存查询";
  $where = "o.is_delete = 1 and w.is_del = 1 and w.userid = '$userid' and (w.status = 1 or w.status =2)";
  if ($_SESSION['uid'] != 99999) {
	$where .= " and o.pid3 = '".$_SESSION['uid']."'";
	}
   if ($search) {
		$where .= " and orderid like '%".$search."%'";
	}
  $wa = $db->query("select w.* from yasa_warehouse as w 
  	left join yasa_order as o on o.order_id = w.orderid
  	where $where");
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>库存详情</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10pxs;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 20px;
			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}
		</style>
  		<div class="search">
	        <form action="" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="hidden" name="userid" value="<?=$userid; ?>">

	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	 库存表信息>YY><?=$userid; ?>
	    </div>
	    <div class="content">
			<table border="1" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>订单号</th>
						<th>件数</th>
						<th>体积</th>
						<th>重量</th>
						<th>发货方式</th>
						<th>库龄</th>
						<th>跟单翻译</th>
						<th>备注</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($aa= $db->fetch_array($wa)) { 
							$user = $db->get_one("select u.username from yasa_order as o 
								left join yasa_user as u on u.uid = o.pid3 where o.is_delete = 1 and o.order_id = '$aa[orderid]'");
							// print_r($user);die;
							$allcount += $aa['count'];
							$allvolume += $aa['volume'];
							$allweight += $aa['weight'];?>
						<tr>
							<td><?=$aa[orderid]; ?></td>
							<td><?=$aa[count]; ?></td>
							<td><?=round($aa[volume],2); ?></td>
							<td><?=round($aa[weight]); ?></td>
							<td><?=$aa[fahuo]; ?></td>
							<td><?=(strtotime(date("Y-m-d"))-strtotime($aa['rukutime']))/(60*60*24); ?>天</td>
							<td><?=$user[username]; ?></td>
							<td><?=$aa[remake]; ?></td>
						</tr>
					<? } ?>
					
				</tbody>
				<thead>
					<tr>
						<td>合计</td>
						<td><?=$allcount; ?></td>
						<td><?=round($allvolume,2); ?></td>
						<td><?=$allweight; ?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</thead>
			</table>
	    </div>
      <?php include_once("include/footer.php"); ?>
	    
  	</body>
</html>
