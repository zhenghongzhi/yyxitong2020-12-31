<?php     
  header("Content-Type: text/html;charset=utf-8");
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "订单处理";
  $where = "1 and o.is_delete = 1 and o.addtime > ".strtotime("-1 years");
  if ($_SESSION['uid'] != 99999) {
  	$where .= " and o.pid3 = '".$_SESSION['uid']."'";
  }
  if ($search) {
  	$where .= " and (o.order_id like '%".$search."%' or k.kehu_number like '%".$search."%')";
  }
  $type = $type?$type:0;
  $dai = $where." and o.isshenhe = ".$type;
  if ($type) {
  	$order = ' o.addtime desc';
  }else{
  	$order = ' o.addtime asc';
  }
  // $que = $where." and o.isshenhe = ".$type;
  // echo $dai;die;
  $daisql = $db->query("select o.*,k.kehu_number from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where $dai order by $order");
  // echo "select o.*,k.kehu_number from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where $dai order by o.addtime desc";die;
  // $quesql = $db->query("select o.*,k.kehu_number from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where $que order by o.addtime desc");
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>订单处理</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script src="js/jquery-1.8.3.min.js"></script>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
			.content{
				padding: 10px; 
				margin-bottom: 100px;
			}
			.content .top{
				border-bottom: 1px solid #f0f0f0;
			}
			.content .top a{
				display: inline-block;
				padding: 5px 15px;
				float: left;
			}
			.dangqian{
				background: #0099ff;
				color: #fff;
			}
			table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 10px;
			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}
			table{table-layout: fixed;}
			td{word-break: break-all; word-wrap:break-word;}
		</style>
  		<div class="search">
	        <form action="" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton" value="<?=$search; ?>"> 
	          	<input type="hidden" name="type" value="<?=$type; ?>">
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	订单处理
	    </div>
	    <div class="content">
			<div class="top">
				<a href="orderprocessing.php?type=0" class="<?=$type==0?'dangqian':''; ?>">待确认</a>
				<a href="orderprocessing.php?type=1" class="<?=$type==1?'dangqian':''; ?>">已确认</a>
				<div class="clear"></div>
			</div>
			<div class="middle">
				<table border="1" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th width="30%">下单日期</th>
							<th width="30%">订单编号</th>
							<th width="25%">客户编号</th>
							<th width="15%">操作</th>
						</tr>
					</thead>
					<tbody>
						<?php while ($d = $db->fetch_array($daisql)) { ?>
							
							<tr>
								<td><?=date("Y/m/d",$d['addtime']); ?></td>
								<? if($type == 0){ ?>
									<td><?=$d['order_id']; ?></td>
								<? }else{ ?>
									<td><a href="orderd.php?id=<?=$d['id']; ?>&k=1"><?=$d['order_id']; ?></a></td>
								<? } ?>
								<td><?=$d['kehu_number']; ?></td>
								<? if($type == 0){ ?>
									<?php 
										if ($_SESSION['uid'] == 99999) {
											$url = 'orderd.php?id='.$d['id'];
										}else{
											$url = 'orderadd.php?id='.$d['id'];
										}
									 ?>
								<td><a href="<?=$url; ?>">修改</a><a href="ordercontract.php?orderid=<?=$d['id']; ?>">查询</a></td>
								<? }else{ ?>
								<td>已确认</td>
								<? } ?>
							</tr>
						<?php } ?>
						
					</tbody>
				</table>
				<!-- <table border="1" cellpadding="0" cellspacing="0" style="display: none;">
					<thead>
						<tr>
							<th width="30%">下单日期</th>
							<th width="30%">订单编号</th>
							<th width="25%">客户编号</th>
							<th width="15%">操作</th>
						</tr>
					</thead>
					<tbody>
						<?php while ($q = $db->fetch_array($quesql)) { ?>
							
							<tr>
								<td><?=date("Y/m/d",$q['addtime']); ?></td>
								<td><a href="orderd.php?id=<?=$q['id']; ?>&k=1"><?=$q['order_id']; ?></a></td>
								<td><?=$q['kehu_number']; ?></td>
								<td>已确认</td>
							</tr>
						<?php } ?>
						
					</tbody>
				</table> -->
			</div>
	    </div>
	    <div style="height: 100px;width: 100%;"></div>
    	<?php include_once("include/footer.php"); ?>
	    
  	</body>

  	<script>
  		// $('.top a').click(function(){
  		// 	var index = $(this).index();
  		// 	$(".top a").removeClass("dangqian");
  		// 	$(this).addClass("dangqian");
  		// 	$('table').hide();
  		// 	$('table').eq(index).show();
  		// })
  	</script>
</html>
