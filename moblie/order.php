<?php 
	header("Content-Type: text/html;charset=utf-8");
	include_once("../include/common.ini.php");
	include_once("error.inc.php");
	include_once("include/common.php");
	$title = "订单查询";

 ?>
 <!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>付款申请</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		   
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
			.button{
				display: block;
				width: 80%;
				margin: 0 auto;
				margin-top: 50px;
				height: 60px;
				text-align: center;
				line-height: 60px;
				background: #09f;
				color: #fff;
				border-radius: 20px;
			}
		</style>
  		
	    <div class="shaixuan">
	    	
	    </div>
	    <div class="content">

			<a href="http://47.106.88.138:8082/#/business/business/orderQuery" class="button">订单查询</a>
			<a href="orderprocessing.php" class="button">订单处理</a>
			<a href="orderadd.php" class="button">订单添加</a>
			<div class="clear"></div>
		</div>
			
	 
    	<?php include_once("include/footer.php"); ?>
	    
  	</body>
</html>