<?php 
    include_once("../include/common.ini.php");
    include_once("error.inc.php");
    header("Content-type: text/html; charset=utf-8");
    if($method == 'logout'){
        $_SESSION['user']='';
        foreach($_SESSION as $key=>$v){
            unset($_SESSION[$key]);
        }
        header("location:index.php");
        exit;
    }

    if($_SESSION['user']){
        header("Location:index.php");
        exit;
    }
    
    if($method == 'CheckLogin'){
        if( trim(strtolower($_POST['verifyCode']))==""){
            $Errmsg = $Errmsg."请输入附加码。";
            $FoundErr = true;
        }elseif( $_SESSION['captcha']==""){
            $Errmsg = $Errmsg."请不要重复提交，如需重新登陆请返回登陆页面。";
            $FoundErr = true;
        }elseif(trim(strtolower($_POST['verifyCode'])) != $_SESSION['captcha']){
            $Errmsg = $Errmsg."你输入的附加码和系统产生的不符。";
            $FoundErr = true;
        }
        


        if($username == "" || $password == ""){
            $Errmsg = $Errmsg."请输入登陆用户名或密码。";
            $FoundErr = true;
        }

        if($FoundErr){
            echo "<script>alert('".$Errmsg."');</script>";
        }
        $onlineip = "";
        if(getenv('HTTP_CLIENT_IP')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif(getenv('HTTP_X_FORWARDED_FOR')) { 
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif(getenv('REMOTE_ADDR')) { 
            $onlineip = getenv('REMOTE_ADDR');
        } else { 

            $onlineip = $HTTP_SERVER_VARS['REMOTE_ADDR'];
        }
        $IP = $onlineip;
        $Sql = "select * from cuiniao_admin where username='$username'";
        $Rs = $db->get_one($Sql);
        if($Rs['username']==''){
            $Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
            // er($Errmsg,2);
            echo "<script>alert('".$Errmsg."');</script>";
        }else{
            // echo 1;die;3
            // echo md5($password);
            if($Rs["password"] != md5($password)){
                $Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
            // print_r($Rs);exit;
                // er($Errmsg,2);
                echo "<script>alert('".$Errmsg."');</script>";
                // echo 2;die;
                
            }else{
                // echo 6;die;
                $_SESSION['user'] = $username;
                $_SESSION['cuiniao_id'] = $Rs['id'];
                $_SESSION['fyid']=$Rs['fyid'];
                $_SESSION['mcid']=$Rs['mcid'];
                $_SESSION['wxid']=$Rs['wxid'];
                $_SESSION['uid']=$Rs['uid'];
                $db->query("update cuiniao_admin set LastLoginIP='$IP',logintime='".date("Y-m-d H:i:s")."' where username='$username'");
                setCookie('name',$username,time()+60*60*24*14);
                setCookie('z',$password,time()+60*60*24*14);
                // echo 1;die;
                header("Location:index.php");
                $_SESSION['captcha']="";
                exit;
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>登录</title>
        <link rel="stylesheet" href="fonts/iconfont.css"/>
        <link rel="stylesheet" href="css/font.css"/>
        <link rel="stylesheet" href="css/mui.css"/>
        <link rel="stylesheet" href="css/weui.min.css"/>
        <link rel="stylesheet" href="css/jquery-weui.min.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        <link rel="stylesheet" href="css/pages/login.css"/>
        <script src="js/jquery-1.8.3.min.js""></script>
        <script>(function (doc, win) {
          var docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
              var clientWidth = docEl.clientWidth;
              if (!clientWidth) return;
              docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
            };

          if (!doc.addEventListener) return;
          win.addEventListener(resizeEvt, recalc, false);
          doc.addEventListener('DOMContentLoaded', recalc, false);
        })(document, window);</script>
    </head>
    <body>
        <div class="header">
            <h1>业务管理系统</h1>
        </div>
        <div class="login-wrap" style="margin-top:20px;">
            <div class="login-box">
                <form name="admininfo" method="post" action="login.php" id="forms">
                    <div class="input-wrap">
                        <input type="text" name="username" placeholder="账号/邮箱" value="<?=$_COOKIE['name']; ?>">
                    </div>
                    <div class="input-wrap">
                        <input type="password" name="password" placeholder="密码" value="<?=$_COOKIE['z']; ?>">
                    </div>
                    <div class="input-wrap">
                        <input type="text" name="verifyCode" placeholder="验证码">

                        <img id="cc" src="../include/cool-php-captcha_admin/captcha.php" class="validate-code"  onclick="document.getElementById('cc').src='../include/cool-php-captcha_admin/captcha.php?'+Math.random();">
                    </div>
                    <input type="hidden" value="CheckLogin" name="method">
                    <!-- <input type="submit" class="weui_btn login-btn weui_btn_primary" style="margin-top:20px;" value="登录"> -->
                </form>
            </div>

            <a class="weui_btn login-btn weui_btn_primary" style="margin-top:20px;">登录</a>
            <script>
                $('.login-btn').click(function(){
                    console.log(1);
                    $("#forms").submit();
                })
            </script>
        </div>
        <div class="footer">
            Copyright&nbsp;&copy;&nbsp;xxxxxxxxxxx&nbsp;版权所有
        </div>
    </body>
</html>