<?php     
	include_once("../include/common.ini.php");
	include_once("error.inc.php");
	include_once("include/common.php"); 
	$title = "佣金查询";
	if ($_SESSION['uid'] != 99999) {
		$where .= " and o.pid3 = '".$_SESSION['uid']."'";
		$user = $db->get_one("select * from yasa_user where uid = '$_SESSION['uid']'");
	}
	$where = "1 and o.is_delete = 1 and w.is_del = 1 and o.addtime >= 1580486400";
	if ($user) {
		$where .= " and u.username like '%".$user[username]."%'";
	}
	$starttime = $starttime?$starttime:date("Y-m-d", strtotime("-1 year"));
	$endtime = $endtime?$endtime:date("Y-m-d");

	$where .= " and w.chucangtime BETWEEN '$starttime' and '$endtime'";
	// echo $where;
	$sql = $db->query("select w.money,u.id as uids,u.username,u.nickname,o.yongjin,o.bili,o.hybili1,o.kybili1,o.hybili2,o.kybili2 from yasa_warehouse as w 
		left join yasa_order as o on w.orderid = o.order_id 
		left join yasa_user as u on u.uid = o.pid3 where $where");


?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>库存</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 10px;
			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}
		</style>
  		<div class="search">
	        <form action="" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	 佣金信息
	    </div>
	    <div class="content">
			<table border="1" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>客户归属</th>
						<th>件数</th>
						<th>体积</th>
						<th>重量</th>
						<th>发货方式</th>
						<th>最老库龄</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($k = $db->fetch_array($khsql)) {
						$w = $db->get_one("select sum(count) as count,sum(volume) as volume,sum(weight) as weight,min(rukutime) as rukutime from yasa_warehouse where khtype = $k[id] and is_del = 1 and fahuo = '$k[fahuo]' and (status = 1 or status = 2)");
						$allcount += $w['count'];
						$allvolume += $w['volume'];
						$allweight += $w['weight'];

					 ?> 
						
						<tr>
							<td><a href="stockd.php?kh=<?=$k[id]; ?>&fahuo=<?=$k[fahuo]; ?>"><?=$k[subject]; ?></a></td>
							<td><?=round($w['count']); ?></td>
							<td><?=round($w['volume'],2); ?></td>
							<td><?=round($w['weight']); ?></td>
							<td><?=$k['fahuo']; ?></td>
							<td><?=(strtotime(date("Y-m-d"))-strtotime($w['rukutime']))/(60*60*24); ?>天</td>
						</tr>
					<?php } ?>
				</tbody>
				<thead>
					<tr>
						<td>合计</td>
						<td><?=$allcount; ?></td>
						<td><?=round($allvolume,2); ?></td>
						<td><?=$allweight; ?></td>
						<td></td>
						<td></td>
					</tr>
				</thead>
			</table>
	    </div>
      <?php include_once("include/footer.php"); ?>
  	</body>
</html>