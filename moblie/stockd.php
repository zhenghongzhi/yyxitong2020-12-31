<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php"); 
  $title = "库存查询";
  $khtype = $db->get_one("select * from yasa_kehutype where id = $kh");
  $where = "o.is_delete = 1 and w.is_del = 1 and w.khtype = $kh and w.fahuo = '$fahuo' and (w.status = 1 or w.status =2)";
  if ($_SESSION['uid'] != 99999) {
	$where .= " and o.pid3 = '".$_SESSION['uid']."'";
	}
  if ($search) {
		$where .= " and userid like '%".$search."%'";
	}
  $wa = $db->query("select w.*,sum(w.count) as sumcount,sum(w.volume) as sumvolume,sum(w.weight) as sumweight,min(w.rukutime) as minrukutime from yasa_warehouse as w 
  	left join yasa_order as o on o.order_id = w.orderid
  	where $where group by w.userid");
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>库存详情</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 10px;
			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}
		</style>
  		<div class="search">
	        <form action="" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="hidden" name="kh" value="<?=$kh; ?>">
	          	<input type="hidden" name="fahuo" value="<?=$fahuo; ?>">
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	 库存表信息><?=$khtype['subject']; ?>
	    </div>
	    <div class="content">
			<table border="1" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>客户编号</th>
						<th>件数</th>
						<th>体积</th>
						<th>重量</th>
						<th>发货方式</th>
						<th>库龄</th>
						<!-- <th>跟单翻译</th> -->
					</tr>
				</thead>
				<tbody>
					<?php while ($aa= $db->fetch_array($wa)) { 

							$allcount += $aa['count'];
							$allvolume += $aa['volume'];
							$allweight += $aa['weight'];?>
						<tr>
							<td><?=$aa[userid]; ?></td>
							<td><a href="stockdd.php?userid=<?=$aa[userid]?>"><?=$aa[sumcount]; ?></a></td>
							<td><?=round($aa[sumvolume],2); ?></td>
							<td><?=round($aa[sumweight]); ?></td>
							<td><?=$fahuo; ?></td>
							<td><?=(strtotime(date("Y-m-d"))-strtotime($aa['rukutime']))/(60*60*24); ?>天</td>
							<!-- <td>马</td> -->
						</tr>
					<? } ?>
					
				</tbody>
				<thead>
					<tr>
						<td>合计</td>
						<td><?=$allcount; ?></td>
						<td><?=round($allvolume,2); ?></td>
						<td><?=$allweight; ?></td>
						<td></td>
						<td></td>
						<!-- <td></td> -->
					</tr>
				</thead>
			</table>
	    </div>
      <?php include_once("include/footer.php"); ?>
	    
  	</body>
</html>
