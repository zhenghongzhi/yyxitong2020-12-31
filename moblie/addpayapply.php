<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "审批";

  if ($search) {
    $order = $db->get_one("select o.id,o.order_id,o.type1,o.type5,o.isshenhe,k.kehu_number,s.subject,s.kehu_number as snumber,s.bankNo,s.openName from yasa_order as o 
      left join yasa_kehu as k on k.id = o.pid1 
      left join yasa_shangjia as s on s.id = o.pid2
      where o.is_delete = 1 and o.order_id = '".$search."' and o.pid3 = '".$_SESSION['uid']."'");
     $caiwuurl1 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$order[order_id];
      $caiwu1 = curl_get_https($caiwuurl1);
      $caiwu1 = json_decode($caiwu1);
  }
  // print_r($order);die;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
      <title>申请记录</title>
      <link rel="stylesheet" href="fonts/iconfont.css"/>
      <link rel="stylesheet" href="css/font.css"/>
      <link rel="stylesheet" href="css/weui.min.css"/>
      <link rel="stylesheet" href="css/jquery-weui.min.css"/>
      <link rel="stylesheet" href="css/mui.css"/>
      <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      <script>(function (doc, win) {
        var docEl = doc.documentElement,
          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
          recalc = function () {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
          };

        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
      })(document, window);</script>
  </head>
  <style>
    .clear{
      clear: both;
    }
    .searchbutton{
      width: 90%;
      border-radius: 10px;
    }
    .search {
        padding-top: 44px;
    }
    .search input{
      height: 30px;
    }
   .shaixuan{
      padding: 10px;
      background-color:rgba(102, 102, 102, 0.06);
    }
    .content{
      padding: 10px; 
    }
    .content p {
      margin-top: 20px;
    }
    .shenqing{
      display: inline-block;
      width: 70px;
      height: 32px;
      background-color: rgb(0, 153, 255);
      color: #fff;
      text-align: center;
      line-height: 32px;
      margin-left: 20px;
    }
  </style>
  <body>
      <?php include_once("include/header.php"); ?>
      <div class="search">
        <form action="" style="position: relative;">
          <input type="input" name="search" class="searchbutton" value="<?=$search; ?>"> 
          <input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
        </form>
      </div>
      <div class="shaixuan">
        订单查询>付款申请
      </div>
      <? if ($search) { 
        if ($order) { ?>
          <div class="content">
            <p>订单编号：<?=$order['order_id']; ?></p>
            <p>客户编号：<?=$order['kehu_number']; ?></p>
            <p>总货款：￥<?=$order['type1']?$order['type1']:0; ?></p>
            <p>已付款：￥<?=$order[type5]+$caiwu1->deposit+$caiwu1->balance; ?></p>
            <p>会员号：<?=$order['snumber']; ?></p>
            <p>供应商：<?=$order['subject']; ?></p>
            <p>付款账号：<?=$order['bankNo']; ?></p>
            <p>账户户名：<?=$order['openName']; ?></p>
          </div>
          <? if($order['isshenhe']){ ?>
            <a href="addshenpi.php?id=<?=$order['id']; ?>" class="shenqing">申请</a>
           <? }else{ ?>
            <a href="orderd.php?id=<?=$order['id']; ?>" class="shenqing">编辑订单</a>
            <br>
            联系管理员确定订单
           <? } ?>
          
      <?  }else{
        echo '找不到订单';   
      }
      } ?>
      <?php include_once("include/footer.php"); ?>
  </body>
</html>