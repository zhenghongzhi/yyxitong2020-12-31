<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php"); 
  $title = "申请记录";
  if ($_SESSION['wxid']) {
    $where = "o.is_delete = 1 and s.moudle = 'pay' and s.shenqing = '".$_SESSION['wxid']."'";
    if ($search) {
      $where .= " and (o.order_id like '%".$search."%' or k.kehu_number like '%".$search."%')";
    }
    if ($status) {
      $where .= " and s.status =".$status;
    }
    if ($time) {
      if ($time == "week") {
        $where .= " and o.addtime BETWEEN ".strtotime(date("Y-m-d",strtotime("-7 day")))." AND  ".time();
      }elseif ($time == "month") {
        $where .= " and o.addtime BETWEEN ".strtotime(date("Y-m-d",strtotime("-1 month")))." AND  ".time();
      }
    }
   
    $arrsql = $db->query("SELECT s.*,o.order_id,k.kehu_number FROM yasa_shenpi as s
      LEFT JOIN yasa_order as o on o.id = s.oid 
      LEFT JOIN yasa_kehu as k on k.id = s.uid
     WHERE $where order by s.addtime desc");
  }
  // print_r($_SERVER);die;
  $statusarr = array(
      '1' => '审核中',
      '2' => '已通过',
      '3' => '已驳回',
      '4' => '已撤销',
      '6' => '通过后撤销',
      '7' => '已删除',
      '8' => '应付未付',
      '9' => '支付复核',
      '10' => '已支付'
    );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
      <title>申请记录</title>
      <link rel="stylesheet" href="fonts/iconfont.css"/>
      <link rel="stylesheet" href="css/font.css"/>
      <link rel="stylesheet" href="css/weui.min.css"/>
      <link rel="stylesheet" href="css/jquery-weui.min.css"/>
      <link rel="stylesheet" href="css/mui.css"/>
      <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      <script src="js/jquery-1.8.3.min.js"></script>
      <script>(function (doc, win) {
        var docEl = doc.documentElement,
          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
          recalc = function () {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
          };

        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
      })(document, window);</script>
  </head>
  <style>
    .clear{
      clear: both;
    }
    .searchbutton{
      width: 90%;
      border-radius: 10px;
    }
    .search {
        padding-top: 44px;
    }
    .search input{
      height: 30px;
    }
    .shaixuan div{
      width: 50%;
      float: left;
      text-align: center;
      padding: 10px 0px;
      background-color:rgba(102, 102, 102, 0.06);
    }
    .mcontent{
      margin-bottom: 150px;
    }
    .content{
      height: 120px;
      border-bottom: 1px solid #f0f0f0;
      margin-top: 10px;
    }
    .content div{
      text-align: center;
      float: left;
      /*height: 120px;*/
    }
    .content a{
      width: 100%;
    }
    .content p{
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
      word-break: break-all;
    }
    .ls{
      width: 33%;
    }
    .zhuangtai{
      display: block;
      padding: 4px;
      border: 1px solid green;
      text-align: center;
      width: 40%;
      margin: 0px auto;
      font-size: 12px;
      margin-top: 20px;
    }
    .tijiaotime{
      /*text-align: center;*/
      margin-top: 20px;
    }
    .button{
      position: fixed;
      bottom: 80px;
      width: 100px;
      left: 50%;
      margin-left: -50px;
    }
    .addbutton{
      display: inline-block;
      width: 100px;
      height: 30px;
      line-height: 30px;
      text-align: center;
      background: #0099ff;
      color: #fff;
      border-radius: 10px;
    }
    .zzstatus{
      z-index: 999;
      width: 100%;
      height: 100%;
      display: none;
      position: fixed;
      top: 0px;
      background: rgb(0,0,0,0.4);
    }
    .status{
      position: fixed;
      bottom: 0px;
      width: 100%;
      padding: 10px 0px;
      background-color: rgb(240, 240, 240);
    }
    .status ul li {
      padding: 0px 20px;
      height: 40px;
      line-height: 40px;
      border-bottom: 1px solid #ddd;
    }
    .zztime{
      z-index: 999;
      width: 100%;
      height: 100%;
      display: none;
      position: fixed;
      top: 0px;
      background: rgb(0,0,0,0.4);
    }
    .time{
      position: fixed;
      bottom: 0px;
      width: 100%;
      padding: 10px 0px;
      background-color: rgb(240, 240, 240);
    }
    .time ul li {
      padding: 0px 20px;
      height: 40px;
      line-height: 40px;
      border-bottom: 1px solid #ddd;
    }
  </style>
  <body>
      <?php include_once("include/header.php"); ?>
      <div class="search">
        <form action="" style="position: relative;">
          <input type="input" name="search" class="searchbutton" value="<?=$search; ?>"> 
          <input type="hidden" name="time" value="<?=$time; ?>">
          <input type="hidden" name="status" value="<?=$status; ?>">
          <input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
        </form>
      </div>
      <div class="shaixuan">
        <div class="statusopen">
          审批状态
        </div>
        <div class="timeopen">
          提交时间
        </div>
      </div>
      <div class="clear"></div>
      <div class="mcontent">
        <?php while ($arr = $db->fetch_array($arrsql)) { ?>
          <div class="content">
            <a href="payapplyd.php?id=<?=$arr['id']; ?>">
              <div style="font-size: 13px;" class="ls">
                  <p>申请人：<?=$_SESSION['cuiniao_Admin_Name']; ?></p>
                  <p>订单编号：<?=$arr['order_id']; ?></p>
                  <p>客户编号：<?=$arr['kehu_number']; ?></p>
                  <p>实际应付：￥<?=$arr['money']; ?></p>
              </div>
              <div class="ls">
                <span class="zhuangtai"><?=$statusarr[$arr['status']]; ?></span>
              </div>
              <div class="tijiaotime ls">
                <p><?=date("Y-m-d",$arr['addtime']); ?></p>
              </div>
              <!-- <div class="clear"></div> -->
            </a>
          </div>
         <? } ?>
      </div>
      <div class="clear"></div>
      <div class="button">
        <a href="addpayapply.php" class="addbutton">添加新申请</a>
      </div>
      
      <div class="zzstatus">
        <div class="status">
          <ul>
            <li><a href="?status=0&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">全部</a></li>
            <li><a href="?status=1&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">审批中</a></li>
            <li><a href="?status=2&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">已通过</a></li>
            <li><a href="?status=3&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">已驳回</a></li>
            <li><a href="?status=10&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">已支付</a></li>
            <li><a href="?status=11&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">待入账</a></li>
            <li><a href="?status=12&time=<?=$time?$time:0; ?>&search=<?=$search; ?>">已入账</a></li>
          </ul>
        </div>
      </div>
      <div class="zztime">
        <div class="time">
          <ul>
            <li><a href="?status=<?=$status?$status:0; ?>&time=0&search=<?=$search; ?>">全部</a></li>
            <li><a href="?status=<?=$status?$status:0; ?>&time=week&search=<?=$search; ?>">近1周</a></li>
            <li><a href="?status=<?=$status?$status:0; ?>&time=month&search=<?=$search; ?>">近1月</a></li>
          </ul>
        </div>
      </div>
      <?php include_once("include/footer.php"); ?>
  </body>
  <script>
    $('.zzstatus').click(function(){
      $(this).hide();
    })
    $('.statusopen').click(function(){
      $('.zzstatus').show();
    })
    $('.zztime').click(function(){
      $(this).hide();
    })
    $('.timeopen').click(function(){
      $('.zztime').show();
    })
  </script>
</html>