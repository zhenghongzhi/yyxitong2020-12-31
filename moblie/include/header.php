<header>
  <div class="titlebar reverse">
      <a href="javascript:history.back(-1);">
        <i class="iconfont"><</i>
      </a>
      <h1><?=$title; ?></h1>
      <a href="login.php?method=logout" class="app">
        <i class="iconfont">&#xe60e;</i>
      </a>
  </div>
</header>