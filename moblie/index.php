<?php 
    include_once("../include/common.ini.php");
    include_once("error.inc.php");
    include_once("include/common.php");

 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <title>主页面</title>
        <link rel="stylesheet" href="fonts/iconfont.css"/>
        <link rel="stylesheet" href="css/font.css"/>
        <link rel="stylesheet" href="css/weui.min.css"/>
        <link rel="stylesheet" href="css/jquery-weui.min.css"/>
        <link rel="stylesheet" href="css/mui.css"/>
        <link rel="stylesheet" href="css/pages/index.css"/>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <script>
            (function (doc, win) {
              var docEl = doc.documentElement,
                resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
                recalc = function () {
                  var clientWidth = docEl.clientWidth;
                  if (!clientWidth) return;
                  docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
                };

              if (!doc.addEventListener) return;
              win.addEventListener(resizeEvt, recalc, false);
              doc.addEventListener('DOMContentLoaded', recalc, false);
            })(document, window);
        </script>
    </head>
    
    
    <body>
        <header>
            <div class="titlebar reverse" >
                <h1>YY业务主页面</h1>
				<a href="login.php?method=logout" class="app">
                    <i class="iconfont">&#xe60e;</i>
                </a>
            </div>
        </header>
        <article style="padding-bottom:14px;">
            <div class="list-wrap">
                <h4>业务操作</h4>
                <ul class="app-list">
                    <li>
                        <div class="app-wrap" onclick="window.location.href='order.php'">
                            <!-- <a href="order.php"> -->
                                <img src="images/订单查询.png" alt="" width="60">
                                <span>订单查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='http://47.106.88.138:8082/#/customer/customer/list'">
                            <!-- <a href="http://47.106.88.138:8082/#/customer/customer/list"> -->
                                <img src="images/账单查询.png" alt="" width="60">
                                <span>账单查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='business.php'">
                            <!-- <a href="business.php"> -->
                                <img src="images/商家查询.png" alt="" width="60">
                                <span>商家查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='receipt.php'">
                            <!-- <a href="receipt.php"> -->
                                <img src="images/入仓签收.png" alt="" width="60">
                                <span>入仓签收单</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='payapply.php'">
                            <!-- <a href="payapply.php"> -->
                                <img src="images/付款申请.png" alt="" width="60">
                                <span>货款付款申请</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='http://47.106.88.138:8082/#/business/business/collectionReceipt'">
                           <!--  <a href="http://47.106.88.138:8082/#/business/business/collectionReceipt"> -->
                                <img src="images/收款水单.png" alt="" width="60">
                                <span>收款水单</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='encasement.php'">
                            <!-- <a href="encasement.php"> -->
                                <img src="images/装箱单查询.png" alt="" width="60">
                                <span>装箱单查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='stock.php'">
                            <!-- <a href="stock.php"> -->
                                <img src="images/库存查询.png" alt="" width="60">
                                <span>库存查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap">
                            <a>
                                <img src="images/物流查询.png" alt="" width="60">
                                <span>物流查询</span>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='reimbursementlist.php'">
                            <!-- <a href="reimbursementlist.php"> -->
                                <img src="images/物流查询.png" alt="" width="60">
                                <span>其他款付款申请</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <li>
                        <div class="app-wrap" onclick="window.location.href='yongjin.php'">
                            <!-- <a href="reimbursementlist.php"> -->
                                <img src="images/物流查询.png" alt="" width="60">
                                <span>佣金查询</span>
                            <!-- </a> -->
                        </div>
                    </li>
                    <!-- <li>
                        <div class="app-wrap">
                            <a href="rebate.php">
                                <img src="images/物流查询.png" alt="" width="60">
                                <span>个人返点查询</span>
                            </a>
                        </div>
                    </li> -->
                </ul>
            </div>
        </article>
        <?php include_once("include/footer.php"); ?>
    </body>
</html>