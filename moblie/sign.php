<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $warehouse = $db->get_one("select * from yasa_warehouse where id = $id");
  $title = "签收信息";
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>付款申请</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <!-- <link rel="stylesheet" href="css/weui.min.css"/> -->
	    <!-- <link rel="stylesheet" href="css/jquery-weui.min.css"/> -->
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      	<script src="js/jquery-1.8.3.min.js"></script>
	    <script src="js/jq-signature.js"></script>
     	<script type="text/javascript" src="js/layer/layer.js"></script>
	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
  			*{
  				cursor:pointer;
  			}
  			body,html{overflow: hidden;height: 100%;}
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
		    .content{
		    	width: 90%;
		    	margin: 0px auto;
		    	padding: 15px 8px;
		    	border-radius: 20px;
		    	background: rgba(204, 204, 204, 0.21);
		    	margin-top: 20px;
		    	border: 1px solid rgb(204, 204, 204);
		    }
			.content table tr td{
				width: 25%;
				text-align: left;
				height: 40px;
				line-height: 40px;
			}
			.radio{
				margin-top: 30px;
				padding: 10px;
				margin-left: 10px;
			}
			.radio input{
				width: 20px;
				height: 20px;
				-webkit-appearance:checkbox;
			}
			#shi,#fou{
				margin-left: 40px;
			}
			.js-signature{
				margin: 10px;
				margin-top: 20px;
				height: 200px;
				border: 1px solid rgb(204, 204, 204)
			}
			.submit{
				display: flex;
  				justify-content: center;
  				margin-top: 40px;
  				margin-bottom: 100px;
			}
			.button{
				width: 74px;
    			height: 35px;
    			background-color: rgb(0, 153, 255);
    			border: 1px solid rgb(102, 102, 102);
    			color: #fff;
    			border-radius: 10px;
			}
			.quxiao{
				margin-left: 50px;
			}
			.btn{
				padding: 10px;
				width: 100px;
				text-align: center;
				color: #fff;
				background: rgb(0, 153, 255);
				border-radius: 10px;
				float: left;
				margin-left: 10px;
			}
		</style>
		<div class="search">
		</div>
		<div class="shaixuan">
	    	 签收信息
	    </div>
	    <form id="form" action="warehouseapi.php">
			<div class="radio">
		    	是否已收齐
		    	<input type="radio" value="2" name="finish" id="shi"><label for="shi">是</label>
		    	<input type="radio" value="1" name="finish" id="fou"><label for="fou">否</label> 
			</div>
			
			<div class="js-signature" data-width="600" data-height="200" data-border="1px solid black" data-line-color="#bc0000" data-auto-fit="true"></div>
		
			<p><div id="clearBtn" class="btn btn-default" onclick="clearCanvas();">重新签名</div>&nbsp;<div id="saveBtn" class="btn btn-default" onclick="saveSignature();" disabled>确定签名</div></p>
			
			<input type="hidden" value="" name="sign" id="sign">
			<input type="hidden" value="<?=$id; ?>" name="id">
			<div class="submit">
				<input type="button" value="提交" class="button queding">
				<input type="button" value="取消" class="button quxiao">
			</div>
		</form>
		<div style="height: 100px;width: 100%;"></div>
    	<?php include_once("include/footer.php"); ?>	
  	</body>
  	<script type="text/javascript">

		$(document).on('ready', function() {
			if ($('.js-signature').length) {
				$('.js-signature').jqSignature();
			}
		});

		function clearCanvas() {
			// $('#signature').html('<p><em>Your signature will appear here when you click "Save Signature"</em></p>');
			$('.js-signature').jqSignature('clearCanvas');
			$('#saveBtn').attr('disabled', true);
			$('#sign').val("");
		}

		function saveSignature() {
			var dataUrl = $('.js-signature').jqSignature('getDataURL');
			$('#sign').val(dataUrl);
	        
			
		}

		$('.js-signature').on('jq.signature.changed', function() {
			$('#saveBtn').attr('disabled', false);
		});

		$('.queding').click(function(){
	        var a = 1;
	        
	        var val=$('input:radio[name="finish"]:checked').val();
	        var form = $('#form').serialize();
            if(val==null){
                layer.msg('请完成信息',function(){

                });
                return false;
            }
            var sign = $("#sign").val();
            if (!sign) {
            	layer.msg('请确定签名',function(){

                });
                return false;
            }
	        if (a == 1) {
	          	layer.msg('是否确定提交审批？', {
		            time: 0 //不自动关闭
		            ,btn: ['是', '否']
		            ,yes: function(a){
		            	$('#form').submit();
		            	// alert(1);
		             //  	$('.layui-layer-btn1').click();
		             // 	$.post('warehouseapi.php',form,
		             //        // 调用服务端成功后的回调函数
		             //      	function(data){ 
		             //      		alert(data);
		             //            layer.msg(data.msg,{time: 2000},function(){
		             //               window.location.href="receipt.php?rad="+Math.random();;
		             //            });
		             //       	},'json')
		            	}
	          	});
	        }
      	});
		
      	document.body.addEventListener('touchmove', (e) => {
		  e.preventDefault();
		}, { passive: false });
	</script>
</html>