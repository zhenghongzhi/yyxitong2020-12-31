<?php 
header("Content-type: text/html; charset=utf-8");  
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "商家查询";
  $page = $page?$page:1;
  // echo 1;die;
  // $data = array('field' =>'memberNo','op'=> '\$like','value'=>array('CH-02500'));

  // print_r($data);die;	
  // $url = 'http://pushapi.guangzhouyueyang.com/v1/api/supplierapi?pageSize=10&pageNo=1';
  // $url = 'http://pushapi.guangzhouyueyang.com/v1/member/list?pageSize=40&pageNo=1';
  // // v1/label/list?pageSize=100&pageNo=1
  // // v1/member/list?pageSize=10&pageNo=1
  // $shangjia = zhzcurl($url,$data);
  // $shangjia = json_decode($shangjia);
  // print_r($shangjia);die;
  // $p=new Page(100,4,$page,8);
  // print_r($lebelarr);
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>商家查询</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script src="js/jquery-1.8.3.min.js"></script>

	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 70%;
		      	border-radius: 10px;
		    }
		    .shuaixuan{
				height: 30px;
				border:1;
			}
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
			.content{
				padding: 10px; 
			}
			.content .top{
				border-bottom: 1px solid #f0f0f0;
			}
			.content .top a{
				display: inline-block;
				padding: 5px 15px;
				float: left;
			}
			.dangqian{
				background: #0099ff;
				color: #fff;
			}
			table{
				margin: 0 auto;
				width: 95%;
				text-align: center;
				margin-top: 10px;

			}
			table thead{
				background-color: rgba(204, 204, 204, 0.23);
			}

		</style>
  		<div class="search">
	        <form style="position: relative;">
	          	<select name="shuaixuan" id="shuaixuan" id="shuaixuan">
					<option value="memberNo">商家编号</option>
					<option value="companyName">商家名字</option>
	          	</select>
	          	<input type="input" name="search" class="searchbutton" id="search"> 
	          	<input type="button" value="搜索" style="position: absolute;right: 0px;top:0px;" onclick="post()">
	        </form>
	    </div>
	    <div class="shaixuan">
	    	商家信息
	    </div>
	    <div class="content">
			<div class="middle">
				<table border="1" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<th width="30%">会员号</th>
							<th width="30%">商家名称</th>
							<th width="25%">行业</th>
							<th width="15%">操作</th>
						</tr>
					</thead>
					<tbody id="tbody">
						
						
					</tbody>
				</table>
			</div>
	    </div>
	    <div style="height: 100px;width: 100%;"></div>
    	<?php include_once("include/footer.php"); ?>
	    
  	</body>

  	<script>
  		function post(){
  			var field = $("#shuaixuan").val();
  			var value = $('#search').val();
			var str = [{"field":field,"Type":"string","op":"$like","value":[value]}];
			var jsarr=JSON.stringify(str);

		    var xhr=new XMLHttpRequest()
		    xhr.open('post','http://pushapi.guangzhouyueyang.com/v1/member/list?pageSize=10&pageNo=1')
		  	// xhr.setRequestHeader();
		    xhr.setRequestHeader('Accept','application/json,text/plain, */*')
		    xhr.setRequestHeader('Content-Type','application/json;charset=UTF-8')
		    xhr.setRequestHeader('X-Token','59136090-d702-11ea-b1e0-ff9e8fcdb84f')
		    
		    xhr.send(jsarr)
		    xhr.onreadystatechange=function(){
		        if (xhr.readyState==4){
		        	var res = JSON.parse(xhr.response)
		        	var arr = res.data.rows;
		        	var str = "";
		        	for(var p in arr){//遍历json对象的每个key/value对,p为key
						str += "<tr>";
						str += "<td>"+arr[p]['memberNo']+"</td>";
						str += "<td>"+arr[p]['companyName']+"</td>";
						str += "<td>"+arr[p]['label']['name']+"</td>";
						str += "<td><a href='businessd.php?id="+arr[p]['memberNo']+"'>查看</a></td>";
						str += "</tr>";
					}
					$('#tbody').html(str);
		        }
		    }

		}
	</script>
</html>
