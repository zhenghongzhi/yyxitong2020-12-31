<?php 
	include_once("../include/common.ini.php");
	include_once("error.inc.php");
	include_once("include/common.php");
    header("Content-type: text/html; charset=utf-8");
    function hebing($imgpath , $font , $font_file , $font_size , $circleSize , $signpath){
        $img = imagecreatefromstring(file_get_contents($imgpath));
        // echo $img;
        $black = imagecolorallocate($img, 0, 0, 0);//字体颜色 RGB
        foreach ($font as $key => $value) {
            // $fontBox = imagettfbbox($font_size, 0, $font_file, $value['nickName']);//文字水平居中实质
            // print_r($value);die;
            imagettftext ($img, $font_size, 0, $value['left'], $value['top'], $black, $font_file, $value['nickName'] );
        }
        // echo $signpath;
        $signimg = imagecreatefromstring(file_get_contents($signpath));
        imagecopyresampled($img,$signimg,1220,3000,0,0,420,420,355,200);
        // echo $signimg;die;

        $imgage = '../upfiles/'.date('Ymd',time()).'/'.time().rand(0,9).".jpg";//生成图片名称
        imagejpeg($img,$imgage);
        return $imgage;

    }

    
    $path = "../upfiles/";
    $res = base64_image_content($sign,$path);
    // echo 1;die;
    
    // echo $sign;die;
    if ($res) {
        $warehouse = $db->get_one("select * from yasa_warehouse where jincangdanhao = '$id' and is_del = 1");

        $all = $db->get_one("select sum(count) as count,sum(volume) as volume,sum(weight) as weight,sum(money) as money,userid,jincangdanhao,rukutime,GROUP_CONCAT(DISTINCT(orderid)) as orderid,GROUP_CONCAT(DISTINCT(product)) as product,bztype from yasa_warehouse where jincangdanhao = '$id' and is_del = 1");

        $order = $db->get_one("select s.* from yasa_order as o
            left join yasa_shangjia as s on s.id = o.pid2
            where o.is_delete = 1 and o.order_id = '".$warehouse['orderid']."'");

        // print_r($warehouse);die;
        $fahuo = $db->get_one("select GROUP_CONCAT(CONCAT_WS(',',fahuo) SEPARATOR ',') as fahuo from yasa_warehouse where jincangdanhao = '$id' and is_del = 1 group by jincangdanhao");
        $fahuoarr = explode(',', $fahuo['fahuo']);
        // print_r($fahuoarr);die;
        // print_r($order);die;
        $imgpath  = '1.jpg';
        
        $font_file = 'simhei.ttf';//字体
        $font_size = 30;   //字体大小
        $circleSize = 0; //旋转角度
        
        
        // print_r($warehouse);die;
        //交货单位
        $fontarr[0]['nickName'] = $order['subject'];
        $fontarr[0]['left'] = '300';
        $fontarr[0]['top'] = '420';

        //收货单位
        $fontarr[1]['nickName'] = '柠檬仓储';
        $fontarr[1]['left'] = '1260';
        $fontarr[1]['top'] = '420';

        //入库时间
        $fontarr[2]['nickName'] = $all['rukutime'];
        $fontarr[2]['left'] = '2160';
        $fontarr[2]['top'] = '420';

        //客户唛头
        $fontarr[3]['nickName'] = $all['userid'];
        $fontarr[3]['left'] = '300';
        $fontarr[3]['top'] = '600';

        //订货单号
        $fontarr[4]['nickName'] = $all['orderid'];
        $fontarr[4]['left'] = '1260';
        $fontarr[4]['top'] = '600';
        
        //商家会员号
        $fontarr[5]['nickName'] = $order['kehu_number'];
        $fontarr[5]['left'] = '2120';
        $fontarr[5]['top'] = '600';

        //货物名称
        $fontarr[6]['nickName'] = $all['product'];
        $fontarr[6]['left'] = '300';
        $fontarr[6]['top'] = '840';

        //入库数量
        $fontarr[7]['nickName'] = $all['count'];
        $fontarr[7]['left'] = '980';
        $fontarr[7]['top'] = '770';

        //单位
        // $fontarr[8]['nickName'] = '啊啊啊啊8';
        // $fontarr[8]['left'] = '1610';
        // $fontarr[8]['top'] = '770';
            
        //总件数
        // $fontarr[9]['nickName'] = $warehouse['product'];
        // $fontarr[9]['left'] = '2120';
        // $fontarr[9]['top'] = '770';

        //货物货值
        $fontarr[10]['nickName'] = $all['money'];
        $fontarr[10]['left'] = '980';
        $fontarr[10]['top'] = '915';

        //重量
        $fontarr[11]['nickName'] = $all['weight'];
        $fontarr[11]['left'] = '1610';
        $fontarr[11]['top'] = '915';

        //体积
        $fontarr[12]['nickName'] = $all['volume'];
        $fontarr[12]['left'] = '2120';
        $fontarr[12]['top'] = '915';

        //送货方式
        $fontarr[13]['nickName'] = '√';
        $fontarr[13]['left'] = '235';//235、475、730、1045
        $fontarr[13]['top'] = '1060';

        //附加费
        $fontarr[14]['nickName'] = $warehouse['fahuo'];
        $fontarr[14]['left'] = '1560';
        $fontarr[14]['top'] = '1060';

        //检验方式
        // $fontarr[15]['nickName'] = '√';
        // $fontarr[15]['left'] = '235';//235、415、595、780
        // $fontarr[15]['top'] = '1245';

        //外观包装
        // $fontarr[16]['nickName'] = '√'; 
        // $fontarr[16]['left'] = '550';//225、370、550
        // $fontarr[16]['top'] = '1415';

        //发货方式
        if ($fahuoarr[0]) {
            
            $fontarr[23]['nickName'] = '√';
            $fontarr[23]['left'] = $fahuoarr[0] == '海运'?300:570;//300、570、835
            $fontarr[23]['top'] = '1590';
        }
        if ($fahuoarr[1]) {
            
            $fontarr[24]['nickName'] = '√';
            $fontarr[24]['left'] = $fahuoarr[1] == '海运'?300:570;//300、570、835
            $fontarr[24]['top'] = '1590';
        }

        //目的港
        // $fontarr[18]['nickName'] = '√';
        // $fontarr[18]['left'] = '1990';//1615、1760、1990
        // $fontarr[18]['top'] = '1590';

        //业务签名
        // $fontarr[19]['nickName'] = '啊啊啊啊19';
        // $fontarr[19]['left'] = '1220';
        // $fontarr[19]['top'] = '3160';

        //制单
        $fontarr[20]['nickName'] = '';
        $fontarr[20]['left'] = '2030';
        $fontarr[20]['top'] = '3180';

        //审核
        $fontarr[21]['nickName'] = '';
        $fontarr[21]['left'] = '2030';
        $fontarr[21]['top'] = '3350';

        //订单
        $fontarr[22]['nickName'] = $id;
        $fontarr[22]['left'] = '2040';
        $fontarr[22]['top'] = '160';
        // return $pngName;
        // header('Content-Type: image/jpeg');
        // imagepng($pic);
                
        $qianshoudan = hebing($imgpath , $fontarr , $font_file ,$font_size , $circleSize ,$res);
        $addtime = time();
        $jincangdanhao = $id;
        $img = $qianshoudan;
        $sql = get_cname('yasa_ckqianshou');
        eval("\$sql=\"$sql\";");
        $db->query($sql);

        $res1 = $db->query("update yasa_warehouse set is_finish = ".$finish." where is_del = 1 and jincangdanhao = ".$id);
        $k['code'] = 1;
        $k['msg'] = '成功';
        echo '<script>alert("成功"); window.location.href="receipt.php";</script>';
    }else{
        $k['code'] = 0;
        $k['msg'] = '失败';
        echo '<script>alert("失败"); window.location.href="receipt.php";</script>';
    }

	
	
	
 ?>