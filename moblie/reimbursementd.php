<?php     
	header("Content-Type: text/html;charset=utf-8");
  	include_once("../include/common.ini.php");
  	include_once("error.inc.php");
  	include_once("include/common.php"); 

  	$title = "申请详情";

  	$shenpi = $db->get_one("SELECT s.* FROM yasa_shenpi as s
     WHERE s.id = $id");
  	$contents = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);
  	// print_r($contents);die;
  	$shenhei = json_decode($shenpi[spcontent],JSON_UNESCAPED_UNICODE);
  	$shenpiimg = unserialize($shenpi['img']);
  	// print_r($shenhei['ApprovalInfo']['SpRecord']);die;
	//获取token 有效期2小时
	$corpid = 'ww36bc4d6ec9858223';//企业微信号
	$secret = 'gDbD1b8zqdeap_c3kQrkMOwhgx47GdhGswRi_p4fzo0';
	$template_id = '3TmB9ZVQsdQ2A8Ho3kPW4RtGHY2j3d4uLFUAAUxW';//模板id

	$res = getToken($corpid,$secret);

	// $approvalcontent = getApproval($res[access_token],$template_id);
	// $template_content = $approvalcontent->template_content->controls;

	$modulesql = "select * from yasa_module where template_id = '$template_id'";
	$module = $db->get_one($modulesql);
	$guize = unserialize($module[content]);
	// print_r($shenpiimg);die;
	// $order = $db->get_one("select * from yasa_order where id = ".$shenpi['oid']);
	// $qianshoudan = $db->query("SELECT * from yasa_warehouse where orderid = '$shenpi[order_id]' and is_del = 1");
	 $statusarr = array(
      '1' => '审核中',
      '2' => '已通过',
      '3' => '已驳回',
      '4' => '已撤销',
      '6' => '通过后撤销',
      '7' => '已删除',
      '9' => '支付复核',
      '10' => '已支付',
      '11' => '待入账',
      '12' => '已入账'
    );
?>

<!DOCTYPE html>
<html lang="en">
 	<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
      <title>付款申请</title>
      <link rel="stylesheet" href="fonts/iconfont.css"/>
      <link rel="stylesheet" href="css/font.css"/>
      <link rel="stylesheet" href="css/weui.min.css"/>
      <link rel="stylesheet" href="css/jquery-weui.min.css"/>
      <link rel="stylesheet" href="css/mui.css"/>
      <link rel="stylesheet" href="css/pages/catemaintm.css"/>
      <script src="js/jquery-1.8.3.min.js"></script>
      
      <script>(function (doc, win) {
		var docEl = doc.documentElement,
		resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
		recalc = function () {
		var clientWidth = docEl.clientWidth;
		if (!clientWidth) return;
			docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
		};

		if (!doc.addEventListener) return;
			win.addEventListener(resizeEvt, recalc, false);
			doc.addEventListener('DOMContentLoaded', recalc, false);
		})(document, window);
		</script>
 	</head>
  	<body>
	  	<?php include_once("include/header.php"); ?>
	  	<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		    }
			.content{
				padding: 10px; 
				border-bottom: 1px solid #ddd;
				margin-bottom: 50px;
			}
			.content p {
				margin-top: 20px;
			}
			.sppeople{
				margin-bottom: 100px;
			}
			table tr td{
				width: 33%;
			}
			.zz{
		      width: 100%;
		      height: 100%;
		      position: fixed;
		      top: 0px;
		      left: 0px;
		      background: rgba(0,0,0,0.5);
		      z-index: 9999999;
		      display: none;
		      align-items: center
		    }
		    .small{
		      z-index: 1000;
		    }
		    .big{
		        align-items: center
        	}
		</style>
	  	<div class="search">
	       <!--  <form action="payapply.php" style="position: relative;">
	          	<input type="input" name="search" class="searchbutton"> 
	          	<input type="submit" value="搜索" style="position: absolute;right: 0px;top:0px;">
	        </form> -->
	    </div>
	    <div class="shaixuan">
	    	订单查询>报销申请
	    </div>
	    <div class="content">
			<!-- <p>订单编号：<?=$shenpi['order_id']; ?></p>
			<p>客户编号：<?=$shenpi['kehu_number']; ?></p> -->
			<? foreach ($contents as $key => $value) { ?> 
				<p><?=$guize[$key][order]?>：<?=$value; ?></p>
			<? } ?>

			<p>附件</p>
		    <?
				foreach ($shenpiimg as $key => $value) { ?>
					<img src="/yy_app/<?=$value; ?>" alt="" width="50" height="50"  class="small">
				<? }
			?>
	    </div>
		<div class="sppeople">
			<table style="width: 100%;">
				<tr>
					<td width="33%">审批人</td>
					<td width="33%">状态</td>
					<td width="33%">时间</td>
				</tr>
				<? foreach ($shenhei['ApprovalInfo']['SpRecord'] as $key => $value) { ?>
					<tr>
						<td width="33%"><?=$value['Details']['Approver']['UserId']; ?></td>
						<td width="33%"><?=$statusarr[$value['Details']['SpStatus']]; ?></td>
						<td width="33%"><?=$value['Details']['SpTime']?date("Y-m-d H:i:s",$value['Details']['SpTime']):''; ?></td>
					</tr>
				<? } ?>
			</table>

		</div>
		<div class="zz">
	      <img src="" alt="" class="big" style="width: 100%;">
	    </div>
	    <div style="height: 100px;width: 100%;"></div>
		<script>
			 $(document).on("click",".small",function(){
		      $('.zz').css('display','flex');
		      var src = $(this).attr('src');
		      $('.big').attr('src',src);
		      $(".zz").show();
		    })
		    $('.zz').click(function(){
		      $('.zz').hide();
		    })
		</script>
    	<?php include_once("include/footer.php"); ?>
	</body>
</html>
