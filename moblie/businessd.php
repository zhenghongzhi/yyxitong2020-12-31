<?php     
  include_once("../include/common.ini.php");
  include_once("error.inc.php");
  include_once("include/common.php");
  $title = "商家查询";

  // $url = 'http://pushapi.guangzhouyueyang.com/v1/member/list?pageSize=1&pageNo=1';
  // $shangjia = zhzcurl($url,$data);
  

?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	    <title>付款申请</title>
	    <link rel="stylesheet" href="fonts/iconfont.css"/>
	    <link rel="stylesheet" href="css/font.css"/>
	    <link rel="stylesheet" href="css/weui.min.css"/>
	    <link rel="stylesheet" href="css/jquery-weui.min.css"/>
	    <link rel="stylesheet" href="css/mui.css"/>
	    <link rel="stylesheet" href="css/pages/catemaintm.css"/>
	    <script src="js/jquery-1.8.3.min.js"></script>

	    <script>(function (doc, win) {
	        var docEl = doc.documentElement,
	          resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
	          recalc = function () {
	            var clientWidth = docEl.clientWidth;
	            if (!clientWidth) return;
	            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
	          };

	        if (!doc.addEventListener) return;
	        win.addEventListener(resizeEvt, recalc, false);
	        doc.addEventListener('DOMContentLoaded', recalc, false);
	      })(document, window);
	    </script>
  	</head>
  	<body>
  		<?php include_once("include/header.php"); ?>
  		<style>
		    .clear{
		      	clear: both;
		    }
		    .searchbutton{
		      	width: 90%;
		      	border-radius: 10px;
		    }
		    .search {
		        padding-top: 44px;
		    }
		    .search input{
		      	height: 30px;
		    }
		    .shaixuan{
	      		padding: 10px;
		      	background-color:rgba(102, 102, 102, 0.06);
		      	margin-top: 44px;
		    }
			.content{
				padding: 10px; 
			}

			.content p{
				margin-top: 20px;
			}
		</style>
  		
	    <div class="shaixuan">
	    	商家信息
	    </div>
	    <div class="content">
			<!-- <p>商家名称：欣欣服饰</p>
			<p>行业：   服装</p>
			<p>会员号： CH-00001</p>
			<p>账号：   62284800000000000</p>
			<p>户名：   马马马</p>
			<p>手机号码：   132222222222</p> -->
	    </div>
    	<?php include_once("include/footer.php"); ?>
	    
  	</body>
  	<script>
  		function post(){
  			var value = '<?=$id; ?>';

			var str = [{"field":"memberNo","Type":"string","op":"$like","value":[value]}];
			var jsarr=JSON.stringify(str);

		    var xhr=new XMLHttpRequest()
		    xhr.open('post','http://pushapi.guangzhouyueyang.com/v1/member/list?pageSize=1&pageNo=1')
		  	// xhr.setRequestHeader();
		    xhr.setRequestHeader('Accept','application/json,text/plain, */*')
		    xhr.setRequestHeader('Content-Type','application/json;charset=UTF-8')
		    xhr.setRequestHeader('X-Token','59136090-d702-11ea-b1e0-ff9e8fcdb84f')
		    
		    xhr.send(jsarr)
		    xhr.onreadystatechange=function(){
		        if (xhr.readyState==4){
		        	var res = JSON.parse(xhr.response)
		        	var arr = res.data.rows;
		        	var str ='';
					str += "<p>商家名称："+arr[0]['companyName']+"</p>";
					str += "<p>行业："+arr[0]['label']['name']+"</p>";
					str += "<p>会员号："+arr[0]['memberNo']+"</p>";
					str += "<p>账号："+arr[0]['bankNo']+"</p>";
					str += "<p>户名："+arr[0]['openName']+"</p>";
					str += "<p>手机号码："+arr[0]['mobile']+"</p>";
					$('.content').html(str);
		        }
		    }
		}
		post();
  	</script>
</html>
