<?php
session_cache_limiter('private, must-revalidate');
include_once("../../include/common.ini.php");
include_once("common_function.php");
header("Content-type:text/html;charset=utf-8");

$uid =$_POST[uid];
$latitude =$_POST[latitude];
$longitude = $_POST[longitude];
$limit = $_POST[limit];
$publish_type= $_POST[publish_type];
$city= $_POST[city];
$province= $_POST[province];
if($city){ 	
    $city = str_replace(array("市"),"",$city);
	$city =" and A.city = '$city' ";
}
if($province){ 
$province = str_replace(array("省"),"",$province);
$province =" and A.province = '$province' ";
}
$how_far = "150";
$where = "where A.is_delete !=2 and A.publish_type='$publish_type' $province $city ";
$cursor =  $_POST['cursor']?" and A.id<".$_POST['cursor']:"";   
$limit =  $_POST['limit']? $_POST['limit']:10; 

if(empty($uid)||empty($latitude)||empty($longitude )||empty($publish_type))
{$result = array("result"=>"fail");
$result  = urldecode(json_encode($result));	
print $result; exit;}

   

$table=' yasa_publish_news ';
$table_user=' yasa_user ';
$table_group=' yasa_group_list ';
$table_comment=' yasa_publish_news_comment ';
$table_zan_favour=' yasa_pulish_zan_favour ';

/*
SORT_REGULAR - 默认。将每一项按常规顺序排列。
SORT_NUMERIC - 将每一项按数字顺序排列。
SORT_STRING - 将每一项按字母顺序排列
*/
function my_sort($arrays,$sort_key,$sort_order=SORT_ASC,$sort_type=SORT_NUMERIC ){   
        if(is_array($arrays)){   
            foreach ($arrays as $array){   
                if(is_array($array)){   
                    $key_arrays[] = $array[$sort_key];   
                }else{   
                    return false;   
                }   
            }   
        }else{   
            return false;   
        }  
        array_multisort($key_arrays,$sort_order,$sort_type,$arrays);   
        return $arrays;   
} 
	


define(EARTH_RADIUS, 6371);//地球半径，平均半径为6371km

////$lat 已知点的纬度
//$dlng =  2 * asin(sin($distance / (2 * EARTH_RADIUS)) / cos(deg2rad($lat)));
//$dlng = rad2deg($dlng);//转换弧度
//
//$dlat = $distance/EARTH_RADIUS;//EARTH_RADIUS地球半径
//$dlat = rad2deg($dlat);//转换弧度


 /**
 *计算某个经纬度的周围某段距离的正方形的四个点
 *
 *@param lng float 经度
 *@param lat float 纬度
 *@param distance float 该点所在圆的半径，该圆与此正方形内切，默认值为0.5千米
 *@return array 正方形的四个点的经纬度坐标
 */
 function returnSquarePoint($lng, $lat,$distance = 0.5){
 
    $dlng =  2 * asin(sin($distance / (2 * EARTH_RADIUS)) / cos(deg2rad($lat)));
    $dlng = rad2deg($dlng);
     
    $dlat = $distance/EARTH_RADIUS;
    $dlat = rad2deg($dlat);
     
    return array(
                'left-top'=>array('lat'=>$lat + $dlat,'lng'=>$lng-$dlng),
                'right-top'=>array('lat'=>$lat + $dlat, 'lng'=>$lng + $dlng),
                'left-bottom'=>array('lat'=>$lat - $dlat, 'lng'=>$lng - $dlng),
                'right-bottom'=>array('lat'=>$lat - $dlat, 'lng'=>$lng + $dlng)
                );
 }

/**
*求两个已知经纬度之间的距离,单位为米
*@param lng1,lng2 经度
*@param lat1,lat2 纬度
*@return float 距离，单位米
*@author www.Alixixi.com
**/
function getdistance($lng1,$lat1,$lng2,$lat2){
	//将角度转为狐度
	$radLat1=deg2rad($lat1);//deg2rad()函数将角度转换为弧度
	$radLat2=deg2rad($lat2);
	$radLng1=deg2rad($lng1);
	$radLng2=deg2rad($lng2);
	$a=$radLat1-$radLat2;
	$b=$radLng1-$radLng2;
	$s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;
	return $s;
}
/** 
* @desc 根据两点间的经纬度计算距离 
* @param float $lat 纬度值 
* @param float $lng 经度值 
*/
function getdistance2($lat1, $lng1, $lat2, $lng2) 
{ 
$earthRadius = 6367000; //approximate radius of earth in meters 
 
/* 
Convert these degrees to radians 
to work with the formula 
*/
 
$lat1 = ($lat1 * pi() ) / 180; 
$lng1 = ($lng1 * pi() ) / 180; 
 
$lat2 = ($lat2 * pi() ) / 180; 
$lng2 = ($lng2 * pi() ) / 180; 
 
/* 
Using the 
Haversine formula 
 
http://en.wikipedia.org/wiki/Haversine_formula 
 
calculate the distance 
*/
 
$calcLongitude = $lng2 - $lng1; 
$calcLatitude = $lat2 - $lat1; 
$stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2); 
$stepTwo = 2 * asin(min(1, sqrt($stepOne))); 
$calculatedDistance = $earthRadius * $stepTwo/1000; 
 
return round($calculatedDistance,2)."km"; 
} 
//$info_sql = "select * from  $table where lat<>0 and lat>{$squares['right-bottom']['lat']} and lat<{$squares['left-top']['lat']} and lng>{$squares['left-top']['lng']} and lng<{$squares['right-bottom']['lng']} "; 



//真确使用此函数计算得到结果后，带入sql查询。 longitude  latitude
$squares = returnSquarePoint($longitude, $latitude,$how_far);
//var_dump($squares);
//(2 * 6378.137* ASIN(SQRT(POW(SIN(PI()*($latitude -A.latitude)/360),2)+COS(PI()*$longitude/180)* COS(A.latitude * PI()/180)*POW(SIN(PI()*($longitude -A.longitude)/360),2)))) as juli 
//order by (A.latitude-$latitude)*(A.latitude-$latitude)+(A.longitude-$longitude)*(A.longitude-$longitude)) desc 
$info_sql = "
 select A.*,
 B.logo as g_logo,B.group_name as g_name,B.signature as g_signature,
 C.logo as u_logo,C.nickname as u_name ,C.signature as u_signature,
 D.favour as favour,D.zan as zan
 from  $table A 
 left join $table_group B on A.group_id = B.group_id 
 left join $table_user  C on A.uid = C.uid 
 left join $table_zan_favour D on  A.publish_id = D.publish_id  and  D.uid ='$uid'
 $where and A.latitude<>0 and A.latitude>{$squares['right-bottom']['lat']} and A.latitude<{$squares['left-top']['lat']} and A.longitude>{$squares['left-top']['lng']} and A.longitude<{$squares['right-bottom']['lng']}  $cursor 
 order by 
 A.id
 desc limit $limit "; 
//echo $info_sql;
$query=$db->query($info_sql);
$contacts = array();
$count2 = 0;
while($group = $db->fetch_array($query)){
	
	if($group[u_name]){$name =$group[u_name];$logo =$group[u_logo];$signature = $group[g_signature]; }
	if($group[g_name]){$name =$group[g_name];$logo =$group[g_logo]; $signature = $group[u_signature];}
	$juli =getdistance2($longitude,$latitude,$group["longitude"],$group["latitude"]);

//计算评论数
$publish_id =$group["publish_id"];
$comment_count =array();
$comment_sqlall="select count(id) from $table_comment where is_delete !=2 and publish_id =$publish_id   ";
$comment_result_all=mysql_query($comment_sqlall);
$comment_count=list($num) = @mysql_fetch_row($comment_result_all);

//计算点赞数
$zan_count =array();
$zan_sqlall="select count(id) from $table_zan_favour where publish_id ='$publish_id' and zan ='1' ";
$zan_result_all=mysql_query($zan_sqlall);
$zan_count=list($num) = @mysql_fetch_row($zan_result_all);
//计算收藏数

//$favour_count =array();
//$favour_sqlall="select count(id) from $table_zan_favour where publish_id ='$publish_id' and favour ='1'  ";
//$favour_result_all=mysql_query($favour_sqlall);
//$favour_count=list($num) = @mysql_fetch_row($favour_result_all);
	$cursor=$group["id"];
	if($group["zan"] !=1){$group["zan"]=2;}
	if($group["favour"] !=1){$group["favour"]=2;}
	$contacts[$count2] = array (
							  'owner'=>urlencode($group["uid"]),
							  'publish_id'=>urlencode($group[publish_id]),			
							  'author' =>urlencode($name),
							  'logo' =>urlencode($logo),
							  'signature'=>urlencode($signature),
							//  'is_recommend' =>urlencode($group["is_recommend"]),
							   'images' =>urlencode($group["images"]),
							   'images_original' =>urlencode($group["images_original"]),
							  'subject' =>urlencode($group["subject"]),
							  'content' =>urlencode(pack("H*",$group["content"])),
							  'view_count' =>urlencode($group["view"]),
							  'pinglun_count'=>urlencode($comment_count[0]),
							  'zan_count' =>urlencode($zan_count[0]),
							//  'favour_count' =>urlencode($favour_count[0]),
							  'zan' =>urlencode($group["zan"]),
							  'favour' =>urlencode($group["favour"]),
							//  'start_time' =>urlencode($group["start_time"]),
							//  'end_time' =>urlencode($group["end_time"]),
							//  'phone' =>urlencode($group["phone"]),
							//  'address' =>urlencode($group["address"]),
							  'addtime' =>urlencode($group["addtime"]),
							 //  'juli_mysql' =>urlencode($group["juli"]),
							 
							 
						
							  'start_time' =>urlencode($group["start_time"]),
							  'end_time' =>urlencode($group["end_time"]),
							  'phone' =>urlencode($group["phone"]),
							  'longitude' =>urlencode($group["longitude"]),
							  'latitude' =>urlencode($group["latitude"]),
							  'company' =>urlencode($group["company"]),
							   'address' =>urlencode($group["address"]),
							   'start_time' =>urlencode(date("Y.m.d H:i",$group["start_time"])),
							  'end_time' =>urlencode(date("Y.m.d H:i",$group["end_time"])),
							  'company' =>urlencode($group["company"]),
							  'contact_name' =>urlencode($group["contact_name"]),
							  'type' =>urlencode($group["type"]),
							  'money' =>urlencode($group["money"]),
							 
							  'yingye_time' =>urlencode($group["start_time"]."-".$group["end_time"]),
							 
							 
							 
							   'juli'=>urlencode($juli)
									  
                  );							
	  $count2++;
}

if($count2>0){
 $contacts = my_sort($contacts,'juli',SORT_ASC,SORT_NUMERIC  );
 $result = array("result"=>"success","count"=>$count2,"cursor"=>$cursor,"lists"=>$contacts);
 $result  = urldecode(json_encode($result));	
}else{						    					   					   						    
  $result = array("result"=>"success","count"=>"0");
  $result  = urldecode(json_encode($result));	
}
  print $result;