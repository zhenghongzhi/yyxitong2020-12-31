<?php
session_cache_limiter('private, must-revalidate');
include_once("../../include/common.ini.php");

/**
 * 获取指定月份的第一天开始和最后一天结束的时间戳
 *
 * @param int $y 年份 $m 月份
 * @return array(本月开始时间，本月结束时间)
 */
function mFristAndLast($y="",$m=""){
 if($y=="") $y=date("Y");
 if($m=="") $m=date("m");
 $m=sprintf("%02d",intval($m));
 $y=str_pad(intval($y),4,"0",STR_PAD_RIGHT);
 
 $m>12||$m<1?$m=1:$m=$m;
 $firstday=strtotime($y.$m."01000000");
 $firstdaystr=date("Y-m-01",$firstday);
 $lastday = strtotime(date('Y-m-d 23:59:59', strtotime("$firstdaystr +1 month -1 day")));
 return array("firstday"=>$firstday,"lastday"=>$lastday);
}



function genOrderID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM basketball_order_temp";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);

    return "OD$result";
}

function genUserID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_user";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);

    return "u$result";
}

function genGroupID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_group_list";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "g$result";
}
function genJoinID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM basketball_join_merber";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "J$result";
}
function genScoreID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM basketball_join_score";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "Sc$result";
}
function genPublishYanhuoID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_product_yanhuo";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "yh$result";
}
function genPublishID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_publish_news";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "p$result";
}
function genApplyID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM basketball_apply_list";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "ap$result";
}
function genCommentID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_publish_news_comment";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "c$result";
}
function genChatroomID(){
	global $db;
	$querystr = "SELECT max(id) as id FROM yasa_chatroom";   	

	$query=$db->query($querystr);
	$res= $db->fetch_array($query);


    $maxid=(int)$res["id"];     
    $result = $maxid*10000000+rand(1000000, 9999999);
	
    return "c$result";
}
