<?php
session_cache_limiter('private, must-revalidate');
include_once("../../include/common.ini.php");

header("Content-type:text/html;charset=utf-8");

if(empty($_POST)){print "ERROR"; exit;}

// 表单信息处理
if(get_magic_quotes_gpc()){
	$subject = htmlspecialchars(trim($_POST['subject']));
	$tel = htmlspecialchars(trim($_POST['tel']));
    $towho = htmlspecialchars(trim($_POST['towho']));
	$size = htmlspecialchars(trim($_POST['size']));
	$user_id = htmlspecialchars(trim($_POST['user_id']));
	$kehu_tel = htmlspecialchars(trim($_POST['kehu_tel']));
	$kehu_name = htmlspecialchars(trim($_POST['kehu_name']));
	$kehu_address = htmlspecialchars(trim($_POST['kehu_address']));
	$size = htmlspecialchars(trim($_POST['size']));
	$sun = htmlspecialchars(trim($_POST['sun']));
	$sx1 = htmlspecialchars(trim($_POST['sx1']));
	$sx2 = htmlspecialchars(trim($_POST['sx2']));
	$sx3 = htmlspecialchars(trim($_POST['sx3']));
	$sx4 = htmlspecialchars(trim($_POST['sx4']));
	$sx5 = htmlspecialchars(trim($_POST['sx5']));
	$sx6 = htmlspecialchars(trim($_POST['sx6']));
	$sx7 = htmlspecialchars(trim($_POST['sx7']));
	$sx8 = htmlspecialchars(trim($_POST['sx8']));
	$sx9 = htmlspecialchars(trim($_POST['sx9']));
	$sx10 = htmlspecialchars(trim($_POST['sx10']));
	$sx11 = htmlspecialchars(trim($_POST['sx11']));
	$sx12 = htmlspecialchars(trim($_POST['sx12']));
	$sx13 = htmlspecialchars(trim($_POST['sx13']));
	$sx14 = htmlspecialchars(trim($_POST['sx14']));
	$content =trim("分光器数目：".$sun."<br/>规模:".$size."<br/>OLT:".$sx1."<br/>主干配线光缆:".$sx2."<br/>接续:".$sx3."<br/>一级分光器之前的光缆:".$sx4."<br/>一级分光器:".$sx5."<br/>一级分光器箱体:".$sx6."<br/>一级之后的光缆:".$sx7."<br/>皮线光缆及室内信息插座:".$sx8."<br/>一二级之间的光缆：".$sx9."<br/>二级分光器:".$sx10."<br/>二级分光器箱体:".$sx11."<br/>楼内交接箱:".$sx12."<br/>垂直光缆:".$sx13."<br/>配线交接箱:".$sx14 );
		$content_for_app = htmlspecialchars(trim("@@*@@分光器数目：".$sun."@@*@@规模:".$size."@@*@@OLT:".$sx1."@@*@@主干配线光缆:".$sx2."@@*@@接续:".$sx3."@@*@@一级分光器之前的光缆:".$sx4."@@*@@一级分光器:".$sx5."@@*@@一级分光器箱体:".$sx6."@@*@@一级之后的光缆:".$sx7."@@*@@皮线光缆及室内信息插座:".$sx8."@@*@@一二级之间的光缆：".$sx9."@@*@@二级分光器:".$sx10."@@*@@二级分光器箱体:".$sx11."@@*@@楼内交接箱:".$sx12."@@*@@垂直光缆:".$sx13."@@*@@配线交接箱:".$sx14 ));
} else {
	$subject = addslashes(htmlspecialchars(trim($_POST['subject'])));
	$tel = addslashes(htmlspecialchars(trim($_POST['tel'])));
    $towho = addslashes(htmlspecialchars(trim($_POST['towho'])));
	$size = addslashes(htmlspecialchars(trim($_POST['size'])));
	$user_id = addslashes(htmlspecialchars(trim($_POST['user_id'])));
	$kehu_tel = addslashes(htmlspecialchars(trim($_POST['kehu_tel'])));
	$kehu_name = addslashes(htmlspecialchars(trim($_POST['kehu_name'])));
	$kehu_address = addslashes(htmlspecialchars(trim($_POST['kehu_address'])));
	$size= addslashes(htmlspecialchars(trim($_POST['size'])));
	$sun = addslashes(htmlspecialchars(trim($_POST['sun'])));
	$sx1 = addslashes(htmlspecialchars(trim($_POST['sx1'])));
	$sx2 = addslashes(htmlspecialchars(trim($_POST['sx2'])));
	$sx3 = addslashes(htmlspecialchars(trim($_POST['sx3'])));
	$sx4 = addslashes(htmlspecialchars(trim($_POST['sx4'])));
	$sx5 = addslashes(htmlspecialchars(trim($_POST['sx5'])));
	$sx6 = addslashes(htmlspecialchars(trim($_POST['sx6'])));
	$sx7 = addslashes(htmlspecialchars(trim($_POST['sx7'])));
	$sx8 = addslashes(htmlspecialchars(trim($_POST['sx8'])));
	$sx9 = addslashes(htmlspecialchars(trim($_POST['sx9'])));
	$sx10 =addslashes(htmlspecialchars(trim($_POST['sx10'])));
	$sx11 =addslashes(htmlspecialchars(trim($_POST['sx11'])));
	$sx12 =addslashes(htmlspecialchars(trim($_POST['sx12'])));
	$sx13 =addslashes(htmlspecialchars(trim($_POST['sx13'])));
	$sx14 =addslashes(htmlspecialchars(trim($_POST['sx14'])));
	
    $content =trim("分光器数目：".$sun."<br/>规模:".$size."<br/>OLT:".$sx1."<br/>主干配线光缆:".$sx2."<br/>接续:".$sx3."<br/>一级分光器之前的光缆:".$sx4."<br/>一级分光器:".$sx5."<br/>一级分光器箱体:".$sx6."<br/>一级之后的光缆:".$sx7."<br/>皮线光缆及室内信息插座:".$sx8."一二级之间的光缆：".$sx9."<br/>二级分光器:".$sx10."<br/>二级分光器箱体:".$sx11."<br/>楼内交接箱:".$sx12."<br/>垂直光缆:".$sx13."<br/>配线交接箱:".$sx14 );
	$content_for_app = addslashes(htmlspecialchars(trim("@@*@@分光器数目：".$sun."@@*@@规模:".$size."@@*@@OLT:".$sx1."@@*@@主干配线光缆:".$sx2."@@*@@接续:".$sx3."@@*@@一级分光器之前的光缆:".$sx4."@@*@@一级分光器:".$sx5."@@*@@一级分光器箱体:".$sx6."@@*@@一级之后的光缆:".$sx7."@@*@@皮线光缆及室内信息插座:".$sx8."一二级之间的光缆：".$sx9."@@*@@二级分光器:".$sx10."@@*@@二级分光器箱体:".$sx11."@@*@@楼内交接箱:".$sx12."@@*@@垂直光缆:".$sx13."@@*@@配线交接箱:".$sx14 )));
}


$addtime = date("Y-m-d H:i:s");
$sql = get_cname("cuiniao_case_guestbook");
eval("\$sql=\"$sql\";");
$db->query($sql);
echo "SUCESS";