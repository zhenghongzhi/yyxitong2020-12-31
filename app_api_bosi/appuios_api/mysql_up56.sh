#!/bin/bash
IN_DIR="/www/wdlinux"
MYS_VER="5.1.63"
if [ ! $1 ];then
        MYS_VER="5.6.22"
else
        MYS_VER=$1
fi

if [ ! -f mysql-${MYS_VER}.tar.gz ];then
        wget -c http://www.mysql.com/get/Downloads/MySQL-5.6/mysql-${MYS_VER}.tar.gz/from/http://cdn.mysql.com/
fi
wget -c http://www.cmake.org/files/v2.8/cmake-2.8.10.2.tar.gz
tar xzvf cmake-2.8.10.2.tar.gz
cd cmake-2.8.10.2
./configure
make&make install
cd ..
tar zxvf mysql-${MYS_VER}.tar.gz
cd mysql-${MYS_VER}
cmake -DCMAKE_INSTALL_PREFIX=$IN_DIR/mysql-$MYS_VER -DSYSCONFDIR=$IN_DIR/etc -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_SSL=bundled -DWITH_DEBUG=OFF -DWITH_EXTRA_CHARSETS=complex -DENABLED_PROFILING=ON -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_MEMORY_STORAGE_ENGINE=1 -DENABLE_DOWNLOADS=1
[ $? != 0 ] && echo "configure err" && exit
make
[ $? != 0 ] && echo "make err" && exit
make install
[ $? != 0 ] && echo "make install err" && exit
service mysqld stop
if [ ! -d /www/wdlinux/mysql_bk ];then
        mkdir -p /www/wdlinux/mysql_bk
        if [ -d /www/wdlinux/mysql/var ];then
                cp -pR /www/wdlinux/mysql/var/* /www/wdlinux/mysql_bk/
        else
                cp -pR /www/wdlinux/mysql/data/* /www/wdlinux/mysql_bk/
        fi
fi
rm -f /www/wdlinux/mysql
ln -sf $IN_DIR/mysql-$MYS_VER /www/wdlinux/mysql
sh scripts/mysql_install_db.sh --user=mysql --basedir=/www/wdlinux/mysql --datadir=/www/wdlinux/mysql/data
chown -R mysql.mysql /www/wdlinux/mysql/data
cp -pR /www/wdlinux/mysql_bk/* /www/wdlinux/mysql/data/
cp support-files/mysql.server /www/wdlinux/init.d/mysqld
chmod 755 /www/wdlinux/init.d/mysqld
service mysqld restart
echo
ln -sf /www/wdlinux/mysql/lib/libmysqlclient.so.* /usr/lib/
sleep 2
sh /www/wdlinux/tools/mysql_wdcp_chg.sh
echo
echo "mysql update is OK"