#!/bin/bash
# MYSQL 5.6 update scripts
# Author: Arefly
# Url: http://www.arefly.com

IN_DIR="/www/wdlinux"

if [ ! $1 ];then
	MYS_VER="5.6.15"
else
	MYS_VER=$1
fi

echo "THANK YOU FOR USING UPDATE SCRIPT MADE BY AREFLY.COM"
echo "YOU ARE GOING TO UPDATE YOUR MYSQL TO ${MYS_VER}"
echo "REMEMBER, YOUR PHP VERSION NEED TO BE 5.5 OR ABOVE!"
echo "YOU CAN JUST HAVE A REST"
echo "IT MAY TAKE A LOT OF TIME"
echo
#read -p "PRESS ENTER IF YOU REALLY WANT TO UPDATE"
read -p "DO YOU REALLY WANT TO UPDATE? (Y/N): " yn
if [ "$yn" == "Y" ] || [ "$yn" == "y" ]; then
	echo "MYSQL IS NOW UPDATING!"
else
	exit
fi
echo
echo "-------------------------------------------------------------"
echo


if [ ! -f mysql-${MYS_VER}.tar.gz ];then
	# old: wget -c http://dl.wdlinux.cn:5180/soft/mysql-${MYS_VER}.tar.gz
	wget -c http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-${MYS_VER}.tar.gz
fi
yum install -y cmake bison libmcrypt-devel libjpeg-devel libpng-devel freetype-devel curl-devel openssl-devel libxml2-devel zip unzip
tar zxvf mysql-${MYS_VER}.tar.gz
cd mysql-${MYS_VER}
echo "START CONFIGURING MYSQL"
sleep 3
make clean
cmake -DCMAKE_INSTALL_PREFIX=$IN_DIR/mysql-$MYS_VER -DSYSCONFDIR=$IN_DIR/etc -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_SSL=bundled -DWITH_DEBUG=OFF -DWITH_EXTRA_CHARSETS=complex -DENABLED_PROFILING=ON -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_MEMORY_STORAGE_ENGINE=1 -DENABLE_DOWNLOADS=1
[ $? != 0 ] && echo "NO! CONFIGURE ERROR! TRY AGAIN OR ASK IN THE BBS! :(" && exit
echo "START MAKE"
sleep 3
make
[ $? != 0 ] && echo "NO! MAKE ERROR! TRY AGAIN OR ASK IN THE BBS! :(" && exit
echo "START MAKE INSTALL"
sleep 3
make install
[ $? != 0 ] && echo "NO! MAKE INSTALL ERROR! TRY AGAIN OR ASK IN THE BBS! :(" && exit
service mysqld stop
if [ ! -d /www/wdlinux/mysql_bk ];then
mkdir -p /www/wdlinux/mysql_bk
cp -pR /www/wdlinux/mysql/data/* /www/wdlinux/mysql_bk
fi
rm -f /www/wdlinux/mysql
ln -sf $IN_DIR/mysql-$MYS_VER /www/wdlinux/mysql
sh scripts/mysql_install_db.sh --user=mysql --basedir=/www/wdlinux/mysql --datadir=/www/wdlinux/mysql/data
chown -R mysql.mysql /www/wdlinux/mysql/data
mv /www/wdlinux/mysql/data/mysql /www/wdlinux/mysql/data/mysqlo
cp -pR /www/wdlinux/mysql_bk/* /www/wdlinux/mysql/data/
cp support-files/mysql.server /www/wdlinux/init.d/mysqld
chmod 755 /www/wdlinux/init.d/mysqld
service mysqld restart
sh scripts/mysql_install_db.sh --user=mysql --basedir=/www/wdlinux/mysql --datadir=/www/wdlinux/mysql/data
echo
if [ -d /www/wdlinux/mysql-5.1.63 ];then
	ln -sf /www/wdlinux/mysql-5.1.63/lib/mysql/libmysqlclient.so.16* /usr/lib/
fi
cd ..
rm -rf mysql-${Ver}/
rm -rf mysql-${Ver}.tar.gz
sleep 2
sh /www/wdlinux/tools/mysql_wdcp_chg.sh
service mysqld restart
echo
echo "-------------------------------------------------------------"
echo "MYSQL UPDATE FINISH! :D"
echo "NOW YOUR MYSQL VERSION IS ${MYS_VER}!"
echo "UPDATE SCRIPT MADE BY AREFLY.COM"
echo "THANK YOU FOR USING!"
echo
echo "WDCP (C) COPYRIGHT"
echo
echo "PS: I THINK YOU NEED RESTART SERVER AFTER UPDATE."
echo "PS2: REMEMBER TO VISIT AREFLY.COM! :D"
echo
