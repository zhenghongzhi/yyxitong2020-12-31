<?php  
    include_once("config.php");
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

        <title>cupload例子</title>
    </head>
<body>
<style>
    #aaa img{
        width: 146px;
        height: 146px;
    }
</style>
<div id="aaa" contentEditable="true" style="height:146px; width:146px;border:1px solid #ccc"></div>
    <script src="../../moblie/js/jquery-1.8.3.min.js"></script>.
    <script src="../../moblie/js/layer/layer.js"></script>

<button onclick="upload()">开始上传</button>
<script>
// 处理粘贴事件
        $("#aaa").on('paste', function(eventObj) {
            // 处理粘贴事件
            var event = eventObj.originalEvent;
            var imageRe = new RegExp(/image\/.*/);
            var fileList = $.map(event.clipboardData.items, function (o) {
                if(!imageRe.test(o.type)){ return }
                var blob = o.getAsFile();
                return blob;
            });
            if(fileList.length <= 0){ return }
            // event.preventDefault();
        });
        function dataURLtoBlob(dataurl) {
                var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                while (n--) {
                    u8arr[n] = bstr.charCodeAt(n);
                }
                return new Blob([u8arr], {type: mime});
        }
        var id =<?=$_GET[id]; ?>;
        function upload() {
            
            var image = $('img').attr('src');
            console.log(image)
            var imgpath = dataURLtoBlob(image);
            var file = new FormData();

            file.append("id", id);
            file.append("type", 1);
            file.append("fileType", 1);
            file.append("file", imgpath);
            
            $.ajax({
                url: '<?=$urlzheng; ?>/v1/upload/uploadImg',
                type: 'POST',
                data: file,
                processData: false,
                contentType: false,
                headers: {
                    'X-Token': '59136090-d702-11ea-b1e0-ff9e8fcdb84f'
                },
                timeout: 10000,
                dataType: "json",
                success: function (data) {
                    console.log(data.message);
                    if (data.message == '上传完成') {
                        layer.msg(data.message,{time: 2000},function(){
                           parent.location.reload();
                        });
                    }else{
                        layer.msg(data.message,{time: 2000},function(){
                           
                        });
                    }
                    
                  
                },
                error: function () {

                }
            });

            
        }
</script>
</body>
</html>