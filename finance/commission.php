<?php 

	include_once("../include/common.ini.php");
	header('Content-Type: text/html;charset=utf-8');
	header('Access-Control-Allow-Origin:*'); // *代表允许任何网址请求
	header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); // 允许请求的类型

	header('Access-Control-Allow-Headers: Content-Type,Content-Length,Accept-Encoding,X-Requested-with, Origin ,X-TOKEN, Accept, Authorization'); // 设置允许自定义请求头的字段

	//获取token 有效期2小时
	$corpid = 'ww36bc4d6ec9858223';//企业微信号
	$secret = 'gDbD1b8zqdeap_c3kQrkMOwhgx47GdhGswRi_p4fzo0';
	$template_id = '3TkaR4s25WNoii4bpF19dmp3MRFihLD6m5HVaQ5h';//模板id
	// $template_id = 'Bs5NGctvECRA43i8dWEEWubcWvrAq379tTZ6vz3Vz';//模板id
	
	$res = getToken($corpid,$secret);

	$approvalcontent = getApproval($res[access_token],$template_id);
	$template_content = $approvalcontent->template_content->controls;

	$where = "1 and s.moudle = 'pay' and s.status not in(1,3,4) and entrystatus = 1";
	if ($spno) {
		$where .= " and s.spno like '%".$spno."%'";
	}
	if ($order) {
		$where .= " and o.order_id = '".$order."'";
	}
	if ($uid) {
		$where .= " and k.kehu_number = '".$uid."'";
	}
	if ($money) {
		$where .= " and s.money = '".$money."'";
	}
	if ($applicant) {
		$where .= " and u.username like '%".$applicant."%'";
	}

	if ($starttime) {
		$where .= " and s.addtime >= ".strtotime($starttime);
	}

	if ($endtime) {
		$where .= " and s.endtime <=".strtotime($endtime);
	}
	if ($memberno) {
		$where .= " and s.content like '%".$memberno."%'";
	}
	if ($iscommission == 1) {
		$where .= " and (s.isyongjin = 1 or s.isyongjin = 0)";
	}elseif ($iscommission == 2) {
		$where .= " and s.isyongjin = 2";
	}


	$shenpisql = $db->query("select s.content,u.username,s.spno,s.addtime,s.status,s.spcontent,s.fdzftime,s.isyongjin,s.fdremark from yasa_shenpi as s 
		left join yasa_order as o on o.id = s.oid 
		left join yasa_user as u on u.uid = o.pid3
		left join yasa_kehu as k on k.id = s.uid where $where");

	$modulesql = "select * from yasa_module where template_id = '$template_id'";
	$module = $db->get_one($modulesql);
	$guize = unserialize($module[content]);
	$mo = array(
		'支付类型' 	=> 'paytype',//1
		'订单编号' 	=> 'orderid',//1
		'客户编号' 	=> 'usernumber',//1
		'总金额'   	=> 'total', //1
		'返点'     	=> 'rebate',//1
		'商家会员号'=> 'businessnumber',//1
		'商家名称' 	=> 'businessname',//1
		// '账上余额' 	=> 'balanceol',//1
		// '在途资金' 	=> 'transitmoney',//1
		// '客户类型' 	=> 'customertype',//1
		// '客户已付' 	=> 'pay', //1
		// '余额'     	=> 'balance', //1 
		// '其他（±）'	=> 'other',//1
		'实际应付款'=> 'money',//1
		// '欠款金额'  => 'arrears',//1
		// '户名'     	=> 'businessuser',//1
		// '商家会员号'=> 'businessNo',//1
		// '跟单编号' 	=> 'documentaryNo', //1
		'佣金' 	    => 'commission', //1
		// '进仓金额' 	=> 'warehousmoney', //1
		// '货物是否收齐' 	=> 'finish', //1
		// '备注说明' 	=> 'remark', //1
		// '预计到仓日期' => 'warehousetime' //1
	);

	$i = 0;
	while ($shenpi = $db->fetch_array($shenpisql)) {
		$arr[$i]['content'] = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);
		$arr[$i]['spcontent'] = json_decode($shenpi[spcontent],JSON_UNESCAPED_UNICODE);
		$arr[$i]['fdremark'] = json_decode($shenpi[fdremark],JSON_UNESCAPED_UNICODE);
		
		$arr[$i]['username'] = $shenpi[username];
		if ($shenpi[isyongjin] == 1 || $shenpi[isyongjin] == 0) {
			$arr[$i]['iscommission'] = 1;
		}else{
			$arr[$i]['iscommission'] = 2;
		}
		
		$arr[$i]['spno'] = $shenpi[spno];
		$arr[$i]['addtime'] = date("Y-m-d" ,$shenpi[addtime]);
		$arr[$i]['fdzftime'] = $shenpi[fdzftime]?date("Y-m-d" ,$shenpi[fdzftime]):'';
		if ($shenpi['status'] == 1) {
			$shenpi = '审批中';
		}elseif ($shenpi['status'] == 2) {
			$shenpi = '已审批';
		}elseif ($shenpi['status'] == 3) {
			$shenpi = '已驳回';
		}elseif ($shenpi['status'] == 8) {
			$shenpi = '应付未付';
		}elseif ($shenpi['status'] == 9) {
			$shenpi = '支付复核';
		}elseif ($shenpi['status'] == 10) {
			$shenpi = '已支付';
		}
		$arr[$i]['status'] = $shenpi;
		$i++;
	}
	$z = 0;
	foreach ($arr as $key => $value) {
		// print_r($value);die;
		// echo $commission;
		if ($value['content']['Number-1578331494475']) {
			foreach ($value['content'] as $k => $val) {
				if ($mo[$guize[$k][order]]) {
					$m[$z][$mo[$guize[$k][order]]] = $val;
				}
			}
			$commission = floor(($value['content']['Number-1575249432100'] * $value['content']['Selector-1575249886295'])/100);
			$m[$z]['commissionmoney'] = $commission;
			$m[$z]['username'] = $value['username'];
			$m[$z]['spno'] = $value['spno'];
			$m[$z]['addtime'] = $value['addtime'];
			$m[$z]['status'] = $value['status'];
			$m[$z]['zftime'] = $value['fdzftime'];
			$m[$z]['iscommission'] = $value['iscommission'];
			$m[$z]['collection1'] = $value['fdremark']['collection1'];
			$m[$z]['collection2'] = $value['fdremark']['collection2'];
			$m[$z]['money1'] = $value['fdremark']['money1'];
			$m[$z]['money2'] = $value['fdremark']['money2'];
			$m[$z]['paytime'] = $value['fdremark']['paytime'];
			$m[$z]['remark'] = $value['fdremark']['remark'];
			$m[$z]['glspno'] = $value['fdremark']['spno'];
			$z++;
		}
	}

	$data['code'] = 20000;
	$data['data'] = $m;
	echo json_encode($data);
 ?>	