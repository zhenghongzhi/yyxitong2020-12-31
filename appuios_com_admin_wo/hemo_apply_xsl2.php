<?
session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");
include_once("checkuser.php");

$pagename = "申请表管理";

$ename = "内容";

$table = "cuiniao_apply";
$father_table = "cuiniao_apply_type";

$uses = "显示";

$nouses = "隐藏";

$hasisvalid = "1";
$hasorders = "1";



if($act == 'add'){
	$burl = " - 添加".$ename;
}elseif($act == 'edit'){
	$burl = " - 编辑".$ename;
}

if($keyword){
	$burl = " - 搜索结果";
}

$isfile=1; //是否有上传文件

$do_search="1";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - <?=$pagename?> </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>
<body>
<?
if(empty($act) || ($keyword && $act!='edit')){
?>
<h1>
<span class="action-span1"><a href="admin_index.php">管理中心</a>  - <?=$pagename?><?=$burl?> </span>
<div style="clear:both"></div>
</h1>
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<script src="js/utils.js"></script>
<div class="form-div">
  <form action="?act=download" name="searchForm" method="get" id="down_form">
  <input type="hidden" name="act" value="download" />
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
  
    类别：
    <select name="search_pid" id="search_pid">
      <option value="" <?=select(0,$npid)?>>请选择</option>
      <?
	  	$query1=$db->query("select * from $father_table where pid=0 ");
		while($q=$db->fetch_array($query1)){
		?>
      <option value="<?=$q['id']?>" <?=select($q['id'],$npid)?>>
        <?=$q['subject']?>
      </option>
      <?
		}
		?>
    </select>
  开始日期
<input class="laydate-icon" id="start" value="<?=$day?>" name="day_start" datatype='*' required="required" >
结束日期
<input class="laydate-icon" id="end" value="<?=$day?>" name="day_end" datatype='*' required="required">

    <input type="submit" value=" 导出 " class="button" />
  </form>
</div>
 
<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/Validform5.3.2/js/Validform_v5.3.2.js"></script>
<script src="laydate/laydate.js"></script>
<script>
$(function(){
	
	var demo=$("#down_form").Validform({
		tiptype:2,
		label:".label",
		showAllError:false
		
	});

});
laydate.skin('molv');//切换皮肤，请查看skins下面皮肤库

var start = {
    elem: '#start',
    format: 'YYYY/MM/DD hh:mm:ss',
    max: '2099-06-16 23:59:59', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
    }
};
var end = {
    elem: '#end',
    format: 'YYYY/MM/DD hh:mm:ss',
    min: laydate.now(),
    max: '2099-06-16 23:59:59',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);

</script>
<? include_once("foot.php");?>

<?
}elseif($act == 'download'){
/*echo strtotime($day_start);
echo "<hr>";
echo strtotime($day_end) ;
echo "<hr>";
echo 	$search_pid ;
echo "<hr>";
exit;
*/
?>

<div >
<?
header("content-type:application/vnd.ms-excel");
$date=mktime();
$tt="content-disposition:filename=".$date.".xls";
header($tt);?>
<table width="100%" >
<tr style="border:1px #CCC solid;">
<th width="200" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">公司名称</span></th>
<th width="400" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">工商注册地址</span></th>
<th width="273" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">公司行业</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">法人姓名</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">法人手机</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">法人邮箱</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目名称</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目所属领域</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目负责人</span></th>
<th width="200" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目负责人手机</span></th>
<th width="400" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目负责人邮箱</span></th>
<th width="273" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">知识产权情况</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目和产品现状</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">融资情况</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">投资人</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">投资金额（元）</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">团队成员情况</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">项目申请入孵的目的</span></th>
<th width="400" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">团队资源岗位个数</span></th>
<th width="273" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">团队资源床位个数</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">对电信资源的需求</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">信息来源</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">所属类别</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">审核情况</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">申请人邮箱</span></th>
<th width="278" bordercolor="#000000" bgcolor="#6699FF"><span class="STYLE2">提交申请时间</span></th>
</tr>

<?
$start=strtotime($day_start);
$end=strtotime($day_end) ;
if($search_pid){

	$where ="where $start < addtime and addtime<$end and pid=$search_pid order by id desc";
}else{
$where ="where $start < addtime and addtime<$end order by id desc";
}
$query=$db->query("select * from $table $where");
while($a=$db->fetch_array($query)){
?>
   <tr style="border:1px #666 solid;">
<td height="22" align="left" bordercolor="#000000"><span class="STYLE2">
  <?=$a['company']?>
</span> </td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[province]?>·<?=$a[city]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[hangye]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[fr_name]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
  <?=$a[fr_tel]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[fr_email]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[project_name]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[lingyu]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[project_username]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[project_tel]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[project_email]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[knowledge]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
 <?=$a[project_xianzhuang]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[touzi]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<? if($a[touzi_who]){echo $a[touzi_who]; }else{echo "暂无";}?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<? if($a[touzi_money]){echo $a[touzi_money]; }else{echo "暂无";}?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?
$html="";
if($a[team_json]){
$arr=json_decode($a[team_json]);
$html.="<table><tr><th>成员名称</th><th>职位</th><th>学历</th><th>户籍</th></tr>";
foreach($arr->team_json as $v){
$html.="<tr><td align=center >".$v->name." </td><td align=center>".$v->zhiwei." </td><td align=center>".$v->xueli." </td><td align=center>".$v->huji." </td></tr>";
}
$html.="</table>";
echo $html;
}else{
	echo "<table><tr><td></td><td></td><td></td><td></td></tr></table>";
	}
?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<? 
$aim  = explode('@@',$a[aim]);
$i=1;
$temp_html="";
foreach($aim as $v)
{
$temp_html.=$i.".".$v."<br/>";	
$i++;
}
echo $temp_html;
?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[team_need_gw]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[team_need_cw]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[team_need_telecom]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[info_comefrom]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?
$hxl_sql="select subject from $father_table  where id=".$a['pid'];
if($hxl_temp=$db->get_one($hxl_sql)){
echo $hxl_temp['subject'];
}else{
echo '无';
}
?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<? if($a[status]==0 or empty($a[status])){echo "<font color=red>待审核</font>";}
						elseif($a[status]==1){echo "<font color=green>通过</font>";}
						elseif($a[status]==2){echo "<font color=gray>未通过</font>";}
					?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=$a[loginuser_email]?>
</span></td>
<td align="center" bordercolor="#000000"><span class="STYLE2">
<?=date("Y-m-d",$a['addtime'])?> 
</span></td>
</tr>
 

<?
}
?>
  </table>

</div>


<?
}
?>

</body>
</html>