<?
include_once("../include/common.ini.php");
include_once("error.inc.php");

if($method == 'logout'){
	$_SESSION['cuiniao_Admin_Name']='';
	foreach($_SESSION as $key=>$v){
		unset($_SESSION[$key]);
	}
	header("location:appuios.php");
	exit;
}

if($_SESSION['cuiniao_Admin_Name']){
	header("Location:admin_index.php");
	exit;
}


if($method == 'CheckLogin'){

	if($verify==""){
		$Errmsg = $Errmsg."请输入附加码。";
		$FoundErr = true;
	}elseif($_SESSION['login_check_number']==""){
		$Errmsg = $Errmsg."请不要重复提交，如需重新登陆请返回登陆页面。";
		$FoundErr = true;
	}elseif($verify != $_SESSION['login_check_number']){
		$Errmsg = $Errmsg."你输入的附加码和系统产生的不符。";
		$FoundErr = true;
	}
	$_SESSION['login_check_number']="";

	if($username == "" || $password == ""){
		$Errmsg = $Errmsg."请输入登陆用户名或密码。";
		$FoundErr = true;
	}

	if($FoundErr){
		er($Errmsg,2);
		exit;
	}
	
	$onlineip = "";
	if(getenv('HTTP_CLIENT_IP')) {
		$onlineip = getenv('HTTP_CLIENT_IP');
	} elseif(getenv('HTTP_X_FORWARDED_FOR')) { 
		$onlineip = getenv('HTTP_X_FORWARDED_FOR');
	} elseif(getenv('REMOTE_ADDR')) { 
		$onlineip = getenv('REMOTE_ADDR');
	} else { 
		$onlineip = $HTTP_SERVER_VARS['REMOTE_ADDR'];
	}
	$IP = $onlineip;

	$Sql = "select * from cuiniao_admin where username='$username'";
	$Rs = $db->get_one($Sql);
	if($Rs['username']==''){
		$Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
		er($Errmsg,2);
		exit;
	}else{
		if($Rs["password"] != md5($password)){
			$Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
			er($Errmsg,2);
			exit;
		}else{	
			$_SESSION['cuiniao_Admin_Name'] = $username;
			$_SESSION['cuiniao_id'] = $Rs['id'];
			$_SESSION['user_test_rights']=$Rs['rights'];
			
			$db->query("update cuiniao_admin set LastLoginIP='$IP',logintime='".date("Y-m-d H:i:s")."' where username='$username'");
			header("Location:admin_index.php");
			exit;
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>---后台管理系统---</title>
<link href="./login_ui/css/global.css" rel="stylesheet" type="text/css" />
<link href="./login_ui/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="./login_ui/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./login_ui/js/jquery-ui.js"></script>

</head>

<body>
<style>
.top1000{ width:1000px; margin:0 auto;}
.top_l{ padding-top:6px;}
</style>
<div class="main-content">
<div class="main1000"> <img class="phone" alt="phone" src="./login_ui/images/phone.png" id="zll" style="top:120px;display: inline;">
  <p class="txt1"><img src="./login_ui/images/login_huanyin.png" width="251" height="109" /></p>
  <form name="admininfo" method="post" action="appuios.php">
    <div class="login">
      <h2>管理系统</h2>
      <p>
        <label>帐&nbsp;号</label>
        <input  name="username" id="idname"  type="text" class="index_k1" size="25"/>
      </p>
      <p>
        <label>密&nbsp;码</label>
        <input  name="password" id="password" type="password"  class="index_k1" size="25" onclick= "password1=this;showkeyboard();this.readOnly=1;Calc.password.value=''" />
      </p>
      <p >
        <label >验证码</label>
        <input type="text"  name="verify" id="verify"  maxlength=4 class="index_k1" size="10" style="background-color:transparent">
        <input type="hidden" value="<?=md5($_SESSION['login_check_number'])?>" name="validate1">
        <input type="hidden" value="CheckLogin" name="method">
      </p>
      <p>
        <label ></label>
        <img id="validates"  height="35" width="190" src="validate.php?act=init&id=1"> </p>
      <p class="pass-form-item">
        <input name="Submit" type="image" src="./login_ui/images/reg.jpg" border="0" onClick="check()" style="border:0; height:38px" />
      </p>
    </div>
  </form>
  <div class="yinying"><img src="./login_ui/images/login_yiyin.png" width="320" height="4" /></div>
  <div class="news" >
    <div class="gr"></div>
    <ul>
      <li style=" width:545px;">
        <div class="w545">
          <div class="inner"> <a href="javascript:void(0)" class="d1"></a> </div>
          <div class="products-desc">
            <h3 class="title">手机APP开发</h3>
            <div class="desc"> 企业APP是企业和客户沟通的最便捷、低成本、互动性最好的渠道。<br>
              方便快捷高效，同时互动性高，是把企业植入用户的最佳方法。<br>
              移动互联网最便捷的入口，拥有一款能在不同手机上运行的APP客户端是移动营销制胜的法宝！ </div>
          </div>
        </div>
      </li>
      <li>
        <div class="w545">
          <div class="inner"> <a href="javascript:void(0)" class="d2"></a> </div>
          <div class="products-desc">
            <h3 class="title">网站开发</h3>
            <div class="desc"> 网站是一个企业对外的窗口，是客户了解你公司的第一入口。<br>
              可以在很大程度上促进企业的销售，是所有网络营销手段引流的最终归宿。<br>
              如今互联网时代，网站在客户成交过程中的作用举足轻重。</div>
          </div>
        </div>
      </li>
      <li>
        <div class="w545">
          <div class="inner"> <a href="javascript:void(0)" class="d3"></a> </div>
          <div class="products-desc">
            <div class="w545">
              <h3 class="title">微信开发</h3>
              <div class="desc"> 微信营销是一个集微信公众接口、微信达人、微信资讯， <br>
                旨在通过方便实用的接口帮助微信营销用户管理提高个人和团队效率的管理产品。 <br>
                通过"微信导航"为微信玩家们更好的找到自己喜欢的微信。</div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="clear"></div>
  <div  class="copyright">技术支持:<a href="http://www.appuios.com" target="_blank">翠鸟网络</a></div>
</div>
<script>
$(function() {
	
    var $phone = $('.phone');
    var top = $phone.css('top');
    $phone.css({top: '-160px', display: 'inline'}).animate({top: top}, 2000, 'easeOutBounce') 
    $('.txt1').slideDown(2000);
	
	$(".d1").hover(
	 function(){
	   	 $(".d3").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d2").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d1").parent().parent().parent().animate({ width:'545px'},300)
	  }
	)
	$(".d2").hover(
	 function(){
	   	 $(".d1").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d3").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d2").parent().parent().parent().animate({ width:'545px'},300)
	  }
	)
	$(".d3").hover(
	 function(){
	   	 $(".d1").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d2").parent().parent().parent().animate({ width:'135px'},300)
		 $(".d3").parent().parent().parent().animate({ width:'545px'},300)
	  }
	)
	
});
 </script> 
</body>
</html> 
