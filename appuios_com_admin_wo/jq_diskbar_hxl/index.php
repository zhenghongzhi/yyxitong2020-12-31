﻿<?php
//在平常编程中有时需要获取磁盘空间使用情况，大部分情况都是使用disk_free_space和disk_total_space函数。
//下面实例就是获取服务器所有磁盘空间大小，实例如下：
/**
* 字节格式化 把字节数格式为B K M G T P E Z Y 描述的大小
* @param int $size 大小
* @param int $dec 显示类型
* @return int
*/
//禁用错误报告
error_reporting(0);

function byte_format($size,$dec=2)
{
$a = array("B", "KB", "MB", "GB", "TB", "PB","EB","ZB","YB");
$pos = 0;
while ($size >= 1024)
{
$size /= 1024;
$pos++;
}
return round($size,$dec)." ".$a[$pos];
}
/**
* 取得单个磁盘信息
* @param $letter
* @return array
*/
function get_disk_space($letter="")
{
//获取磁盘信息
$diskct = 0;
$disk = array();
/*if(@disk_total_space($key)!=NULL) *为防止影响服务器，不检查软驱
{
$diskct=1;
$disk["A"]=round((@disk_free_space($key)/(1024*1024*1024)),2)."G / ".round((@disk_total_space($key)/(1024*1024*1024)),2).'G';
}*/
$diskz = 0; //磁盘总容量
$diskk = 0; //磁盘剩余容量
$is_disk = $letter.':';
if(@disk_total_space($is_disk)!=NULL)
{
$diskct++;
$disk[$letter][0] = byte_format(@disk_free_space($is_disk));
$disk[$letter][1] = byte_format(@disk_total_space($is_disk));
$disk[$letter][2] = round(((@disk_free_space($is_disk)/(1024*1024*1024))/(@disk_total_space($is_disk)/(1024*1024*1024)))*100,2).'%';
$diskk+=byte_format(@disk_free_space($is_disk));
$diskz+=byte_format(@disk_total_space($is_disk));
}
return $disk; 
}
/**
* 取得磁盘使用情况
* @return var
*/
function get_spec_disk($type='system')
{
$disk = array();
switch ($type)
{
case 'system':
//strrev(array_pop(explode(':',strrev(getenv_info('SystemRoot')))));//取得系统盘符
$disk = get_disk_space(strrev(array_pop(explode(':',strrev(getenv('SystemRoot'))))));
break;
case 'all':
foreach (range('b','z') as $letter)
{
$disk = array_merge($disk,get_disk_space($letter));
}
break;
default:
$disk = get_disk_space($type);
break;
}
return $disk;
}


//获取空间使用情况数组
$grobal_a=get_spec_disk();
$a_whight="800";
$a_all=$grobal_a['C'][1];
$a_use=$grobal_a['C'][0];
$loading_bar=$grobal_a['C'][2];
//print_r ($grobal_a);
//array (size=1)
//  'C' => 
//    array (size=3)
//      0 => string '53.22 GB' (length=8)
//      1 => string '100.09 GB' (length=9)
//      2 => string '53.17%' (length=6)
//

?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="no-follow" />
<meta name="description" content="no-follow" />
<title>进度条</title>
<style>
        body {
            background:  #f0f0f0;
			width: 360px;
            
			font: 13px 'trebuchet MS',Arial,Helvetica;
        }
	
		h2, p {


  color: #555555;

  font-size: 15px;
  line-height: 1.3em;



			color: #555555;

		}
		
		/*---------------------------*/			
        
        .progress-bar {
            background-color:#3e4145;
            height: 25px;
            padding: 3px;
            width: <?php echo $a_whight."px";?>;
           		
            -moz-border-radius: 5px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
            -moz-box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
			-webkit-box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
			box-shadow: 0 1px 2px #000 inset, 0 1px 0  #828282;           
        }
        
        .progress-bar span {
            display: inline-block;
            height: 100%;
			background-color:#848484;
            -moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;
            -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
			-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
			box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
			-webkit-transition: width .4s ease-in-out;
			-moz-transition: width .4s ease-in-out;
			-ms-transition: width .4s ease-in-out;
			-o-transition: width .4s ease-in-out;
			transition: width .4s ease-in-out;		
        }
		
		/*---------------------------*/			
		
        .blue span {
            background-color: #34c2e3;   
        }

        .orange span {
			  background-color: #fecf23;
			  background-image: -webkit-gradient(linear, left top, left bottom, from(#fecf23), to(#fd9215));
			  background-image: -webkit-linear-gradient(top, #fecf23, #fd9215);
			  background-image: -moz-linear-gradient(top, #fecf23, #fd9215);
			  background-image: -ms-linear-gradient(top, #fecf23, #fd9215);
			  background-image: -o-linear-gradient(top, #fecf23, #fd9215);
			  background-image: linear-gradient(top, #fecf23, #fd9215);  
        }	

        .green span {
			  background-color: #a5df41;
			  background-image: -webkit-gradient(linear, left top, left bottom, from(#a5df41), to(#4ca916));
			  background-image: -webkit-linear-gradient(top, #a5df41, #4ca916);
			  background-image: -moz-linear-gradient(top, #a5df41, #4ca916);
			  background-image: -ms-linear-gradient(top, #a5df41, #4ca916);
			  background-image: -o-linear-gradient(top, #a5df41, #4ca916);
			  background-image: linear-gradient(top, #a5df41, #4ca916);  
        }		
		
		/*---------------------------*/		
		
		.stripes span {
            -webkit-background-size: 30px 30px;
            -moz-background-size: 30px 30px;
            background-size: 30px 30px;			
			background-image: -webkit-gradient(linear, left top, right bottom,
								color-stop(.25, rgba(255, 255, 255, .15)), color-stop(.25, transparent),
								color-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .15)),
								color-stop(.75, rgba(255, 255, 255, .15)), color-stop(.75, transparent),
								to(transparent));
            background-image: -webkit-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%,
                                transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%,
                                transparent 75%, transparent);
            background-image: -moz-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%,
                                transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%,
                                transparent 75%, transparent);
            background-image: -ms-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%,
                                transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%,
                                transparent 75%, transparent);
            background-image: -o-linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%,
                                transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%,
                                transparent 75%, transparent);
            background-image: linear-gradient(135deg, rgba(255, 255, 255, .15) 25%, transparent 25%,
                                transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%,
                                transparent 75%, transparent);            
            
            -webkit-animation: animate-stripes 3s linear infinite;
            -moz-animation: animate-stripes 3s linear infinite;       		
		}
        
        @-webkit-keyframes animate-stripes { 
			0% {background-position: 0 0;} 100% {background-position: 60px 0;}
        }
        
        
        @-moz-keyframes animate-stripes {
			0% {background-position: 0 0;} 100% {background-position: 60px 0;}
        }
		
		/*---------------------------*/	 

		.shine span {
			position: relative;
		}
		
		.shine span::after {
			content: '';
			opacity: 0;
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background: #fff;
            -moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;			
			
            -webkit-animation: animate-shine 2s ease-out infinite;
            -moz-animation: animate-shine 2s ease-out infinite; 			
		}

        @-webkit-keyframes animate-shine { 
			0% {opacity: 0; width: 0;}
			50% {opacity: .5;}
			100% {opacity: 0; width: 95%;}
        }
        
        
        @-moz-keyframes animate-shine {
			0% {opacity: 0; width: 0;}
			50% {opacity: .5;}
			100% {opacity: 0; width: 95%;}
        }

		/*---------------------------*/	 
		
		.glow span {
            -moz-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;
			-webkit-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;
			box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;
			
            -webkit-animation: animate-glow 1s ease-out infinite;
            -moz-animation: animate-glow 1s ease-out infinite; 			
		}

		@-webkit-keyframes animate-glow {
		 0% { -webkit-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;} 
		 50% { -webkit-box-shadow: 0 5px 5px rgba(255, 255, 255, .3) inset, 0 -5px 5px rgba(255, 255, 255, .3) inset;} 
		 100% { -webkit-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;}
		 }

		@-moz-keyframes animate-glow {
		 0% { -moz-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;} 
		 50% { -moz-box-shadow: 0 5px 5px rgba(255, 255, 255, .3) inset, 0 -5px 5px rgba(255, 255, 255, .3) inset;} 
		 100% { -moz-box-shadow: 0 5px 5px rgba(255, 255, 255, .7) inset, 0 -5px 5px rgba(255, 255, 255, .7) inset;}
		 }
    </style>
</head>

<body>


<h2>服务器空间:</h2>
<div style="float:left; "><div class="progress-bar green shine"  >
    <span style=" float:left;width: <?php echo $loading_bar;?>"></span>
	<div style="float:left; color:#FFF; font: 15px 'trebuchet MS',Arial,Helvetica;
  font-size: 15px;
  line-height: 1.3em;
"><?php 
	echo "&nbsp;".$loading_bar; ?>
   
</div>

</div> 

<div style="float:left;"><p><?php 
	echo "总空间为：".$a_all; 
	echo "|已使用：".$a_use;  ?>
    </p>
</div>


<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
	$(document).ready(function(){ 
		$('.quarter').click(function(){
			$(this).parent().prev().children('span').css('width','25%');
			});
		$('.half').click(function(){
			$(this).parent().prev().children('span').css('width','50%');
			});
		$('.three-quarters').click(function(){
			$(this).parent().prev().children('span').css('width','75%');
			});
		$('.full').click(function(){
			$(this).parent().prev().children('span').css('width','100%');
			});			
	});
</script>
</body>
</html>