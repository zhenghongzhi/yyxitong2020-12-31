<?php
define('ROOT','..'); 
require("PicThumb.class.php");
function mutil_make_thumbpic($source_temp , $cut_pic_type="fit"){
	$source = '../'.$source_temp;
	
	$tempArr =explode("/",$source_temp);
	$time_position = count($tempArr)-2;
	//var_dump($tempArr);
	$picurl_addtime = $tempArr[$time_position];
$logfile = 'PicThumb.log';
$dest1 = ROOT.'/cache/u10086/640x400/'.$picurl_addtime.'/'.basename($source);
$dest2 = ROOT.'/cache/u10086/320x200/'.$picurl_addtime.'/'.basename($source);
$dest3 = ROOT.'/cache/u10086/320x150/'.$picurl_addtime.'/'.basename($source);
$dest4 = ROOT.'/cache/u10086/120x80/'.$picurl_addtime.'/'.basename($source);
$dest5 = ROOT.'/cache/u10086/90x110/'.$picurl_addtime.'/'.basename($source);
$dest6 = ROOT.'/cache/u10086/200x200/'.$picurl_addtime.'/'.basename($source);



//图1
$param = array(
    'type' => $cut_pic_type,
    'width' => 640,
    'height' => 400,
    'bgcolor' => '#ffffff'
);// 按比例生成缩略图,不足部分用#FF0000填充
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest1);
//if($flag){
//  echo '<img src="'.$dest1.'">';
//}else{
//    echo 'create thumb fail';
//}


//图2
$param = array(
    'type' => $cut_pic_type,
    'width' => 320,
    'height' => 200,
    'bgcolor' => '#ffffff'
);
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest2);

//图3
$param = array(
    'type' => $cut_pic_type,
    'width' => 320,
    'height' => 150,
    'bgcolor' => '#ffffff'
);
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest3);

//图4
$param = array(
    'type' => $cut_pic_type,
    'width' => 120,
    'height' => 80,
    'bgcolor' => '#ffffff'
);// 按比例生成缩略图,不足部分用#FF0000填充
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest4);

//图5
$param = array(
    'type' => $cut_pic_type,
    'width' => 90,
    'height' => 110,
    'bgcolor' => '#ffffff'
);
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest5);

//图6
$param = array(
    'type' => $cut_pic_type,
    'width' => 60,
    'height' => 60,
    'bgcolor' => '#ffffff'
);// 按比例生成缩略图,不足部分用#FF0000填充
$obj = new PicThumb($logfile);
$obj->set_config($param);
$flag = $obj->create_thumb($source, $dest6);

}