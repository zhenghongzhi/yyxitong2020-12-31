<?
session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");
include_once("checkuser.php");

$pagename = "公司地址仓库类别管理";

$ename = "类别";

$table = "yasa_warehouse_type";
$father_table = "yasa_warehouse_type";


$uses = "显示";

$nouses = "隐藏";

$hasisvalid = "0";
$hasorders = "1";



if($act == 'add'){
	$burl = " - 添加".$ename;
}elseif($act == 'edit'){
	$burl = " - 编辑".$ename;
}

if($keyword){
	$burl = " - 搜索结果";
}

$isfile="1"; //是否有上传文件

$do_search="";

if($act == 'edits'){

	$arr[0]=array($subject,"标题",'s',255);
	if($hasorders){
		$arr[1]=array($orders,"排序号",'n',11);
	}
	
	$msg = upc($arr);

	$msg?er($msg,2):NULL;

	$upload_dir = "../upfiles/";
		$max_size = 5000;
		$file_name = "upload"; 
		if(!empty($_FILES[$file_name]['name'])){
			$picurl = "upfiles/".upload_file($file_name,$upload_dir,$max_size);
		}
		if($picurl){
			$picurl = " ,picurl='$picurl' ";
	}
	$sql ="update $table set 
	subject='$subject' , 
	type1='$type1',
	type2='$type2',
 	pid='$pid', 
 	orders='$orders'
	$picurl
	where id='$id'";
	
	$db->query($sql);
	
	
	$links[0]['text']="返回列表";
	$links[0]['href']="?page=$page&keyword=".urlencode($keyword);
	$links[1]['text']="查看编辑结果";
	$links[1]['href']="?act=edit&id=$id&page=$page&keyword=".urlencode($keyword);
	
	er("编辑成功",0,$links);

}elseif($act == 'adds'){
	
	$arr[0]=array($subject,"标题",'s',255);
	
	if($hasorders){
		$arr[1]=array($orders,"排序号",'n',11);
	}
	
	$msg = upc($arr);
	
	$msg?er($msg,2):NULL;
	
	$upload_dir = "../upfiles/";
		$max_size = 5000;
		$file_name = "upload"; 
		if(!empty($_FILES[$file_name]['name'])){
			$picurl = "upfiles/".upload_file($file_name,$upload_dir,$max_size);
		}
	
	$sql = get_cname($table);
	eval("\$sql=\"$sql\";");
	$db->query($sql);
	
	

	
	$links[0]['text']="返回列表";
	$links[0]['href']="?";
	$links[1]['text']="继续添加";
	$links[1]['href']="?act=add&pid=$pid";
	
	er("添加成功",0,$links);

	
}elseif($act == 'del'){

	if(!empty($moderate)){
		if(is_array($moderate)) $moderate =implode('\',\'', $moderate);
		
		$ids=str_replace("'","",$moderate);
		
		$db->query("delete from $table where  id in($ids)");
		$links[0]['text']="返回列表";
		$links[0]['href']="?";
		er("删除成功",0,$links);
		
	}else{
		$links[0]['text']="返回列表";
		$links[0]['href']="?";
		er("请选择要删除的记录",2,$links);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - <?=$pagename?> </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<script src="js/utils.js"></script>
</head>
<body>
<h1>
<span class="action-span" onclick="window.location='?<?=empty($act)?"act=add":""?>'" style="cursor:hand;">
<?
if(empty($act) && empty($keyword)){
?>
<a href="?act=add">添加<?=$ename?></a>
<?
}else{
?>
<a href="?">返回列表</a>
<?
}
?>
</span>
<span class="action-span1"><a href="admin_index.php">管理中心</a>  - <?=$pagename?><?=$burl?> </span>
<div style="clear:both"></div>
</h1>
<?
if($do_search){
?>
<div class="form-div">
  <form action="?acts=search" name="searchForm" method="get">
  <input type="hidden" name="acts" value="search" />
  <input type="hidden" name="pagesizes" value="<?=$pagesizes?>" />
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    标题 
    <input type="text" name="keyword" value="<?=$keyword?>" size="15" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>
<?
}
if(empty($act) || ($keyword && $act!='edit')){
?>
<form method="post" action="?act=del" name="listForm">
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
<tr>
<th width="58" align="center"><nobr>选择</nobr></th>
<th width="271">类别名称</th>
<th width="372" style="display:none">所属类别</th>
<th width="329">排 序 号</th>
</tr>

<?

$pagesize=50;

$cd=" where pid=0 ";

if($keyword){
	$cd.=" and subject like '%".$keyword."%' ";
}

$sql = "select count(*) as c from $table  $cd ";

if($hasorders){
	$order_str = " orders desc, ";
}
$sql2 = "select * from $table $cd  order by $order_str id";
$baselink ="?keyword=".urlencode($keyword)."&pagesizes=$pagesizes";
$pages="";
$counts=0;
$query = cp($sql,$sql2,$pagesize,$baselink,$pages,$counts);
while($a=$db->fetch_array($query)){
?>
   <tr>
<td align="center"><input type="checkbox" name="moderate[]" value="<?=$a['id']?>"></td>
<td height="22" align="left">&nbsp;<span> 
<a href="?act=edit&id=<?=$a['id']?>&page=<?=$page?>&acts=<?=$acts?>&keyword=<?=urlencode ($keyword)?>"> 
<?=$a['subject']?></a></span> </td>
<td align="center"  style="display:none"><?=$a['pid']?></td>
<td align="center"><?=$a['orders']?></td>
</tr>
   
   <?
   $query1=$db->query("select * from $table where pid='$a[id]' order by orders desc,id");
   while($b=$db->fetch_array($query1)){
   	?>
		   <tr>
		<td align="center"><input type="checkbox" name="moderate[]" value="<?=$b['id']?>"></td>
		<td height="22" align="left">&nbsp;｜------<span> 
		<a href="?act=edit&id=<?=$b['id']?>&page=<?=$page?>&acts=<?=$acts?>&keyword=<?=urlencode ($keyword)?>"> 
		<?=$b['subject']?></a></span> </td>
		<td align="center" style="display:none"><?=$a['subject']?></td>
		<td align="center" ><?=$b['orders']?></td>
		</tr>
	<?
   }
   ?>
 

<?
}
if($counts>0){
?>	
<tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="0"> 
     <tr> 
       <td width="31%" bgcolor="#FFFFFF" style="padding-left:17px;"><input type="checkbox" name="chkall" onClick="javascript:CheckAll(this.form)"> <input name="Submit4" type="submit" class="button" value="执行删除" /></td> 
       <td width="69%" align="right" style="padding-right:10px;"><?=$pages?></td> 
     </tr> 
   </table></td></tr>

<?
}else{
?>
<tr><td colspan="4" height="30" style=" padding:10px 0px 10px 30px; color:#660000"><?=$keyword?"搜索不到相关记录":"暂时没有记录"?></td></tr>
<?
}
?>
  </table>

</div>
</form>
<?
}elseif($act == 'edit'){
	$a=$db->get_one("select * from $table where id='$id' ");
?>
<form method="post" action="?act=edits" <?=$isfile?'enctype="multipart/form-data"':''?>  name="listForm">
<input type="hidden" name="id" value="<?=$id?>">
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th colspan="2" align="left">添加<?=$ename?></th>
  </tr>
  <tr>
<td align=right>名称：</td>
<td><input name="subject" type="text"  class='must_input'  id="subject" value="<?=$a['subject']?>" /></td>
</tr>

<?

$query1=$db->query("select * from $father_table where pid=0");
?>
      <tr>
        <td align=right>所属类别：</td>
        <td><select name="pid" id="pid" class="must_select">
        <option value="0" <?=select(0,$a['pid'])?> >请选择【默认一级分类】</option>
		<?
		while($q=$db->fetch_array($query1)){
		?>
		<option value="<?=$q['id']?>" <?=select($q['id'],$a['pid'])?> ><?=$q['subject']?></option>
		<?
		}
		?>
          </select> </td>
      </tr>


<tr>
<td align=right>排 序 号：</td><td><input name="orders" type="text"  class='must_input'  id="orders" value="<?=$a['orders']?>" /></td>
</tr>
<tr >
  <td align=right>上传图片：</td>
  <td><label>
    <input name="upload" type="file" id="upload" />240*300
  </label></td>
</tr>
    <?
if($a['picurl']){
?>
<tr >
  <td align=right>缩略图预览：</td>
  <td><img src="../<?=$a['picurl']?>" width="100" /></td>
</tr>
<?
}
?>  
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="Submit" value="提交" class="button" onclick="return checkfill()"  />
       　 </td>
  </tr>
</table>
</div>
</form>
<?
}elseif($act == 'add'){
?>

<form method="post" action="?act=adds" <?=$isfile?'enctype="multipart/form-data"':''?>  name="listForm">
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th colspan="2" align="left">添加<?=$ename?></th>
  </tr>
  <tr>
<td align=right>名称：</td>
<td><input name="subject" type="text"  class='must_input'  id="subject" value="<?=$a['subject']?>" /></td>
</tr>

<?

$query1=$db->query("select * from $father_table where pid=0");
?>
      <tr>
        <td align=right>所属类别：</td>
        <td><select name="pid" id="pid" class="must_select">
        <option value="0">请选择【默认一级分类】</option>
		<?
		while($q=$db->fetch_array($query1)){
		?>
		<option value="<?=$q['id']?>"><?=$q['subject']?></option>
		<?
		}
		?>
          </select> </td>
      </tr>


<tr>
<td align=right>排 序 号：</td><td><input name="orders" type="text"  class='must_input'  id="orders" value="0" /></td>
</tr>
<tr >
  <td align=right>上传图片：</td>
  <td><label>
    <input name="upload" type="file" id="upload" />240*300
  </label></td>
</tr>
      
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="Submit" value="提交" class="button" onclick="return checkfill()"  />
       　 </td>
  </tr>
</table>
</div>
</form>
<?
}
include_once("foot.php");
?>
</body>
</html>