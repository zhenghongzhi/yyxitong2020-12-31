<?php
session_cache_limiter('private, must-revalidate');
include_once("../../include/common.ini.php");
include_once("common_function.php");
header("Content-type:text/html;charset=utf-8");
  
function curlpost($c_url,$data)
{
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $c_url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
   // curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
       echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo; // 返回数据
}


function curlget($c_url,$data)
{
//初始化
$ch = curl_init();
curl_setopt($ch,CURLOPT_URL,$c_url.http_build_query($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
$output = curl_exec($ch);
curl_close($ch);
return $output;
}

//curl GET + POST 提交
function filePostContents($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_COOKIESESSION, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_TIMEOUT, 600);
    //curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);        
    //curl_setopt($ch, CURLOPT_REFERER,_REFERER_);        
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $r = curl_exec($ch);
    curl_close($ch);
    return $r;
}
//http://api.map.baidu.com/place/v2/eventsearch?query=美食&event=groupon&region=131&bounds=39.915,116.404,39.935,116.435&output=json&page_size=1&ak=E4805d16520de693a3fe707cdc962045
//http://api.map.baidu.com/place/v2/search?ak=您的密钥&output=json&query=%E7%AF%AE%E7%90%83%E9%A6%86&page_size=20&page_num=0&scope=2&location=39.915,116.404&radius=20000
	$url="http://api.map.baidu.com/place/v2/search?";


    $data['ak']="USBEEIeWHQHiAZdXzxrQywKT";
	$data['output']="json";
	$data['query']="篮球场";
	$data['page_size']="10";
	$data['page_num']="1";
	$data['scope']="2";
	$data['location']="23.16667,113.23333";
	$data['radius']="20000";
	$re = curlget($url,$data);
    $re = json_decode($re,true);
	
	print_r($re);
	echo "<hr>";
////////
//http://api.map.baidu.com/place/v2/eventsearch?query=美食&event=groupon&region=131&location=39.928,116.423&radius=5000&output=xml&page_size=1&ak=E4805d16520de693a3fe707cdc962045	
	$url="http://api.map.baidu.com/place/v2/eventsearch?query=美食&event=groupon&region=131&location=39.928,116.423&radius=5000&output=xml&page_size=1&ak=USBEEIeWHQHiAZdXzxrQywKT";
	$data2['ak']="USBEEIeWHQHiAZdXzxrQywKT";
	$data2['query']="美食";
	$data2['event']="groupon";
	$data2['region']="131";
	$data2['location']="38.76623,116.43213";
	$data2['radius']="5000";
	$data2['output']="json";
	//$data2['filter']="groupon_type:3";
	$data2['page_size']="1";
	//$data2['page_num']="1";
	$re = curlget($url,$data2);
    $re = json_decode($re,true);
	
	var_dump($re);
	
	echo "<hr>";	
//    $data['geotable_id']="127448";//你的数据库ID    
//    $data['page_index']="0";
//	$data['page_size']="10";
	
