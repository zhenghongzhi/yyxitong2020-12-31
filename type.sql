-- ----------------------------
-- Table structure for `yasa_hotel_type`
-- ----------------------------
DROP TABLE IF EXISTS `yasa_hotel_type`;
CREATE TABLE `yasa_hotel_type` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `picurl` varchar(255) DEFAULT NULL,
  `type1` varchar(300) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `type2` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `yasa_market_type`
-- ----------------------------
DROP TABLE IF EXISTS `yasa_market_type`;
CREATE TABLE `yasa_market_type` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `picurl` varchar(255) DEFAULT NULL,
  `type1` varchar(300) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `type2` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `yasa_warehouse_type`
-- ----------------------------
DROP TABLE IF EXISTS `yasa_warehouse_type`;
CREATE TABLE `yasa_warehouse_type` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `orders` int(11) DEFAULT '0',
  `picurl` varchar(255) DEFAULT NULL,
  `type1` varchar(300) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `type2` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;