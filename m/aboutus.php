

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>公司简介</title>
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Makes your prototype chrome-less once bookmarked to your phone's home screen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Set a shorter title for iOS6 devices when saved to home screen -->
    <meta name="apple-mobile-web-app-title" content="tianya touch">

  

    <!-- Include the CSS -->
	<link rel="stylesheet" href="css/touch-global.css"/>
    <link rel="stylesheet" href="css/main_m_68d4b19.css"/>

</head>
<style>
.right100 img{
	width: 100%;

  height: auto;
}
.center2{
  font-size: 16px;
  line-height: 40px;
  padding-top: 5px;
  width: 96%;
  padding-left:2%;
  padding-right:2%;
  margin: 0 auto;
	}
</style>
<body style="background:#f8f8f8;">
	<!-- header -->
	<header class="header-static clearfix" >
		<div class="left">
			<ul class="list-nav">
				<li><a href="index.php"><i class="icon-chevron-left"></i></a></li>
			</ul>
		</div>
		<div class="center">
			公司简介
		</div>
		<div class="right">
			<ul class="list-set">
				
			</ul>
		</div>
	</header>

	<!-- content -->
	<div class="content" id="page_login">
	<div class="right100"> <div class="center2">
             <p> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;广交医药速运专注医药供应链管理及服务，为医药行业提供合规的仓储、物流、配送、快递等多元化服务，
             凭借多年医药物流运营经验、及对新版GSP物流要求的深刻洞悉，为医药行业客户提供创新、高效
             、安全的物流选择，让物流赋予企业更大商业价值，赋予消费者更卓越的体验。</p>
                </div></div>	
	</div>

</body>
</html>