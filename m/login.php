

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>我的广交</title>
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Makes your prototype chrome-less once bookmarked to your phone's home screen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Set a shorter title for iOS6 devices when saved to home screen -->
    <meta name="apple-mobile-web-app-title" content="tianya touch">

  

    <!-- Include the CSS -->
	<link rel="stylesheet" href="css/touch-global.css"/>
    <link rel="stylesheet" href="css/main_m_68d4b19.css"/>

</head>
<body style="background:#f8f8f8;">
	<!-- header -->
	<header class="header-static clearfix" >
		<div class="left">
			<ul class="list-nav">
				<li><a href="index.php"><i class="icon-chevron-left"></i></a></li>
			</ul>
		</div>
		<div class="center">
			我的广交
		</div>
		<div class="right">
			<ul class="list-set">
				<li class="item item-user"><a href="#">注册</a></li>
			</ul>
		</div>
	</header>

	<!-- content -->
	<div class="content" id="page_login">
		<form method="post" action="#">
			<ul class="input-wrapper">
                <li class="i-username">
                    <input type="text" name="vwriter" id="user_name" placeholder="请输入账号" value="" />
                </li>
                <li class="i-password">
                    <input type="password" name="vpassword" id="password" placeholder="请输入密码" />
                    <img src="1.png"  height="40px" >
                </li>
				
            </ul>
            <div class="form-control">
                <button class="btn-login" type="submit">登 录</button>
            </div>
            <div class="form-control clearfix">
                <label class="left auto-login"><input type="checkbox" name="rmflag" value="1" checked />
	                <span class="rmflag-ico"></span>记住密码
                </label>
                <a class="right" href="#">忘记密码</a>
            </div>
            

		</form>
		
	</div>

</body>
</html>