<?
include_once("../include/common.ini.php");
include_once("error.inc.php");
include_once("checkuser.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>-后台管理系统-</title>
<link rel="stylesheet" href="theme/index_menu_data/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_menu_data/menu.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_menu_data/invalid.css" type="text/css" media="screen">
<script language="javascript" src="theme/index_menu_data/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="theme/index_menu_data/admin.js"></script>
<script src="theme/frame.js" language="javascript" type="text/javascript"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {
	font-size: 12px;
	color: #000000;
}
.STYLE5 {
	font-size: 12px;
}
.STYLE7 {
	font-size: 12px;
	color: #FFFFFF;
}
-->
</style>
</head>
<body>
<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
  <div id="sidebar">
    <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
      
      <h1 id="sidebar-title"><a href="#">信息管理系统</a></h1>
      
      <!-- Logo (221px wide) --> 
      <a href=""><img id="logo" src="theme/index_menu_data/logo.png" alt="zcncms logo"></a> 
      <!-- Sidebar Profile links -->
      <div id="profile-links"> <a href="#">当前帐号:
        <?=$_SESSION['cuiniao_Admin_Name']?>
        [
        <?=$_SESSION['cuiniao_flag']==1?"管理员":"管理员"?>
        ] </a> <br>
        <a href="index.php?method=logout" title="退出" target="_parent">退出</a> </div>
      <ul id="main-nav" >
        <!-- Accordion Menu -->
   <li> <a style="padding-right: 15px;" id="templets" href="javascript:void(0);" class="nav-top-item"> 管理员信息 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="templets_main" href="password.php" target="main">信息修改</a></li>
            <? if($_SESSION['user_test_rights']==2){ ?>
            <li><a class="nav-bottom-item"  href="admin.php" target="main">用户列表</a></li>
            <li><a class="nav-bottom-item"  href="admin.php?act=add" target="main">添加用户</a></li>
             <li><a class="nav-bottom-item"  href="hemo_app_setting.php?id=1&act=edit" target="main">系统配置</a></li>
            <? }?>
          </ul>
        </li>
        
           
      
        
         <? if($_SESSION['sundy_right1']==1){ ?>
    
            <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">客户商家管理 </a>
              <ul style="display: none;">
                <li><a class="nav-bottom-item" id="media_main" href="hemo_app_kehu.php" target="main">录入客户列表</a></li>
                 <li><a class="nav-bottom-item" id="media_main" href="hemo_app_kehu_type.php" target="main">客户类别</a></li>
                 <li><a class="nav-bottom-item" id="media_main" href="hemo_app_kehu_tuisong.php" target="main">客户推送</a></li>
                <li><a class="nav-bottom-item" id="media_main" href="hemo_app_shangjia.php" target="main">录入商家列表</a></li>
                <li><a class="nav-bottom-item" id="media_main" href="kehuarchives.php" target="main">客户档案</a></li>
              </ul>
            </li>
          <? }?>
          <? if($_SESSION['sundy_right2']==1){ ?>
            <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">APP用户管理 </a>
              <ul style="display: none;">
              <li><a class="nav-bottom-item" id="media_main" href="hemo_app_user2.php" target="main">APP客户列表</a></li>
                <li><a class="nav-bottom-item" id="media_main" href="hemo_app_jifen.php" target="main">APP客户积分</a></li>
                <li><a class="nav-bottom-item" id="media_main" href="hemo_app_djj.php" target="main">APP客户代金卷</a></li>
                <li><a class="nav-bottom-item" id="media_main" href="hemo_app_user.php" target="main">APP翻译列表</a></li>
                       <li><a class="nav-bottom-item" id="media_main" href="hemo_app_fangyi_type.php" target="main">翻译类别</a></li>
              </ul>
            </li>
         <? }?>
          <? if($_SESSION['sundy_right3']==1 || $_SESSION['sundy_right21']==1){ ?>
           
            <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">订单管理 </a>
              <ul style="display: none;">

                <?php if ($_SESSION['sundy_right21']==1 && $_SESSION['sundy_right3']==0) { ?>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_order.php" target="main">订单列表</a></li>
                <? }else{ ?>
                  
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_order.php" target="main">订单列表</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_product.php" target="main">商品列表</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="crm_purchaseorder.php" target="main">采购订单管理</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_product_yanhuo.php" target="main">验货列表</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_product_xiangzi.php" target="main">装箱单列表</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_product_zhuangui.php" target="main">装柜列表</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="hemo_app_product_tidan.php" target="main">装柜客户列表</a></li>
                  <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_product_jiesuan3.php" target="main">汇总装箱单列表</a></li>

                  <li><a class="nav-bottom-item" href="kucun.php" target="main">系统库存统计</a></li>
                  <!-- <li><a class="nav-bottom-item" id="media_main" href="crm_product.php" target="main">商品管理</a></li> -->
                  <li><a class="nav-bottom-item" id="media_main" href="yingshou.php" target="main">订单跟踪管理</a></li>
                  <li><a class="nav-bottom-item" href="zhuanzhang.php" target="main">转账申请</a></li>
                  <li><a class="nav-bottom-item" href="product_type.php" target="main">产品分类</a></li>
                  <li><a class="nav-bottom-item" id="media_main" href="crm_productcate.php" target="main">采购商品分类管理</a></li>


                <? } ?>
                   
              </ul>
            </li>
         <? }?>

         <? if($_SESSION['sundy_right18'] == 1 || $_SESSION['sundy_right19'] == 1 || $_SESSION['sundy_right20'] == 1){ ?>

          <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">仓库管理辅助 </a>
            <ul style="display: none;">
              <li><a class="nav-bottom-item" id="media_main" href="kehutype.php" target="main">客户类型</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="diqu.php" target="main">公司管理</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="muchuan.php" target="main">木船服务商管理</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="cangku.php" target="main">国内仓库管理</a></li>
            </ul>
         </li>
         <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">国内仓库管理 </a>
           <ul style="display: none;">
              <? if($_SESSION['sundy_right18']==1 || $_SESSION['sundy_right19']==1 || $_SESSION['sundy_right20']==1){ ?>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=5" target="main">入仓</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=3" target="main">出仓</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=4" target="main">库存</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=10" target="main">退仓</a></li>
              <? } ?>
              <? if($_SESSION['sundy_right19']==1 || $_SESSION['sundy_right20']==1) { ?>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=1" target="main">待配舱</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=2" target="main">已配舱</a></li>
              <? } ?> 
              <!-- <li><a class="nav-bottom-item" href="zengzhi.php" target="main">增值服务费</a></li> -->
              <li><a class="nav-bottom-item" href="fanghuosq.php" target="main">放货申请</a></li>
             
              


                <!-- <li><a class="nav-bottom-item" href="warehouse.php?zt=5" target="main">入仓</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=3" target="main">出仓</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=4" target="main">库存</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=1" target="main">待配舱</a></li>
                <li><a class="nav-bottom-item" href="warehouse.php?zt=2" target="main">配舱中</a></li> -->
            </ul>
         </li>
        
         <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">迪拜仓库管理 </a>
           <ul style="display: none;">
             <li><a class="nav-bottom-item" href="dibaiwarehouse.php?zt=1" target="main">转港货物</a></li>
             <li><a class="nav-bottom-item" href="dibaiwarehouse.php?zt=2" target="main">待到港</a></li>
             <li><a class="nav-bottom-item" href="dibaiwarehouse1.php" target="main">已到港</a></li>
             <li><a class="nav-bottom-item" href="dibaipaisong.php" target="main">已派送</a></li>
             <li><a class="nav-bottom-item" href="dibaikucun.php" target="main">库存</a></li>
             <!-- <li><a class="nav-bottom-item" href="qianshouliebiao.php" target="main">签收单</a></li> -->
            </ul>
         </li>

         <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">伊朗仓库管理 </a>
           <ul style="display: none;">
             <li><a class="nav-bottom-item" href="ylwarehouse.php?zt=1" target="main">转港货物</a></li>
             <li><a class="nav-bottom-item" href="ylwarehouse.php?zt=2" target="main">待到港</a></li>
             <li><a class="nav-bottom-item" href="ylwarehouse.php?zt=3" target="main">已到港</a></li>
             <li><a class="nav-bottom-item" href="ylpaisong.php" target="main">已派送</a></li>
             <li><a class="nav-bottom-item" href="ylkucun.php" target="main">库存</a></li>
            </ul>
         </li>
          <? } ?>
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">业务账单管理</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" href="lookyundan.php" target="main">一程运费单</a></li>
            <li><a class="nav-bottom-item" href="yllookyunfei.php" target="main">二程运费单</a></li>
            <li><a class="nav-bottom-item" href="zengzhiorder.php" target="main">增值服务单</a></li>
            <li><a class="nav-bottom-item" id="media_main" href="duizhangdan.php" target="main">客户对账单</a></li>
            

          </ul>
        </li>
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">统计报表</a>
          <ul style="display: none;">
              <li><a class="nav-bottom-item" id="media_main" href="yingshoutongji.php" target="main">订单跟踪汇报</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="statisticsorder.php" target="main">订单统计报表</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="statisticswuliu.php" target="main">物流统计报表</a></li>
              <li><a class="nav-bottom-item" id="media_main" href="statisticskehu.php" target="main">客户统计报表</a></li>

          </ul>
        </li>
          <? if($_SESSION['sundy_right22']==1){ ?>
         <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">物品登记管理 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" href="proregistration.php" target="main">物品登记入仓</a></li>
            <li><a class="nav-bottom-item" href="proregistrationchu.php" target="main">物品登记出仓</a></li>
            <li><a class="nav-bottom-item" href="proregistrationkucun.php" target="main">物品登记库存</a></li>
          </ul>
         </li>
        <? } ?>


          <? if($_SESSION['sundy_right4']==1){ ?>
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">结算包管理</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_product_jiesuan2.php" target="main">结算包列表</a></li>
            <li><a class="nav-bottom-item" id="templets_one" href="hemo_app_product_jiesuan2.php?act=add" target="main">添加结算包</a></li>
          </ul>
        </li>
         <? }?>
         
     
          <? if($_SESSION['sundy_right6']==1){ ?>
        
        <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">考勤管理 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="media_main" href="hemo_app_xinwen2.php" target="main">考勤列表</a></li>
          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right7']==1){ ?>
         
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">任务及充值</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_xinwen3.php" target="main">任务列表</a></li>
             <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_chongzi.php" target="main">充值列表</a></li>
          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right8']==1){ ?>
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">兑换管理</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_xinwen5.php" target="main">兑换列表</a></li>
            <li><a class="nav-bottom-item" id="templets_one" href="hemo_app_xinwen5.php?act=add" target="main">添加兑换</a></li>
          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right9']==1){ ?>
           <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">出租车管理</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_xinwen6.php" target="main">出租车列表</a></li>
          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right10']==1){ ?>
        <li> <a style="padding-right: 15px;" id="channel" href="javascript:void(0);" class="nav-top-item">查询管理</a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_xinwen7.php" target="main">酒店列表</a></li>
               <li><a class="nav-bottom-item" id="mychannel_main" href="hemo_app_xinwen9.php" target="main">市场信息列表</a></li>

          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right11']==1){ ?>
          <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">翻译录入客户 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="media_main" href="hemo_app_xinwen1.php" target="main">客户列表</a></li>
          </ul>
        </li>
         <? }?>
          <? if($_SESSION['sundy_right12']==1){ ?>
     
           <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">群组管理 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="media_main" href="./api/hemo_group_list.php" target="main">群组列表</a></li>
          </ul>
          </li>
           <? }?>
            <? if($_SESSION['sundy_right13']==1){ ?>
            <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">广告管理 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="media_main" href="hemo_app_news.php" target="main">广告列表</a></li>
       <li><a class="nav-bottom-item" id="media_main" href="hemo_app_news.php?act=add" target="main">广告添加</a></li>
         </ul>
        </li>
          <? } ?>
            <? if($_SESSION['sundy_right14']==1){ ?>
               <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">企业圈管理 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" id="media_main" href="hemo_goods.php" target="main">产品列表</a></li>
       <li><a class="nav-bottom-item" id="media_main" href="hemo_goods_type.php" target="main">产品类别</a></li>
        <li><a class="nav-bottom-item" id="media_main" href="hemo_app_kehu_order.php" target="main">企业圈订单</a></li>
         </ul>
        </li>
            <? }?>
          <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item">审批模块管理 </a>
            <ul style="display: none;">
              <li><a class="nav-bottom-item" id="media_main" href="module.php" target="main">审批列表</a></li>
           </ul>
        </li>
     
        <li style="display:none"> <a style="padding-right: 15px;" href="javascript:void(0);" class="nav-top-item"> 版权信息 </a>
          <ul style="display: none;">
            <li><a class="nav-bottom-item" href="http://willwell.taobao.com/" target="_blank">Powered by 翠鸟网络</a></li>
            <li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=445992114&amp;site=qq&amp;menu=yes">QQ：445992114</a></li>
          </ul>
        </li>
      </ul>
      <!-- End #main-nav --> 
      
      <script>
                $(".nav-bottom-item").click(function(){
                    $(".nav-bottom-item").removeClass('current');
                    $(".nav-top-item").removeClass('current');
                    $(this).addClass("current");
                    $(this).parents("li").children(".nav-top-item").addClass("current");
                });   
			 
                </script> 
    </div>
    <!-- End #messages --> 
    
    <!-- End #messages --> 
    
  </div>
</div>
</body>
</html>