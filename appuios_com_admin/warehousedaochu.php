<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');


	
		// print_r($_GET);die;
		$where = "1 and w.is_del = 1"; 
		if ($zt == 1) {
			$where .= " and w.status = 1 and (w.spstatus = 2 or w.spstatus = 3)";
		}elseif($zt == 2){
			$where .= " and w.status = 2";

		}elseif($zt == 3){
			$where .= " and w.status = 3";

		}elseif ($zt == 4) {
			$where .= " and (w.status = 1 or w.status = 2)";
		}elseif ($zt == 5) {
			
		}
		if ($k_order_id) {
			$where .= " and w.orderid like '%".$k_order_id."%'";
		}
		if ($k_kehu_number) {
			$where .= " and w.userid like '%".$k_kehu_number."%'";
		}
		if ($k_khtype) {
			$where .= " and w.khtype = ".$k_khtype;
		}
		if ($k_protype) {
			$where .= " and w.protype = '$k_protype'";
		}
		if ($k_fahuo) {
			$where .= " and w.fahuo = '$k_fahuo'";
		}
		if ($k_jincangdanhao) {
			$where .= " and w.jincangdanhao = '$k_jincangdanhao'";
		}
		if ($k_fahuohao) {
			$where .= " and w.fahuohao like '%".$k_fahuohao."%'";
		}
		if ($k_addtime) {
			$where .= " and w.rukutime >= '$k_addtime'";
		}
		if ($k_endtime) {
			$where .= " and w.rukutime <= '$k_endtime'";
		}
		if ($k_status) {
			$where .= " and w.status = '$k_status'";
		}
		if ($k_spstatus) {
			$where .= " and w.spstatus = '$k_spstatus'";
		}
		if ($k_chuaddtime) {
			$where .= " and w.chucangtime >= '$k_chuaddtime'";
		}
		if ($k_chuendtime) {
			$where .= " and w.chucangtime <= '$k_chuendtime'";
		}
		

		$db->query("SET NAMES UTF8");
		// $result = $db->query("SELECT SUM(type1) AS money, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime >= $start AND addtime <= $end GROUP BY pid1 ORDER BY money DESC");
		
		$orderby = " order by w.status asc,w.rukutime desc";
		$sql = "select w.*,k.subject from yasa_warehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype
		where ".$where.$orderby;
		// echo $sql;die;
		$result = $db->query($sql);
		// $db->close();

		
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle('汇总');
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '订单号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '货物品类');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '总数量');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '总重量');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '总体积');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '包装类型');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '发货方式');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '进仓单号');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '入库日期');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '出仓日期');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '订单状态');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '备注');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '添加时间');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', '审批状态');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', '品名');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', '是否已完成收货');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', '发货号');




		$i = 2;
		while($row = $db->fetch_array($result)) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row[orderid]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row[protype]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row[count]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row[weight]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row[volume]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row[money]);

			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row[bztype]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row[fahuo]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row[jincangdanhao]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row[rukutime]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row[chucangtime]);
			if ($row[status]==1) {
				$status = '入仓';
			}elseif ($row[status] == 2) {
				$status = '配舱';
			}else{
				$status = '出仓';
			}
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $status);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row[remake]);
			
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, date("Y-m-d H:i:s",$row[addtime]));
			if ($row[spstatus]==1) {
				$spstatus = '待审批';
			}elseif ($row[spstatus] == 2) {
				$spstatus = '已审批';
			}
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $spstatus);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $row[product]);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $row[is_finish]==1?'否':'是');
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $row[fahuohao]);

			
			$i += 1;
		}
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	

 ?>