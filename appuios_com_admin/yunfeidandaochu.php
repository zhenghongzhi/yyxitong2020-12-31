<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	/*
		1 运单excel导出
		2 运单pdf导出
		3 运单客户excel导出
		4 运单客户pdf导出
		5 运单财务导出
		6 按公司运单execl导出
	 */ 
	$danwei = array('海运' => '（m³）', '空运'=>'（kg）');
	$danwei1 = array('海运' => '体积', '空运'=>'重量');
	$danwei2 = array('海运' => 'CBM', '空运'=>'KG');

	function plus_minus_conversion($number = 0){
	    return $number > 0 ? -1 * $number : abs($number);
	}

	if ($type == 1) {//
		$title = '账单'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username,a1.username as username1,d.strsubject from yasa_yunfeidan as y 
			left join cuiniao_admin as a on a.id = y.luruyuanid 
			left join cuiniao_admin as a1 on a1.id = y.spid
			left join yasa_diqu as d on d.id = y.diquid
			where y.id = $id");
		$yunfeisql = $db->query("select yc.*,k.subject from yasa_yunfei_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype where yid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}




		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('A2:P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		// 字体大小12
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:Q7')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension("B:D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F:Q")->setAutoSize(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');

		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('I2:P2');

		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		$objPHPExcel->getActiveSheet()->mergeCells('I3:P3');

		$objPHPExcel->getActiveSheet()->mergeCells('A4:H4');
		$objPHPExcel->getActiveSheet()->mergeCells('I4:P4');

		$objPHPExcel->getActiveSheet()->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->mergeCells('M5:N5');

		$objPHPExcel->getActiveSheet()->mergeCells('A6:A7');
		$objPHPExcel->getActiveSheet()->mergeCells('B6:B7');
		$objPHPExcel->getActiveSheet()->mergeCells('C6:C7');

		$objPHPExcel->getActiveSheet()->mergeCells('D6:E6');

		$objPHPExcel->getActiveSheet()->mergeCells('F6:H6');
		$objPHPExcel->getActiveSheet()->mergeCells('I6:K6');
		$objPHPExcel->getActiveSheet()->mergeCells('L6:N6');

		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->mergeCells('O6:Q6');
			
		}else{
			$objPHPExcel->getActiveSheet()->mergeCells('O6:P6');
		}



		$objPHPExcel->getActiveSheet()->setCellValue('A1', $yunfeidan[strsubject]."第（$yunfeidan[fahuohao]）货柜明细");
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "装柜时间：".$yunfeidan[fahuotime]);+
		$objPHPExcel->getActiveSheet()->setCellValue('I2', "货柜号：".$yunfeidan[huoguihao]);
		$objPHPExcel->getActiveSheet()->setCellValue('A3', "发货公司：".$yunfeidan[fahuogongsi]);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', "货柜尺寸：".$yunfeidan[guihaochicun]);
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "船公司：".$yunfeidan[chuangongsi]);
		$objPHPExcel->getActiveSheet()->setCellValue('I4', "封条号：".$yunfeidan[fengtiao]);
		$objPHPExcel->getActiveSheet()->setCellValue('M5', "汇率：USD");
		$objPHPExcel->getActiveSheet()->setCellValue('O5', $yunfeidan[huilv]);
		$objPHPExcel->getActiveSheet()->setCellValue('P5', 1);

		$objPHPExcel->getActiveSheet()->setCellValue('A6', '序号');
		$objPHPExcel->getActiveSheet()->setCellValue('B6', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('C6', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('D6', '货物信息');
		$objPHPExcel->getActiveSheet()->setCellValue('F6', '普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', '非普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('L6', '特货运费');
		$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('O6', '入账金额');
		$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);

		$objPHPExcel->getActiveSheet()->setCellValue('D7', "数量\n（CTNS)");
		$objPHPExcel->getActiveSheet()->getStyle('D7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('E7', "实发".$danwei1[$yunfeidan[fahuo]]."\n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('E7')->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setCellValue('F7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('F7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('G7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', '总价');


		$objPHPExcel->getActiveSheet()->setCellValue('I7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('I7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('J7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('L7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('L7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('M7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('O7', "总运费\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('O7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('P7', "其他费用\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('P7')->getAlignment()->setWrapText(true);
		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->setCellValue('Q7', "成本\n（￥）");
			$objPHPExcel->getActiveSheet()->getStyle('Q7')->getAlignment()->setWrapText(true);
		}

		$i = 8;
		$j = 1;
		foreach ($arr2 as $key => $value) {
			foreach ($value as $ke => $val){

				if ($val['普货'][0][remake]) {
					$remake[] = $val['普货'][0][userid].':'.$val['普货'][0][remake];
				}
				if ($val['非普货'][0][remake]) {
					$remake[] = $val['非普货'][0][userid].':'.$val['非普货'][0][remake];
				}
				if ($val['特货'][0][remake]) {
					$remake[] = $val['特货'][0][userid].':'.$val['特货'][0][remake];
				}		

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $key);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $ke);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count]);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['普货'][0][tjzl]+$val['非普货'][0][tjzl]+$val['特货'][0][tjzl]);

				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['非普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['非普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['特货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $val['特货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=ROUND(SUM(H'.$i.'+K'.$i.'+N'.$i.'),0)');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui]);

				if($yunfeidan[fahuo]=='空运'){ 
					// print_r($val);
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $val['普货'][0][xmchengben]+$val['非普货'][0][xmchengben]+$val['特货'][0][xmchengben]);
				}


				$i ++;
				$j ++;


			}
		}
		// die;
		$style_array = array(
        	'borders' => array(
            	'allborders' => array(
                	'style' => \PHPExcel_Style_Border::BORDER_THIN
            	)
       		)
        );
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,'=SUM(H8:H'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i,'=SUM(K8:K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i,'=SUM(N8:N'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=SUM(O8:O'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i,'=SUM(P8:P'.($i-1).')');

		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+3),'收入');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+3),'=ROUND(SUM(O'.($i).':P'.($i).'),0)');

		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+4).':N'.($i+4));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+4),implode(',',$remake));

		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+4),'成本');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+4),round($yunfeidan[chengben]));
		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+5),'毛利');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+5),"=ROUND(P".($i+3)."-P".($i+4).",0)");

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+6),'制单人');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($i+6),$yunfeidan[username]);

		$objPHPExcel->getActiveSheet()->setCellValue('E'.($i+6),'审批人');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($i+6),$yunfeidan[username1]);

		$objPHPExcel->getActiveSheet()->setCellValue('I'.($i+6),'总经理签字');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+6),'财务签字');
		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->getStyle('A1:Q'.($i-1))->applyFromArray($style_array);

		}else{
			$objPHPExcel->getActiveSheet()->getStyle('A1:P'.($i-1))->applyFromArray($style_array);
		}
		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}elseif ($type == 2) {
		
	}elseif ($type == 3) {
		// echo $starttime;die;
		// echo $starttime;die;
		// echo "select yc.*,y.fahuohao,y.fahuotime,y.fahuo from yasa_yunfei_content as yc left join yasa_yunfeidan as y on yc.yid = y.id where yc.khtype = $id and y.is_del = 1 and y.fahuotime > $starttime and y.fahuotime < $endtime";die;
		$useryundan = $db->query("select yc.*,y.fahuohao,y.fahuotime,y.fahuo,y.diquid from yasa_yunfei_content as yc left join yasa_yunfeidan as y on yc.yid = y.id where yc.khtype = $id and y.is_del = 1 and y.fahuotime >= '$starttime' and y.fahuotime <= '$endtime'");
		// echo 'select yc.* from yasa_yunfei_content as yc left join yasa_yunfeidan as y on yc.yid = y.id where yc.khtype = $userid and y.is_del = 1 and y.fahuotime > $starttime and y.fahuotime < $endtime';die;
		$khtype = $db->get_one("select * from yasa_kehutype where id = $id");



		$url = "http://47.106.88.138:8080/api/account/lemonyy1?number=$khtype[subject]&startDate=$starttime&endDate=$endtime";
		$res = curl_get_https($url);
		$res = json_decode($res);


		// echo '<pre>';
		$yundan1 = array();
		while ($yundan = $db->fetch_array($useryundan)) {
			$diquid = $yundan[diquid];
			$yundan1[$yundan[fahuohao].$yundan[userid]][]=$yundan;
		}
		$diqu = $db->get_one("select * from yasa_diqu where id = $gongsi");


		
		// 增值服务费
  		$zengzhisql = $db->query("select * from yasa_zengzhi where khtype = $id and fytime between '$starttime' and '$endtime' order by id desc");

  		//迪拜到伊朗的运费单
  		$yluseryundan = $db->query("select yc.*,y.number,y.fhtime from yasa_ylyunfei_content as yc 
		left join yasa_ylyunfeidan as y on yc.yid = y.id
		where yc.kehutype = $id and y.is_del = 1 and y.fhtime >= '$starttime' and y.fhtime <= '$endtime'");



		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A:K")->setAutoSize(true);
		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:L3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:L4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:L6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:L7');
		
		$objPHPExcel->getActiveSheet()->mergeCells('A8:E8');
		$objPHPExcel->getActiveSheet()->mergeCells('F8:G8');
		$objPHPExcel->getActiveSheet()->mergeCells('H8:L8');
		$objPHPExcel->getActiveSheet()->mergeCells('D9:E9');

		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);


		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->setCellValue('A2',$diqu[subject]);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true)->setName('Times New Roman')->setSize(14)->getColor()->setRGB('000000'); 


		$objPHPExcel->getActiveSheet()->setCellValue('A3',$diqu[pysubject]);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setName('宋体')->setSize(14)->getColor()->setRGB('366092'); 

		$objPHPExcel->getActiveSheet()->setCellValue('A4','公司地址：'.$diqu[address]);
		$objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setName('宋体')->setSize(14)->getColor()->setRGB('000000'); 

		$objPHPExcel->getActiveSheet()->setCellValue('A5','Add:'.$diqu[pyaddress]);
		$objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true)->setName('Times New Roman')->setSize(14)->getColor()->setRGB('000000'); 

		$objPHPExcel->getActiveSheet()->setCellValue('A6','公司邮箱地址：'.$diqu[email]);
		$objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setName('宋体')->setSize(14)->getColor()->setRGB('000000'); 

		$objPHPExcel->getActiveSheet()->setCellValue('A7','Email:'.$diqu[email]);
		$objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true)->setName('Times New Roman')->setSize(14)->getColor()->setRGB('000000'); 

		$objPHPExcel->getActiveSheet()->setCellValue('F8','对账单');
		$objPHPExcel->getActiveSheet()->getStyle('F8')->getFont()->setBold(true)->setName('宋体')->setSize(18)->getColor()->setRGB('000000'); 

		$objPHPExcel->getActiveSheet()->setCellValue('D9','To：'.$khtype[subject]);
		$objPHPExcel->getActiveSheet()->getStyle('D9')->getFont()->setBold(true)->setName('宋体')->setSize(14)->getColor()->setRGB('000000'); 
		$objPHPExcel->getActiveSheet()->setCellValue('J9','打印日期：');
		$objPHPExcel->getActiveSheet()->setCellValue('K9',date(Ymd));
		$objPHPExcel->getActiveSheet()->getStyle('K9')->getFont()->setBold(true)->setName('宋体')->setSize(12)->getColor()->setRGB('000000');

		$objPHPExcel->getActiveSheet()->setCellValue('A11', '序号');
		$objPHPExcel->getActiveSheet()->setCellValue('B11', '发货号');
		$objPHPExcel->getActiveSheet()->setCellValue('C11', '装柜日期');
		$objPHPExcel->getActiveSheet()->setCellValue('D11', '发货方式');
		$objPHPExcel->getActiveSheet()->setCellValue('E11', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('F11', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('G11', '计量数');
		$objPHPExcel->getActiveSheet()->setCellValue('H11', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('I11', '总价');
		$objPHPExcel->getActiveSheet()->setCellValue('J11', '币种');
		$objPHPExcel->getActiveSheet()->setCellValue('K11', '其他费用');
		$objPHPExcel->getActiveSheet()->setCellValue('L11', '合计');
		$objPHPExcel->getActiveSheet()->setCellValue('M11', '小票合计');
		$objPHPExcel->getActiveSheet()->setCellValue('N11', '备注');
		$i = 12;
		$j = 1;
		
		foreach ($yundan1 as $key => $value){
			$z = 0;
		    $count = count($value);
			foreach ($value as $ke => $val){
				$z += $val[sjtjzl]*$val[danjia]*$val[huilv]+$val[zhuangui];
				if ($ke == 0) { 
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val[fahuohao]);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val[fahuotime]);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val[fahuo]);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val[userid]);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[count].'CTNS');
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val[sjtjzl].$danwei[$val[fahuo]]);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val[huilv]==1?'￥'.round($val[danjia],0):'$'.round($val[danjia],0));
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '￥'.$val[sjtjzl]*$val[danjia]*$val[huilv]);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val[huilv]==1?'RMB':'USD');
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '￥'.round($val[zhuangui],0));
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '￥'.($val[sjtjzl]*$val[danjia]*$val[huilv]+$val[zhuangui]));
					// if($count==1){
					// 	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '￥'.round($z,0));
					// }else{
					// 	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
					// }
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '备注');
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[count].'CTNS');
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val[sjtjzl].$danwei[$val[fahuo]]);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val[huilv]==1?'￥'.round($val[danjia],0):'$'.round($val[danjia],0));
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '￥'.$val[sjtjzl]*$val[danjia]*$val[huilv]);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val[huilv]==1?'RMB':'USD');
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '￥'.round($val[zhuangui],0));
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '￥'.($val[sjtjzl]*$val[danjia]*$val[huilv]+$val[zhuangui]));
					// if($ke == ($count - 1)){
					// 	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '￥'.round($z,0));
					// }else{
					// 	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
					// }
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '备注');
				}
				$i++;
				$j++;
				//$allmoney += $val[sjtjzl]*$val[danjia]*$val[huilv]+$val[zhuangui];
			}
			$allmoney += round($z,0);
		}

		// 迪拜到伊朗的运费单
		while ($yluseryundan1 = $db->fetch_array($yluseryundan)) { 
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $yluseryundan1[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $yluseryundan1[fhtime]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '木船');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $yluseryundan1[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $yluseryundan1[count]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $yluseryundan1[sjcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '￥'.round($yluseryundan1[danjia],0));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '￥'.$yluseryundan1[sjcount]*$yluseryundan1[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, 'RMB');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '￥'.round($yluseryundan1[other],0));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '￥'.($yluseryundan1[sjcount]*$yluseryundan1[danjia]+$yluseryundan1[other]));
			//$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '￥'.round(($yluseryundan1[sjcount]*$yluseryundan1[danjia]+$yluseryundan1[other]),0));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '备注');

			$allmoney += round($yluseryundan1[sjcount]*$yluseryundan1[danjia]+$yluseryundan1[other],0);

			$i++;
			$j++;
		} 
		// 增值服务费
		while ($zengzhi = $db->fetch_array($zengzhisql)) {

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $zengzhi[danhao]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zengzhi[fytime]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '增值服务费');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $zengzhi[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $zengzhi[shuliang]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '￥'.round($zengzhi[danjia],0));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '￥'.$zengzhi[shuliang]*$zengzhi[danjia]*$zengzhi[huilv]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $zengzhi[bizhong]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '￥'.($zengzhi[shuliang]*$zengzhi[danjia]*$zengzhi[huilv]));
			//$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '￥'.rounc(($zengzhi[shuliang]*$zengzhi[danjia]*$zengzhi[huilv]),0));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $zengzhi[xiangmu]);
			$allmoney += round($zengzhi[shuliang]*$zengzhi[danjia]*$zengzhi[huilv],0);

			$i++;
			$j++;
		}



		$objPHPExcel->getActiveSheet()->setCellValue('J'.($i+2), '本票费用合计');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.($i+2), '￥'.$allmoney);
		// if ($res->initialBalance) {
			
			$objPHPExcel->getActiveSheet()->setCellValue('J'.($i+3), '上票对账欠款');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.($i+3), '￥'.plus_minus_conversion($res->initialBalance));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.($i+4), '本票付款');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.($i+4), '￥'.$res->income);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.($i+5), '欠款合计');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.($i+5), '￥'.plus_minus_conversion(($res->initialBalance+$res->income-$allmoney)));
		// }


		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+6).':L'.($i+6));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+6), '以上各项费用请贵司仔细查收确认,如若无疑议，请尽快签字回传并安排付款,当日内不回传视同确认费用，如因付款');

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+7).':L'.($i+7));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+7), '不及时所产生的扣货费或其它一切费用,均由贵司承担!!!!谢谢合作！');

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+9).':L'.($i+9));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+9), $diqu[subject]);

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+10).':L'.($i+10));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+10), 'Bank account');

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+11).':L'.($i+11));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+11), $diqu[bank]);

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+12).':L'.($i+12));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+12), '户名（user name）  '.$diqu[bankuser]);
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+13).':L'.($i+13));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+13), '账号（account number）   '.$diqu[banknumber]);

		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+14).':L'.($i+14));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+14), 'bank of deposit：'.$diqu[bankguishu]);

		$objPHPExcel->getActiveSheet()->mergeCells('I'.($i+16).':L'.($i+16));
		$objPHPExcel->getActiveSheet()->setCellValue('I'.($i+16), '广州越洋进出口贸易有限公司');
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.($i+6).':L'.($i+16))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('A'.($i+6).':L'.($i+16))->getFont()->setBold(true)->setName('宋体')->setSize(14)->getColor()->setRGB('000000'); 


		$objDrawing = new PHPExcel_Worksheet_Drawing();
        /*设置图片路径*/
        $objDrawing->setPath('images/logo.png');
        $objDrawing->setResizeProportional(false);
        /*设置图片高度*/
        $objDrawing->setWidth(100);
        $objDrawing->setHeight(90);
        /*设置图片要插入的单元格*/
        $objDrawing->setCoordinates("A1");
        /*设置图片所在单元格的格式*/
        $objDrawing->getShadow()->setVisible(true);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

        $objDrawing1 = new PHPExcel_Worksheet_Drawing();
        /*设置图片路径*/
        $objDrawing1->setPath('images/gongzhang.png');
        /*设置图片高度*/
        $objDrawing1->setResizeProportional(true);
        $objDrawing1->setWidth(150);
        // $objDrawing1->setHeight(150);
        /*设置图片要插入的单元格*/
        $objDrawing1->setCoordinates('E'.($i+15));
        /*设置图片所在单元格的格式*/
        $objDrawing1->getShadow()->setVisible(true);
        $objDrawing1->setWorksheet($objPHPExcel->getActiveSheet());


		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);




	}elseif ($type == 4) {

	}elseif ($type == 5) {
		$title = '财务账单'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username from yasa_yunfeidan as y
			left join cuiniao_admin as a on a.id = y.luruyuanid where y.id = $id");
		$yunfeisql = $db->query("select yc.*,k.subject,k.is_merge from yasa_yunfei_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype where yid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}
		// echo '<pre>';
		// print_r($arr2);die;

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '订单编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '付款');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '付款方式');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '币种');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '业务员');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '摘要');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '日期');
		$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		$i = 2;
		foreach ($arr2 as $key => $value) {
			foreach ($value as $ke => $val){
				$str = 'dubai/';
				$str .= $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count].'CTNS/';
				$str1 = array();
				$zhuangui = 0;
				if ($val['普货']) {
					$userid = $val['普货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[]=$val['普货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['普货'][0][danjia],0);
					$zhuangui += $val['普货'][0][zhuangui];
					$huilv = $val['普货'][0][huilv];
				}
				if ($val['非普货']) {
					// $userid = $val['非普货'][0][userid];
					$userid = $val['非普货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[] =$val['非普货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['非普货'][0][danjia],0);
					$zhuangui += $val['非普货'][0][zhuangui];
					$huilv = $val['非普货'][0][huilv];
				}
				if ($val['特货']) {
					
					// $userid = $val['特货'][0][userid];
					$userid = $val['特货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[] =$val['特货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['特货'][0][danjia],0);
					$zhuangui += $val['特货'][0][zhuangui];
					$huilv = $val['特货'][0][huilv];

				}


				$str2 = implode('+',$str1);
				$str = $str.$str2;

				$zong = round($val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]+$val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]+$val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv],0);

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $yunfeidan[fahuohao]);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zong);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $yunfeidan[fahuo].'费');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $huilv == 1?'RMB':'USD');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $yunfeidan[username]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $str);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $yunfeidan[fahuotime]);

				$i++;
				if ($zhuangui != 0) {
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $userid);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $yunfeidan[fahuohao]);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zhuangui);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '装柜费');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'RMB');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $yunfeidan[username]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count].'CTNS');
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $yunfeidan[fahuotime]);

					$i++;
				}
			}
		}	

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}elseif ($type == 6) {
		$title = '账单'.date("Y-m-d");
		$filename = $title;
		// $yunfeidan = $db->get_one("select y.*,a.username from yasa_yunfeidan as y left join cuiniao_admin as a on a.id = y.luruyuanid where y.diquid = $id");

		$yunfeisql = $db->query("select yc.*,k.subject from yasa_yunfei_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype
			left join yasa_yunfeidan as y on y.id = yc.yid
			where y.diquid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}


		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('A2:P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		// 字体大小12
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:P7')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension("B:D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F:P")->setAutoSize(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);


		// 合并单元格 表头

		$objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
		$objPHPExcel->getActiveSheet()->mergeCells('B1:B2');
		$objPHPExcel->getActiveSheet()->mergeCells('C1:C2');

		$objPHPExcel->getActiveSheet()->mergeCells('D1:E1');

		$objPHPExcel->getActiveSheet()->mergeCells('F1:H1');
		$objPHPExcel->getActiveSheet()->mergeCells('I1:K1');
		$objPHPExcel->getActiveSheet()->mergeCells('L1:N1');
		$objPHPExcel->getActiveSheet()->mergeCells('O1:P1');

		

		// $objPHPExcel->getActiveSheet()->setCellValue('A1', "第（$yunfeidan[fahuohao]）货柜明细");
		// $objPHPExcel->getActiveSheet()->setCellValue('A2', "装柜时间：".$yunfeidan[fahuotime]);
		// $objPHPExcel->getActiveSheet()->setCellValue('I2', "货柜号：".$yunfeidan[huoguihao]);
		// $objPHPExcel->getActiveSheet()->setCellValue('A3', "发货公司：".$yunfeidan[fahuogongsi]);
		// $objPHPExcel->getActiveSheet()->setCellValue('I3', "货柜尺寸：".$yunfeidan[guihaochicun]);
		// $objPHPExcel->getActiveSheet()->setCellValue('A4', "船公司：".$yunfeidan[chuangongsi]);
		// $objPHPExcel->getActiveSheet()->setCellValue('I4', "封条号：".$yunfeidan[fengtiao]);
		// $objPHPExcel->getActiveSheet()->setCellValue('M5', "汇率：USD");
		// $objPHPExcel->getActiveSheet()->setCellValue('O5', $yunfeidan[huilv]);
		// $objPHPExcel->getActiveSheet()->setCellValue('P5', 1);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '序号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '货物信息');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '非普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '特货运费');
		$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '入账金额');
		$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);

		$objPHPExcel->getActiveSheet()->setCellValue('D2', "数量\n（CTNS)");
		$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('E2', "实发".$danwei1[$yunfeidan[fahuo]]."\n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setCellValue('F2', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('G2', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('H2', '总价');


		$objPHPExcel->getActiveSheet()->setCellValue('I2', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('J2', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('K2', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('L2', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('M2', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('N2', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('O2', "总运费\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('P2', "其他费用\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('P2')->getAlignment()->setWrapText(true);
		// $objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(100);

		$i = 3;
		$j = 1;
		foreach ($arr2 as $key => $value) {
			foreach ($value as $ke => $val){
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $key);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $ke);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count]);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['普货'][0][tjzl]+$val['非普货'][0][tjzl]+$val['特货'][0][tjzl]);

				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['非普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['非普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['特货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $val['特货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=ROUND(SUM(H'.$i.'+K'.$i.'+N'.$i.'),0)');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui]);


				$i ++;
				$j ++;


			}
		}
		$style_array = array(
        	'borders' => array(
            	'allborders' => array(
                	'style' => \PHPExcel_Style_Border::BORDER_THIN
            	)
       		)
        );
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,'=SUM(H3:H'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i,'=SUM(K3:K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i,'=SUM(N3:N'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=SUM(O3:O'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i,'=SUM(P3:P'.($i-1).')');

		


		$objPHPExcel->getActiveSheet()->getStyle('A1:P'.($i-1))->applyFromArray($style_array);

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

		// echo '<pre>';
		// print_r($arr2);die;
	}elseif ($type == 7) {
		if ($id != 'all') {
			$where = ' and y.muchuanid = '.$id;
		}
		$yunfei = $db->query("select y.*,sum(yc.chengben) as chengben from yasa_ylyunfeidan as y 
		left join yasa_ylyunfei_content as yc on yc.yid = y.id
		where y.is_del = 1 and y.fhtime >= '$starttime' and y.fhtime <= '$endtime'".$where);
		while ($yunfeidan = $db->fetch_array($yunfei)) {
			if ($yunfeidan[zfstatus] == 1) {
				$zongmoney = 0;
			}elseif ($yunfeidan[zfstatus] == 2) {
				$money = unserialize($yunfeidan[zfmoney]);
				$zongmoney = 0;
				foreach ($money as $key => $value) {
					$zongmoney += $value;
				}
			}else{
				$zongmoney = 0;
			}
			if ($yunfeidan[bizhong] == 'CNY') {
				$rmbmoney += $yunfeidan[chengben] - $zongmoney; 
			// echo $zongmoney;
			}elseif ($yunfeidan[bizhong] == 'IRR') {
				$irrmoney += $yunfeidan[chengben] - $zongmoney; 
			}elseif ($yunfeidan[bizhong] == 'USD') {
				$usdmoney += $yunfeidan[chengben] - $zongmoney; 
			}
		}


	
		$sql = $db->query("select yc.*,y.number,y.fhtime,y.pstime,y.bizhong,y.zfstatus,y.zfmoney,y.bizhong,k.subject from yasa_ylyunfei_content as yc 
			left join yasa_ylyunfeidan as y on yc.yid = y.id 
			left join yasa_kehutype as k on k.id = yc.kehutype
			where y.is_del = 1 and y.fhtime >= '$starttime' and y.fhtime <= '$endtime'".$where);

		
		if ($id != 'all') {
			$muchuan = $db->get_one("select * from yasa_muchuan where id = $id");
		}


		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension("A:K")->setAutoSize(true);

		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
		$objPHPExcel->getActiveSheet()->setCellValue('A1', '迪拜到德黑兰运费与服务商对账单');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', '服务商:'.$muchuan[subject]);

		$objPHPExcel->getActiveSheet()->setCellValue('A3', '序号');
		$objPHPExcel->getActiveSheet()->setCellValue('B3', '发货时间');
		$objPHPExcel->getActiveSheet()->setCellValue('C3', '派送时间');
		$objPHPExcel->getActiveSheet()->setCellValue('D3', '批次号');
		$objPHPExcel->getActiveSheet()->setCellValue('E3', '木船号');
		$objPHPExcel->getActiveSheet()->setCellValue('F3', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('G3', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('H3', '货物数量');
		$objPHPExcel->getActiveSheet()->setCellValue('I3', '产品名称');
		$objPHPExcel->getActiveSheet()->setCellValue('J3', '计量数');
		$objPHPExcel->getActiveSheet()->setCellValue('K3', '单位');
		$objPHPExcel->getActiveSheet()->setCellValue('L3', '运费总额');
		$objPHPExcel->getActiveSheet()->setCellValue('M3', '成本');
		$objPHPExcel->getActiveSheet()->setCellValue('N3', '币种');
		$objPHPExcel->getActiveSheet()->setCellValue('O3', '状态');

		$i =1;
		$j = 4;
		while ($sql1 = $db->fetch_array($sql)) {

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $sql1[fhtime]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $sql1[pstime]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $sql1[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $sql1[muchuanhao]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $sql1[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $sql1[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $sql1[count]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $sql1[proname]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$j, $sql1[sjcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $sql1[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$j, $sql1[sjcount]*$sql1[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$j, $sql1[chengben]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$j, $sql1[bizhong]);
			if ($sql1[zfstatus] == 1) {
				$msg = '未支付';
				$zongmoney = 0;
			}elseif ($sql1[zfstatus] == 2) {
				$msg = '部分支付';
				$money = unserialize($sql1[zfmoney]);
				$zongmoney = 0;
				foreach ($money as $key => $value) {
					$zongmoney += $value;
				}
				$msg .= '  已支付'.$zongmoney;
			}else{
				$zongmoney = 0;
				$msg = '已支付';
			}

			$objPHPExcel->getActiveSheet()->setCellValue('N'.$j, $msg);
			$j++;
			$i++;
		}
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($j+3), '应付未付');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.($j+3), $rmbmoney?$rmbmoney:"0".'RMB');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.($j+4), $irrmoney?$irrmoney:"0".'IRR');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.($j+5), $usdmoney?$usdmoney:"0".'USD');


		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}elseif ($type == 8) {
		$title = '账单'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username,a1.username as username1,d.strsubject from yasa_yunfeidan as y 
			left join cuiniao_admin as a on a.id = y.luruyuanid 
			left join cuiniao_admin as a1 on a1.id = y.spid
			left join yasa_diqu as d on d.id = y.diquid
			where y.id = $id");
		$yunfeisql = $db->query("select yc.*,k.subject from yasa_yunfei_chengben_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype where yid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}




		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('A2:P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		// 字体大小12
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:Q7')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension("B:D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F:Q")->setAutoSize(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');

		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('I2:P2');

		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		$objPHPExcel->getActiveSheet()->mergeCells('I3:P3');

		$objPHPExcel->getActiveSheet()->mergeCells('A4:H4');
		$objPHPExcel->getActiveSheet()->mergeCells('I4:P4');

		$objPHPExcel->getActiveSheet()->mergeCells('A5:L5');
		$objPHPExcel->getActiveSheet()->mergeCells('M5:N5');

		$objPHPExcel->getActiveSheet()->mergeCells('A6:A7');
		$objPHPExcel->getActiveSheet()->mergeCells('B6:B7');
		$objPHPExcel->getActiveSheet()->mergeCells('C6:C7');

		$objPHPExcel->getActiveSheet()->mergeCells('D6:E6');

		$objPHPExcel->getActiveSheet()->mergeCells('F6:H6');
		$objPHPExcel->getActiveSheet()->mergeCells('I6:K6');
		$objPHPExcel->getActiveSheet()->mergeCells('L6:N6');

		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->mergeCells('O6:Q6');
			
		}else{
			$objPHPExcel->getActiveSheet()->mergeCells('O6:P6');
		}



		$objPHPExcel->getActiveSheet()->setCellValue('A1', $yunfeidan[strsubject]."第（$yunfeidan[fahuohao]）货柜明细");
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "装柜时间：".$yunfeidan[fahuotime]);+
		$objPHPExcel->getActiveSheet()->setCellValue('I2', "货柜号：".$yunfeidan[huoguihao]);
		$objPHPExcel->getActiveSheet()->setCellValue('A3', "发货公司：".$yunfeidan[fahuogongsi]);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', "货柜尺寸：".$yunfeidan[guihaochicun]);
		$objPHPExcel->getActiveSheet()->setCellValue('A4', "船公司：".$yunfeidan[chuangongsi]);
		$objPHPExcel->getActiveSheet()->setCellValue('I4', "封条号：".$yunfeidan[fengtiao]);
		$objPHPExcel->getActiveSheet()->setCellValue('M5', "汇率：USD");
		$objPHPExcel->getActiveSheet()->setCellValue('O5', $yunfeidan[huilv]);
		$objPHPExcel->getActiveSheet()->setCellValue('P5', 1);

		$objPHPExcel->getActiveSheet()->setCellValue('A6', '序号');
		$objPHPExcel->getActiveSheet()->setCellValue('B6', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('C6', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('D6', '货物信息');
		$objPHPExcel->getActiveSheet()->setCellValue('F6', '普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', '非普货运费');
		$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('L6', '特货运费');
		$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'ffff00')
		        )
		    )
		);
		$objPHPExcel->getActiveSheet()->setCellValue('O6', '入账金额');
		$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray(
		    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'c4bd97')
		        )
		    )
		);

		$objPHPExcel->getActiveSheet()->setCellValue('D7', "数量\n（CTNS)");
		$objPHPExcel->getActiveSheet()->getStyle('D7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('E7', "实发".$danwei1[$yunfeidan[fahuo]]."\n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('E7')->getAlignment()->setWrapText(true);


		$objPHPExcel->getActiveSheet()->setCellValue('F7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('F7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('G7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('H7', '总价');


		$objPHPExcel->getActiveSheet()->setCellValue('I7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('I7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('J7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('K7', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('L7', "数量 \n".$danwei[$yunfeidan[fahuo]]);
		$objPHPExcel->getActiveSheet()->getStyle('L7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('M7', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('N7', '总价');

		$objPHPExcel->getActiveSheet()->setCellValue('O7', "总运费\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('O7')->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->setCellValue('P7', "其他费用\n（￥）");
		$objPHPExcel->getActiveSheet()->getStyle('P7')->getAlignment()->setWrapText(true);
		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->setCellValue('Q7', "成本\n（￥）");
			$objPHPExcel->getActiveSheet()->getStyle('Q7')->getAlignment()->setWrapText(true);
		}

		$i = 8;
		$j = 1;
		foreach ($arr2 as $key => $value) {
			foreach ($value as $ke => $val){

				if ($val['普货'][0][remake]) {
					$remake[] = $val['普货'][0][userid].':'.$val['普货'][0][remake];
				}
				if ($val['非普货'][0][remake]) {
					$remake[] = $val['非普货'][0][userid].':'.$val['非普货'][0][remake];
				}
				if ($val['特货'][0][remake]) {
					$remake[] = $val['特货'][0][userid].':'.$val['特货'][0][remake];
				}		

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $key);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $ke);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count]);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['普货'][0][tjzl]+$val['非普货'][0][tjzl]+$val['特货'][0][tjzl]);

				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['非普货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['非普货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['特货'][0][sjtjzl]);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $val['特货'][0][danjia]);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv]);

				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=ROUND(SUM(H'.$i.'+K'.$i.'+N'.$i.'),0)');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui]);

				if($yunfeidan[fahuo]=='空运'){ 
					// print_r($val);
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $val['普货'][0][xmchengben]+$val['非普货'][0][xmchengben]+$val['特货'][0][xmchengben]);
				}


				$i ++;
				$j ++;


			}
		}
		// die;
		$style_array = array(
        	'borders' => array(
            	'allborders' => array(
                	'style' => \PHPExcel_Style_Border::BORDER_THIN
            	)
       		)
        );
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,'=SUM(H8:H'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i,'=SUM(K8:K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i,'=SUM(N8:N'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i,'=SUM(O8:O'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i,'=SUM(P8:P'.($i-1).')');

		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+3),'收入');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+3),'=ROUND(SUM(O'.($i).':P'.($i).'),0)');

		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+4).':N'.($i+4));
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+4),implode(',',$remake));

		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+4),'成本');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+4),round($yunfeidan[chengben]));
		$objPHPExcel->getActiveSheet()->setCellValue('O'.($i+5),'毛利');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+5),"=ROUND(P".($i+3)."-P".($i+4).",0)");

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+6),'制单人');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($i+6),$yunfeidan[username]);

		$objPHPExcel->getActiveSheet()->setCellValue('E'.($i+6),'审批人');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($i+6),$yunfeidan[username1]);

		$objPHPExcel->getActiveSheet()->setCellValue('I'.($i+6),'总经理签字');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+6),'财务签字');
		if($yunfeidan[fahuo]=='空运'){ 
			$objPHPExcel->getActiveSheet()->getStyle('A1:Q'.($i-1))->applyFromArray($style_array);

		}else{
			$objPHPExcel->getActiveSheet()->getStyle('A1:P'.($i-1))->applyFromArray($style_array);
		}
		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}elseif ($type == 9) {
		$title = '财务账单'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username from yasa_yunfeidan as y
			left join cuiniao_admin as a on a.id = y.luruyuanid where y.id = $id");
		$yunfeisql = $db->query("select yc.*,k.subject,k.is_merge from yasa_yunfei_chengben_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype where yid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}
		// echo '<pre>';
		// print_r($arr2);die;

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '订单编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '付款');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '付款方式');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '币种');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '业务员');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '摘要');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '日期');
		$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		$i = 2;
		foreach ($arr2 as $key => $value) {
			foreach ($value as $ke => $val){
				$str = 'dubai/';
				$str .= $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count].'CTNS/';
				$str1 = array();
				$zhuangui = 0;
				if ($val['普货']) {
					$userid = $val['普货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[]=$val['普货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['普货'][0][danjia],0);
					$zhuangui += $val['普货'][0][zhuangui];
					$huilv = $val['普货'][0][huilv];
				}
				if ($val['非普货']) {
					// $userid = $val['非普货'][0][userid];
					$userid = $val['非普货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[] =$val['非普货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['非普货'][0][danjia],0);
					$zhuangui += $val['非普货'][0][zhuangui];
					$huilv = $val['非普货'][0][huilv];
				}
				if ($val['特货']) {
					// $userid = $val['特货'][0][userid];
					$userid = $val['特货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$str1[] =$val['特货'][0][sjtjzl].$danwei2[$yunfeidan[fahuo]].'*￥'.round($val['特货'][0][danjia],0);
					$zhuangui += $val['特货'][0][zhuangui];
					$huilv = $val['特货'][0][huilv];

				}


				$str2 = implode('+',$str1);
				$str = $str.$str2;

				$zong = round($val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]+$val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]+$val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv],0);

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $yunfeidan[fahuohao]);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zong);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $yunfeidan[fahuo].'费');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $huilv == 1?'RMB':'USD');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $yunfeidan[username]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $str);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $yunfeidan[fahuotime]);

				$i++;
				if ($zhuangui != 0) {
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $userid);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $yunfeidan[fahuohao]);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zhuangui);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '装柜费');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'RMB');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $yunfeidan[username]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['普货'][0][count]+$val['非普货'][0][count]+$val['特货'][0][count].'CTNS');
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $yunfeidan[fahuotime]);

					$i++;
				}
			}
		}	

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}elseif ($type == 10) {
		$title = '订单'.date("Y-m-d");
		$filename = $title;
		$cd=" where 1=1 and A.is_delete =1 and (A.iscai = 1 or A.iscai = 0) and A.isshenhe = 1";

		if($k_order_id){
			$cd.=" and A.order_id like '%".$k_order_id."%' ";
		}
		if($k_kehu_number){
			$cd.=" and B.kehu_number like '%".$k_kehu_number."%' ";
		}
		if($k_shangjia){
			$cd.=" and C.subject like '%".$k_shangjia."%' ";
		}
		if($k_fangyi){
			$cd.=" and D.nickname like '%".$k_fangyi."%' ";
		}

		if($k_addtime){
			$cd.="   and A.addtime >= ".strtotime($k_addtime);
		}
		if($k_endtime){
			$cd.="   and A.addtime <= ".strtotime($k_endtime.' 23:59:59');
		}
		$sql2 = "select 
		A.id,A.type1,A.order_id,A.type2,A.status,A.addtime,A.issend,A.problemmoney,A.problemkehu,A.problemkehufukuan,A.problemshangjia,A.other,	
		B.kehu_number as cc ,
		C.subject as dd  ,
		D.nickname as fanyi_name 
		from yasa_order as A 
		left join yasa_kehu  B on A.pid1 =B.id 
		left join yasa_shangjia C on A.pid2 =C.id 
		left join yasa_user D on A.pid3 =D.uid
		$cd  
		order by A.addtime,A.id desc";

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '订单编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '商家店名');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '翻译名称');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '总金额（元）');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '总货款错误');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '客户错误');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '客户付款错误');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '会员号错误');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '其他错误');
		$sql2 = $db->query($sql2);
		$i = 2;
		while ($sql = $db->fetch_array($sql2)) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $sql[order_id]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $sql[cc]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $sql[dd]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $sql[fanyi_name]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $sql[type1]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $sql[problemmoney]==1?'√':'');
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $sql[problemkehu]==1?'√':'');
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $sql[problemkehufukuan]==1?'√':'');
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $sql[problemshangjia]==1?'√':'');
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $sql[other]);
			$i++;
		}

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}elseif ($type == 11) {
		$title = '金蝶'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username from yasa_yunfeidan as y
			left join cuiniao_admin as a on a.id = y.luruyuanid where y.id = $id");
		$yunfeisql = $db->query("select yc.*,k.subject,k.is_merge from yasa_yunfei_content as yc 
			left join yasa_kehutype as k on k.id = yc.khtype where yid = $id");
		$arr =array();
		while ($yunfei = $db->fetch_array($yunfeisql)) {
			$arr[$yunfei[subject]][] = $yunfei;
		}
		foreach ($arr as $key => $value) {
			foreach ($value as $ke => $val) {
				$arr1[$key][$val[userid]][] = $val;
			}
		}
		foreach ($arr1 as $key => $value) {
			foreach ($value as $ke => $val) {
				foreach ($val as $k => $v) {
					$arr2[$key][$ke][$v[protype]][]= $v;
				}
			}
		}
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '日期');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '凭证字');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '凭证号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '附件数');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '分录序号');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '摘要');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '科目代码');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '科目名称');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '借方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '贷方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '客户');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '职员');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '项目');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '部门');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '存货');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', '是否限定');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', '自定义辅助核算类别');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', '自定义辅助核算编码');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', '自定义辅助核算类别1');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', '自定义辅助核算编码1');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', '原币金额');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', '币别');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', '汇率');
		$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		$i = 2;
		foreach ($arr2 as $key => $value) {
		
			foreach ($value as $ke => $val){
				if ($val['普货']) {
					$userid = $val['普货'][0][is_merge] == 1 ? $val['普货'][0][userid]:$key;
					$daima = $val['普货'][0][is_merge] == 1 ? '2203':'11220101';
					$daima1 = $val['普货'][0][is_merge] == 1 ? '50010201':'50010202';
					$daima2 = $val['普货'][0][is_merge] == 1 ? '505102':'505102';
				}
				if ($val['非普货']) {
					$userid = $val['非普货'][0][is_merge] == 1 ? $val['非普货'][0][userid]:$key;
					$daima = $val['非普货'][0][is_merge] == 1 ? '2203':'11220101';
					$daima1 = $val['非普货'][0][is_merge] == 1 ? '50010201':'50010202';
					$daima2 = $val['非普货'][0][is_merge] == 1 ? '505102':'505102';

				}
				if ($val['特货']) {
					$userid = $val['特货'][0][is_merge] == 1 ? $val['特货'][0][userid]:$key;
					$daima = $val['特货'][0][is_merge] == 1 ? '2203':'11220101';
					$daima1 = $val['特货'][0][is_merge] == 1 ? '50010201':'50010202';
					$daima2 = $val['特货'][0][is_merge] == 1 ? '505102':'505102';

				}

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fahuotime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.$yunfeidan[fahuo].'费'.$yunfeidan[fahuohao]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, round($val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]+$val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]+$val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv],0));
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fahuotime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.$yunfeidan[fahuo].'费'.$yunfeidan[fahuohao]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima1);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, round($val['普货'][0][danjia]*$val['普货'][0][sjtjzl]*$val['普货'][0][huilv]+$val['非普货'][0][danjia]*$val['非普货'][0][sjtjzl]*$val['非普货'][0][huilv]+$val['特货'][0][danjia]*$val['特货'][0][sjtjzl]*$val['特货'][0][huilv],0));
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;

				if (($val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui]) != 0) {
					$s = $yunfeidan[fahuo] == '海运'?'装柜':'其他';
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fahuotime]);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.$s.'费'.$yunfeidan[fahuohao]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, round($val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui],0));
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
					$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
					$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
					$i++;

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fahuotime]);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.$s.'费'.$yunfeidan[fahuohao]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima2);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, round($val['普货'][0][zhuangui]+$val['非普货'][0][zhuangui]+$val['特货'][0][zhuangui],0));
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
					$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
					$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
					$i++;
				}
			}
		}

		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx');

        header('Cache-Control: max-age=1');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		// header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		// header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}elseif ($type == 12) {
		$title = '增值服务费'.date("Y-m-d");
		$filename = $title;
		$cd=" where 1";

		if($keyword){
			$cd.=" and z.userid like '%".$keyword."%' ";
		}
		if($khgs){
			$cd.=" and z.khtype = ".$khgs;
		}
		$sql2 = "select z.*,k.subject from yasa_zengzhi as z left join yasa_kehutype as k on k.id = z.khtype  $cd  order by z.id desc";
		$sql = $db->query($sql2);
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '费用产生日期');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '发货票号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '仓库类型');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '客户归属');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '客户唛头');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '订单号');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '单号');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '柜号');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '成本');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '成本计算方式');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '总箱数/件数');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '打包箱数/件数');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '服务项目');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '费用合计');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', '汇率');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', '计费时段');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', '币种');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', '备注');
		$i = 2;
		while ($zengzhi = $db->fetch_array($sql)) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $zengzhi[fytime]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $zengzhi[piaohao]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $zengzhi[is_cang]==1?'国内':'国外');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $zengzhi[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $zengzhi[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $zengzhi[orderid]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $zengzhi[danhao]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $zengzhi[guihao]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $zengzhi[chengben]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $zengzhi[chengbenjisuan]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $zengzhi[gongyingshang]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $zengzhi[count]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $zengzhi[dbcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $zengzhi[xiangmu]);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $zengzhi[money]);
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $zengzhi[shuliang]);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $zengzhi[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $zengzhi[huilv]);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $zengzhi[jifeitime]);
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $zengzhi[bizhong]);
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $zengzhi[remake]);
			$i++;
		}
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx');

        header('Cache-Control: max-age=1');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		// header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		// header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}elseif ($type == 13) {
		$title = '金蝶'.date("Y-m-d");
		$filename = $title;
		$yunfeidan = $db->get_one("select y.*,a.username,a1.username as username1,m.subject from yasa_ylyunfeidan as y 
		left join cuiniao_admin as a on y.luruyuanid = a.id 
		left join cuiniao_admin as a1 on a1.id = y.spid 
		left join yasa_muchuan as m on m.id = y.muchuanid
		where y.id = '$id'");
		$yunfeisql = $db->query("select yc.*,k.subject,y.bizhong,y.huilv,k.is_merge from yasa_ylyunfei_content as yc
		left join yasa_kehutype as k on k.id = yc.kehutype 
		left join yasa_ylyunfeidan as y on y.id = yc.yid
		where yc.yid = $id");

		
		// print_r($yunfeidan);
		// print_r($arr1);die;
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '日期');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '凭证字');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '凭证号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '附件数');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '分录序号');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '摘要');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '科目代码');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '科目名称');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '借方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '贷方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '客户');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '职员');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '项目');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '部门');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '存货');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', '是否限定');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', '自定义辅助核算类别');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', '自定义辅助核算编码');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', '自定义辅助核算类别1');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', '自定义辅助核算编码1');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', '原币金额');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', '币别');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', '汇率');
		$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		$i = 2;
		while($val = $db->fetch_array($yunfeisql)){
			$userid = $val[is_merge] == 1 ? $val[userid]:$val[subject];
			if ($val[is_merge] == 1) {
				$daima1 = '2203';
				$daima = '50010201';
			}else{
				$daima1 = '11220101';
				$daima = '50010202';
			}
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fhtime]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.'迪拜-德黑兰运费'.$yunfeidan[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima1);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val[danjia]*$val[sjcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
			$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
			$i++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fhtime]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.'迪拜-德黑兰运费'.$yunfeidan[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val[danjia]*$val[sjcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
			$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
			$i++;
			if ($val[other] != 0) {
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fhtime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.'迪拜-德黑兰其他费用'.$yunfeidan[number]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, round($val[other],0));
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $yunfeidan[fhtime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '应收'.$userid.'迪拜-德黑兰其他费用'.$yunfeidan[number]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $daima1);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, round($val[other],0));
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;
			}
		}
		

		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx');

        header('Cache-Control: max-age=1');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		// header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		// header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}elseif ($type == 14) {
		$title = '金蝶'.date("Y-m-d");
		$filename = $title;
		$zengzhi = $db->query("select z.*,k.subject,k.is_merge from yasa_zengzhi as z left join yasa_kehutype as k on k.id = z.khtype where z.fytime ='$cstime' and z.is_del = 1");
		$zdan = $db->get_one("select * from yasa_zengzhiorder where cstime = '$cstime'");
	
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '日期');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '凭证字');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '凭证号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '附件数');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '分录序号');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '摘要');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '科目代码');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '科目名称');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '借方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '贷方金额');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '客户');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '职员');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '项目');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '部门');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '存货');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', '是否限定');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', '自定义辅助核算类别');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', '自定义辅助核算编码');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', '自定义辅助核算类别1');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', '自定义辅助核算编码1');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', '原币金额');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', '币别');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', '汇率');
		$objPHPExcel->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		$i = 2;
		while($val = $db->fetch_array($zengzhi)){
			// print_r($val);
			if ($val[money] != 0) {
				if ($val[is_merge]== 1) {
					$userid = $val[userid];
					if ($val[xiangmutype] == 1) {//卸货
						$jie = '2203';
						$dai = '505106';
					}elseif ($val[xiangmutype]==2) {//派送
						$jie = '2203';
						$dai = '505106';
					}elseif ($val[xiangmutype]==3) {//打包
						$jie = '2203';
						$dai = '505102';
					}elseif ($val[xiangmutype]==4) {//仓储
						$jie = '2203';
						$dai = '505107';
					}
				}else{
					$userid = $val[subject];
					if ($val[xiangmutype] == 1) {//卸货
						$jie = '11220101';
						$dai = '505106';
					}elseif ($val[xiangmutype]==2) {//派送
						$jie = '11220101';
						$dai = '505106';
					}elseif ($val[xiangmutype]==3) {//打包
						$jie = '11220101';
						$dai = '505102';
					}elseif ($val[xiangmutype]==4) {//仓储
						$jie = '11220101';
						$dai = '505107';
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $zdan[cstime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[xiangmu].$zdan[danhao]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $jie);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val[money]);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $zdan[cstime]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '记');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '1111');
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '1000');
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[xiangmu].$zdan[danhao]);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $dai);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val[money]);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $userid);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '1');
				$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, 'RMB');
				$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '1');
				$i++;
			}
			
		}


		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx');

        header('Cache-Control: max-age=1');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		// header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		// header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}

 ?>