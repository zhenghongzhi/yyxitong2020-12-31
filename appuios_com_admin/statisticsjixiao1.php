<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	// include_once("checkuser.php");
	$setting=$db->get_one("select * from yy_setting where id = 1");
	$starttime = date("Y-m-01",strtotime($time));
	$endtime = date('Y-m-d',strtotime(date("Y-m-01",strtotime("+1 month",strtotime($starttime))))-1);
	if ($type == 1) {
		$where = "1 and o.is_delete = 1 and w.is_del = 1 and o.addtime >= 1580486400";
		if ($name) {
			$where .= " and u.username like '%".$name."%'";
		}
		$where .= " and w.chucangtime BETWEEN '$starttime' and '$endtime'";
		$sql = $db->query("select w.orderid,w.money,w.userid,w.jincangdanhao,u.id as uids,u.username,u.nickname,o.yongjin,w.chucangtime,o.bili,o.hybili1,o.kybili1,o.hybili2,o.kybili2,o.type1 from yasa_warehouse as w 
			left join yasa_order as o on w.orderid = o.order_id 
			left join yasa_user as u on u.uid = o.pid3 where $where");
	}elseif ($type == 2) {
		$where1 = "1 and y.is_del = 1 and o.is_delete = 1 and w.is_del = 1 and o.addtime >= 1580486400";
		$where1 .= " and y.fahuotime BETWEEN '$starttime' and '$endtime'";
		if ($name) {
			$where1 .= " and u.username like '%".$name."%'";
		}

		if ($fahuo) {
			$where1 .= " and y.fahuo = '$fahuo'";
		}
		$sql1 = $db->query("select w.userid,w.userid,w.orderid,w.money,w.fahuo,w.weight,w.volume,w.chucangtime,w.jincangdanhao,u.id as uids,u.username,u.nickname,o.yongjin,k.is_merge,y.fahuotime,y.fahuohao,o.bili,o.hybili1,o.kybili1,o.hybili2,o.kybili2,o.type1 from yasa_warehouse as w 
			left join yasa_order as o on w.orderid = o.order_id 
			left join yasa_yunfeidan as y on y.fahuohao = w.fahuohao
			left join yasa_user as u on u.uid = o.pid3 
			left join yasa_kehutype as k on k.id = w.khtype
			where $where1");
	}
 ?>
 <html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/layer/layer.js"></script>
	</head>
	<body>
		<style>	
			.k{
				/*width: 640px;*/
				margin: 0px auto;
				text-align: center;
				padding: 0px;
				font-size: 12px;
			}
			.k td,.k th{
				border-color: #f0f0f0;
				width: 50px;
    			height: 30px;
			}

			h2{
				text-align: center;	
			}
			input{
				width: 50px;
			}
			.noborder{
				border: 0px !important;
			}
		</style>
		<h2><?=$time;?><?=$name;?><?=$type==2?$fahuo:'佣金'; ?>明细</h2>
		<form id="form">
			<div class="content">
			<p style="text-align: right; margin-right: 200px;"></p>
			<table cellpadding="0" cellspacing="0" border="1" class="k" style="border:2px solid #000;">
				<?php if ($type == 1) { ?>
					<tr>
						<th style="width: 120px;">入仓号</th>
						<th style="width: 120px;">客户编号</th>
						<th style="width: 120px;">订单号</th>
						<th style="width: 120px;">总金额</th>
						<th style="width: 120px;">出仓金额</th>
						<th style="width: 120px;">实收佣金</th>
						<th style="width: 120px;">佣金比例/提成比例</th>
						<th style="width: 120px;">提成金额</th>
						<th style="width: 120px;">出仓时间</th>
					</tr>
					<?php 
						$all = 0;
					while ($s = $db->fetch_array($sql)) { 
						$bili = $s[bili]?$s[bili]:$setting[bili];
						$all += round($s[money]*$s[yongjin]*$bili/10000,2);
						?>
						<tr>
							<td><?=$s[jincangdanhao]; ?></td>
							<td><?=$s[userid]; ?></td>
							<td><?=$s[orderid]; ?></td>
							<td><?=$s[type1]; ?></td>
							<td><?=$s[money]; ?></td>
							<td><?=round($s[money]*$s[yongjin]/100,2); ?></td>
							<td><?=$s[yongjin]; ?>%/<?=$bili; ?>%</td>
							<td><?=round($s[money]*$s[yongjin]*$bili/10000,2); ?></td>
							<td><?=$s[chucangtime]; ?></td>

						</tr>
					<? } ?>
					<tr>
						<td>合计</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><?=$all; ?></td>
						<td></td>
					</tr>
				<? }elseif ($type == 2) { ?>
					<tr>
						<th style="width: 120px;">发货号</th>
						<th style="width: 120px;">入仓号</th>
						<th style="width: 120px;">客户编号</th>
						<th style="width: 120px;">订单号</th>
						<th style="width: 120px;">总金额</th>
						<th style="width: 120px;">出仓金额</th>
						<th style="width: 120px;"><?=$fahuo=='海运'?"体积":"重量"; ?></th>
						<th style="width: 120px;">提成基数</th>
						<th style="width: 120px;">提成金额</th>
						<th style="width: 120px;">出仓时间</th>
					</tr>
					<?php 
						$all = 0;
					while ($s = $db->fetch_array($sql1)) { 
						if ($s[is_merge] == 1) {
							$bili1 = $s[hybili1]?$s[hybili1]:$setting[hybili1];
							$bili2 = $s[kybili1]?$s[kybili1]:$setting[kybili1];
							if ($fahuo == '海运') {
								$hyticheng1 += $s[volume] * $bili1;
							}elseif ($fahuo == '空运') {
								$kyticheng1 += $s[weight] * $bili2;
							}
						}else{
							$bili1 = $s[hybili2]?$s[hybili2]:$setting[hybili2];
							$bili2 = $s[kybili2]?$s[kybili2]:$setting[kybili2];
							if ($fahuo == '海运') {
								$hyticheng1 += $s[volume] * $bili1;
							}elseif ($fahuo == '空运') {
								$kyticheng1 += $s[weight] * $bili2;
							}
						}
						if ($fahuo == '海运') {
							$all += round($s[volume]*$bili1,2);
						}else{
							$all += round($s[weight]*$bili2,2);
						}
						
						?>
						<tr>
							<td><?=$s[fahuohao]; ?></td>
							<td><?=$s[jincangdanhao]; ?></td>
							<td><?=$s[userid]; ?></td>
							<td><?=$s[orderid]; ?></td>
							<td><?=$s[type1]; ?></td>
							<td><?=$s[money]; ?></td>
							<td><?=$fahuo=='海运'?$s[volume]:$s[weight]; ?></td>
							<td><?=$fahuo=='海运'?$bili1:$bili2; ?></td>
							<td><?=$fahuo=='海运'?round($s[volume]*$bili1,2):round($s[weight]*$bili2,2); ?></td>
							<td><?=$s[chucangtime]; ?></td>

						</tr>
					<? } ?>
						<tr>
							<td>合计</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><?=$all; ?></td>
							<td></td>

						</tr>

				<? } ?>
					

				
				
			</table>

			</div>
			
		</form>


	</body>
</html>