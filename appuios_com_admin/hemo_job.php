<?
session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");
include_once("checkuser.php");

$pagename = "招聘信息管理";

$ename = "招聘信息";

$table = "cuiniao_job";

$uses = "显示";

$nouses = "隐藏";

$hasisvalid = "1";
$hasorders = "1";



if($act == 'add'){
	$burl = " - 添加".$ename;
}elseif($act == 'edit'){
	$burl = " - 编辑".$ename;
}

if($keyword){
	$burl = " - 搜索结果";
}

$isfile=1; //是否有上传文件

$do_search="1";

if($act == 'edits'){

	$arr[0]=array($subject,"招聘岗位",'s',255);
	$arr[1]=array($nums,"招聘人数",'s',255);
	$arr[2]=array($address,"工作地点",'s',255);
	$arr[3]=array($endtime,"有效期",'s',255);
	$arr[4]=array($intro,"工作职责");
	$arr[5]=array($content,"岗位要求");
	
	if($hasorders){
		$arr[6]=array($orders,"排序号",'n',11);
	}
	
	$msg = upc($arr);

	$msg?er($msg,2):NULL;
	
	$intro = nl2br($intro);
	$content = nl2br($content);
	$db->query("update $table set 
	subject='$subject' , 
 	isvalid='$isvalid', 
 	orders='$orders', 
	intro='$intro',
	nums='$nums',
	endtime='$endtime',
	address='$address',
 	content='$content'
	where id='$id'");
	
	
	
	$links[0]['text']="返回列表";
	$links[0]['href']="?pid=$pid22&page=$page&npid=$npid&keyword=".urlencode($keyword);
	$links[1]['text']="查看编辑结果";
	$links[1]['href']="?act=edit&id=$id&page=$page&npid=$npid&pid=$pid22&keyword=".urlencode($keyword);
	
	er("编辑成功",0,$links);

}elseif($act == 'adds'){
	
	$arr[0]=array($subject,"招聘岗位",'s',255);
	$arr[1]=array($nums,"招聘人数",'s',255);
	$arr[2]=array($address,"工作地点",'s',255);
	$arr[3]=array($endtime,"有效期",'s',255);
	$arr[4]=array($intro,"工作职责");
	$arr[5]=array($content,"岗位要求");
	
	if($hasorders){
		$arr[6]=array($orders,"排序号",'n',11);
	}
	
	$msg = upc($arr);
	
	$msg?er($msg,2):NULL;
    
	$intro = nl2br($intro);
	$content = nl2br($content);
	$sql = get_cname($table);
	eval("\$sql=\"$sql\";");
	$db->query($sql);
	
	$links[0]['text']="返回列表";
	$links[0]['href']="?";
	$links[1]['text']="继续添加";
	$links[1]['href']="?act=add";
	
	er("添加成功",0,$links);

	
}elseif($act == 'del'){
	if(!empty($moderate)){
		if(is_array($moderate)) $moderate =implode('\',\'', $moderate);
		
		$ids=str_replace("'","",$moderate);
		
		$db->query("delete from $table where id in($ids)");
		$links[0]['text']="返回列表";
		$links[0]['href']="?pid=$pid";
		er("删除成功",0,$links);
		
	}else{
		$links[0]['text']="返回列表";
		$links[0]['href']="?pid=$pid";
		er("请选择要删除的记录",2,$links);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>管理中心 - <?=$pagename?> </title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/calendar.css" rel="stylesheet" type="text/css" />
<script src="js/utils.js"></script>
<script type="text/javascript" src="js/calendar.js" ></script>  
<script type="text/javascript" src="js/calendar-zh.js" ></script>
<script type="text/javascript" src="js/calendar-setup.js"></script>
<style type="text/css">
<!--
.STYLE1 {color: #FF0000}
-->
</style>
</head>
<body>
<h1>
<span class="action-span" onclick="window.location='?<?=empty($act)?"act=add&pid=$pid&npid=$npid":"pid=$pid&npid=$npid"?>'" style="cursor:hand;">
<?
if(empty($act) && empty($keyword)){
?>
<a href="?act=add">添加<?=$ename?></a>
<?
}else{
?>
<a href="?">返回列表</a>
<?
}
?>
</span>
<span class="action-span1"><a href="admin_index.php" >管理中心</a>  - <?=$pagename?><?=$burl?> </span>
<div style="clear:both"></div>
</h1>
<?
if($do_search){
?>
<div class="form-div">
  <form action="?acts=search" name="searchForm" method="get">
  <input type="hidden" name="acts" value="search" />
  <input type="hidden" name="pid" value="<?=$pid?>" />
  <input type="hidden" name="pagesizes" value="<?=$pagesizes?>" />
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    标题 
    <input type="text" name="keyword" value="<?=$keyword?>" size="15" />
    <input type="submit" value=" 搜索 " class="button" />
  </form>
</div>
<?
}
if(empty($act) || ($keyword && $act!='edit')){
?>
<form method="post" action="?act=del" name="listForm">
<input type="hidden" name="pid" value="<?=$pid?>" />
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
<tr>
<th width="5%" align="center"><nobr>选择</nobr></th>
<th width="13%">招聘岗位</th>
<th width="13%">招聘人数</th>
<th width="13%">工作地点</th>
<th width="13%">排 序 号</th>
<th width="13%">状态</th>
<th width="13%">有效期</th>
</tr>

<?

$cd=" where 1=1 ";

if($keyword){
	$cd.=" and subject like '%".$keyword."%' ";
}

$sql = "select count(*) as c from $table $cd ";

if($hasorders){
	$order_str = " orders desc, ";
}
$sql2 = "select * from $table $cd order by $order_str id desc";
$baselink ="?keyword=".urlencode($keyword)."&pagesizes=$pagesizes&pid=$pid&npid=$npid";
$pages="";
$counts=0;
$query = cp($sql,$sql2,$pagesize,$baselink,$pages,$counts);
while($a=$db->fetch_array($query)){
?>
   <tr>
<td align="center"><input type="checkbox" name="moderate[]" value="<?=$a['id']?>"></td>
<td height="22" align="center">&nbsp;<span> 
<a href="?act=edit&id=<?=$a['id']?>&pid=<?=$pid?>&page=<?=$page?>&acts=<?=$acts?>&keyword=<?=urlencode ($keyword)?>&npid=<?=$npid?>"><?=$a['subject']?></a></span> </td>
<td align="center"><?=$a['nums']?></td>
<td align="center"><?=$a['address']?></td>
<td align="center"><?=$a['orders']?></td>
<td align="center"><?=$a['isvalid']==1?"$uses":"<font color=red>$nouses</font>"?></td>
   <td align="center"><?=$a['endtime']?></td>
   </tr>
<?
}
if($counts>0){
?>	
<tr><td colspan="9"><table width="100%" border="0" cellspacing="0" cellpadding="0"> 
     <tr> 
       <td width="31%" bgcolor="#FFFFFF" style="padding-left:17px;"><input type="checkbox" name="chkall" onClick="javascript:CheckAll(this.form)"> <input name="Submit4" type="submit" class="button" value="执行删除" /></td> 
       <td width="69%" align="right" style="padding-right:10px;"><?=$pages?></td> 
     </tr> 
   </table></td></tr>
<?
}else{
?>
<tr><td colspan="9" height="30" style=" padding:10px 0px 10px 30px; color:#660000"><?=$keyword?"搜索不到相关记录":"暂时没有记录"?></td></tr>
<?
}
?>
  </table>
</div>
</form>
<?
}elseif($act == 'edit'){
	$a=$db->get_one("select * from $table where id='$id' ");
?>
<form action="?act=edits" method="post" enctype="multipart/form-data" name="listForm" id="listForm" <?=$isfile?'enctype="multipart/form-data"':''?>>
<input type="hidden" id="id" name="id" value="<?=$id?>" />
<input type="hidden" id="page" name="page" value="<?=$page?>" />
<input type="hidden" id="keyword" name="keyword" value="<?=$keyword?>" />
<input type="hidden" id="acts" name="acts" value="<?=$acts?>" />
  <div class="list-div" id="div">
    <table width="100%" cellspacing="1" cellpadding="2" id="list-table">
      <tr>
        <th colspan="2" align="left">编辑<?=$ename?></th>
      </tr>
      <tr>
<td align=right>招聘岗位：</td>
<td><input name="subject" type="text"  class='input'  id="subject" value="<?=$a['subject']?>" /></td>
</tr>
      <tr>
        <td align=right>招聘人数：</td>
        <td><input name="nums" type="text"  class='input'  id="nums" value="<?=$a['nums']?>" /></td>
      </tr>	

<tr>
<td align=right>状　　态：</td>
<td>
<select name="isvalid" id="isvalid">
 
           <option value="1" <?=$a['isvalid']==1?"selected":""?>><?=$uses?></option>
 
<option value="0" <?=$a['isvalid']==0?"selected":""?>><?=$nouses?></option>
         </select></td>
</tr>
<tr>
<td align=right>排 序 号：</td><td><input name="orders" type="text"  class='input'  id="orders" value="<?=$a['orders']?>" /></td>
</tr>
<tr>
  <td align="right">工作地点：</td>
  <td><input name="address" type="text"  class='input'  id="address" value="<?=$a['address']?>" /></td>
</tr>
<tr>
  <td align="right">有效期：</td>
  <td><input class="input" type="text" name="endtime" id="endtime" onclick="return showCalendar('endtime', 'y-mm-dd');" value="<?=$a['endtime']?>" /></td>
</tr>
<tr>
  <td align="right">工作职责：</td>
  <td><textarea name="intro" cols="50" rows="7" id="intro"><?=$a['intro']?></textarea></td>
</tr>

<tr>
<td align=right valign=top>岗位要求：</td>
<td><textarea name="content" cols="50" rows="7" id="content"><?=$a['content']?>
</textarea></td>
</tr>

      <tr>
        <td>&nbsp;</td>
        <td><input name="Submit3" type="submit" class="button" value="提交" onclick="return checkfill()" />
 　          
 <input name="Submit22" type="reset" class="button" value="重置"/></td>
      </tr>
    </table>
  </div>
</form>
<?
}elseif($act == 'add'){
?>

<form method="post" action="?act=adds" <?=$isfile?'enctype="multipart/form-data"':''?>  name="listForm">
<div class="list-div" id="listDiv">
<table width="100%" cellspacing="1" cellpadding="2" id="list-table">
  <tr>
    <th colspan="2" align="left">添加<?=$ename?></th>
  </tr>
  <tr>
<td align=right>招聘岗位：</td>
<td><input name="subject" type="text"  class='input'  id="subject" /></td>
</tr>
      <tr>
        <td align="right">招聘人数：</td>
        <td><input name="nums" type="text"  class='input'  id="nums" /></td>
      </tr>

<tr>
<td align=right>状　　态：</td>
<td>
<select name="isvalid" id="isvalid">
 
           <option value="1"><?=$uses?></option>
 
<option value="0"><?=$nouses?></option>
         </select></td>
</tr>
<tr>
<td align=right>排 序 号：</td><td><input name="orders" type="text"  class='input'  id="orders" value="0" /></td>
</tr>
<tr>
  <td align="right">工作地点：</td>
  <td><input name="address" type="text"  class='input'  id="address" /></td>
</tr>
<tr>
  <td align="right">有效期：</td>
  <td><input class="input" type="text" name="endtime" id="endtime" onclick="return showCalendar('endtime', 'y-mm-dd');" /></td>
</tr>
<tr>
  <td align="right">工作职责：</td>
  <td><textarea name="intro" cols="50" rows="7" id="intro"></textarea></td>
</tr>

<tr>
<td align=right valign=top>岗位要求：</td>
<td><textarea name="content" cols="50" rows="7" id="content"></textarea></td>
</tr>

  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="Submit" value="提交" class="button" onclick="return checkfill()"  />
       　 
       <input type="reset" name="Submit2" value="重置" class="button"/></td>
  </tr>
</table>
</div>
</form>
<?
}
include_once("foot.php");
?>
</body>
</html>