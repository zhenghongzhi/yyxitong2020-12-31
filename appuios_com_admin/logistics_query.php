<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

$kehu_number = isset($_GET["kehu_number"]) ? $_GET["kehu_number"] : null;

if($kehu_number) {

	$db->query("SET NAMES UTF8");
	$result = $db->query("SELECT id, pid1, order_id, type2, addtime FROM yasa_order WHERE pid1 = (SELECT id FROM yasa_kehu WHERE kehu_number = \"$kehu_number\") ORDER BY addtime DESC");

}

?>

<head>
	<style type="text/css">
		.main { text-align: center; }
		table { margin: auto; }
		table tr th { width: 200px; }
	</style>
</head>

<div class="main">

	<form action="">
		<input type="text" name="kehu_number" placeholder="请输入客户编号">
		<input type="submit" name="提交">
	</form>
	<br>

	<? if($kehu_number){ ?>

	<table border="1">
		<caption>物流状态查询<br>logistics Status</caption>
		<thead>
	      <tr>
	        <th>订单号<br>Order No.</th>
	        <th>商品数量<br>Quantity</th>
	        <th>下单日期<br>Ordr Date</th>      
	      </tr>
	    </thead>
	    <tbody>
	    	<?
	    		while($row = $db->fetch_array($result)) {
		    		echo "<tr>";
		    		echo "<td>";
		    		echo "<a target=\"_blank\" href=\"logistics_query_info.php?orderId=".$row["id"]."&kehuId=".$row["pid1"]."\">".$row["order_id"]."</a>";
		    		echo "</td>";
		    		echo "<td>";
					echo $row["type2"];
		    		echo "</td>";
		    		echo "<td>";
					echo date("Y-m-d", $row["addtime"]);
		    		echo "</td>";
		    		echo "</tr>";
	    		}
	    	?>
	    </tbody>
	</table>

	<? } ?>
</div>
