<?

// if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
// 	exit();
// }
header('Content-Type: text/html;charset=utf-8');
header('Access-Control-Allow-Origin:*'); // *代表允许任何网址请求
header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); // 允许请求的类型

header('Access-Control-Allow-Headers: Content-Type,Content-Length,Accept-Encoding,X-Requested-with, Origin ,X-TOKEN, Accept, Authorization'); // 设置允许自定义请求头的字段


// if($_SERVER['REQUEST_METHOD']=='OPTIONS'){
//     //exit是用来结束程序执行的,如果参数是字符串，PHP将会直接把字符串输出，
//     //如果参数是整型（范围是0-254），那个参数将会被作为结束状态使用。
//     exit('options类型的请求，结束');
// }

function runtime($mode=0)   {
    static   $t;  
    if(!$mode)   {  
        $t   =   microtime();
        return;
    }  
    $t1   =   microtime();  
    list($m0,$s0)   =   explode("   ",$t);  
    list($m1,$s1)   =   explode("   ",$t1);  
    return   sprintf("%.3f ms",($s1+$m1-$s0-$m0)*1000);
}
include_once("../include/common.ini.php");

$userId = isset($_GET["userId"]) ? $_GET["userId"] : null;
$yasa_product_ruku = 'yasa_product_ruku';
$yasa_order = 'yasa_order';
$yasa_kehu = 'yasa_kehu';
$yasa_product_zhuangui_huowu = 'yasa_product_zhuangui_huowu';
$yasa_product_zhuangui = 'yasa_product_zhuangui';
$yasa_product_ruku = 'yasa_product_ruku';
$yasa_product = 'yasa_product';
$yasa_product_xiangzi_huowu = 'yasa_product_xiangzi_huowu';
if ($userId) {
	$uesrsql = $db->query("SELECT id FROM $yasa_kehu WHERE kehu_number = '$userId'");

	$counts = $db->num_rows($uesrsql);
	
	if ($counts > 0) {
		// 用户表
		$user = $db->fetch_array($uesrsql);
		/*
			type1 总金额
			type13 实际金额
			type5 定金

		 */
		// echo $user['id'];die;
		$ordersql = $db->query("SELECT id,order_id,type1 as zongjia,type13 as shijijia,type5 as dingjin,addtime FROM $yasa_order WHERE pid1 = '$user[id]' order by addtime desc");
		if ($db->num_rows($ordersql) > 0) {
			$orderarr = array();
			while ($order = $db->fetch_array($ordersql)) {
				$orderid[] = $order[id];
				$orderarr[$order[id]]['zongjia'] = $order[zongjia];
				$orderarr[$order[id]]['shijijia'] = $order[shijijia];
				$orderarr[$order[id]]['dingjin'] = $order[dingjin];
				$orderarr[$order[id]]['order_id'] = $order[order_id];
				$orderarr[$order[id]]['addtime'] = date("Y-m-d",$order[addtime]);
			}
				
			$orderidstr = implode(',',$orderid);
			// $zongsql = "select R.pid1,R.status,A.type1 as xianzi_shuliang,E.type5 as product_price from yasa_product_xiangzi_huowu as A 
			// left join yasa_product as E on E.id = A.pid3 
			// left join yasa_product_ruku as R on R.only_id = A.pid1 
			// where 1=1 and A.is_delete =1 and R.status >=2 and R.pid1 in($orderidstr)";
			// runtime();
			$zongsql = "select R.pid1,R.status,A.type1 as xianzi_shuliang,E.type5 as product_price from yasa_product_xiangzi_huowu as A 
			left join yasa_product as E on E.id = A.pid3 
			left join yasa_product_ruku as R on R.only_id = A.pid1 
			where 1=1 and A.is_delete =1 and R.status >=2 and R.pid1 in(SELECT id FROM $yasa_order WHERE pid1 = $user[id])";
			$zongsql1 = $db->query($zongsql);

			
			$zongarr = array();
			// echo runtime(1);
			while ($zong = $db->fetch_array($zongsql1)) {
				$zongjia = 0;
				$zongjia +=$zong['xianzi_shuliang']*$zong['product_price']+0;
				$zongarr[$zong[pid1]] += $zongjia;
			}
			$db->free_result($zongsql1);

			// $zhuanguisql = "select c.id,a.type1 as guihao from yasa_product_zhuangui AS a
			// LEFT JOIN yasa_product_zhuangui_huowu AS b on a.only_id = b.pid1
			// LEFT JOIN yasa_order as c on b.pid2 = c.id 
			// WHERE c.id in($orderidstr) group by a.type1,c.id";
			$zhuanguisql = "select c.id,a.type1 as guihao from yasa_product_zhuangui AS a
			LEFT JOIN yasa_product_zhuangui_huowu AS b on a.only_id = b.pid1
			LEFT JOIN yasa_order as c on b.pid2 = c.id 
			WHERE c.id in(SELECT id FROM $yasa_order WHERE pid1 = $user[id]) group by a.type1,c.id";

			
			$zhuanguisql1 = $db->query($zhuanguisql);
			$zhuanguiarr = array();
			while ($zhuangui = $db->fetch_array($zhuanguisql1)) {
				$zhuanguiarr[$zhuangui[id]][] = $zhuangui[guihao];
			}
			$db->free_result($zhuanguisql);


			$count = count($orderid);
			$data = array();
			// print_r($zongarr);exit;
			foreach ($orderid as $key => $value) {
				$data[$key][id] = $value;
				$data[$key][order_id] = $orderarr[$value][order_id];
				$data[$key][zongjia] = $orderarr[$value][zongjia];
				$data[$key][shijijia] = $orderarr[$value][shijijia];
				$data[$key][dingjin] = $orderarr[$value][dingjin];
				$data[$key][addtime] = $orderarr[$value][addtime];
				$data[$key][yifu] = $zongarr[$value];
				$data[$key][guihao] = implode(',',$zhuanguiarr[$value]);
				if ($orderarr[$value][shijijia]) {
					$data[$key][weikuan] = $orderarr[$value][shijijia] - $orderarr[$value][dingjin];
				}else{
					$data[$key][weikuan] = $orderarr[$value][zongjia] - $orderarr[$value][dingjin];
				}
			}
			// print_r($data);exit;
			$msg['code'] = 20000;
			$msg['content'] = $data;
			
			
		}else{
			$msg['code'] = 20000;
			$msg['msg'] = '没有订单数据';
		}
	}else{
		$msg['code'] = 20000;
		$msg['msg'] = '用户编号有误';
	}

}else{
	$msg['code'] = 20000;
	$msg['msg'] = '请填写客户编号';
}
echo json_encode($msg);
?>