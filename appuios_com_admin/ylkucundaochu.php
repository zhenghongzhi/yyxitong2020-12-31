<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');

	// print_r($_GET);die;
	$where = "1 and w.status = 2";
	if ($k_khtype) {
		$where .= " and w.khtype = ".$k_khtype;
	}
	if ($k_kehu_number) {
		$where .= " and w.userid like '%".$k_kehu_number."%'";
	}
	if ($k_fyid) {
		$where .= " and w.xzmuchuan = '$k_fyid'";
	}


	$sql2 = $db->query("select w.id,w.userid,sum(w.count) as count,sum(w.weight) as weight,sum(w.volume) as volume,sum(w.money) as money,w.fahuohao,k.subject,group_concat(m.subject) as subject1,group_concat(w.id) as id 
		from yasa_ylwarehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype 
		left join yasa_muchuan as m on m.id = w.xzmuchuan
		where $where group by w.userid");
	
	
	$arr = array();
	while ($a = $db->fetch_array($sql2)) {
		
		if ($a[id]) {
			$qianshou = $db->get_one("select sum(count) as count, sum(weight) as weight,sum(volume) as volume from yasa_ylqianshou where ylid in($a[id])");
		}
		
		$a[count] = $a[count]-$qianshou[count];
		$a[weight] = $a[weight]-$qianshou[weight];
		$a[volume] = $a[volume]-$qianshou[volume];


		$arr[] = $a;
	}




	$filename = '伊朗未派送订单(' . date('Ymd-His') . ')';



	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle('未派送订单');
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	

	$objPHPExcel->getActiveSheet()->setCellValue('A1', '序号');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', '公司名称');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', '客户编号');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', '数量');
	$objPHPExcel->getActiveSheet()->setCellValue('E1', '立方');
	$objPHPExcel->getActiveSheet()->setCellValue('F1', '摘要');




	$i = 2;
	foreach ($arr as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $key+1);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[subject1]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[userid]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[count]);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value[volume]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value[remake]);
		$i++;
	}
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="迪拜未派送订单(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);
 ?>