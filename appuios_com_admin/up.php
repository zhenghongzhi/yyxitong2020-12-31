<?php
//禁用错误报告
error_reporting(0);
require("./PicThumb/mutil_make_thumbpic.php");
?>
<?php
$uptypes=array(
    'image/jpg',
    'image/jpeg',
    'image/png',
    'image/pjpeg',
    'image/gif',
    'image/bmp',
    'image/x-png'
);

$max_file_size=2000000;     //上传文件大小限制, 单位BYTE
$picurl_addtime=date("Ymd");
$destination_folder="../upfiles/".$picurl_addtime."/"; //上传文件路径
?>
<html>
<head>
<title>添加上传图片</title>
<style type="text/css">
<!--
body
{
   font-size: 12px;
  margin:0px;
}
input, select, textarea { border:1px solid #CCC; font-size:12px; padding:2px; font-family:"宋体"; }
-->
</style>
</head>

<body>
<div align="center" style="height:100px">
<form enctype="multipart/form-data" method="post" name="upform">
  <input name="upfile" type="file" size="30">
  <input type="submit" value="上传"><br>
  <div style="font-size:13px; padding-top:10px; color:#333">
  允许上传的图片格式为：JPG 、JPEG 、 PNG 、GIF 、PJPEG 
  </div>

</form>
</div>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (!is_uploaded_file($_FILES["upfile"]['tmp_name']))
    //是否存在文件
    {
         echo "图片不存在!";
         exit;
    }

    $file = $_FILES["upfile"];
    if($max_file_size < $file["size"])
    //检查文件大小
    {
        echo "文件太大!";
        exit;
    }

    if(!in_array($file["type"], $uptypes))
    //检查文件类型
    {
        echo "文件类型不符!".$file["type"];
        exit;
    }

    if(!file_exists($destination_folder))
    {
        mkdir($destination_folder);
    }

    $filename=$file["tmp_name"];
    $image_size = getimagesize($filename);
    $pinfo=pathinfo($file["name"]);
    $ftype=$pinfo['extension'];
    $destination = $destination_folder.date("Ymd").time().".".$ftype;
    if (file_exists($destination) && $overwrite != true)
    {
        echo "同名文件已经存在了";
        exit;
    }

    if(!move_uploaded_file ($filename, $destination))
    {
        echo "移动文件出错";
        exit;
    }

    $pinfo=pathinfo($destination);
    $fname=$pinfo[basename];
	
	$picurl = "upfiles/". $picurl_addtime."/".$fname;
	mutil_make_thumbpic($picurl);
 ?>
    <script type ="text/javascript">
  <?php
  if(isset($_GET['label'])){//如果是批量上传?>
    //window.parent.document.getElementById("pic_name<?php echo $_GET['label']?>").value = "<?php echo $file["name"]?>";
    window.parent.document.getElementById("pic_url<?php echo $_GET['label']?>").value = 'upfiles/<?php echo $picurl_addtime."/".$fname?>';
//    window.parent.document.getElementById('light').style.display='none';
//    window.parent.document.getElementById('fade').style.display='none';
//    window.close();
//alert("上传成功！ 宽度:<?=$image_size[0]?>px 长度:<?=$image_size[1]?>px  大小:<?=$file["size"].' '.bytes?>");
 window.parent.document.getElementById("success").innerHTML=("<p>---上传成功!----<br/> 宽度:<?=$image_size[0]?>px  长度:<?=$image_size[1]?>px  大小:<?=$file["size"].' '.字节?> </p>");
  <?php }
  else{?>
   window.parent.document.thisform.uploadfile.value='upfiles/<?=$picurl_addtime."/".$fname?>';
   window.parent.document.getElementById("success").innerHTML=("上传成功！<br/> 宽度:<?=$image_size[0]?>px 长度:<?=$image_size[1]?>px  大小:<?=$file["size"].' '.字节?>");
   
  <?php }?>
 </script>
 <?
}
?>
</body>
</html>