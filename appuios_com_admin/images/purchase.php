<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

$start = isset($_GET["start"]) ? $_GET["start"] : null;
$end = isset($_GET["end"]) ? $_GET["end"] : null;

if($start && $end) {

	$start = strtotime($start);
	$end = strtotime($end);
	$end = $end + (24*60*60);
	// echo $start;
	// echo "<br>";
	// echo $end;

	$result = $db->query("SELECT SUM(type1) AS total FROM yasa_product WHERE addtime >= $start AND addtime <= $end");
	$row = $db->fetch_array($result);
	$total = (int)$row["total"];
	// echo $total;

	$db->query("SET NAMES UTF8");
	$sql = "SELECT o.order_id,o.addtime,k.kehu_number,p.subject,p.type1,p.type4,p.type5,p.type6,s.subject as subject1,s.username,s.phone_ts,s.phone,s.address,u.nickname FROM yasa_product as p left join yasa_order as o on p.pid1 = o.id left join yasa_shangjia as s on o.pid2 = s.id left join yasa_kehu as k on o.pid1 = k.id left join yasa_user as u on o.pid3 = u.uid WHERE o.addtime >= $start and o.addtime <= $end ORDER BY o.addtime DESC";
	// echo $sql;die;
	$result = $db->query($sql);

}

?>

<head>
	<style type="text/css">
		.main { text-align: center; }
		table { margin: auto; }
		table tr th { width: 200px; }
	</style>
</head>

<div class="main">

	<form action="">
		开始日期：<input type="date" name="start">
		结束日期：<input type="date" name="end">
		<input type="submit" name="提交">
		<input type="button" value="导出" class="daochu">
	</form>
	<br>

	<? if($start && $end){ ?>

	<table border="1">
		<caption>采购信息统计</caption>
		<thead>
	      <tr>
	        <th width="20">订单</th>
	        <th width="20">客户编号</th>
	        <th width="20">翻译</th>
	        <th width="20">名称</th>
	        <th width="20">款号</th>
	        <th width="20">数量</th>
	        <th width="20">单价</th>
	        <th width="20">金额</th>
	        <th width="20">供应商</th>
	        <th width="20">联系人</th>
	        <th width="20">联系电话1</th>
	        <th width="20">联系电话2</th>
	        <th width="20">地址</th>
	        <th width="20">时间</th>     
	      </tr>
	    </thead>
	    <tbody>
	    	<?php 
	    		while($row = $result->fetch_assoc()) { ?>
	    		<tr>
					<td><?php echo $row['order_id'] ?></td>
					<td><?php echo $row['kehu_number'] ?></td>
					<td><?php echo $row['nickname'] ?></td>
					<td><?php echo $row['subject'] ?></td>
					<td><?php echo $row['type1'] ?></td>
					<td><?php echo $row['type4'] ?></td>
					<td><?php echo $row['type5'] ?></td>
					<td><?php echo $row['type6'] ?></td>
					<td><?php echo $row['subject1'] ?></td>
					<td><?php echo $row['username'] ?></td>
					<td><?php echo $row['phone_ts'] ?></td>
					<td><?php echo $row['phone'] ?></td>
					<td><?php echo $row['address'] ?></td>
					<td><?php echo date("Y-m-d H:i:s",$row['addtime']) ?></td>
	    		</tr>
	    	<? } ?>
	    </tbody>
	</table>

	<? } ?>
</div>
