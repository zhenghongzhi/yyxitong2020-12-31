<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
<script src="js/progressBar.js"></script>
<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

// $kehuId = isset($_GET["kehuId"]) ? $_GET["kehuId"] : null;
// $orderId = isset($_GET["orderId"]) ? $_GET["orderId"] : null;
$logistics = isset($_GET["logistics"]) ? $_GET["logistics"] : null;
if ($logistics) {
	$order = $db->query("SELECT * FROM yasa_order WHERE order_id = '$logistics'");
	$countsorder = $db->num_rows($order);
}

$rows = $db->fetch_array($order);
$orderId = $rows[id];
$kehuId = $rows[pid1];
if($logistics && $countsorder > 0) {
	// echo 1;die;
	$db->query("SET NAMES UTF8");
	$result = $db->query("SELECT A.type1 AS p1, A.type2 AS p2, A.type3 AS p3, A.status AS p4, A.addtime AS p5, B.type1 AS p6, B.type5 AS p7, B.type7 AS p8, B.status AS p9, B.addtime AS p10, C.status AS p11, C.addtime AS p12 FROM yasa_product_ruku A, yasa_product_zhuangui B, yasa_product_tidan C WHERE A.pid1 = $orderId AND A.is_delete = 1 AND B.only_id = (SELECT pid1 FROM yasa_product_zhuangui_huowu WHERE pid2 = $orderId AND is_delete = 1 LIMIT 1) AND B.is_delete = 1 AND C.pid1 = (SELECT pid1 FROM yasa_product_zhuangui_huowu WHERE pid2 = $orderId AND is_delete = 1 LIMIT 1) AND C.pid2 = $kehuId AND C.is_delete = 1");
	$counts = $db->num_rows($result);

	// 还有一个条件如果到港时间存在直接百分百
	$daogang = $db->query("SELECT id,status FROM yasa_product_zhuangui where find_in_set($orderId,pid1)");

	$a = $db->fetch_array($daogang);
	if ($a[status] > 2) {
		$tian = 25;
	}else{
		$time = time();
		$addtime = 1573290000;
		$tian = floor((time() - $addtime)/(3600*24));
		if ($tian >= 22) {
			$tian = 22;
		}
	}

	$baifenbi = $tian/25*100;


}
	
	
?>

<head>
	<style type="text/css">
		.main { text-align: center; }
		table { margin: auto; }
		table tr th { width: 500px; }
		table tr td { text-align: center; }
	</style>
</head>

<div class="main">
	<? if ($counts > 0 ) { ?>
		<? if($logistics){ ?>
			<div style="text-align: center; padding: 20px;">
				<p>物流进度</p>
				<div class="div" w="<?=$baifenbi; ?>?">
					
				</div>
			</div>

			<table border="1">
				<thead>
			      <tr>
			        <th colspan="2">装箱信息<br>Packing Info</th>
			        <th colspan="2">装柜信息<br>Shipping Info</th>
			        <th colspan="1">客户签收<br>Signed</th>      
			      </tr>
			    </thead>
			    <tbody>
			    	<?
			    		while($row = $db->fetch_array($result)) {
				    		echo "<tr>";
				    		echo "<td>".$row["p1"]."</td>";

				    		echo "<td>";
				    		echo $row["p2"]." ";
				    		if($row["p4"] == 1) { echo "已入库 Warehousing "; }
				    		if($row["p4"] == 2) { echo "已装柜出仓 containerized "; }
				    		if($row["p4"] == 3) { echo "已到目的港 arrived port "; }
				    		if($row["p4"] == 4) { echo "已出目的港 out port "; }
				    		echo date("Y-m-d", $row["p5"]);
				    		echo "</td>";

				    		echo "<td>".$row["p6"]."</td>";
				    		
				    		echo "<td>";
				    		echo $row["p7"]." ";
				    		if($row["p9"] == 1) { echo "未装柜 "; }
				    		if($row["p9"] == 2) { echo "已装柜 "; }
				    		echo date("Y-m-d", $row["p10"]);
				    		echo "</td>";

				    		echo "<td>";
				    		if($row["p11"] == 1) { echo "未签收 unsigned "; }
				    		if($row["p11"] == 2) { echo "已签收 signed "; }
				    		echo date("Y-m-d", $row["p12"]);
				    		echo "</td>";
				    		echo "</tr>";
			    		}
			    	?>
			    </tbody>
			</table>

		<? } ?>
	<? }else{ ?>
		<table border="1">
			<thead>
		      <tr>
		        <th colspan="2">装箱信息<br>Packing Info</th>
		        <th colspan="2">装柜信息<br>Shipping Info</th>
		        <th colspan="1">客户签收<br>Signed</th>      
		      </tr>
		    </thead>
		    <tbody>
		    	<td colspan="5">no date</td>
		    </tbody>
		</table>
	<? } ?>

</div>
