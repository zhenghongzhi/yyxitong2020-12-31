<?
include_once("../include/common.ini.php");
include_once("error.inc.php");
include_once("checkuser.php");
?>
<!--This is IE DTD patch , Don't delete this line.-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>--后台管理系统--</title>
<link href="theme/css/frame.css" rel="stylesheet" type="text/css" />
<script src="theme/jquery.js" language="javascript" type="text/javascript"></script>
<script src="theme/js/frame.js" language="javascript" type="text/javascript"></script>
<link href="theme/images/style1/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#skinlist {
    display: block;
    height: 11px;
	margin-top: 10px;
    overflow: hidden;
    width: 86px;
}
#skin div {
    float: left;
}
#skin li {
    cursor: pointer;
    float: left;
    height: 11px;
    width: 14px;
}
#def div, #s1 div, #s2 div, #s3 div, #s4 div{
    background-image: url("theme/images/skinbutton.png");
    background-repeat: no-repeat;
}
#s1 div {
    background-position: 0 0px;
}
#s2 div {
    background-position: 0 -11px;
}
#s3 div {
    background-position: 0 -22px;
}
#s4 div {
    background-position: 0 -33px;
}
#s1 div.sel {
    background: url("theme/images/skinbutton.png") no-repeat scroll -14px top transparent;
}
#s2 div.sel {
    background: url("theme/images/skinbutton.png") no-repeat scroll -14px -11px transparent;
}
#s3 div.sel {
    background: url("theme/images/skinbutton.png") no-repeat scroll -14px -22px transparent;
}
#s4 div.sel {
    background: url("theme/images/skinbutton.png") no-repeat scroll -14px -33px transparent;
}
html { overflow-x:hidden; }
</style>
</head>
<body class="showmenu">
<div class="left">
  <div class="menu" id="menu">
    <iframe src="index_menu.php" id="menufra" name="menu" frameborder="0" scrolling="auto"></iframe>
  </div>
</div>
<div class="right">
  <div class="main">
  
    <iframe id="main" name="main" frameborder="0" src="index_body.php" scrolling="auto"></iframe>
 
  </div>
  <!--<div id="help"><span id="content"><a href="#">栏目管理操作使用说明</a></span></div>-->
</div>
<script language="javascript">
function JumpFrame(url1, url2){
    jquery('#menufra').get(0).src = url1;
    jquery('#main').get(0).src = url2;
}
(function($)
{
 	$("#skinlist>li").click(function()
	 {
		 var adminskin = $(this).index() + 1;
		 var csshref = "theme/images/style"+adminskin+"/style.css";
		 $("#skinlist>li").each(function(){$(this).children('div').attr('class', '')});
		 $("#topdedelogo").attr('src', 'theme/images/style'+adminskin+'/admin_top_logo.gif')
		 $('link').each(function()
		 {
			 if($(this).attr('href').match(/style.css$/))
			 {
				 $(this).attr('href',csshref);
			 }
		 });
		 $(this).children('div').attr('class', 'sel');
		 $(window.frames["menu"].document).find("link").each(function()
		 {
			 if($(this).attr('href').match(/style.css$/))
			 {
				 $(this).attr('href',csshref);
			 }
		 });
		 $(window.frames["main"].document).find("link").each(function()
		 {
			 if($(this).attr('href').match(/style.css$/))
			 {
				 $(this).attr('href',csshref);
			 }
		 });
		 $.get('index_body.php?dopost=setskin&cskin='+adminskin);
	 });
})(jquery);
</script>
</body>
</html>

