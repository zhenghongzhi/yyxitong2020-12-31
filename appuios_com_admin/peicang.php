<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once("checkuser.php");
	// echo $ids;die;
	$warehousesql = $db->query("select w.*,k.subject from yasa_warehouse as w left join yasa_kehutype as k on w.khtype = k.id where w.id in ($ids)");
	$protypearr = array();
	// print_r($_GET);die;
 ?>

 <html>
	<head>
		<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/layer/layer.js"></script>
	</head>
	<body>
		<style>	
			table{
				width: 640px;
				margin: 0px auto;
				text-align: center;
				padding: 0px;
			}
			table td{
				border:1px solid #000;
			}
			h2{
				text-align: center;	
			}
		</style>
		<form id="form">
			<input type="hidden" name="ids" value="<?=$ids?>">
			<input type="hidden" name="type" value="2">
			<h2>发货号<input type="text" name="fahuohao" id="fahuohao"></h2>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>客户归属</td>
					<td>客户编号</td>
					<td>订单编号</td>
					<td>货物类型</td>
					<td>数量</td>
					<td>重量</td>
					<td>体积</td>
				</tr>
				<?php while($warehouse=$db->fetch_array($warehousesql)){ 
					$zongcount += $warehouse[count];
					$zongweight += $warehouse[weight];
					$zongvolume += $warehouse[volume];

					if ($warehouse[protype] == '普货') {
						$pucount += $warehouse[count];
						$puweight += $warehouse[weight];
						$puvolume += $warehouse[volume];

					}elseif ($warehouse[protype] == '非普货') {
						$feicount += $warehouse[count];
						$feiweight += $warehouse[weight];
						$feivolume += $warehouse[volume];
					}else{
						$tecount += $warehouse[count];
						$teweight += $warehouse[weight];
						$tevolume += $warehouse[volume];
					}
					$protypearr['普货']['count'] = $pucount;
					$protypearr['普货']['weight'] = $puweight;
					$protypearr['普货']['volume'] = $puvolume;

					$protypearr['非普货']['count'] = $feicount;
					$protypearr['非普货']['weight'] = $feiweight;
					$protypearr['非普货']['volume'] = $feivolume;				
					$protypearr['特货']['count'] = $tecount;
					$protypearr['特货']['weight'] = $teweight;
					$protypearr['特货']['volume'] = $tevolume;	
				?>
				<tr>
					<td><?=$warehouse[subject]; ?></td>
					<td><?=$warehouse[userid]; ?></td>
					<td><?=$warehouse[orderid]; ?></td>
					<td><?=$warehouse[protype]; ?></td>
					<td><?=$warehouse[count]; ?></td>
					<td><?=$warehouse[weight]; ?></td>
					<td><?=$warehouse[volume]; ?></td>
				</tr>
				<?php } ?>
					
				<?php foreach ($protypearr as $key => $value) { ?>
					<tr>
						<td colspan="4"><?=$key; ?></td>
						<td><?=$value[count]?$value[count]:0; ?></td>
						<td><?=$value[weight]?$value[weight]:0; ?></td>
						<td><?=$value[volume]?$value[volume]:0; ?></td>
					</tr>
				<?php } ?>


				<tr>
					<td colspan="4">合计</td>
					<td>总数量：<?=$zongcount;?></td>
					<td>总重量：<?=$zongweight;?></td>
					<td>总体积：<?=$zongvolume;?></td>
				</tr>
				<tr>
					<td colspan="7" style="text-align: right;">
						<input type="button" value="确定" class="sure"> 
						<input type="button" value="取消" class="cancel"> 
					</td>
				</tr>
			</table>
		</form>

		<script>
			var index = parent.layer.getFrameIndex(window.name);
			$('.cancel').click(function(){
				parent.layer.close(index);
			})
			$('.sure').click(function(){
				var fahuohao = $("#fahuohao").val();
				if (fahuohao == '') {
					layer.msg('请填写发货号');
					return false;
				}
				if (sure('确定配舱吗？')) {
					var form = $("#form").serialize();
					$.ajax({ url: "warehousestatus.php",dataType: "json", data: form, success: function(msg){
						console.log(msg);
						if (msg.code == 1) {
				       		layer.msg(msg.msg,{time: 2000},function(){
				       			window.parent.location.reload();
				       			parent.layer.close(index);
				       		});
						}else{
							layer.msg(msg.msg,{time: 2000},function(){

							});
						}
				    }});
				}
				
			})
			function sure(obj){
		      if(confirm(obj)){
		        return true;
		      }else{
		        return false;
		      }
		  	}
		</script>
	</body>
 </html>