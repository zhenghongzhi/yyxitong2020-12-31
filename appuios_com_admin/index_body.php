<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<?
session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");
include_once("checkuser.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>-后台管理系统-</title>
<link rel="stylesheet" href="theme/index_menu_data/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_body_data/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_menu_data/invalid.css" type="text/css" media="screen">	
<script language="javascript" src="theme/index_menu_data/jquery.js" type="text/javascript"></script>	
<script type="text/javascript" src="theme/index_menu_data/admin.js"></script>
<script src="theme/frame.js" language="javascript" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
	window.onload=function (){
		setInterval("document.getElementById('time').innerHTML=new Date().toLocaleString()+' 星期'+'日一二三四五六'.charAt(new Date().getDay());",1000);
	}
</script>
	
</head>

<body>
<div id="main-content">
			
			<h2>欢迎访问管理后台</h2>
		
            <script>
                $(".shortcut-button").click(function(){
                    curhref=$(this).attr('href');
 
                    $("#main-nav li ul",window.parent.menu.document).hide(); // Slide up all sub menus except the one clicked

	  $(".nav-bottom-item[href='"+curhref+"']",window.parent.menu.document).parent().parent().show(); // Slide down the clicked sub menu
                    $(".nav-bottom-item",window.parent.menu.document).removeClass('current');
                    $(".nav-top-item",window.parent.menu.document).removeClass('current');
                    $(".nav-bottom-item[href='"+curhref+"']",window.parent.menu.document).addClass("current");
                    $(".nav-bottom-item[href='"+curhref+"']",window.parent.menu.document).parents("li").children(".nav-top-item").addClass("current");
                });        
        
    </script>            
	
    
    <?

function real_server_ip()   
{   
    static $serverip = NULL;   
  
    if ($serverip !== NULL)   
    {   
        return $serverip;   
    }   
  
    if (isset($_SERVER))   
    {   
        if (isset($_SERVER['SERVER_ADDR']))   
        {   
            $serverip = $_SERVER['SERVER_ADDR'];   
        }   
        else  
        {   
            $serverip = '0.0.0.0';   
        }   
    }   
    else  
    {   
        $serverip = getenv('SERVER_ADDR');   
    }   
  
    return $serverip;   
}  

function gd_version()
    {
        static $version = -1;

        if ($version >= 0)
        {
            return $version;
        }

        if (!extension_loaded('gd'))
        {
            $version = 0;
        }
        else
        {
            // 尝试使用gd_info函数
            if (PHP_VERSION >= '4.3')
            {
                if (function_exists('gd_info'))
                {
                    $ver_info = gd_info();
                    preg_match('/\d/', $ver_info['GD Version'], $match);
                    $version = $match[0];
                }
                else
                {
                    if (function_exists('imagecreatetruecolor'))
                    {
                        $version = 2;
                    }
                    elseif (function_exists('imagecreate'))
                    {
                        $version = 1;
                    }
                }
            }
            else
            {
                if (preg_match('/phpinfo/', ini_get('disable_functions')))
                {
                    /* 如果phpinfo被禁用，无法确定gd版本 */
                    $version = 1;
                }
                else
                {
                  // 使用phpinfo函数
                   ob_start();
                   phpinfo(8);
                   $info = ob_get_contents();
                   ob_end_clean();
                   $info = stristr($info, 'gd version');
                   preg_match('/\d/', $info, $match);
                   $version = $match[0];
                }
             }
        }

        return $version;
     }
$mysql_ver = $db->version();   // 获得 MySQL 版本

    /* 系统信息 */
    $sys_info['os']            = PHP_OS;
    $sys_info['ip']            = real_server_ip();
    $sys_info['web_server']    = $_SERVER['SERVER_SOFTWARE'];
    $sys_info['php_ver']       = PHP_VERSION;
    $sys_info['mysql_ver']     = $mysql_ver;
    $sys_info['zlib']          = function_exists('gzclose') ? "是":"否";
    $sys_info['safe_mode']     = (boolean) ini_get('safe_mode') ? "是":"否";
    $sys_info['safe_mode_gid'] = (boolean) ini_get('safe_mode_gid') ? "是":"否";
    $sys_info['timezone']      = function_exists("date_default_timezone_get") ? date_default_timezone_get() : "无";
    $sys_info['socket']        = function_exists('fsockopen') ? "是":"否";

 	$gd = gd_version();
    if ($gd == 0)
    {
        $sys_info['gd'] = 'N/A';
    }
    else
    {
        if ($gd == 1)
        {
            $sys_info['gd'] = 'GD1';
        }
        else
        {
            $sys_info['gd'] = 'GD2';
        }

        $sys_info['gd'] .= ' (';

        /* 检查系统支持的图片类型 */
        if ($gd && (imagetypes() & IMG_JPG) > 0)
        {
            $sys_info['gd'] .= ' JPEG';
        }

        if ($gd && (imagetypes() & IMG_GIF) > 0)
        {
            $sys_info['gd'] .= ' GIF';
        }

        if ($gd && (imagetypes() & IMG_PNG) > 0)
        {
            $sys_info['gd'] .= ' PNG';
        }

        $sys_info['gd'] .= ')';
    }

    /* IP库版本 */
    $sys_info['max_filesize'] = ini_get('upload_max_filesize');
?>		
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3 style="cursor: s-resize;">系统信息</h3>
					
					
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div style="display: block;" class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
						
						
						<table>
						 
							<tbody>
								<tr class="alt-row">
									<td>服务器解译引擎：</td>
									<td>nginx/1.10.2</td>
								</tr>
								<tr>
									<td>操作系统：</td>
									<td>Linux</td>
								</tr>
								<tr class="alt-row">
									<td>网站域名/IP：</td>
									<td><?=$sys_info['ip']?></td>
								</tr>
								
								<tr>
									<td>开发版本：</td>
									<td>2.3.0</td>
								</tr>
								<tr  class="alt-row">
									<td>数据库版本：</td>
									<td>5.7.12</td>
								</tr>
								<tr >
									<td>服务器时间：</td>
									<td><div class="fRight mR10" id="time">2014年1月13日 21:57:07 星期一</div></td>
								</tr>
								<tr class="alt-row">
									<td>文件上传的最大规格：</td>
									<td><?=$sys_info['max_filesize']?></td>
								</tr>
								
                                <tr>
									<td>Socket 支持：</td>
									<td><?=$sys_info['socket']?></td>
								</tr>

								<tr  class="alt-row">
									<td>GD 版本：</td>
									<td><?=$sys_info['gd']?></td>
								</tr>
                                 <tr  >
									<td>时区：</td>
									<td>  <?=$sys_info['timezone']?></td>
								</tr>
                              <tr  class="alt-row">
									<td>Zlib 支持：</td>
									<td><?=$sys_info['zlib']?></td>
								</tr>
                              
							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
				</div> <!-- End .content-box-content -->
<!--				<div style="float:left">
                    <iframe runat="server" src="jq_diskbar_hxl/index.php" width="900" height="120" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"></iframe>
                    </div>-->
			</div> <!-- End .content-box -->
			
			<div class="clear"></div>
			
						<div id="footer">
				<small> <!-- footer -->
							技术支持： <a href="http://www.appuios.com" target="_blank">翠鸟网络</a> 
				</small>
			</div><!-- End #footer -->
			
		</div> <!-- End #main-content -->	
	
<script language="javascript">
function JumpFrame(url1, url2){
    jquery('#menufra',window.parent.document).get(0).src = url1;
    jquery('#main',window.parent.document).get(0).src = url2;
}
</script>  

            
</body></html>