<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
include_once("../include/common.ini.php");
include_once("error.inc.php");

if($method == 'logout'){
	$_SESSION['cuiniao_Admin_Name']='';
	foreach($_SESSION as $key=>$v){
		unset($_SESSION[$key]);
	}
	header("location:index.php");
	exit;
}

if($_SESSION['cuiniao_Admin_Name']){
	header("Location:admin_index.php");
	exit;
}


if($method == 'CheckLogin'){

	if( trim(strtolower($_POST['verifyCode']))==""){
		$Errmsg = $Errmsg."请输入附加码。";
		$FoundErr = true;
	}elseif( $_SESSION['captcha']==""){
		$Errmsg = $Errmsg."请不要重复提交，如需重新登陆请返回登陆页面。";
		$FoundErr = true;
	}elseif(trim(strtolower($_POST['verifyCode'])) != $_SESSION['captcha']){
		$Errmsg = $Errmsg."你输入的附加码和系统产生的不符。";
		$FoundErr = true;
	}
	
	

	if($username == "" || $password == ""){
		$Errmsg = $Errmsg."请输入登陆用户名或密码。";
		$FoundErr = true;
	}

	if($FoundErr){
		er($Errmsg,2);
		exit;
	}
	
	$onlineip = "";
	if(getenv('HTTP_CLIENT_IP')) {
		$onlineip = getenv('HTTP_CLIENT_IP');
	} elseif(getenv('HTTP_X_FORWARDED_FOR')) { 
		$onlineip = getenv('HTTP_X_FORWARDED_FOR');
	} elseif(getenv('REMOTE_ADDR')) { 
		$onlineip = getenv('REMOTE_ADDR');
	} else { 
		$onlineip = $HTTP_SERVER_VARS['REMOTE_ADDR'];
	}
	$IP = $onlineip;

	$Sql = "select * from cuiniao_admin where username='$username'";
	$Rs = $db->get_one($Sql);

	if($Rs['username']==''){
		$Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
		er($Errmsg,2);
		exit;
	}else{
		if($Rs["password"] != md5($password)){
			$Errmsg = "您输入的用户名和密码不正确或者您不是系统管理员。";
			er($Errmsg,2);
			exit;
		}else{
			$_SESSION['cuiniao_Admin_Name'] = $username;
			$_SESSION['cuiniao_id'] = $Rs['id'];
			$_SESSION['user_test_rights']=$Rs['rights'];
			$_SESSION['sundy_right1']=$Rs['right1'];
			$_SESSION['sundy_right2']=$Rs['right2'];
			$_SESSION['sundy_right3']=$Rs['right3'];
			$_SESSION['sundy_right4']=$Rs['right4'];	
			$_SESSION['sundy_right5']=$Rs['right5'];
			$_SESSION['sundy_right6']=$Rs['right6'];
			$_SESSION['sundy_right7']=$Rs['right7'];
			$_SESSION['sundy_right8']=$Rs['right8'];
			$_SESSION['sundy_right9']=$Rs['right9'];
			$_SESSION['sundy_right10']=$Rs['right10'];
			$_SESSION['sundy_right11']=$Rs['right11'];
			$_SESSION['sundy_right12']=$Rs['right12'];
			$_SESSION['sundy_right13']=$Rs['right13'];
			$_SESSION['sundy_right14']=$Rs['right14'];
			$_SESSION['sundy_right17']=$Rs['right17'];
			$_SESSION['sundy_right18']=$Rs['right18'];
			$_SESSION['sundy_right19']=$Rs['right19'];
			$_SESSION['sundy_right20']=$Rs['right20'];
			$_SESSION['sundy_right21']=$Rs['right21'];
			$_SESSION['sundy_right22']=$Rs['right22'];
			$_SESSION['fyid']=$Rs['fyid'];
			$_SESSION['mcid']=$Rs['mcid'];
			$_SESSION['wxid']=$Rs['wxid'];
			$_SESSION['qxid']=$Rs['qxid'];

			$db->query("update cuiniao_admin set LastLoginIP='$IP',logintime='".date("Y-m-d H:i:s")."' where username='$username'");
			header("Location:admin_index.php");
			$_SESSION['captcha']="";
			exit;
		}
	}
}
?>
<!doctype html>

<html>

<head>

	<meta charset="UTF-8">

	<title>管理系统</title>
	<meta name="robots" content="noindex, nofollow">
	<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->

  <link rel="stylesheet" href="admin_css_js_new/css/service.css">

    <!--[if lt IE 9]>

      <script src="admin_css_js_new/js/html5shiv.js"></script>

    <![endif]-->

	<script src="admin_css_js_new/js/jquery-1.8.3.js"></script>

</head>

<body class="login-bg login-TY">

  <div class="login-form">
 
   <form name="admininfo" method="post" action="index.php">
    <div class="mod">
  
    <label class="left">用户名</label>
    <input type="text" class="account right" id="account" name="username" ></div>

    <div class="mod">
    <label class="left">密&nbsp&nbsp&nbsp码 </label>
    <input type="password" class="password right" id="password" name="password"></div>

	<div class="mod"><label class="left">验证码</label>
          <input class="small_input txt"   style="width:60px" placeholder="验证码" name="verifyCode" id="verifyCodeId"/>
         <span  onclick="document.getElementById('cc').src='../include/cool-php-captcha_admin/captcha.php?'+Math.random();
    document.getElementById('applyform').focus();" id="change-image"> 
    <img src="../include/cool-php-captcha_admin/captcha.php" id="cc" alt="不清楚？换一张"  align="absmiddle" style="width:80px; height:40px; margin-left:30px"/> </span>
       <input type="hidden" value="CheckLogin" name="method">
        <span class="capcpde_txt" style="display:none">换一张</span>
    </div>
   <!--  <div class="rememberPsd"><input type="checkbox" class="remember">记住密码</div> -->

    <div class="btn-row">
    
    <input name="Submit" value="登录" type="submit" border="0" onClick="check()" style="width: 234px;
  height: 46px;
  border: none;
  border-radius: 3px;
  background-color: #477ab9;
  color: #fff;
  font-size: 14px;
  letter-spacing: 2px;
  cursor: pointer;" class="login" />
    </form>
    </div>
    
  </div>
	<div class="error-txt">
      <p class="error"></p>

    </div>
  <script>

	function setCookie(c_name,value,expiredays){

	    var exdate=new Date();

	    exdate.setDate(exdate.getDate()+expiredays);

	    document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
	}
	function getCookie(name){
	    if (document.cookie.length>0){

	      c_start=document.cookie.indexOf(name + "=");

	      if (c_start!=-1){ 

	        c_start=c_start + name.length+1 ;

	        c_end=document.cookie.indexOf(";",c_start);

	        if (c_end==-1) c_end=document.cookie.length;

	        return unescape(document.cookie.substring(c_start,c_end));

	      } 
	    }
	    return "";
	}
	function deleteCookie(name) {
	    var date = new Date();
	    date.setTime(date.getTime()-10000);
	    document.cookie = name+'=v;expire='+date.toGMTString();
	}
	var shake = function(obj){
		obj.animate({"margin-left":205},50).delay(10).animate({"margin-left":195},50).delay(10).animate({"margin-left":200},50);
	};
	
    $(function(){
	    var www=false;
		//var host='www.phone580.com';
	    var host=window.location.host;
		$(document).off('click','#capcode_img,.capcpde_txt').on('click','#capcode_img,.capcpde_txt',function(event){changeCode('#capcode_img');
	    });
	    $('button.login').bind('click',function(){
			var $this=$(this);
			if($this.parents(".login-form").hasClass('active')){
				return false;
			}else{
				$this.parents(".login-form").addClass('active');
				setTimeout(function(){
					$this.parents(".login-form").removeClass('active');
				},3000);
				if(!$('#account').val()){
					$('p.error').html('请输入用户名！');
					$('.error-txt').removeClass('error').addClass('warning').fadeIn().delay(3000).fadeOut();
					shake($('#account').parent());
					$('#account').focus();
					return false;
				}else if(!$('#password').val()){
					$('p.error').html('请输入密码！');
					$('.error-txt').removeClass('error').addClass('warning').fadeIn().delay(3000).fadeOut();
					shake($('#password').parent());
					$('#password').focus();
					return false;
				}
	      		if($this.attr('disabled'))return false;
	      		$this.attr('disabled','disabled');  
				$('p.error').html('正在登录中请稍后...');
				$('.error-txt').removeClass('error').addClass('ing').fadeIn();
			    var log=$('#account').val(),
				    pwd=$('#password').val(),
				    capcode = $('#capcode').val();
				$.ajax({
				  url: 'http://'+host+'/fbsapi/api/user/userlogin?userName='+log+'&password='+pwd+'&capcode='+capcode,
				  type:"GET",
				  dataType:"jsonp",
				  jsonp:"callback",
				  success:function(data){
					if(data.success==true){
						$('p.error').html('页面正在打开...');
						$('.error-txt').removeClass('error').addClass('ing').fadeIn();
						var token = data.valueObject.authToken,
							regionId = data.valueObject.userInfo.user.regionId;
						  if (getCookie('authTokens')) {
							deleteCookie('authTokens');
						  }
						  if (getCookie('username')) {
							deleteCookie('username');
						  }
						  if (getCookie('regionId')) {
							deleteCookie('regionId');
						  }
						  setCookie('authTokens', token);
						  setCookie('username', log);
						  setCookie('regionId', regionId);
						  window.location.href = 'http://'+host+'/fzsReport/comm/index.html';
					}else{
						$('.error-txt').removeClass('warning ing').addClass('error').fadeIn().delay(3000).fadeOut();
						$('p.error').html(data.message);	
						changeCode('#capcode_img');
					}
				
					$this.removeAttr('disabled');
				
				  },
				
				  error:function(error){
				
					$this.removeAttr('disabled');
				
				  }		
				});
			}
	    });
	    $('#account,#password,#capcode').bind('keydown',function(e){
	      if(e.keyCode==13){
	        $('button.login').click();
	      }
	    });
    });
  </script>
</body>
</html>