<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

$time = time();
$time_7 = $time - (7*24*60*60);
$time_15 = $time - (15*24*60*60);
$time_30 = $time - (30*24*60*60);

$result1 = $db->query("SELECT DISTINCT order_id, addtime, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime <= $time_15 AND addtime >= $time_30 AND id NOT IN (SELECT pid1 FROM yasa_product_ruku WHERE addtime <= $time_30)");

$result2 = $db->query("SELECT DISTINCT order_id, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime >= $time_30 AND addtime <= $time_7 AND id IN (SELECT DISTINCT pid1 FROM yasa_product_ruku WHERE addtime >= $time_30) AND pid1 NOT IN (SELECT DISTINCT pid2 FROM yasa_product_zhuangui_huowu WHERE addtime >= $time_30)");

?>

<head>
	<style type="text/css">
		.main {
			display: flex;
			justify-content: space-evenly;
		}
		table tr th {
			width: 200px;
		}
	</style>
</head>

<div class="main">

	<table border="1">
		<caption>超15天未送货订单</caption>
		<thead>
	      <tr>
	        <th>订单编号</th>
	        <th>客户编号</th>
	        <th>订单提交日期</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<?
	    		while($row = $db->fetch_array($result1)) {
		    		echo "<tr>";
		    		echo "<td>";
		    		echo $row["order_id"];
		    		echo "</td>";
		    		echo "<td>";
					echo $row["kehu_number"];
		    		echo "</td>";
		    		echo "<td>";
					echo date("Y-m-d", $row["addtime"]);
		    		echo "</td>";
		    		echo "</tr>";
	    		}
	    	?>
	    </tbody>
	</table>
	
	<table border="1">
		<caption>进仓超7天未装柜</caption>
		<thead>
	      <tr>
	        <th>订单编号</th>
	        <th>客户编号</th>
	        <th>进仓日期</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<?
	    		while($row = $db->fetch_array($result2)) {
		    		echo "<tr>";
		    		echo "<td>";
		    		echo $row["order_id"];
		    		echo "</td>";
		    		echo "<td>";
					echo $row["kehu_number"];
		    		echo "</td>";
		    		echo "<td>";
		    		echo "</td>";
		    		echo "</tr>";
	    		}
	    	?>
	    </tbody>
	</table>
</div>
