<?
include_once("../include/common.ini.php");
include_once("error.inc.php");
include_once("checkuser.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>-后台管理系统-</title>
<link rel="stylesheet" href="theme/index_menu_data/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_menu_data/menu.css" type="text/css" media="screen">
<link rel="stylesheet" href="theme/index_menu_data/invalid.css" type="text/css" media="screen">
<script language="javascript" src="theme/index_menu_data/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="theme/index_menu_data/admin.js"></script>
<script src="theme/frame.js" language="javascript" type="text/javascript"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {
	font-size: 12px;
	color: #000000;
}
.STYLE5 {
	font-size: 12px;
}
.STYLE7 {
	font-size: 12px;
	color: #FFFFFF;
}
-->
</style>

<?php 
  $dquserqxid  = json_decode($_SESSION[qxid]);
  // print_r($_SESSION);exit;
  //菜单
  $menusql = $db->query("select * from yasa_menu order by sort asc");
  while ($menu = $db->fetch_array($menusql)) {
    if (in_array($menu[id], $dquserqxid)) {
      if ($menu[pid] == 0) {
        $yiarr[] = $menu;
      }else{
        $erarr[] = $menu;
      }
    }
  }
  foreach ($yiarr as $key => $value) {
    foreach ($erarr as $ke => $val) {
      if ($val[pid]==$value[id]) {
        $yiarr[$key][children][] = $val;
      }
    }
    
  }
  // print_r($yiarr);die;
  // $yjarrjson = json_encode($yjarr1);

 ?>
</head>
<body>
<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
  <div id="sidebar">
    <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
      
      <h1 id="sidebar-title"><a href="#">信息管理系统</a></h1>
      
      <!-- Logo (221px wide) --> 
      <a href=""><img id="logo" src="theme/index_menu_data/logo.png" alt="zcncms logo"></a> 
      <!-- Sidebar Profile links -->
      <div id="profile-links"> <a href="#">当前帐号:
        <?=$_SESSION['cuiniao_Admin_Name']?>
        [
        <?=$_SESSION['cuiniao_flag']==1?"管理员":"管理员"?>
        ] </a> <br>
        <a href="index.php?method=logout" title="退出" target="_parent">退出</a> </div>
        <!-- Accordion Menu -->
        <ul id="main-nav" >
          <!-- <li> 
            <a style="padding-right: 15px;" id="templets" href="javascript:void(0);" class="nav-top-item"> 管理员信息 </a>
            <ul style="display: none;">
              <li><a class="nav-bottom-item" id="templets_main" href="password.php" target="main">信息修改</a></li>
              <? if($_SESSION['user_test_rights']==2){ ?>
              <li><a class="nav-bottom-item"  href="admin.php" target="main">用户列表</a></li>
              <li><a class="nav-bottom-item"  href="admin.php?act=add" target="main">添加用户</a></li>
              <li><a class="nav-bottom-item"  href="hemo_app_setting.php?id=1&act=edit" target="main">系统配置</a></li>
            <? }?>
            </ul>
          </li> -->
           
        <?php 
            foreach ($yiarr as $key => $value) { ?>
              <li > <a style="padding-right: 15px;" id="media" href="javascript:void(0);" class="nav-top-item"><?=$value[name]; ?> </a>
              <ul style="display: none;">
                <?php if ($value[children]) {
                   foreach ($value[children] as $ke => $val) { ?>
                      <li><a class="nav-bottom-item" id="media_main" href="<?=$val[url]; ?>" target="main"><?=$val[name]; ?></a></li>
                  <? }
                } ?>
              </ul>
              </li>
            <? }
         ?>



        </ul>
      <!-- End #main-nav --> 
      
      <script>
                $(".nav-bottom-item").click(function(){
                    $(".nav-bottom-item").removeClass('current');
                    $(".nav-top-item").removeClass('current');
                    $(this).addClass("current");
                    $(this).parents("li").children(".nav-top-item").addClass("current");
                });   
			 
                </script> 
    </div>
    <!-- End #messages --> 
    
    <!-- End #messages --> 
    
  </div>
</div>
</body>
</html>