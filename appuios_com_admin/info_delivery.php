<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?

$start = isset($_GET["start"]) ? $_GET["start"] : null;
$end = isset($_GET["end"]) ? $_GET["end"] : null;

if($start && $end) {

	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");

	$start = strtotime($start);
	$end = strtotime($end);
	$end = $end + (24*60*60);
	// echo $start;
	echo "<br>";
	// echo $end;

	$result = $db->query("SELECT DISTINCT order_id, addtime, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime <= $end AND addtime >= $start AND id NOT IN (SELECT pid1 FROM yasa_product_ruku)");

	//  WHERE addtime <= $time_30

}

?>

<head>
	<style type="text/css">
		.main { text-align: center; }
		table { margin: auto; }
		table tr th { width: 200px; }
	</style>
</head>

<div class="main">

	<form action="">
		开始日期：<input type="date" name="start">
		结束日期：<input type="date" name="end">
		<input type="submit" name="提交">
	</form>
	<br>

	<? if($start && $end){ ?>
	<table border="1">
		<caption>未送货订单</caption>
		<thead>
	      <tr>
	        <th>订单编号</th>
	        <th>客户编号</th>
	        <th>订单提交日期</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<? 
	    		while($row = $db->fetch_array($result)) {
		    		echo "<tr>";
		    		echo "<td>";
		    		echo $row["order_id"];
		    		echo "</td>";
		    		echo "<td>";
					echo $row["kehu_number"];
		    		echo "</td>";
		    		echo "<td>";
					echo date("Y-m-d", $row["addtime"]);
		    		echo "</td>";
		    		echo "</tr>";
	    		}
	    	?>
	    </tbody>
	</table>

	<?php } ?>
</div>
