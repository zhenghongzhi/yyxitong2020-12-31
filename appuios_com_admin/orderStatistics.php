<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

$start = isset($_GET["start"]) ? $_GET["start"] : null;
$end = isset($_GET["end"]) ? $_GET["end"] : null;

if($start && $end) {

	$oneday = 24*60*60;
	$start = strtotime($_GET["start"]);
	$end = strtotime($_GET["end"]);
	$end = $end+$oneday;

	// 订单数量
	$result = $db->query("SELECT COUNT(1) AS count FROM yasa_order WHERE addtime >= $start AND addtime <= $end");
	$row = $db->fetch_array($result);
	$count = (int)$row["count"];

	// 订单总金额
	$result = $db->query("SELECT SUM(type1) AS total FROM yasa_order WHERE addtime >= $start AND addtime <= $end");
	$row = $db->fetch_array($result);
	$total = (int)$row["total"];
	
	// 客户数量
	$result = $db->query("SELECT COUNT(DISTINCT pid1) AS customers FROM yasa_order WHERE addtime >= $start AND addtime <= $end");
	$row = $db->fetch_array($result);
	$customers = (int)$row["customers"];

	// 验收
	$result = $db->query("SELECT COUNT(DISTINCT A.id) AS count_yanhuo FROM yasa_order A LEFT JOIN yasa_product_yanhuo B ON A.id = B.pid1 WHERE A.addtime >= $start AND A.addtime <= $end AND B.addtime >= $start");
	$row = $db->fetch_array($result);
	$count_yanhuo = (int)$row["count_yanhuo"];

	// 装箱
	$result = $db->query("SELECT COUNT(DISTINCT A.id) AS count_ruku FROM yasa_order A INNER JOIN yasa_product_ruku B ON A.id = B.pid1 WHERE A.addtime >= $start AND A.addtime <=$end");
	$row = $db->fetch_array($result);
	$count_ruku = (int)$row["count_ruku"];

	// 装柜
	$result = $db->query("SELECT COUNT(DISTINCT A.id) AS count_zhuanggui FROM yasa_order A INNER JOIN yasa_product_zhuangui_huowu B ON A.id = B.pid2 WHERE A.addtime >= $start AND A.addtime <=$end");
	$row = $db->fetch_array($result);
	$count_zhuanggui = (int)$row["count_zhuanggui"];

	// 签收
	$result = $db->query("SELECT COUNT(DISTINCT A.id) AS count_qianshou FROM yasa_order A INNER JOIN yasa_product_zhuangui_huowu B ON A.id = B.pid2 INNER JOIN yasa_product_tidan C ON B.pid1 = C.pid1  WHERE A.addtime >= $start AND A.addtime <=$end");
	$row = $db->fetch_array($result);
	$count_qianshou = (int)$row["count_qianshou"];

	$db->close();
}
?>

<body>
	<form action="">
		开始日期：<input type="date" name="start">
		结束日期：<input type="date" name="end">
		<input type="submit" name="提交">
	</form>

	<? if($start && $end){ ?>
	<table border="1">
	    <thead>
	      <tr>
	        <th>title</th>
	        <th>value</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <td>订单总数量</td>
	        <td><? echo $count; ?></td>
	      </tr>
	      <tr>
	        <td>订单总金额</td>
	        <td><? echo $total; ?></td>
	      </tr>
	      <tr>
	        <td>客户数量</td>
	        <td><? echo $customers; ?></td>
	      </tr>
	      <tr>
	        <td>平均客户单价</td>
	        <td><? if ($count > 0) { echo intval(floor($total/$count)); } ?></td>
	      </tr>
	      <tr>
	        <td>平均客户采购</td>
	        <td><? if ($customers > 0) { echo intval(floor($total/$customers)); } ?></td>
	      </tr>
	      <tr>
	        <td>已验收</td>
	        <td><? echo $count_yanhuo; ?></td>
	      </tr>
	      <tr>
	        <td>已入库装箱</td>
	        <td><? echo $count_ruku; ?></td>
	      </tr>
	      <tr>
	        <td>已装柜</td>
	        <td><? echo $count_zhuanggui; ?></td>
	      </tr>
	      <tr>
	        <td>已签收</td>
	        <td><? echo $count_qianshou; ?></td>
	      </tr>
	    </tbody>
	</table>
	<? } ?>
</body>