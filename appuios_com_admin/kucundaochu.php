<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');

	$where = "1 and w.is_del = 1 and (w.status = 1 or w.status = 2)"; 
	
	if ($k_order_id) {
		$where .= " and w.orderid like '%".$k_order_id."%'";
	}
	if ($k_kehu_number) {
		$where .= " and w.userid like '%".$k_kehu_number."%'";
	}
	if ($k_khtype) {
		$where .= " and w.khtype = ".$k_khtype;
	}
	if ($k_protype) {
		$where .= " and w.protype = '$k_protype'";
	}
	if ($k_fahuo) {
		$where .= " and w.fahuo = '$k_fahuo'";
	}
	if ($k_jincangdanhao) {
		$where .= " and w.jincangdanhao = '$k_jincangdanhao'";
	}
	if ($k_fahuohao) {
		$where .= " and w.fahuohao like '%".$k_fahuohao."%'";
	}
	if ($k_addtime) {
		$where .= " and w.rukutime >= '$k_addtime'";
	}
	if ($k_endtime) {
		$where .= " and w.rukutime <= '$k_endtime'";
	}
	if ($k_status) {
		$where .= " and w.status = '$k_status'";
	}
	if ($k_spstatus) {
		$where .= " and w.spstatus = '$k_spstatus'";
	}
		
	$orderby = "order by w.status asc,w.rukutime asc";
	$sql2 = $db->query("select w.userid,w.count,w.weight,w.volume,w.money,w.fahuohao,w.money,w.rukutime,w.orderid,w.jincangdanhao,k.subject,w.remake,w.status,w.chucangtime,w.tuicangtime from yasa_warehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype
			where $where $orderby ");

	$filename = '国内仓库存(' . date('Ymd-His') . ')';
		
	$objPHPExcel = new PHPExcel();
	$phpColor = new PHPExcel_Style_Color();
	$phpColor->setRGB('AE0000'); 
	$objPHPExcel->getProperties()->setTitle('国内仓库存');
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	

	$objPHPExcel->getActiveSheet()->setCellValue('A1', '序号');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', '进仓单');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', '公司名称');	
	$objPHPExcel->getActiveSheet()->setCellValue('D1', '客户编号');
	$objPHPExcel->getActiveSheet()->setCellValue('E1', '订单号');
	$objPHPExcel->getActiveSheet()->setCellValue('F1', '数量');
	$objPHPExcel->getActiveSheet()->setCellValue('G1', '立方');
	$objPHPExcel->getActiveSheet()->setCellValue('H1', '金额');
	$objPHPExcel->getActiveSheet()->setCellValue('I1', '入库时间');
	$objPHPExcel->getActiveSheet()->setCellValue('J1', '摘要');
	$objPHPExcel->getActiveSheet()->setCellValue('K1', '库龄');

	

			// $phpColor->setRGB('FF0000'); 

	$i = 2;
	$j = 1;
	while ($a = $db->fetch_array($sql2)) {
		if ($a[status] == 1 || $a[status] == 2) { 
			$kuling = (strtotime(date("Y-m-d"))-strtotime($a['rukutime']))/(60*60*24);
		}elseif ($a['status'] == 3) {
			$kuling = (strtotime($a['chucangtime'])-strtotime($a['rukutime']))/(60*60*24);
		}elseif ($a['status'] == 10) {
			$kuling = (strtotime($a['tuicangtime'])-strtotime($a['rukutime']))/(60*60*24);
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $a[jincangdanhao]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $a[subject]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $a[userid]);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $a[orderid]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $a[count]);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $a[volume]);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $a[money]);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $a[rukutime]);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $a[remake]);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $kuling);
		if ($kuling > 5) {
			// $objPHPExcel->getActiveSheet()->getStyle("A".$i.":K".$i)->getFont()->setColor("6F6F6F");
			$objPHPExcel->getActiveSheet()->getStyle("A".$i.":K".$i)->getFont()->setColor( $phpColor );
		}
		$i++;
		$j++;
	}
	$i++;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '序号');
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '新增唛头');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '新增件数');
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '新增立方');

	$i++;
	$w = "1 and is_del = 1"; 
	if ($k_khtype) {
		$w .= " and khtype = ".$k_khtype;
	}
	$w .= " and rukutime = '".date('Y-m-d')."'";
	
	$newsql = $db->query("select * from yasa_warehouse where $w");
	$z = 1;
	while ($new = $db->fetch_array($newsql)) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $z);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $new['userid']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $new['count']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $new['volume']);
		$i++;
		$z++;
	}


	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="汇总表(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');
  	
  	$xlsData = ob_get_contents();
  	ob_end_clean();
	
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);


	

 ?>