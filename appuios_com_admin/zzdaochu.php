<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	/*
		1.
	 */ 
	
	if ($type == 1) {//转账清单
		$title = '转账清单'.date("Y-m-d");
		$filename = $title;
		$template_id = "3TkaR4s25WNoii4bpF19dmp3MRFihLD6m5HVaQ5h";
		$module = $db->get_one("select * from yasa_module where template_id = '$template_id'");
		$guize = unserialize($module[content]);

		$shenpisql = $db->query("select s.*,u.username from yasa_shenpi as s 
		left join yasa_user as u on u.id = s.userid where s.id in ($id)");
		

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:U1');



		$objPHPExcel->getActiveSheet()->setCellValue('A1', "越洋进出口贸易有限公司-7977");
		$objPHPExcel->getActiveSheet()->setCellValue('A3', "序号");
		$objPHPExcel->getActiveSheet()->setCellValue('B3', "业务员");
		$j = "C";
		foreach ($guize as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue($j.'3', $value[order]);
			$j++;
		}
		
		$i = 4;
		$shu = 1;
		while ($shenpi = $db->fetch_array($shenpisql)) {
			$z = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $shu);
			$z++;
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $shenpi[username]);
			$z++;
			$content = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);
			foreach ($guize as $key => $value) {
				if ($value[order] == '商家账号') {
					$objPHPExcel->getActiveSheet()->setCellValue($z.$i, "'".$content[$key]);
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content[$key]);
				}
				
				$z ++;
			}
			$i++;
			$shu++;


		}
		
	

		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}elseif ($type == 2) { //蚂蚁搬家推送
		$title = '蚂蚁搬家推送'.date("Y-m-d");
		$filename = $title;
		$template_id = "3TkaR4s25WNoii4bpF19dmp3MRFihLD6m5HVaQ5h";
		$module = $db->get_one("select * from yasa_module where template_id = '$template_id'");
		$guize = unserialize($module[content]);

		$shenpisql = $db->query("select s.*,u.username from yasa_shenpi as s 
		left join yasa_user as u on u.id = s.userid where s.id in ($id)");
		

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "序号");
		$objPHPExcel->getActiveSheet()->setCellValue('B1', "订单编号");
		$objPHPExcel->getActiveSheet()->setCellValue('C1', "客户编号");
		$objPHPExcel->getActiveSheet()->setCellValue('D1', "商家会员号");
		$objPHPExcel->getActiveSheet()->setCellValue('E1', "跟单编号");
		
		
		$i = 2;
		$shu = 1;
		while ($shenpi = $db->fetch_array($shenpisql)) {
			$z = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $shu);
			$z++;
			$content = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);
			foreach ($guize as $key => $value) {
				if ($value[order] == '订单编号' || $value[order] == '客户编号' || $value[order] == '商家会员号' || $value[order] == '跟单编号') {
					$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content[$key]);
					$z++;
				}
			}
			$i++;
			$shu++;


		}
		
	

		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}elseif ($type == 3) { //付款模板
		$title = '付款模板'.date("Y-m-d");
		$filename = $title;
		$template_id = "3TkaR4s25WNoii4bpF19dmp3MRFihLD6m5HVaQ5h";
		$module = $db->get_one("select * from yasa_module where template_id = '$template_id'");
		$guize = unserialize($module[content]);

		$shenpisql = $db->query("select s.*,u.username from yasa_shenpi as s 
		left join yasa_user as u on u.id = s.userid where s.id in ($id)");
		
		// $shenpisql1 = $db->query("select s.*,u.username from yasa_shenpi as s 
		// left join yasa_user as u on u.id = s.userid where s.id in ($id)");

		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "业务员");
		$objPHPExcel->getActiveSheet()->setCellValue('B1', "币种");
		$objPHPExcel->getActiveSheet()->setCellValue('C1', "订单编号");
		$objPHPExcel->getActiveSheet()->setCellValue('D1', "客户编号");
		$objPHPExcel->getActiveSheet()->setCellValue('E1', "付款");
		$objPHPExcel->getActiveSheet()->setCellValue('F1', "支付类型");
		$objPHPExcel->getActiveSheet()->setCellValue('G1', "摘要");
		$objPHPExcel->getActiveSheet()->setCellValue('H1', "日期");
		
		
		$i = 2;
		$shu = 1;
		while ($shenpi = $db->fetch_array($shenpisql)) {
			$z = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $shenpi[username]);
			$z++;
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, "RMB");
			$z++;
			$content = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);
			// echo $content['Selector-1575249886295'];die;
			// print_r($shenpi);
			foreach ($guize as $key => $value) {
				if ($value[order] == '订单编号' || $value[order] == '客户编号' || $value[order] == '实际应付款' || $value[order] == '支付类型' || $value[order] == '备注说明') {
					if ($value[order] == '实际应付款') {
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content['Number-1575249432100']+$content['Number-1578331494475']);
					}
					elseif ($value[order] == '备注说明') {
						if ($content[$key]) {
							$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content[$key]);
						}else{
							$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content['Selector-1575249404063']);//默认支付类型
						}
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content[$key]);
					}
					$z++;
				}
			}
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, date("Y-m-d",$shenpi[addtime]));
			$z++;

			$i++;
			$shu++;
			$shenpi1[] = $shenpi;
		}
		// echo $i;die;
		// 佣金
		foreach ($shenpi1 as $key => $shenpi) {
			
			$z = "A";
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $shenpi[username]);
			$z++;
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, "RMB");
			$z++;
			$content = json_decode($shenpi[content],JSON_UNESCAPED_UNICODE);

			// print_r($shenpi);
			foreach ($guize as $key => $value) {
				if ($value[order] == '订单编号' || $value[order] == '客户编号' || $value[order] == '实际应付款' || $value[order] == '支付类型' || $value[order] == '备注说明') {
					if ($value[order] == '实际应付款') {
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, ($content['Number-1575249432100']+$content['Number-1578331494475'])*$content['Selector-1575249886295']/100);
					}
					elseif ($value[order] == '支付类型') {	
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, '佣金');
					}
					elseif ($value[order] == '订单编号') {
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, 'YJ'.$content[$key]);		
					}
					elseif ($value[order] == '备注说明') {
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, '佣金');
					}
					else{
						$objPHPExcel->getActiveSheet()->setCellValue($z.$i, $content[$key]);
					}
					$z++;
				}
			}
			$objPHPExcel->getActiveSheet()->setCellValue($z.$i, date("Y-m-d",$shenpi[addtime]));
			$z++;

			$i++;
			$shu++;
		}
		
		// die;

		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}

 ?>