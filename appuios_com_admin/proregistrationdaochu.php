<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	/*
		
	 */ 

	if ($type == 1) {//
		$title = '物品库存'.date("Y-m-d");
		$filename = $title;


		$sql = $db->query("select proname,sum(p.num) as num,p.number,p.danwei,sum(p.num*p.danjia) as money,d.subject,p.cangku from yasa_proregistration as p left join yasa_diqu as d on p.cangku = d.id where p.is_del = 1 and p.pid = 0 group by p.number,p.cangku");
		while ($sq = $db->fetch_array($sql)) {

			$m = $db->get_one("select sum(num) as num,sum(danjia*num) as money from yasa_proregistration where is_del = 1 and pid != 0 and number = '".$sq['number']."' and cangku = $sq[cangku]");
			if (($sq[num] - $m[num]) > 0) {
				$sq['shengyu'] = $sq[num] - $m[num];
				$sq['shengyujine'] = $sq[money] - $m[money];
				$arr[] = $sq;
			}
		}



		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);


		$objPHPExcel->getActiveSheet()->setCellValue('A1', '货号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '品名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '单位');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '仓库');
		$i = 2;
		foreach ($arr as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $value[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[proname]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[shengyu]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[danwei]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value[shengyujine]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value[subject]);
			$i++;
		}

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}elseif ($type == 2) {
		$title = '物品入库'.date("Y-m-d");
		$filename = $title;


		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);


		$sql = $db->query("select p.*,d.subject from yasa_proregistration as p left join yasa_diqu as d on p.cangku = d.id where p.is_del = 1 and p.pid = 0");

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '类型');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '单号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '货号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '品名');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '单位');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '仓库');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '所属部门');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '采购员');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '采购时间');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '备注');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '添加时间');
		$i = 2;
		while ($value = $db->fetch_array($sql)) {
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '采购');
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[orderid]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[proname]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value[num]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $value[danwei]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $value[num]*$value[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $value[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $value[bumen]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $value[lyuser]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, date("Y-m-d",$value[lyaddtime]));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $value[remake]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, date("Y-m-d",$value[addtime]));
			$i++;
		}
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);

	}elseif ($type == 3) {
		$title = '物品出库'.date("Y-m-d");
		$filename = $title;


		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);


		$sql = $db->query("select p.*,d.subject from yasa_proregistration as p left join yasa_diqu as d on p.cangku = d.id where p.is_del = 1 and p.pid != 0");

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '类型');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '单号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '货号');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '品名');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '单位');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '仓库');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '所属部门');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '领用员');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '领用时间');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '备注');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '添加时间');
		$i = 2;
		while ($value = $db->fetch_array($sql)) {
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '领用');
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[orderid]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[number]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[proname]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value[num]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $value[danwei]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $value[num]*$value[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $value[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $value[bumen]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $value[lyuser]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, date("Y-m-d",$value[lyaddtime]));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $value[remake]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, date("Y-m-d",$value[addtime]));
			$i++;
		}
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);
	}

 ?>