<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");
include_once('api/PHPExcel.php');
include_once('api/PHPExcel/Writer/Excel2007.php');


	$starttime = isset($_GET["starttime"]) ? $_GET["starttime"] : null;
	$endtime = isset($_GET["endtime"]) ? $_GET["endtime"] : null;
	$userid = isset($_GET["userid"]) ? $_GET["userid"] : null;
	$orderid = isset($_GET["orderid"]) ? $_GET["orderid"] : null;
	$starttime1 = $starttime;
	$endtime1 = $endtime;





	

	$starttime = strtotime($starttime);
	$endtime = strtotime($endtime);
	$endtime = $endtime + (24*60*60);
	// 唛头 kehu.kehu_number 1
	// 订单号 order.orderid 1
	// 箱号 ruku.type1 1
	// 品名 xiangzi_huowu.subject 
	// 系统品名 product.subject 1
	// 款号 product.type1 1
	// 件/箱 xiangzi_huowu.type1 1
	// 总价 product.type5 1
	// 总数量 xiangzi_huowu.type1 zong 1
	// 箱数 ruku.type3 1
	// 单位 ruku.type4 1
	// 体积 ruku.type5 1
	// 重量 ruku.type6 1
	$where = "WHERE pxh.is_delete = 1 and pr.addtime >= $starttime and pr.addtime <=$endtime";
	if ($userid) {
		$where .= " and k.kehu_number = '$userid'";
	}
	if ($orderid) {
		$where .= " and o.order_id = '$orderid'";
	}
	$db->query("SET NAMES UTF8");
	$sql = "SELECT pxh.subject,pxh.type1 as jian,pr.type1 as xianghao,pr.type3 as xiangshu,pr.type4 as danwei,pr.type5 as tiji,pr.type6 as zhongliang,o.order_id,k.kehu_number,p.subject as subject1,p.type1 as kuanhao,p.type5 as danjia
	FROM yasa_product_xiangzi_huowu as pxh
	LEFT JOIN yasa_product_ruku as pr on pr.only_id = pxh.pid1
	LEFT JOIN yasa_product as p on p.id = pxh.pid3 
	LEFT JOIN yasa_order as o on o.id = pr.pid1
	LEFT JOIN yasa_kehu as k on k.id = o.pid1
	".$where;

	$result = $db->query($sql);



	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle($title);
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// 所有单元格水平居中
	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	// 合并单元格 表头
	$objPHPExcel->getActiveSheet()->mergeCells('A1:O1');
	$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:O3');
	$objPHPExcel->getActiveSheet()->mergeCells('A4:O4');
	$objPHPExcel->getActiveSheet()->mergeCells('A5:O5');

	$objPHPExcel->getActiveSheet()->mergeCells('D6:E6');
	$objPHPExcel->getActiveSheet()->mergeCells('D7:E7');

	$objPHPExcel->getActiveSheet()->getStyle('A1:O8')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);

	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName('宋体')->setSize(24)->getColor()->setRGB('000000'); 
	$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setName('宋体')->setSize(20)->getColor()->setRGB('000000'); 
	$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setName('宋体')->setSize(18)->getColor()->setRGB('000000'); 
	$objPHPExcel->getActiveSheet()->getStyle('A4:A5')->getFont()->setName('宋体')->setSize(16)->getColor()->setRGB('000000'); 
	$objPHPExcel->getActiveSheet()->getStyle('A6:O6')->getFont()->setName('宋体')->setSize(11)->getColor()->setRGB('000000'); 
	

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'GUANGZHOU YUEYANG IMPORT AND EXPORT CO.,LTD.');
	$objPHPExcel->getActiveSheet()->setCellValue('A2', '装箱单');
	$objPHPExcel->getActiveSheet()->setCellValue('A3', '（PACKING LIST)');
	$objPHPExcel->getActiveSheet()->setCellValue('A4', '电话（Tel）:0086-2081216640  装柜时间：2019-03-14');
	$objPHPExcel->getActiveSheet()->setCellValue('A5', '传真（Fax) :0086-2081216639  提货地点: DUBAI');
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'کد مشتری');
	$objPHPExcel->getActiveSheet()->setCellValue('B6', 'شماره فاکتور');
	$objPHPExcel->getActiveSheet()->setCellValue('C6', 'شماره کارتن');
	$objPHPExcel->getActiveSheet()->setCellValue('D6', 'نام جنس');
	$objPHPExcel->getActiveSheet()->setCellValue('F6', 'کد جنس');
	$objPHPExcel->getActiveSheet()->setCellValue('G6', 'تعداد در کارتن');
	$objPHPExcel->getActiveSheet()->setCellValue('H6', 'فی');
	$objPHPExcel->getActiveSheet()->setCellValue('I6', 'مبلغ');
	$objPHPExcel->getActiveSheet()->setCellValue('J6', 'تعداد کل');
	$objPHPExcel->getActiveSheet()->setCellValue('K6', 'تعداد کارتن');
	$objPHPExcel->getActiveSheet()->setCellValue('L6', 'واحد');
	$objPHPExcel->getActiveSheet()->setCellValue('M6', 'حجم');
	$objPHPExcel->getActiveSheet()->setCellValue('N6', 'وزن');

	$objPHPExcel->getActiveSheet()->setCellValue('A7', 'MARK');
	$objPHPExcel->getActiveSheet()->setCellValue('B7', 'ORDER');
	$objPHPExcel->getActiveSheet()->setCellValue('C7', 'CTN NO');
	$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Commodity');
	$objPHPExcel->getActiveSheet()->setCellValue('F7', 'ITEN NO.');
	$objPHPExcel->getActiveSheet()->setCellValue('G7', 'PCS/CTN');
	$objPHPExcel->getActiveSheet()->setCellValue('H7', 'PRICE(RMB)');
	$objPHPExcel->getActiveSheet()->setCellValue('I7', 'AMOUNT(RMB)');
	$objPHPExcel->getActiveSheet()->setCellValue('J7', 'T.T.QIY');
	$objPHPExcel->getActiveSheet()->setCellValue('K7', 'CTN');
	$objPHPExcel->getActiveSheet()->setCellValue('L7', '');
	$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Remark');
	$objPHPExcel->getActiveSheet()->setCellValue('N7', 'Weight');

	$objPHPExcel->getActiveSheet()->setCellValue('A8', '唛头');
	$objPHPExcel->getActiveSheet()->setCellValue('B8', '订单号');
	$objPHPExcel->getActiveSheet()->setCellValue('C8', '箱号');
	$objPHPExcel->getActiveSheet()->setCellValue('D8', '品名');
	$objPHPExcel->getActiveSheet()->setCellValue('E8', '系统品名');
	$objPHPExcel->getActiveSheet()->setCellValue('F8', '款号');
	$objPHPExcel->getActiveSheet()->setCellValue('G8', '件/箱');
	$objPHPExcel->getActiveSheet()->setCellValue('H8', '单价');
	$objPHPExcel->getActiveSheet()->setCellValue('I8', '总价');
	$objPHPExcel->getActiveSheet()->setCellValue('J8', '总数量');
	$objPHPExcel->getActiveSheet()->setCellValue('K8', '箱数');
	$objPHPExcel->getActiveSheet()->setCellValue('L8', '单位');
	$objPHPExcel->getActiveSheet()->setCellValue('M8', '体积');
	$objPHPExcel->getActiveSheet()->setCellValue('N8', '重量');
	
	$i = 9;
	while ($row = $db->fetch_array($result)) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row[kehu_number]);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row[order_id]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row[xianghao]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row[subject]);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row[subject1]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row[kuanhao]);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row[jian]);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row[danjia]);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, "=(G".$i."*H".$i.")");
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row[jian]);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row[xiangshu]);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row[danwei]);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row[tiji]);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row[zhongliang]);
		$i++;
	}
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '合计');
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I9:I'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J9:J'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=SUM(K9:K'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, '=SUM(M9:M'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=SUM(N9:N'.($i-1).')');

	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);

?>