<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');


	// 分类列表
	$protypelist0sql = "select * from  yasa_product_type order by number asc";
	$protypelist0 = $db->query($protypelist0sql);
	while($a=$db->fetch_array($protypelist0)){
		$protypelist[] = $a;
	}

	$start = isset($_GET["start"]) ? $_GET["start"] : null;
	$end = isset($_GET["end"]) ? $_GET["end"] : null;
	$title = $start.'~'.$end;
	$filename = '订单汇总表('.$start.'~'.$end.')';
	if($start && $end) {

		$start = strtotime($start);
		$end = strtotime($end);
		$end = $end + (24*60*60);
		// echo $start;
		// echo "<br>";
		// echo $end;


		// $result = $db->query("SELECT SUM(type1) AS total FROM yasa_product WHERE addtime >= $start AND addtime <= $end");
		// $row = $db->fetch_array($result);

		// $total = (int)$row["total"];
		// echo $total;

		$db->query("SET NAMES UTF8");
		// $result = $db->query("SELECT SUM(type1) AS money, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime >= $start AND addtime <= $end GROUP BY pid1 ORDER BY money DESC");
		

	// 	$sql = "SELECT C.id,C.order_id,C.addtime,E.kehu_number,B.subject,B.type1,B.type4,B.type5,B.type6,B.cate,D.subject as subject1,D.username,D.phone_ts,D.phone,D.address,F.nickname,A.type1 as xianzi_shuliang,B.type5 as product_price
	// FROM yasa_product_xiangzi_huowu as A 
	// left join yasa_product as B on B.id=A.pid3
	// left join yasa_order as C on C.id = B.pid1
	// left join yasa_shangjia as D on D.id = C.pid2
	// left join yasa_kehu as E on E.id = C.pid1
	// left join yasa_user as F on F.uid = C.pid3
	// WHERE C.addtime >= $start and C.addtime <= $end and A.is_delete = 1 and B.is_delete = 1 ORDER BY C.addtime DESC";
		$sql = "SELECT C.id,C.order_id,C.addtime,E.kehu_number,B.subject,B.type1,B.type4,B.type5,B.type6,B.cate,B.proid,D.subject as subject1,D.username,D.phone_ts,D.phone,D.address,F.nickname,B.type4 as xianzi_shuliang,B.type5 as product_price
	FROM
	yasa_product as B
	left join yasa_order as C on C.id = B.pid1
	left join yasa_shangjia as D on D.id = C.pid2
	left join yasa_kehu as E on E.id = C.pid1
	left join yasa_user as F on F.uid = C.pid3
	WHERE C.addtime >= $start and C.addtime <= $end and B.is_delete = 1 ORDER BY C.addtime DESC";
		// echo $sql;die;
		$result = $db->query($sql);
		// $db->close();

		
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '订单');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '翻译');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '名称');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '款号');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '一级产品分类');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '二级产品分类');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '联系人');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '联系电话1');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '联系电话2');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', '地址');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', '时间');



		$i = 2;
		while($row = $db->fetch_array($result)) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row[order_id]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row[kehu_number]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row[nickname]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row[type1]);
			$cate = $db->get_one("select uid from yasa_crm_cate where id = $row[proid]");
			$cate = explode(',',$cate[uid]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $cate[1]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row[proid]);

			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row[xianzi_shuliang]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row[product_price]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, floatval($row['xianzi_shuliang'])*floatval($row['product_price']));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row[subject1]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row[username]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row[phone_ts]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row[phone]);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row[address]);
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, date("Y-m-d H:i:s",$row[addtime]));
			$i += 1;
		}
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}

 ?>