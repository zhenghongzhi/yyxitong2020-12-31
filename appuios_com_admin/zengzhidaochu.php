<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	// echo $cstime;die;
	$zengzhi = $db->query("select z.*,k.subject from yasa_zengzhi as z left join yasa_kehutype as k on k.id = z.khtype where z.fytime ='$cstime' and z.is_del = 1");
	$zdan = $db->get_one("select * from yasa_zengzhiorder where cstime = '$cstime'");

	
	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle($title);
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	// 所有单元格水平居中
	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	// 合并单元格 表头
	$objPHPExcel->getActiveSheet()->mergeCells('A1:S1');

	$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
	$objPHPExcel->getActiveSheet()->mergeCells('G2:I2');
	$objPHPExcel->getActiveSheet()->mergeCells('J2:R2');

	$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
	$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
	$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
	$objPHPExcel->getActiveSheet()->mergeCells('D3:D4');
	$objPHPExcel->getActiveSheet()->mergeCells('E3:E4');
	$objPHPExcel->getActiveSheet()->mergeCells('F3:F4');

	$objPHPExcel->getActiveSheet()->mergeCells('G3:I3');
	$objPHPExcel->getActiveSheet()->mergeCells('J3:L3');

	$objPHPExcel->getActiveSheet()->mergeCells('M3:M4');
	$objPHPExcel->getActiveSheet()->mergeCells('N3:N4');
	$objPHPExcel->getActiveSheet()->mergeCells('O3:O4');
	$objPHPExcel->getActiveSheet()->mergeCells('P3:P4');
	$objPHPExcel->getActiveSheet()->mergeCells('Q3:Q4');
	$objPHPExcel->getActiveSheet()->mergeCells('R3:R4');
	$objPHPExcel->getActiveSheet()->mergeCells('S3:S4');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', '广州越洋增值服务费账单');
	$objPHPExcel->getActiveSheet()->setCellValue('A2', '单号/票号：'.$zdan['danhao']);
	$objPHPExcel->getActiveSheet()->setCellValue('G2', '产生日期：'.$cstime);
	$objPHPExcel->getActiveSheet()->setCellValue('J2', '发货柜号:'.$zdan['guihao']);
	$objPHPExcel->getActiveSheet()->setCellValue('A3', '发货票号');
	$objPHPExcel->getActiveSheet()->setCellValue('B3', '客户归属');
	$objPHPExcel->getActiveSheet()->setCellValue('C3', '客户唛头');
	$objPHPExcel->getActiveSheet()->setCellValue('D3', '订单号');
	$objPHPExcel->getActiveSheet()->setCellValue('E3', '总件数');
	$objPHPExcel->getActiveSheet()->setCellValue('F3', '打包件数');
	$objPHPExcel->getActiveSheet()->setCellValue('G3', '国内仓库');
	$objPHPExcel->getActiveSheet()->setCellValue('J3', '国外仓库');
	$objPHPExcel->getActiveSheet()->setCellValue('M3', '总计');
	$objPHPExcel->getActiveSheet()->setCellValue('N3', '计费时间段');
	$objPHPExcel->getActiveSheet()->setCellValue('O3', '服务项目');
	$objPHPExcel->getActiveSheet()->setCellValue('P3', '成本');
	$objPHPExcel->getActiveSheet()->setCellValue('Q3', '成本计算方式');
	$objPHPExcel->getActiveSheet()->setCellValue('R3', '供应商');
	$objPHPExcel->getActiveSheet()->setCellValue('S3', '备注');

	$objPHPExcel->getActiveSheet()->setCellValue('G4', '单价');
	$objPHPExcel->getActiveSheet()->setCellValue('H4', '数量');
	$objPHPExcel->getActiveSheet()->setCellValue('I4', '金额');
	$objPHPExcel->getActiveSheet()->setCellValue('J4', '原币金额');
	$objPHPExcel->getActiveSheet()->setCellValue('K4', '汇率');
	$objPHPExcel->getActiveSheet()->setCellValue('L4', '人民币金额');
	$i = 5;
	while ($z = $db->fetch_array($zengzhi)) {
		$allcount += $z[count];
		$alldbcount += $z[dbcount];
		$allchengben += round($z[chengben]);
		if ($z[is_cang] == 1) {
			$allguonei += round($z[danjia]*$z[shuliang]);
			$allzong += round($z[danjia]*$z[shuliang]);
		}else{
			$allguowai += round($z[danjia]*$z[huilv]);
			$allzong += round($z[danjia]*$z[huilv]);
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $z[piaohao]);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $z[subject]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $z[userid]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $z[orderid]);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $z[count]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $z[dbcount]);
		if ($z[is_cang] == 1) {
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $z[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $z[shuliang]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, round($z[danjia]*$z[shuliang]));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, round($z[danjia]*$z[shuliang]));
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $z[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $z[huilv]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, round($z[danjia]*$z[huilv]));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, round($z[danjia]*$z[huilv]));
		}
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $z[jifeitime]);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $z[xiangmu]);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $z[chengben]);
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $z[chengbenjisuan]);
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $z[gongyingshang]);
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $z[remake]);
		$i++;
	}

	$objPHPExcel->getActiveSheet()->setCellValue('D'.($i+1), '合计：');
	$objPHPExcel->getActiveSheet()->setCellValue('E'.($i+1), $allcount);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.($i+1), $alldbcount);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.($i+1), round($allguonei));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.($i+1), round($allguowai));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+1), round($allzong));
	$objPHPExcel->getActiveSheet()->setCellValue('P'.($i+1), round($allchengben));

	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);

 ?>