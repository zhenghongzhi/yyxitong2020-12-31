<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	/*
		1 运单excel导出
		2 运单pdf导出
		3 运单客户excel导出
		4 运单客户pdf导出
		5 运单财务导出
		6 按公司运单execl导出
	 */ 
	
	if ($type == 1) {//
		$title = '账单'.date("Y-m-d");
		$filename = $title;
		// $yunfeidan = $db->get_one("select y.*,a.username,a1.username as username1,m.subject from yasa_ylyunfeidan as y 
		// left join cuiniao_admin as a on y.luruyuanid = a.id 
		// left join cuiniao_admin as a1 on a1.id = y.spid 
		// left join yasa_muchuan as m on m.id = y.muchuanid
		// where y.id = '$id'");

		$yunfeicount = $db->query("select yc.*,k.subject from yasa_ylyunfei_content as yc
		left join yasa_kehutype as k on k.id = yc.kehutype where yc.yid = $id");
		$yunfeidan = $db->get_one("select y.*,m.subject from yasa_ylyunfeidan as y left join yasa_muchuan as m on m.id = y.muchuanid  where y.id = $id");

		
		$all = $db->get_one("select sum(sjcount*danjia) as zong,sum(other) as other,sum(chengben) as chengben from yasa_ylyunfei_content as yc
		left join yasa_kehutype as k on k.id = yc.kehutype where yc.yid = $id");


		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('A2:P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		// 字体大小12
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:Q7')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension("B:D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F:Q")->setAutoSize(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);


		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');

		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');

		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('H3:M3');

		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('H4:M4');

		



		$objPHPExcel->getActiveSheet()->setCellValue('A1', "运费单");
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "批次号：".$yunfeidan[number]);
		$objPHPExcel->getActiveSheet()->setCellValue('A3', "迪拜发货时间:".$yunfeidan[fhtime]);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', "木船号：".$yunfeidan[muchuanhao]);

		$objPHPExcel->getActiveSheet()->setCellValue('A4', "派送时间：".$yunfeidan[pstime]);
		$objPHPExcel->getActiveSheet()->setCellValue('H4', "木船公司：".$yunfeidan[subject]);


		$objPHPExcel->getActiveSheet()->setCellValue('A5', "序号");
		$objPHPExcel->getActiveSheet()->setCellValue('B5', "客户归属");
		$objPHPExcel->getActiveSheet()->setCellValue('C5', "客户编号");
		$objPHPExcel->getActiveSheet()->setCellValue('D5', "货物数量");
		$objPHPExcel->getActiveSheet()->setCellValue('E5', "产品名称");
		$objPHPExcel->getActiveSheet()->setCellValue('F5', "计量数");
		$objPHPExcel->getActiveSheet()->setCellValue('G5', "单位");
		$objPHPExcel->getActiveSheet()->setCellValue('H5', "单价");
		$objPHPExcel->getActiveSheet()->setCellValue('I5', "运费总额");
		$objPHPExcel->getActiveSheet()->setCellValue('J5', "其他费用");
		$objPHPExcel->getActiveSheet()->setCellValue('K5', "成本");
		$objPHPExcel->getActiveSheet()->setCellValue('L5', "毛利");
		$objPHPExcel->getActiveSheet()->setCellValue('M5', "备注");
		

		$i = 6;
		$j = 1;
		while ($a = $db->fetch_array($yunfeicount)) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $a[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $a[userid]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $a[count]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $a[proname]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $a[sjcount]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $a[danwei]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $a[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $a[sjcount]*$a[danjia]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $a[other]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $a[chengben]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $a[danjia]*$a[sjcount]+$a[other]-$a[chengben]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $a[remake]);
			$i++;
		}

		$style_array = array(
        	'borders' => array(
            	'allborders' => array(
                	'style' => \PHPExcel_Style_Border::BORDER_THIN
            	)
       		)
        );
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,'=SUM(I6:I'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i,'=SUM(J6:J'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i,'=SUM(K6:K'.($i-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i,'=SUM(L6:L'.($i-1).')');

		$objPHPExcel->getActiveSheet()->setCellValue('L'.($i+3),'收入');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+3),'=ROUND(SUM(I'.($i).':J'.($i).'),0)');

		$objPHPExcel->getActiveSheet()->setCellValue('L'.($i+4),'成本');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+4),'=K'.$i);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.($i+5),'毛利');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+5),"=ROUND(M".($i+3)."-M".($i+4).",0)");

		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+6),'制单人');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.($i+6),$yunfeidan[username]);

		$objPHPExcel->getActiveSheet()->setCellValue('E'.($i+6),'审批人');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.($i+6),$yunfeidan[username1]);

		$objPHPExcel->getActiveSheet()->setCellValue('I'.($i+6),'总经理签字');
		$objPHPExcel->getActiveSheet()->setCellValue('M'.($i+6),'财务签字');
		// if($yunfeidan[fahuo]=='空运'){ 
		// 	$objPHPExcel->getActiveSheet()->getStyle('A1:Q'.($i-1))->applyFromArray($style_array);

		// }else{
		// 	$objPHPExcel->getActiveSheet()->getStyle('A1:P'.($i-1))->applyFromArray($style_array);
		// }
		

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}

 ?>