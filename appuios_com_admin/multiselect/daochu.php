<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include('api/PHPExcel.php');
	include('api/PHPExcel/Writer/Excel2007.php');

	$start = isset($_GET["start"]) ? $_GET["start"] : null;
	$end = isset($_GET["end"]) ? $_GET["end"] : null;
	$title = $start.'~'.$end;
	$filename = '订单汇总表('.$start.'~'.$end.')';

	if($start && $end) {

		$start = strtotime($start);
		$end = strtotime($end);
		$end = $end + (24*60*60);
		// echo $start;
		// echo "<br>";
		// echo $end;

		$servername = "localhost";
		$username = "root";
		$password = "root";
		$dbname = "yy1.0";
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
		    die("数据库连接失败: " . $conn->connect_error);
		}

		$result = $conn->query("SELECT SUM(type1) AS total FROM yasa_product WHERE addtime >= $start AND addtime <= $end");
		$row = $result->fetch_assoc();

		$total = (int)$row["total"];
		// echo $total;

		$conn->query("SET NAMES UTF8");
		// $result = $conn->query("SELECT SUM(type1) AS money, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime >= $start AND addtime <= $end GROUP BY pid1 ORDER BY money DESC");
		
		$sql = "SELECT o.order_id,o.addtime,k.kehu_number,p.subject,p.type1,p.type4,p.type5,p.type6,s.subject as subject1,s.username,s.phone_ts,s.phone,s.address,u.nickname FROM yasa_product as p left join yasa_order as o on p.pid1 = o.id left join yasa_shangjia as s on o.pid2 = s.id left join yasa_kehu as k on o.pid1 = k.id left join yasa_user as u on o.pid3 = u.uid WHERE o.addtime >= $start and o.addtime <= $end ORDER BY o.addtime DESC limit 10";
		// echo $sql;die;
		$result = $conn->query($sql);
		$conn->close();

		function convertUTF8($str)
		{
		   if(empty($str)) return '';
		   return  iconv('gb2312', 'utf-8', $str);
		}
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);

		


		$objPHPExcel->getActiveSheet()->setCellValue('A1', '订单');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户编号');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '翻译');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '名称');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', '款号');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', '数量');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '单价');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', '金额');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', '供应商');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', '联系人');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', '联系电话1');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', '联系电话2');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', '地址');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', '时间');



		$i = 2;
		while($row = $result->fetch_assoc()) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row[order_id]);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row[kehu_number]);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row[nickname]);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row[subject]);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row[type1]);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row[type4]);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row[type5]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row[type6]);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row[subject1]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row[username]);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row[phone_ts]);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row[phone]);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row[address]);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, date("Y-m-d H:i:s",$row[addtime]));
			$i += 1;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);


	}

 ?>