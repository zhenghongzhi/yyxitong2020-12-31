<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once("checkuser.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');
	$sql = $db->query("select pr.id,k.kehu_number,o.order_id,pr.type1 as xianghao,pxh.subject,p.subject as subject1,p.type1 as kuanhao,pxh.type1 as jian,p.type5 as danjia,pr.type2 as zongshuliang,pr.type3 as xiangshu,pr.type4 as danwei,pr.type5 as tiji,pr.type6 as zhongliang 
		from yasa_product_xiangzi_huowu as pxh 
		left join yasa_product_ruku as pr on pr.only_id = pxh.pid1
		left join yasa_order as o on pr.pid1 = o.id 
	  	left join yasa_kehu as k on o.pid1 = k.id
	  	left join yasa_product as p on p.id = pxh.pid3 
	  	where pr.id in (".$_GET['xiangziid'].")");

	while ($sql1 = $db->fetch_array($sql)) {
		$arr[$sql1[id]][] = $sql1;
	}
	if ($type) {
		$title = '装箱单'.date("Y-m-d");
		$filename = $title;
		$objPHPExcel = new PHPExcel();
		// 标题
		$objPHPExcel->getProperties()->setTitle($title);
		// 设置当前的sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// 所有单元格水平居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		// 字体大小12
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:N8')->getFont()->setBold(true);

		// 合并单元格 表头
		$objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:N2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:N3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:F4');
		$objPHPExcel->getActiveSheet()->mergeCells('G4:N4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:N5');
		$objPHPExcel->getActiveSheet()->mergeCells('D6:E6');
		$objPHPExcel->getActiveSheet()->mergeCells('D7:E7');


		$objPHPExcel->getActiveSheet()->setCellValue('A1', "GUANGZHOU YUEYANG LEMON STORAGE CO. LTD");
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "装箱单");
		$objPHPExcel->getActiveSheet()->setCellValue('A3', "（PACKING LIST)");
		$objPHPExcel->getActiveSheet()->setCellValue('A4', " 电话（Tel）:0086-2081216640  装柜时间：");
		$objPHPExcel->getActiveSheet()->setCellValue('G4', " 预计到港时间：");
		$objPHPExcel->getActiveSheet()->setCellValue('A5', "传真（Fax) :0086-2081216639  提货地点:DUBAI");

		$objPHPExcel->getActiveSheet()->setCellValue('A6', "کد مشتری");
		$objPHPExcel->getActiveSheet()->setCellValue('B6', "شماره فاکتور");
		$objPHPExcel->getActiveSheet()->setCellValue('C6', "شماره کارتن");
		$objPHPExcel->getActiveSheet()->setCellValue('D6', "نام جنس");
		$objPHPExcel->getActiveSheet()->setCellValue('F6', "کد جنس");
		$objPHPExcel->getActiveSheet()->setCellValue('G6', "تعداد در کارتن");
		$objPHPExcel->getActiveSheet()->setCellValue('H6', "فی");
		$objPHPExcel->getActiveSheet()->setCellValue('I6', "مبلغ");
		$objPHPExcel->getActiveSheet()->setCellValue('J6', "تعداد کل");
		$objPHPExcel->getActiveSheet()->setCellValue('K6', "تعداد کارتن");
		$objPHPExcel->getActiveSheet()->setCellValue('L6', "واحد");
		$objPHPExcel->getActiveSheet()->setCellValue('M6', "حجم");
		$objPHPExcel->getActiveSheet()->setCellValue('N6', "وزن");

		$objPHPExcel->getActiveSheet()->setCellValue('A7', "MARK");
		$objPHPExcel->getActiveSheet()->setCellValue('B7', "ORDER");
		$objPHPExcel->getActiveSheet()->setCellValue('C7', "CTN NO");
		$objPHPExcel->getActiveSheet()->setCellValue('D7', "Commodity");
		$objPHPExcel->getActiveSheet()->setCellValue('F7', "ITEN NO.");
		$objPHPExcel->getActiveSheet()->setCellValue('G7', "PCS/CTN");
		$objPHPExcel->getActiveSheet()->setCellValue('H7', "PRICE(RMB)");
		$objPHPExcel->getActiveSheet()->setCellValue('I7', "AMOUNT(RMB)");
		$objPHPExcel->getActiveSheet()->setCellValue('J7', "T.T.QIY");
		$objPHPExcel->getActiveSheet()->setCellValue('K7', "CTN");
		$objPHPExcel->getActiveSheet()->setCellValue('L7', "");
		$objPHPExcel->getActiveSheet()->setCellValue('M7', "Remark");
		$objPHPExcel->getActiveSheet()->setCellValue('N7', "Weight");

		$objPHPExcel->getActiveSheet()->setCellValue('A8', "唛头");
		$objPHPExcel->getActiveSheet()->setCellValue('B8', "订单号");
		$objPHPExcel->getActiveSheet()->setCellValue('C8', "箱号");
		$objPHPExcel->getActiveSheet()->setCellValue('D8', "商家品名");
		$objPHPExcel->getActiveSheet()->setCellValue('E8', "系统品名（中文+波斯语）");
		$objPHPExcel->getActiveSheet()->setCellValue('F8', "款号");
		$objPHPExcel->getActiveSheet()->setCellValue('G8', "件/箱");
		$objPHPExcel->getActiveSheet()->setCellValue('H8', "单价");
		$objPHPExcel->getActiveSheet()->setCellValue('I8', "总价");
		$objPHPExcel->getActiveSheet()->setCellValue('J8', "总数量");
		$objPHPExcel->getActiveSheet()->setCellValue('K8', "箱数");
		$objPHPExcel->getActiveSheet()->setCellValue('L8', "单位");
		$objPHPExcel->getActiveSheet()->setCellValue('M8', "体积");
		$objPHPExcel->getActiveSheet()->setCellValue('N8', "重量");
		$i = 9;
		// print_r($arr);die;
		foreach ($arr as $key => $value) { 
			$count = count($value);
			foreach ($value as $ke => $val) { 
				$jiancount += $val[jian];
				$moneycount += $val[danjia]*$val[jian];
				if ($ke == 0) {
					$xiangshucount += $val[xiangshu];
					$tijicount += $val[tiji];
					$zhongliangcount += $val[zhongliang];
					$z = $i + $count - 1;
					// $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$z);

					$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':A'.$z)->setCellValue('A'.$i, $val[kehu_number]);
					$objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':B'.$z)->setCellValue('B'.$i, $val[order_id]);
					$objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':C'.$z)->setCellValue('C'.$i, $val[xianghao]);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val[subject]);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val[subject1]);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[kuanhao]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val[jian]);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val[danjia]);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val[danjia]*$val[jian]);
					$objPHPExcel->getActiveSheet()->mergeCells('J'.$i.':J'.$z)->setCellValue('J'.$i, $val[zongshuliang]);
					$objPHPExcel->getActiveSheet()->mergeCells('K'.$i.':K'.$z)->setCellValue('K'.$i, $val[xiangshu]);
					$objPHPExcel->getActiveSheet()->mergeCells('L'.$i.':L'.$z)->setCellValue('L'.$i, $val[danwei]);
					$objPHPExcel->getActiveSheet()->mergeCells('M'.$i.':M'.$z)->setCellValue('M'.$i, $val[tiji]);
					$objPHPExcel->getActiveSheet()->mergeCells('N'.$i.':N'.$z)->setCellValue('N'.$i, $val[zhongliang]);
				}else{
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val[subject]);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val[subject1]);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val[kuanhao]);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val[jian]);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val[danjia]);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val[danjia]*$val[jian]);
				}
				$i++;
			}
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $jiancount);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $moneycount);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $xiangshucount);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, "");
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $tijicount);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $zhongliangcount);
		// die;
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_start();//清除缓冲区,避免乱码
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
		header('Cache-Control: max-age=0');
      	$objWriter->save('php://output');

      	$xlsData = ob_get_contents();
      	ob_end_clean();
		
		$data[filename] = $filename;
		$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

      	echo json_encode($data);die;
		
	}
	
	// echo '<pre>';
	// print_r($arr);die;
 ?>
 <html>
	<head>
		<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/layer/layer.js"></script>
	</head>
	<body>
		
		<form id="form">
			<div class="content">
				<style>	
					table{
						text-align: center;
					}
					table tr td{
						height: 30px;
						line-height: 30px;
					}
					.font{
						font-weight: bold;
					}
				</style>
				<table border="1" style="margin: 0px auto;" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="14" class="font">GUANGZHOU YUEYANG LEMON STORAGE CO. LTD</td>
					</tr>
					<tr>
						<td colspan="14" class="font">装箱单</td>
					</tr>
					<tr>
						<td colspan="14" class="font">（PACKING LIST)</td>
					</tr>
					<tr>
						<td colspan="7" class="font"> 电话（Tel）:0086-2081216640  装柜时间：2020-07-26</td>
						<td colspan="7" class="font">预计到港时间：2020-00-00</td>
					</tr>
					<tr>
						<td colspan="14" class="font">传真（Fax) :0086-2081216639  提货地点:DUBAI</td>
					</tr>
					<tr>
						<td class="font">کد مشتری</td>
						<td class="font">شماره فاکتور</td>
						<td class="font">شماره کارتن</td>
						<td class="font" colspan="2">نام جنس</td>
						<td class="font">کد جنس</td>
						<td class="font">تعداد در کارتن</td>
						<td class="font">فی</td>
						<td class="font">مبلغ</td>
						<td class="font">تعداد کل</td>
						<td class="font">تعداد کارتن</td>
						<td class="font">واحد</td>
						<td class="font">حجم</td>
						<td class="font">وزن</td>
					</tr>
					<tr>
						<td class="font">MARK</td>
						<td class="font">ORDER</td>
						<td class="font">CTN NO</td>
						<td class="font" colspan="2">Commodity</td>
						<td class="font">ITEN NO.</td>
						<td class="font">PCS/CTN</td>
						<td class="font">PRICE(RMB)</td>
						<td class="font">AMOUNT(RMB)</td>
						<td class="font">T.T.QIY</td>
						<td class="font">CTN</td>
						<td class="font"></td>
						<td class="font">Remark</td>
						<td class="font">Weight</td>
					</tr>
					<tr>
						<td class="font">唛头</td>
						<td class="font">订单号</td>
						<td class="font">箱号</td>
						<td class="font">商家品名</td>
						<td class="font">系统品名（中文+波斯语）</td>
						<td class="font">款号</td>
						<td class="font">件/箱</td>
						<td class="font">单价</td>
						<td class="font">总价</td>
						<td class="font">总数量</td>
						<td class="font">箱数</td>
						<td class="font">单位</td>
						<td class="font">体积</td>
						<td class="font">重量</td>
					</tr>
					<?php foreach ($arr as $key => $value) { 
						$count = count($value);
						foreach ($value as $ke => $val) { 
								$jiancount += $val[jian];
								$moneycount += $val[danjia]*$val[jian];
								if ($ke == 0) { 
								$xiangshucount += $val[xiangshu];
								$tijicount += $val[tiji];
								$zhongliangcount += $val[zhongliang];?>
									<tr>
										<td rowspan="<?=$count; ?>"><?=$val[kehu_number]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[order_id]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[xianghao]; ?></td>
										<td><?=$val[subject]; ?></td>
										<td><?=$val[subject1]; ?></td>
										<td><?=$val[kuanhao]; ?></td>
										<td><?=$val[jian]; ?></td>
										<td><?=$val[danjia]; ?></td>
										<td><?=$val[danjia]*$val[jian]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[zongshuliang]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[xiangshu]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[danwei]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[tiji]; ?></td>
										<td rowspan="<?=$count; ?>"><?=$val[zhongliang]; ?></td>
									</tr>
								<? }else{ ?>
									<tr>
										<td><?=$val[subject]; ?></td>
										<td><?=$val[subject1]; ?></td>
										<td><?=$val[kuanhao]; ?></td>
										<td><?=$val[jian]; ?></td>
										<td><?=$val[danjia]; ?></td>
										<td><?=$val[danjia]*$val[jian]; ?></td>
									</tr>
								<?}
							?>	
					<?php }
					} ?>
					<tr>
						<td class="font"></td>
						<td class="font"></td>
						<td class="font"></td>
						<td class="font"></td>
						<td class="font"></td>
						<td class="font"></td>
						<td class="font"><?=$jiancount?></td>
						<td class="font"></td>
						<td class="font"><?=$moneycount?></td>
						<td class="font"></td>
						<td class="font"><?=$xiangshucount?></td>
						<td class="font"></td>
						<td class="font"><?=$tijicount?></td>
						<td class="font"><?=$zhongliangcount?></td>
					</tr>
				</table>
			</div>
			<input type="button" class="dayin" value="打印">
	 		<input type="button" onclick="daochu(3)" value="导出">	
		</form>
		<script>
			var index = parent.layer.getFrameIndex(window.name);
			$('.cancel').click(function(){
				parent.layer.close(index);
			})
			function daochu(type){
				var id = "<?=$xiangziid; ?>";
				$.ajax({
			      type:'GET',
			      url: 'xiangzi.php',
			      dataType:'json',
			      data: {'type':1,'xiangziid':id},
			      beforeSend: function(request) {
			        request.setRequestHeader("Authorization", "token信息，验证身份");
			      },
			      success: function(redata) {
			      	console.log(redata)
			        // 创建a标签，设置属性，并触发点击下载
			        var $a = $("<a>");
			        $a.attr("href", redata.file);
			        $a.attr("download", redata.filename);
			        $("body").append($a);
			        $a[0].click();
			        $a.remove();
			      }
			    });
			 }

			$('.dayin').click(function(){
			    var printWindow = window.open("打印窗口","_blank");
				//将明细页面拷贝到新的窗口，这样新的窗口就有了明细页面的样式
				var htmlbox = $("html").html();
				$('.show').hide();
				printWindow.document.write(htmlbox);
				//获得要打印的内容
				var printbox = $(".content").html();
				//将要打印的内容替换到新窗口的body中
				printWindow.document.body.innerHTML = printbox;
				//脚本向窗口(不管是本窗口或其他窗口)写完内容后，必须关闭输出流。在延时脚本的最后一个document.write()方法后面，必须确保含有document.close()方法，不这样做就不能显示图像和表单。
				//并且，任何后面调用的document.write()方法只会把内容追加到页面后，而不会清除现有内容来写入新值。
				printWindow.document.close();
				//打印
				//chrome浏览器使用jqprint插件打印时偶尔空白页问题
				//解决方案：有时候页面总是加载不出来，打印出现空白页，可以设置延迟时间来等待页面的渲染，但是渲染时间的长短跟页面的大小有关，是不固定的。所以这里使用事件监听器。
				// print.portrait   =  false    ;//横向打印 
				printWindow.addEventListener('load', function(){
					printWindow.print();
					//关闭窗口
					printWindow.close(
						$('.show').show()
					);
				});
			})

		</script>
	</body>
</html>