<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once("checkuser.php");

	$warehousesql = $db->query("select w.userid,w.khtype,k.subject,w.count from yasa_ylwarehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype 
		where w.id in($id)");
	$muchuan = $db->query("select * from yasa_muchuan");

	
 ?>

<html>
	<head>
		<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/layer/layer.js"></script>
	</head>
	<body>
		<style>	
			.kk{
				width: 640px;
				margin: 0px auto;
				text-align: center;
				padding: 0px;
			}
			.kk td,table th{
				border:1px solid #000;
			}
			h2{
				text-align: center;	
			}
			input{
				width: 50px;
			}
		</style>
		<h2>运费单</h2>
		
		<form id="form">
			<input type="hidden" value="<?=$_SESSION[cuiniao_id]; ?>" name="luruyuanid">
			<input type="hidden" value="<?=$id; ?>" name="wid">
		<table border="0" style="margin-left: 100px;">
			<tr>
				<td colspan="2" style="text-align: center;">批次号 <input type="text" name="number" id="number" style="width: 160px;"></td>
			</tr>
			<tr>
				<td>迪拜发货时间
				
					<input type="date" name="fhtime" value="<?=date('Y-m-d'); ?>" style="width: 160px;">
					
				</td>
				<td>木船号
					<input type="text" name="muchuanhao" style="width: 160px;" id="muchuanhao">
				</td>
			</tr>
			<tr>
				<td>派送时间
					<input type="date" name="pstime" value="<?=date('Y-m-d'); ?>" style="width: 160px;">
				</td>
				<td>木船服务商
					<select name="muchuanid" id="muchuanid">
						<option value="0">请选择</option>
						<?php while ($muchuan1 = $db->fetch_array($muchuan)) { ?>
							<option value="<?=$muchuan1[id]; ?>"><?=$muchuan1[subject]; ?></option>
						<? } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					汇率：<input type="text" name="huilv" id="huilv" value="1">
					成本币种选择
					<select name="bizhong" id="">
						<option value="CNY">CNY</option>
						<option value="USD">USD</option>
						<option value="IRR">IRR</option>
					</select>
				</td>
			</tr>
			
		</table>
		
		</p>
			<table  cellpadding="0" cellspacing="0" class="kk">
				<tr>
					<th width="427">客户归属</th>
					<th width="427">客户编号</th>
					<th width="427">货物数量</th>
					<th width="427">产品名称</th>
					<th width="427">计量数</th>
					<th width="427">单位</th>
					<th width="427">单价</th>
					<th width="427">其他费用</th>
					<th width="427">成本</th>
					<th width="427">备注</th>
				</tr>
				<?php while ($a = $db->fetch_array($warehousesql)) { ?>
					<tr>
					<th width="427"><?=$a[subject]; ?><input type="hidden" name="khtypearr[]" value="<?=$a[khtype]?>"></th>
					<th width="427"><?=$a[userid]; ?><input type="hidden" name="useridarr[]" value="<?=$a[userid]?>"></th>
					<th width="427"><?=$a[count]; ?><input type="hidden" name="countarr[]" value="<?=$a[count]?>"></th>
					<th width="427"><input type="text" name="pronamearr[]" ></th>
					<th width="427"><input type="text" name="sjcountarr[]" ></th>
					<th width="427"><input type="text" name="danweiarr[]" ></th>
					<th width="427"><input type="text" name="danjiaarr[]" ></th>
					<th width="427"><input type="text" name="otherarr[]" ></th>
					<th width="427"><input type="text" name="chengbenarr[]" ></th>
					<th width="427"><input type="text" name="remakearr[]" ></th>
				</tr>
				<? } ?>
				<tr>
					<td colspan="11" style="text-align: right;">
						<input type="button" value="确定" class="sure"> 
						<input type="button" value="取消" class="cancel"> 
					</td>
				</tr>
			</table>
		</form>
		<script>
			var index = parent.layer.getFrameIndex(window.name);
			$('.cancel').click(function(){
				parent.layer.close(index);
			})
			$('.sure').click(function(){
				if (sure('确定运费单吗？')) {
					var number = $("#number").val();
					var muchuanhao = $("#muchuanhao").val();
					var muchuanid = $("#muchuanid").val();
					var huilv = $("#huilv").val();
					if (!number) {
						alert("请输入批次号");
						return false;
					}
					if (!muchuanhao) {
						alert("请输入木船号");
						return false;
					}
					if (!muchuanid) {
						alert("请选择木船");
						return false;
					}
					if (!huilv) {
						alert("请输入汇率");
						return false;
					}



					var form = $("#form").serialize();
					$.ajax({ url: "ylyunfeidan1.php",dataType: "json", data: form, success: function(msg){
						if (msg.code == 1) {
				       		layer.msg(msg.msg,{time: 2000},function(){
				       			window.parent.location.reload();
				       			parent.layer.close(index);
				       		});
						}else{
							layer.msg(msg.msg,{time: 2000},function(){

							});
						}
				    }});
				}
				
			})
			function sure(obj){
		      if(confirm(obj)){
		        return true;
		      }else{
		        return false;
		      }
		  	}
		</script>
	</body>
 </html>