<?php 
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once("checkuser.php");


	$zengzhi = $db->query("select z.*,k.subject from yasa_zengzhi as z left join yasa_kehutype as k on k.id = z.khtype where z.fytime ='$cstime' and z.is_del = 1");
	$zdan = $db->get_one("select * from yasa_zengzhiorder where cstime = '$cstime'");

	
 ?>
 <html>
	<head>
		<script type="text/javascript" src="js/Validform5.3.2/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/layer/layer.js"></script>
	</head>
	<body>
		<style>	
			.k{
				margin: 0px auto;
				text-align: center;
				padding: 0px;
				font-size: 12px;
			}
			.k td,.k th{
				border-color: #f0f0f0;
				width: 50px;
    			height: 30px;
			}
			h2{
				text-align: center;	
			}
			input{
				width: 50px;
			}
			.noborder{
				border: 0px !important;
			}
		</style>
		<h2>广州越洋增值服务费账单</h2>
		<form id="form">
			<div class="content">
			<table cellpadding="0" cellspacing="0" border="1" class="k" style="border:2px solid #000;">
				<tr>
					<td colspan="6">单号/票号：<?=$zdan['danhao']; ?></td>
					<td colspan="3">产生日期：<?=$cstime; ?></td>
					<td colspan="10">发货柜号：<?=$zdan['guihao']; ?></td>
				</tr>
				<tr>
					<td rowspan="2">发货票号</td>
					<td rowspan="2">客户归属</td>
					<td rowspan="2">客户唛头</td>
					<td rowspan="2">订单号</td>
					<td rowspan="2">总件数</td>
					<td rowspan="2">打包件数</td>
					<td colspan="3">国内仓库</td>
					<td colspan="3">国外仓库</td>
					<td rowspan="2">总计</td>
					<td rowspan="2">计费时间段</td>
					<td rowspan="2">服务项目</td>
					<td rowspan="2">成本</td>
					<td rowspan="2">成本计算方式</td>
					<td rowspan="2">供应商</td>
					<td rowspan="2">备注</td>
				</tr>
				<tr>
					<td>单价：</td>
					<td>数量：</td>
					<td>金额：</td>
					<td>原币金额：</td>
					<td>汇率：</td>
					<td>人民币金额：</td>
				</tr>
				<?php while ($z = $db->fetch_array($zengzhi)) { 
						$allcount += $z[count];
						$alldbcount += $z[dbcount];
						$allchengben += round($z[chengben]);
						if ($z[is_cang] == 1) {
							$allguonei += round($z[danjia]*$z[shuliang]);
							$allzong += round($z[danjia]*$z[shuliang]);
						}else{
							$allguowai += round($z[danjia]*$z[huilv]);
							$allzong += round($z[danjia]*$z[huilv]);
						}
					?>
					<tr>
					
					<td><?=$z['piaohao']; ?></td>
					<td><?=$z['subject']; ?></td>
					<td><?=$z['userid']; ?></td>
					<td><?=$z['orderid']; ?></td>
					<td><?=$z['count']; ?></td>
					<td><?=$z['dbcount']; ?></td>
					<td><?=$z[is_cang]==1?$z[danjia]:''; ?></td>
					<td><?=$z[is_cang]==1?round($z[shuliang]):''; ?></td>
					<td><?=$z[is_cang]==1?round($z[danjia]*$z[shuliang]):''; ?></td>
					<td><?=$z[is_cang]==2?$z[danjia]:''; ?></td>
					<td><?=$z[is_cang]==2?$z[huilv]:''; ?></td>
					<td><?=$z[is_cang]==2?$z[danjia]*$z[huilv]:''; ?></td>
					<td><?=$z[is_cang]==2?round($z[danjia]*$z[huilv]):round($z[danjia]*$z[shuliang]); ?></td>
					<td><?=$z['jifeitime']; ?></td>
					<td><?=$z['xiangmu']; ?></td>
					<td><?=round($z['chengben']); ?></td>
					<td><?=$z['chengbenjisuan']; ?></td>
					<td><?=$z['gongyingshang']; ?></td>
					<td><?=$z['remake']; ?></td>
					</tr>
				<?php } ?>
				
				
			</table>
			<table cellpadding="0" cellspacing="0" border="0" class="k">
				<tr>
					<td colspan="16"></td>
				</tr>
				<tr>
					<td style="width: 52px;"></td>
					<td style="width: 52px;"></td>
					<td>合计</td>
					<td><?=$allcount; ?></td>
					<td><?=$alldbcount; ?></td>
					<td></td>
					<td></td>
					<td>￥<?=round($allguonei,0); ?></td>
					<td></td>
					<td></td>
					<td>￥<?=round($allguowai,0); ?></td>
					<td>￥<?=round($allzong,0); ?></td>
					<td></td>
					<td>￥<?=round($allchengben,0); ?></td>

					<td></td>
				</tr>
				
				<tr class="show">
					<td colspan="20" style="text-align: right;">
						<input type="button" style="width: auto;" value="打印预览" class="dayin">
						<input type="button" style="width: auto;" value="导出excel" onclick="daochu();">
						<input type="button" style="width: auto;" value="导出金蝶excel" onclick=" daochu1(14,'<?=$cstime; ?>');">

						<!-- <input type="button" style="width: auto;" value="导出pdf" onclick=" daochu(2,<?=$id; ?>);"> -->
						<!-- <input type="button" style="width: auto;" value="导出财务入账excel" onclick=" daochu(5,<?=$id; ?>);"> -->


						<input type="button" value="关闭" class="cancel"> 
					</td>
				</tr>
			</table>
			</div>
			
		</form>
		<script>
			var index = parent.layer.getFrameIndex(window.name);
			$('.cancel').click(function(){
				parent.layer.close(index);
			})
			function daochu(){
				var cstime = '<?=$cstime; ?>';
				// alert(cstime);
				$.ajax({
			      type:'GET',
			      url: 'zengzhidaochu.php',
			      dataType:'json',
			      data: {'cstime':cstime},
			      beforeSend: function(request) {
			        request.setRequestHeader("Authorization", "token信息，验证身份");
			      },
			      success: function(redata) {
			      	console.log(redata)
			        // 创建a标签，设置属性，并触发点击下载
			        var $a = $("<a>");
			        $a.attr("href", redata.file);
			        $a.attr("download", redata.filename);
			        $("body").append($a);
			        $a[0].click();
			        $a.remove();
			      }
			    });
			 }
			 function daochu1(type,id){
				$.ajax({
			      type:'GET',
			      url: 'yunfeidandaochu.php',
			      dataType:'json',
			      data: {'type':type,'cstime':id},
			      beforeSend: function(request) {
			        request.setRequestHeader("Authorization", "token信息，验证身份");
			      },
			      success: function(redata) {
			      	console.log(redata)
			        // 创建a标签，设置属性，并触发点击下载
			        var $a = $("<a>");
			        $a.attr("href", redata.file);
			        $a.attr("download", redata.filename);
			        $("body").append($a);
			        $a[0].click();
			        $a.remove();
			      }
			    });
			 }

			$('.dayin').click(function(){
			    var printWindow = window.open("打印窗口","_blank");
				//将明细页面拷贝到新的窗口，这样新的窗口就有了明细页面的样式
				var htmlbox = $("html").html();
				$('.show').hide();
				printWindow.document.write(htmlbox);
				//获得要打印的内容
				var printbox = $("html").html();
				//将要打印的内容替换到新窗口的body中
				printWindow.document.body.innerHTML = printbox;
				//脚本向窗口(不管是本窗口或其他窗口)写完内容后，必须关闭输出流。在延时脚本的最后一个document.write()方法后面，必须确保含有document.close()方法，不这样做就不能显示图像和表单。
				//并且，任何后面调用的document.write()方法只会把内容追加到页面后，而不会清除现有内容来写入新值。
				printWindow.document.close();
				//打印
				//chrome浏览器使用jqprint插件打印时偶尔空白页问题
				//解决方案：有时候页面总是加载不出来，打印出现空白页，可以设置延迟时间来等待页面的渲染，但是渲染时间的长短跟页面的大小有关，是不固定的。所以这里使用事件监听器。
				// print.portrait   =  false    ;//横向打印 
				printWindow.addEventListener('load', function(){
					printWindow.portrait = false;
					printWindow.print();
					//关闭窗口
					printWindow.close(
						$('.show').show()
					);
				});
			})
		</script>
	</body>
</html>