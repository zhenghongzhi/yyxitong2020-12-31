<?php
error_reporting(E_ALL ^ E_NOTICE);

if(PHP_VERSION < '4.1.0') {
	$_GET = &$HTTP_GET_VARS;
	$_POST = &$HTTP_POST_VARS;
	$_COOKIE = &$HTTP_COOKIE_VARS;
	$_SERVER = &$HTTP_SERVER_VARS;
	$_ENV = &$HTTP_ENV_VARS;
	$_FILES = &$HTTP_POST_FILES;
}
$phpself = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
require_once './config.inc.php';
require_once './db_mysql.class.php';

$excepttables = array();

$action = $_GET['action'];
if( empty($action) ) $action = "export";
$ajax = $_GET['ajax'];
$ajax = empty($ajax)?0:1;

if($action=="config") {
	if( !empty($_POST['valuesubmit']) ){
		$dbhost_new = setconfig($_POST['dbhost']);
		$dbuser_new = setconfig($_POST['dbuser']);
		$dbpw_new = setconfig($_POST['dbpw']);
		$dbname_new = setconfig($_POST['dbname']);
		writeconfig($dbhost_new,$dbuser_new,$dbpw_new,$dbname_new);
		cpmsg("链接设置：设置成功，程序将自动返回。", $phpself."?action=".$action);
	}
	cpconfig();
}
else if($action=="showdatabase"){
	$thost = $_GET['thost'];
	$tuser = $_GET['tuser'];
	$tpw = $_GET['tpw'];
	$conn = @mysql_connect($thost, $tuser, $tpw);
	if( $conn ){
		if($query = @mysql_query("SHOW DATABASES")){
			$databaseshtml = "";
			while( $database = @mysql_fetch_array($query,MYSQL_ASSOC) ){
				$databaseshtml .= "<option value=\"".$database['Database']."\">".$database['Database']."</option>";
			}
			echo $databaseshtml;
		}
	}else{
		echo "";
	}
	exit;
}
else{
	$db = new dbstuff;
	$db->connect($dbhost, $dbuser, $dbpw, $dbname, $pconnect);
	$dbuser = $dbpw = $dbname = $pconnect = NULL;
}


function upfile($file_name){
	global $user_dir;
	$files = array();
	$max_size = 2000*1024;
	$files_ext=array('sql','zip');
	
	$count = count($_FILES[$file_name]['name']);
	


	for($i=0;$i<$count;$i++){
		if(!empty($_FILES[$file_name]['name'][$i])){
			$files_size=$_FILES[$file_name]['size'][$i]; 
			$files_name=$_FILES[$file_name]['name'][$i]; 
			$files_tempname = $_FILES[$file_name]['tmp_name'][$i]; 
			$file_ext=substr($files_name,strrpos($files_name,".")+1); 
			$file_ext=strtolower($file_ext); 

			if(!in_array($file_ext,$files_ext)){
				cpmsg('错误的文件格式，只能上传.SQL,.ZIP文件。<a href="'.$phpself.'?action=import">数据恢复</a>');
			}
			if($files_size>$max_size){
				cpmsg('上传的文件不能超过2M！。<a href="'.$phpself.'?action=import">数据恢复</a>');
			}
			
			$upload_file = "databackup/". $files_name;
			
			if(!file_exists($upload_file)){				
				if (move_uploaded_file($files_tempname, $upload_file)) { 
					$files[] = $files_name;
					$files2.= "文件".$files_name."上传成功<br>";
				} 
			}else{
				$files1.= "服务器上已存在文件".$files_name."<br>";
			}
			
			$msg['upload'] = $files;
			$msg['msg'] = $files1;
			$msg['suc'] = $files2;
		}
		if(empty($files) && empty($files1)){
			cpmsg('请选择一个文件再上传<a href="'.$phpself.'?action=import">数据恢复</a>');
		}	
			
		
	}
	
	return $msg;

}

function checkfile($files){
	global $user_dir;
	include_once("databackup/md5_file.php");
	if($files){
		if(is_array($files)){
			for($i=0;$i<count($files);$i++){
				$filename = $files[$i];
				$furl = "databackup/".$filename;
				$mstr = file_get_contents($furl);
				$mdstr = md5($mstr);
				unset($mstr);
				if(empty($md5_arr[$filename])){
					$msg.="文件".$filename."文件名已被改过了，所以该文件无效，已从服务器上删除！<br>";
					unlink($furl);
				}elseif($md5_arr[$filename] != $mdstr){
					$msg.="文件".$filename."已被改过了，所以该文件无效，已从服务器上删除！<br>";
					unlink($furl);
				}
			}
		}
	}
	return $msg;
}

//创建数组缓存
function create_array_str($array_name,$array)
{
	$str = "<?\n";
	$str.="\t\$".$array_name."=array(\n";
	if(is_array($array))
	{
		foreach($array as $key => $value)
		{
		$str1.=empty($str1)?"\t\t\t\t'$key'\t\t\t\t=>'$value'":",\n\t\t\t\t'$key'\t\t\t\t=>'$value'";
		}
	}else{
		$str1.="\t\t\t\t'0'\t\t\t\t=>'$array'\n";
	}
	$str2.=");\n?>";
	return $str.$str1.$str2;
}

//创建MD5效验字段
function cmd5($md5file){
	global $user_dir;
	for($i=0;$i<count($md5file);$i++){
		$filename = $md5file[$i];
		if(file_exists("databackup/".$filename)){
		
			$string = file_get_contents("databackup/".$filename);
			$md5str = md5($string);unset($string);
		
			if(file_exists("databackup/md5_file.php")){
				include_once("databackup/md5_file.php");
				$md5_arr[$filename] = $md5str;
				$str = create_array_str("md5_arr",$md5_arr);	
			}else{	
				$array[$filename] = $md5str;			
				$str = create_array_str("md5_arr",$array);	
			}
			
			$handle = fopen("databackup/md5_file.php",'w');
			fwrite($handle,$str);
			fclose($handle);
		}
	}	
}


if($action == 'upload'){

	$msgs = upfile("data_file");
	$files = $msgs['upload'];
	$msg = checkfile($files);
	$msg1 = $msgs['msg'];
	$msg2 = $msgs['suc'];
	
	$msg= $msg1.$msg2.$msg;
	
	if($msg){
		cpmsg($msg.'<a href="'.$phpself.'?action=import">数据恢复</a>');
	}
	
	
	if($msg){
		cpmsg($msg.'。<a href="'.$phpself.'?action=import">数据恢复</a>');
	}else{
		cpmsg('上傳成功！。<a href="'.$phpself.'?action=import">数据恢复</a>');
	}
	
}


if($action=="export") {

	if( !empty($_POST['exportsubmit']) ){
	
		
		$type = $_POST['type'];
		$setup = $_POST['setup'];
		$sqlcompat = $_POST['sqlcompat'];
		$usezip = 1;
		$method = $_POST['method'];
		$sizelimit = $_POST['sizelimit'];
		$volume = $_POST['volume'];
		$filename = $_POST['filename'];


		$db->query('SET SQL_QUOTE_SHOW_CREATE=0', 'SILENT');

		if(!$filename || preg_match("/(\.)(exe|jsp|asp|aspx|cgi|fcgi|pl)(\.|$)/i", $filename)) {
			cpmsg('你没有输入备份文件名或文件名中使用了敏感的扩展名，请返回修改。');
		}

/*
		$tables = array(
		'zq_about_list',
		'zq_vote_vc'
		);
		要备份的表

*/
		$type ='alldata';
		if($type == 'alldata') {
			$tables = arraykeys2(fetchtablelist($tablepre), 'Name');
		}elseif($type == 'custom') {
			$tables = array();
			if(empty($setup)) {
				$customtablesnew = stripslashes($_POST['customtables']);
				$tables = unserialize($customtablesnew);
			}else{
				$customtables = $_POST['customtables'];
				$customtablesnew = empty($customtables)? '' : serialize($customtables);
				$tables = & $customtables;
			}
			if( !is_array($tables) || empty($tables)) {
				cpmsg('您至少需要选择一个数据表进行备份，请返回修改。');
			}
		}

		$volume = intval($volume) + 1;
		$idstring = '# Identify: '.base64_encode(time().",$version,$type,$method,$volume")."\n";
		$dumpcharset = $sqlcharset ? $sqlcharset : str_replace('-', '', $dbcharset?$dbcharset:"gbk");
		$setnames = ($sqlcharset && $db->version() > '4.1' && (!$sqlcompat || $sqlcompat == 'MYSQL41')) ? "SET NAMES '$dumpcharset';\n\n" : '';
		if($db->version() > '4.1') {
			if($sqlcharset) {
				$db->query("SET NAMES '".$sqlcharset."';\n\n");
			}
			if($sqlcompat == 'MYSQL40') {
				$db->query("SET SQL_MODE='MYSQL40'");
			} elseif($sqlcompat == 'MYSQL41') {
				$db->query("SET SQL_MODE=''");
			}
		}

		$backupfilename = 'databackup/'.str_replace(array('/', '\\', '.'), '', $filename);
		if($usezip) {
			require_once './zip.func.php';
		}

		if($method == 'multivol') {

			$sqldump = '';
			$tableid = intval($_POST['tableid']);
			$startfrom = intval($_POST['startfrom']);
			$startrow = $_POST['startrow'];
			$extendins = $_POST['extendins'];
			$sqlcompat = $_POST['sqlcompat'];
			$usehex = $_POST['usehex'];
			$complete = TRUE;
			for(; $complete && $tableid < count($tables) && strlen($sqldump) + 500 < $sizelimit * 1000; $tableid++) {
				$sqldump .= sqldumptable($tables[$tableid], $startfrom, strlen($sqldump));
				if($complete) {
					$startfrom = 0;
				}
			}

			$dumpfile = $backupfilename."-%s".'.sql';
			!$complete && $tableid--;
			if(trim($sqldump)) {
				$sqldump = "$idstring".
					"# <?exit();?>\n".
					"# DaShan Multi-Volume Data Dump Vol.$volume\n".
					"# Version: DaShan $version\n".
					"# Time: $time\n".
					"# Type: $type\n".
					"# Table Prefix: $tablepre\n".
					"#\n".
					"# DaShan Home: http://www.xmcase.com\n".
					"# Please visit our website for newest infomation about DaShan Web Studio!\n".
					"# --------------------------------------------------------\n\n\n".
					"$setnames".
					$sqldump;
				$dumpfilename = sprintf($dumpfile, $volume);
				@$fp = fopen($dumpfilename, 'wb');
				@flock($fp, 2);
				if(@!fwrite($fp, $sqldump)) {
					@fclose($fp);
					cpmsg('数据文件无法保存到服务器，请检查目录属性！');
				} else {
					fclose($fp);
					if($usezip == 2) {
						$fp = fopen($dumpfilename, "r");
						$content = @fread($fp, filesize($dumpfilename));
						fclose($fp);
						$zip = new zipfile();
						$zip->addFile($content, basename($dumpfilename));
						$fp = fopen(sprintf($backupfilename."-%s".'.zip', $volume), 'w');
						if(@fwrite($fp, $zip->file()) !== FALSE) {
							@unlink($dumpfilename);
						}
						fclose($fp);
					}
					unset($sqldump, $zip, $content);
		cpmsgexport('分卷备份：数据文件#'.$volume.'成功创建，程序将自动继续',$phpself."?action=".$action);
				}
			} else {
				$volume--;
				if($volume<0)$volume = 0;
				if($usezip == 1) {
					$zip = new zipfile();
					$zipfilename = $backupfilename.'.zip';
					$unlinks = '';
					for($i = 1; $i <= $volume; $i++) {
						$filename = sprintf($dumpfile, $i);
						$fp = @fopen($filename, "r");
						$content = @fread($fp, filesize($filename));
						@fclose($fp);
						$zip->addFile($content, basename($filename));
						$unlinks .= "@unlink('$filename');";
						$filelist .= "<li><a href=\"$filename\">$filename\n";
					}
					$fp = fopen($zipfilename, 'w');
					if(@fwrite($fp, $zip->file()) !== FALSE) {
						eval($unlinks);
					} else {
						cpmsg('恭喜您，全部'.$volume.'个备份文件成功创建，备份完成。<a href="'.$phpself.'?action='.$action.'">数据备份</a>\n<br />'.$filelist);
					}
					unset($sqldump, $zip, $content);
					fclose($fp);
					@touch('databackup/index.htm');
					$filename = $zipfilename;
					$filenames = split("databackup/",$filename);
					$cmd5_f[] = $filenames[1];
					cmd5($cmd5_f);
					cpmsg('数据成功备份并压缩至服务器<a href="'.$filename.'">'.$filename.'</a>中。<a href="'.$phpself.'?action='.$action.'">数据备份</a>');
				} else {
					@touch('databackup/index.htm');
					for($i = 1; $i <= $volume; $i++) {
						$filename = sprintf($usezip == 2 ? $backupfilename."-%s".'.zip' : $dumpfile, $i);
						$filelist .= "<li><a href=\"$filename\">$filename\n";
						$filenames = split("databackup/",$filename);
						$cmd5_f[] = $filenames[1];
						
					}
					if($cmd5_f){
						cmd5($cmd5_f);
					}
					cpmsg('恭喜您'.$volume.'个备份文件成功创建，备份完成。<a href="'.$phpself.'?action='.$action.'">数据备份</a><ul>'.$filelist.'</ul>');
				}
			}

		}

	}

	$shelldisabled = function_exists('shell_exec') ? '' : 'disabled';
	$sqlcharsets = "<input class=\"radio\" type=\"radio\" name=\"sqlcharset\" value=\"\" checked> $lang[default]".($dbcharset ? " &nbsp; <input class=\"radio\" type=\"radio\" name=\"sqlcharset\" value=\"$dbcharset\"> ".strtoupper($dbcharset) : '').($db->version() > '4.1' && $dbcharset != 'utf8' ? " &nbsp; <input class=\"radio\" type=\"radio\" name=\"sqlcharset\" value='utf8'> UTF-8</option>" : '');

	$tablelist = "";
	$pnbak_tables = fetchtablelist('',1);
	foreach($pnbak_tables as $key => $tables){
		$rowcount =0;
		$tablelist .="<tr>\n\t<td colspan=\"4\"><b>".(empty($key)?"其它":$key)."数据表</b>&nbsp;&nbsp;<input type=\"checkbox\" name=\"chkall\" onclick=\"exportcheckall(this,'".(empty($key)?"other_":$key)."')\" class=\"checkbox\" checked> <b>全选</b></td>\n</tr>\n";
		$tablelist .= "<tbody id=\"".(empty($key)?"other_":$key)."\">";
		foreach($tables as $table) {
			$tablelist .= ($rowcount % 4 ? '' : "<tr>")."\n\t<td><input class=\"checkbox\" type=\"checkbox\" name=\"customtables[]\" value=\"$table[Name]\" checked> $table[Name]</td>".($rowcount % 4!=3 ? '' : "\n</tr>\n");
				$rowcount++;
		}
		$i = $rowcount%4==0?0:(4-$rowcount%4);
		for(; $i>0;$i--){
			$tablelist .= ($rowcount % 4 ? '' : "<tr>")."\n\t<td>&nbsp;</td>".($rowcount % 4!=3 ? '' : "\n</tr>\n");
			$rowcount++;
		}
		$tablelist .= "</tbody>";
	}

	cpexport();
}
else if($action == 'importzip') {

	require_once 'zip.func.php';
	$datafile_server = $_GET['datafile_server'];
	$confirm = $_GET['confirm'];
	$multivol = $_GET['multivol'];

	$unzip = new SimpleUnzip();
	$unzip->ReadFile($datafile_server);

	if($unzip->Count() == 0 || $unzip->GetError(0) != 0 || !preg_match("/\.sql$/i", $importfile = $unzip->GetName(0))) {
		cpmsg('数据文件不存在：可能服务器不允许上传文件或尺寸超过限制。<a href="'.$phpself.'?action='.$action.'">首页</a>');
	}

	$identify = explode(',', base64_decode(preg_replace("/^# Identify:\s*(\w+).*/s", "\\1", substr($unzip->GetData(0), 0, 256))));
	$confirm = !empty($confirm) ? 1 : 0;
	if(!$confirm && $identify[1] != $version) {
		cpmsg('导入和当前版本不一致的数据极有可能产生无法解决的故障，您确定继续吗？', $phpself.'?action=importzip&datafile_server=$datafile_server&importsubmit=yes&confirm=yes', 'form');
	}

	$sqlfilecount = 0;
	foreach($unzip->Entries as $entry) {
		if(preg_match("/\.sql$/i", $entry->Name)) {
			$fp = fopen('databackup/'.$entry->Name, 'w');
			fwrite($fp, $entry->Data);
			fclose($fp);
			$sqlfilecount++;
		}
	}

	if(!$sqlfilecount) {
		cpmsg('数据文件不存在：可能服务器不允许上传文件或尺寸超过限制。<a href="'.$phpself.'?action='.$action.'">首页</a>');
	}

	$info = basename($datafile_server).' &nbsp; 版本: '.$identify[1].' &nbsp; 类型: '.($identify[2]=="alldata"?"全部数据":"自定义").' 方式: '.($identify[3] == 'multivol' ? "多卷" : "Shell").'<br />';

	if(isset($multivol)) {
		$multivol++;
		$datafile_server = preg_replace("/-(\d+)(\..+)$/", "-$multivol\\2", $datafile_server);
		if(file_exists($datafile_server)) {
			cpmsg('数据文件 #'.$multivol.'成功解压缩，程序将自动继续。', $phpself.'?action=importzip&multivol='.$multivol.'&datafile_vol1='.$datafile_vol1.'&datafile_server='.$datafile_server.'&importsubmit=yes&confirm=yes');
		} else {
			cpmsg('所有分卷文件解压缩完毕，您需要自动导入备份吗？导入后解压缩文件将会被删除。', $phpself.'?action=import&from=server&datafile_server='.$datafile_vol1.'&importsubmit=yes&delunzip=yes', 'form', '', $phpself.'?action=import');
		}
	}

	if($identify[3] == 'multivol' && $identify[4] == 1 && preg_match("/-1(\..+)$/", $datafile_server)) {
		$datafile_vol1 = $datafile_server;
		$datafile_server = preg_replace("/-1(\..+)$/", "-2\\1", $datafile_server);
		if(file_exists($datafile_server)) {
			cpmsg($info.'<br />备份文件解压缩完毕，您需要自动解压缩其它的分卷文件吗？', $phpself.'?action=importzip&multivol=1&datafile_vol1=databackup/'.$importfile.'&datafile_server='.$datafile_server.'&importsubmit=yes&confirm=yes', 'form');
		}
	}

	cpmsg($info.'<br />所有分卷文件解压缩完毕，您需要自动导入备份吗？导入后解压缩文件将会被删除。', $phpself.'?action=import&from=server&datafile_server=databackup/'.$importfile.'&importsubmit=yes&delunzip=yes', 'form', '', $phpself.'?action=import');

}
else if($action=="import") {
	if( empty($_GET['importsubmit']) && empty($_POST['deletesubmit']) ) {
		$exportlog = array();
		if(is_dir('databackup/')) {
			$dir = dir('databackup/');
			while($entry = $dir->read()) {
				$entry = 'databackup/'.$entry;
				if(is_file($entry)) {
					if(preg_match("/\.sql$/i", $entry)) {
						$filesize = filesize($entry);
						$fp = fopen($entry, 'rb');
						$identify = explode(',', base64_decode(preg_replace("/^# Identify:\s*(\w+).*/s", "\\1", fgets($fp, 256))));
						fclose ($fp);
						$exportlog[] = array(
							'version' => $identify[1],
							'type' => $identify[2],
							'method' => $identify[3],
							'volume' => $identify[4],
							'filename' => $entry,
							'dateline' => filemtime($entry),
							'size' => $filesize
						);
					} elseif(preg_match("/\.zip$/i", $entry)) {
						$filesize = filesize($entry);
						$exportlog[] = array(
							'type' => 'zip',
							'filename' => $entry,
							'size' => filesize($entry),
							'dateline' => filemtime($entry)
						);
					}
				}
			}
			$dir->close();
		}
		else{
			cpmsg('目录不存在或无法访问，请检查创建/databackup/目录。');
		}
		$exportinfo = '';
		foreach($exportlog as $info) {
			$info['dateline'] = is_int($info['dateline']) ? date("Y-m-d H:i:s", $info['dateline'] + $timeoffset * 3600) : "未知";
			$info['size'] = sizecount($info['size']);
			$info['volume'] = $info['method'] == 'multivol' ? $info['volume'] : '';
			$info['method'] = $info['type'] != 'zip' ? ($info['method'] == 'multivol' ? '多卷' : 'Shell') : '';
			$exportinfo .= "<tr align=\"center\"><td class=\"altbg1\"><input class=\"checkbox\" type=\"checkbox\" name=\"delete[]\" value=\"".basename($info['filename'])."\"></td>\n".
				"<td class=\"altbg2\"><a href=\"$info[filename]\">".substr(strrchr($info['filename'], "/"), 1)."</a></td>\n".
				
				"<td class=\"altbg2\">$info[dateline]</td>\n".
				
				"<td class=\"altbg2\">$info[size]</td>\n".
				
				"<td class=\"altbg2\">$info[volume]</td>\n".
				($info['type'] == 'zip' ? "<td class=\"altbg1\"><a href=\"".$phpself."?action=importzip&datafile_server=$info[filename]&importsubmit=yes\">[解压缩]</a></td>\n" :
				"<td class=\"altbg1\"><a href=\"".$phpself."?action=import&from=server&datafile_server=$info[filename]&importsubmit=yes\"".
				($info['version'] != $version ? " onclick=\"return confirm('导入和当前版权不一致的数据有可能产生无法解决的故障，您确定继续吗？');\"" : '').">[导入]</a></td>\n");
			$exportinfo .= "</tr>";
		}
		cpimport();
	}else if( !empty($_GET['importsubmit']) ){
		$from = $_GET['from'];
		$from = empty($from)?$_POST['from']:$from;
		$autoimport = $_GET['autoimport'];
		$datafile_server = $_GET['datafile_server'];
		$datafile_server = empty($datafile_server)?$_POST['datafile_server']:$datafile_server;
		$delunzip = $_GET['delunzip'];

		$readerror = 0;
		$datafile = '';
		if($from == 'server') {
			$datafile = ''.$datafile_server;
		}
		/*else if($from == 'local') {
			$datafile = $_FILES['datafile']['tmp_name'];
		}*/
		if(@$fp = fopen($datafile, 'rb')) {
			$sqldump = fgets($fp, 256);
			$identify = explode(',', base64_decode(preg_replace("/^# Identify:\s*(\w+).*/s", "\\1", $sqldump)));
			$dumpinfo = array('method' => $identify[3], 'volume' => intval($identify[4]));
			if($dumpinfo['method'] == 'multivol') {
				$sqldump .= fread($fp, filesize($datafile));
			}
			fclose($fp);
		} else {
			if($autoimport) {
				cpmsg('分卷数据成功导入数据库。<a href="'.$phpself.'?action='.$action.'">首页</a>');
			} else {
				cpmsg('数据文件不存在：可能服务器不允许上传文件或尺寸超过限制。');
			}
		}

		if($dumpinfo['method'] == 'multivol') {
			$sqlquery = splitsql($sqldump);
			unset($sqldump);

			foreach($sqlquery as $sql) {

				$sql = syntablestruct(trim($sql), $db->version() > '4.1', $dbcharset);

				if($sql != '') {
					$db->query($sql, 'SILENT');
					if(($sqlerror = $db->error()) && $db->errno() != 1062) {
						$db->halt('MySQL Query Error', $sql);
					}
				}
			}

			if($delunzip) {
				@unlink($datafile_server);
			}

			$datafile_next = preg_replace("/-($dumpinfo[volume])(\..+)$/", "-".($dumpinfo['volume'] + 1)."\\2", $datafile_server);

			if($dumpinfo['volume'] == 1) {
				cpmsg('分卷数据成功导入数据库，您需要自动导入本次其它的备份吗？',
					$phpself."?action=import&from=server&datafile_server=$datafile_next&autoimport=yes&importsubmit=yes".(!empty($delunzip) ? '&delunzip=yes' : ''),
					'form');
			} elseif($autoimport) {
				cpmsg('数据文件#'.$dumpinfo[volume].'成功导入，程序将自动继续', $phpself."?action=import&from=server&datafile_server=$datafile_next&autoimport=yes&importsubmit=yes".(!empty($delunzip) ? '&delunzip=yes' : ''));
			} else {
				cpmsg('分卷数据成功导入数据库。<a href="'.$phpself.'?action='.$action.'">首页</a>');
			}
		} elseif($dumpinfo['method'] == 'shell') {
			require './config.inc.php';
			list($dbhost, $dbport) = explode(':', $dbhost);

			$query = $db->query("SHOW VARIABLES LIKE 'basedir'");
			list(, $mysql_base) = $db->fetch_array($query, MYSQL_NUM);

			$mysqlbin = $mysql_base == '/' ? '' : addslashes($mysql_base).'bin/';
			shell_exec($mysqlbin.'mysql -h"'.$dbhost.($dbport ? (is_numeric($dbport) ? ' -P'.$dbport : ' -S"'.$dbport.'"') : '').
				'" -u"'.$dbuser.'" -p"'.$dbpw.'" "'.$dbname.'" < '.$datafile);

			cpmsg('分卷数据成功导入数据库。<a href="'.$phpself.'?action='.$action.'">首页</a>');
		} else {
			cpmsg('数据文件格式不对，无法导入，请返回');
		}


	}else if( !empty($_POST['deletesubmit']) ) {
		$delete = $_POST['delete'];
		if(is_array($delete)) {
			foreach($delete as $filename) {
				if($filename!='081227_pazDGnv6-1.sql'){
					@unlink('databackup/'.str_replace(array('/', '\\'), '', $filename));
				}else{
					$delflag=1;
				}
			}
			if($delflag){
				$msg="081227_pazDGnv6-1.sql是测试的备份数据，不能删除.<br>其它的";
			}
			cpmsg($msg.'指定备份文件成功删除。<a href="'.$phpself.'?action='.$action.'">首页</a>');
		} else {
			cpmsg('您没有选择要删除的备份文件，请返回。');
		}
	}

}


function sqldumptable($table, $startfrom = 0, $currsize = 0) {
	global $db, $sizelimit, $startrow, $extendins, $sqlcompat, $sqlcharset, $dumpcharset, $usehex, $complete, $excepttables,$userids,$user_nums;

	$offset = 300;
	$tabledump = '';
	$tablefields = array();







	$query = $db->query("SHOW FULL COLUMNS FROM $table", 'SILENT');
	if(strexists($table, 'adminsessions')) {
		return ;
	} elseif(!$query && $db->errno() == 1146) {
		return;
	} elseif(!$query) {
		$usehex = FALSE;
	} else {
		while($fieldrow = $db->fetch_array($query)) {
			$tablefields[] = $fieldrow;
		}
	}
	
	
	if(!$startfrom) {

		$createtable = $db->query("SHOW CREATE TABLE $table", 'SILENT');

		if(!$db->error()) {
			$tabledump = "DROP TABLE IF EXISTS $table;\n";
		} else {
			return '';
		}

		$create = $db->fetch_row($createtable);

		if(strpos($table, '.') !== FALSE) {
			$tablename = substr($table, strpos($table, '.') + 1);
			$create[1] = str_replace("CREATE TABLE $tablename", 'CREATE TABLE '.$table, $create[1]);
		}
		$tabledump .= $create[1];

		if($sqlcompat == 'MYSQL41' && $db->version() < '4.1') {
			$tabledump = preg_replace("/TYPE\=(.+)/", "ENGINE=\\1 DEFAULT CHARSET=".$dumpcharset, $tabledump);
		}
		if($db->version() > '4.1' && $sqlcharset) {
			$tabledump = preg_replace("/(DEFAULT)*\s*CHARSET=.+/", "DEFAULT CHARSET=".$sqlcharset, $tabledump);
		}

		$query = $db->query("SHOW TABLE STATUS LIKE '$table'");
		$tablestatus = $db->fetch_array($query);
		$tabledump .= ($tablestatus['Auto_increment'] ? " AUTO_INCREMENT=$tablestatus[Auto_increment]" : '').";\n\n";
		if($sqlcompat == 'MYSQL40' && $db->version() >= '4.1' && $db->version() < '5.1') {
			if($tablestatus['Auto_increment'] <> '') {
				$temppos = strpos($tabledump, ',');
				$tabledump = substr($tabledump, 0, $temppos).' auto_increment'.substr($tabledump, $temppos);
			}
			if($tablestatus['Engine'] == 'MEMORY') {
				$tabledump = str_replace('TYPE=MEMORY', 'TYPE=HEAP', $tabledump);
			}
		}
	}

	




	if(!$startfrom) {
		$tabledump .="DELETE FROM $table ;\n";
	}	
	if(!in_array($table, $excepttables)) {
		$tabledumped = 0;
		$numrows = $offset;
		$firstfield = $tablefields[0];

		if($extendins == '0') {
			while($currsize + strlen($tabledump) + 500 < $sizelimit * 1000 && $numrows == $offset) {
				if($firstfield['Extra'] == 'auto_increment') {
					$selectsql = "SELECT * FROM $table  LIMIT $offset";
				} else {
					$selectsql = "SELECT * FROM $table   LIMIT $startfrom, $offset";
				}
				$tabledumped = 1;
				$rows = $db->query($selectsql);
				$numfields = $db->num_fields($rows);

				$numrows = $db->num_rows($rows);
				while($row = $db->fetch_row($rows)) {
					$comma = $t = '';
					for($i = 0; $i < $numfields; $i++) {
						$t .= $comma.($usehex && !empty($row[$i]) && (strexists($tablefields[$i]['Type'], 'char') || strexists($tablefields[$i]['Type'], 'text')) ? '0x'.bin2hex($row[$i]) : '\''.mysql_escape_string($row[$i]).'\'');
						$comma = ',';
					}
					if(strlen($t) + $currsize + strlen($tabledump) + 500 < $sizelimit * 1000) {
						if($firstfield['Extra'] == 'auto_increment') {
							$startfrom = $row[0];
						} else {
							$startfrom++;
						}
						$tabledump .= "INSERT INTO $table VALUES ($t);\n";
					} else {
						$complete = FALSE;
						break 2;
					}
				}
			}
		} else {
			while($currsize + strlen($tabledump) + 500 < $sizelimit * 1000 && $numrows == $offset) {
				if($firstfield['Extra'] == 'auto_increment') {
					$selectsql = "SELECT * FROM $table  LIMIT $offset";
				} else {
					$selectsql = "SELECT * FROM $table  LIMIT  $startfrom, $offset";
				}
				$tabledumped = 1;
				$rows = $db->query($selectsql);
				$numfields = $db->num_fields($rows);

				if($numrows = $db->num_rows($rows)) {
					$t1 = $comma1 = '';
					while($row = $db->fetch_row($rows)) {
						$t2 = $comma2 = '';
						for($i = 0; $i < $numfields; $i++) {
							$t2 .= $comma2.($usehex && !empty($row[$i]) && (strexists($tablefields[$i]['Type'], 'char') || strexists($tablefields[$i]['Type'], 'text'))? '0x'.bin2hex($row[$i]) : '\''.mysql_escape_string($row[$i]).'\'');
							$comma2 = ',';
						}
						if(strlen($t1) + $currsize + strlen($tabledump) + 500 < $sizelimit * 1000) {
							if($firstfield['Extra'] == 'auto_increment') {
								$startfrom = $row[0];
							} else {
								$startfrom++;
							}
							$t1 .= "$comma1 ($t2)";
							$comma1 = ',';
						} else {
							$tabledump .= "INSERT INTO $table VALUES $t1;\n";
							$complete = FALSE;
							break 2;
						}
					}
					$tabledump .= "INSERT INTO $table VALUES $t1;\n";
				}
			}
		}

		$startrow = $startfrom;
		$tabledump .= "\n";
	}

	return $tabledump;
}

function strexists($haystack, $needle) {
	return !(strpos($haystack, $needle) === FALSE);
}

function fetchtablelist($tablepre = '',$iscate = 0) {
	global $db;
	$arr = explode('.', $tablepre);
	$dbname = $arr[1] ? $arr[0] : '';
	$sqladd = $dbname ? " FROM $dbname LIKE '$arr[1]%'" : "LIKE '$tablepre%'";
	!$tablepre && $tablepre = '*';
	$tables = $table = $othertables = array();
	$query = $db->query("SHOW TABLE STATUS $sqladd");
	while($table = $db->fetch_array($query)) {
		if($iscate==1){
			$pntablearr = explode("_",$table['Name']);
			if( count($pntablearr)>1 ){
				if( empty($tables[$pntablearr[0]]) ){
					$tables[$pntablearr[0]] = array();
				}
				$tables[$pntablearr[0]][] = $table;
			}else{
				$table['Name'] = ($dbname ? "$dbname." : '').$table['Name'];
				$othertables[] = $table;
			}
		}else{
			$table['Name'] = ($dbname ? "$dbname." : '').$table['Name'];
			$tables[] = $table;
		}
	}
	if( !empty($othertables) && $iscate){
		$tables[] = $othertables;
	}
	return $tables;
}

function arraykeys2($array, $key2) {
	$return = array();
	foreach($array as $val) {
		$return[] = $val[$key2];
	}
	return $return;
}

function splitsql($sql) {
	$sql = str_replace("\r", "\n", $sql);
	$ret = array();
	$num = 0;
	$queriesarray = explode(";\n", trim($sql));
	unset($sql);
	foreach($queriesarray as $query) {
		$queries = explode("\n", trim($query));
		foreach($queries as $query) {
			$ret[$num] .= $query[0] == "#" ? NULL : $query;
		}
		$num++;
	}
	return($ret);
}

function syntablestruct($sql, $version, $dbcharset) {

	if(strpos(trim(substr($sql, 0, 18)), 'CREATE TABLE') === FALSE) {
		return $sql;
	}

	$sqlversion = strpos($sql, 'ENGINE=') === FALSE ? FALSE : TRUE;

	if($sqlversion === $version) {

		return $sqlversion && $dbcharset ? preg_replace(array('/ character set \w+/i', '/ collate \w+/i', "/DEFAULT CHARSET=\w+/is"), array('', '', "DEFAULT CHARSET=$dbcharset"), $sql) : $sql;
	}

	if($version) {
		return preg_replace(array('/TYPE=HEAP/i', '/TYPE=(\w+)/is'), array("ENGINE=MEMORY DEFAULT CHARSET=$dbcharset", "ENGINE=\\1 DEFAULT CHARSET=$dbcharset"), $sql);

	} else {
		return preg_replace(array('/character set \w+/i', '/collate \w+/i', '/ENGINE=MEMORY/i', '/\s*DEFAULT CHARSET=\w+/is', '/\s*COLLATE=\w+/is', '/ENGINE=(\w+)(.*)/is'), array('', '', 'ENGINE=HEAP', '', '', 'TYPE=\\1\\2'), $sql);
	}
}

function random($length, $numeric = 0) {
	PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
	if($numeric) {
		$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
	} else {
		$hash = '';
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		$max = strlen($chars) - 1;
		for($i = 0; $i < $length; $i++) {
			$hash .= $chars[mt_rand(0, $max)];
		}
	}
	return $hash;
}

function sizecount($filesize) {
	if($filesize >= 1073741824) {
		$filesize = round($filesize / 1073741824 * 100) / 100 . ' GB';
	} elseif($filesize >= 1048576) {
		$filesize = round($filesize / 1048576 * 100) / 100 . ' MB';
	} elseif($filesize >= 1024) {
		$filesize = round($filesize / 1024 * 100) / 100 . ' KB';
	} else {
		$filesize = $filesize . ' Bytes';
	}
	return $filesize;
}

function writeconfig($dbhost,$dbuser,$dbpw,$dbname,$database = 'mysql',$dbcharset = '',$charset = 'gbk'){
	$fp = fopen('./config.inc.php', 'r');
	$configfile = fread($fp, filesize('./config.inc.php'));
	fclose($fp);
	$configfile = preg_replace("/[$]dbhost\s*\=\s*[\"'].*?[\"'];/is", "\$dbhost = '$dbhost';", $configfile);
	$configfile = preg_replace("/[$]dbuser\s*\=\s*[\"'].*?[\"'];/is", "\$dbuser = '$dbuser';", $configfile);
	$configfile = preg_replace("/[$]dbpw\s*\=\s*[\"'].*?[\"'];/is", "\$dbpw = '$dbpw';", $configfile);
	$configfile = preg_replace("/[$]dbname\s*\=\s*[\"'].*?[\"'];/is", "\$dbname = '$dbname';", $configfile);
	$configfile = preg_replace("/[$]database\s*\=\s*[\"'].*?[\"'];/is", "\$database = '$database';", $configfile);
	$configfile = preg_replace("/[$]dbcharset\s*\=\s*[\"'].*?[\"'];/is", "\$dbcharset = '$dbcharset';", $configfile);
	$configfile = preg_replace("/[$]charset\s*\=\s*[\"'].*?[\"'];/is", "\$charset = '$charset';", $configfile);
	$fp = fopen('./config.inc.php', 'w');
	fwrite($fp, trim($configfile));
	fclose($fp);
}

function setconfig($string) {
	if(!get_magic_quotes_gpc()) {
		$string = str_replace('\'', '\\\'', $string);
	} else {
		$string = str_replace('\"', '"', $string);
	}
	return $string;
}

function cpconfig(){
	extract($GLOBALS, EXTR_SKIP);
	cpheader();
?>

<?php
	exit;
}

function cpimport(){
	extract($GLOBALS, EXTR_SKIP);
	cpheader();
?>
 
<div class="list-div" id="listDivs"> 
<script src="../js/utils.js"></script> 
<script src="pnbak.js"></script> 
<form action="<?=$phpself?>?action=upload&importsubmit=yes" name="upload"  enctype="multipart/form-data" method="post">
<table width="100%" border="0" cellpadding="5" cellspacing="1"  id="list-table">
	<tr class="header">
		<th>上传备份数据文件</th>
	</tr>
	<tr><td align="center">
	<strong>注意：下载的数据文件不能做任何的修改，包括文件名也不能修改，否则将导致数据恢复出错！</strong>
	</td></tr>
	<tr>
		<td class="altbg2"  align="center">
		<table cellpadding="0" cellspacing="0" id="video-table"><tr><td><a href="javascript:;" onClick="addImg(this,'video-table')">[+]</a>
              上传文件<input type="file" name="data_file[]" /></td></tr></table></td>
	</tr>
	<tr><td><center><input class="button" type="submit" name="importsubmit" value="提交"></center></td></tr>
</table></div><br />
</form>
<br />
<div class="list-div" id="listDivs"> 
<form action="<?=$phpself?>?action=import" name="deleteform" method="post">
<table width="100%" border="0" cellpadding="5" cellspacing="1"  id="list-table">
	<tr class="header">
		<th colspan="6">数据备份记录</th>
	</tr>
	<tr align="center" class="category">
		<td width="78"><input class="checkbox" type="checkbox" name="chkall" onClick="importcheckall(this,'importfile')">删？</td>
		<td>文件名</td>
		<td>时间</td>
		<td>尺寸</td>
		
		<td>卷号</td>
		<td>操作</td>
	</tr>
	<tbody id="importfile">
<?=$exportinfo?>
	</tbody>
	<tr><td colspan="6"><center><input class="button" type="submit" name="deletesubmit" value="提交"></center></td></tr>
</table></div>
<br />

</form>


<?php
	cpfooter();
	exit;
}

function cpexport(){
	extract($GLOBALS, EXTR_SKIP);
	cpheader();
?>
<div class="list-div" id="listDivs"> 
<form method="post" name="export" id="export" action="<?=$phpself?>?action=export">
<input type="hidden" name="method" value="multivol">
<input type="hidden" name="sizelimit" value="2048">
<input type="hidden" name="extendins" value="0">
<input type="hidden" name="sqlcompat" value="">
<input type="hidden" name="sqlcharset" value="">
<input type="hidden" name="usehex" value="1">
<input type="hidden" name="chkall" value="on">
<input type="hidden" name="usezip" value="2">
<input type="hidden" name="filename" value="<?=date('ymd').'_'.random(8)?>">
<input type="hidden" name="setup" value="1">

<table width="100%" border="0" cellpadding="2" cellspacing="1">
	<tr><td height="50"><center><input class="button" type="submit" name="exportsubmit" value="开始备份"></center></td></tr>
</table>

</form>
</div>
<?php
	cpfooter();
	exit;
}

function cpmsgexport($message, $url_forward = '') {
	extract($GLOBALS, EXTR_SKIP);
	cpheader(0);
	$message = "<form method=\"post\" name=\"exportpara\" id=\"exportpara\" action=\"$url_forward\">".
			"$message<br /><br /><br />\n";
	$message .= "<a href=\"javascript:document.getElementById('exportpara').submit();\">如果您的浏览器没有自动跳转，请点击这里</a>";
	$message .= "<script>setTimeout(\"exportsubmit('exportpara');\", 1000);</script>";
	$message .= "<input type='hidden' name='type' value='".$type."' />";
	$message .= "<input type='hidden' name='saveto' value='server' />";
	$message .= "<input type='hidden' name='filename' value='".$filename."' />";
	$message .= "<input type='hidden' name='method' value='multivol' />";
	$message .= "<input type='hidden' name='sizelimit' value='".$sizelimit."' />";
	$message .= "<input type='hidden' name='volume' value='".$volume."' />";
	$message .= "<input type='hidden' name='tableid' value='".$tableid."' />";
	$message .= "<input type='hidden' name='startfrom' value='".$startrow."' />";
	$message .= "<input type='hidden' name='extendins' value='".$extendins."' />";
	$message .= "<input type='hidden' name='sqlcharset' value='".$sqlcharset."' />";
	$message .= "<input type='hidden' name='sqlcompat' value='".$sqlcompat."' />";
	$message .= "<input type='hidden' name='exportsubmit' value='yes' />";
	$message .= "<input type='hidden' name='usehex' value='".$usehex."' />";
	$message .= "<input type='hidden' name='usezip' value='".$usezip."' />";
	$message .= "<input type='hidden' name='customtables' value='".$customtablesnew."' />";
	$message .= "</form><br /><br /><br />";
?>
<div class="list-div" id="listDivs"> 
<table width="500" border="0" cellpadding="0" cellspacing="0" align="center">
<tr class="header"><th>系統提示</th></tr><tr><td class="altbg2"><div align="center"><br /><br /><br />
<?=$message?></div>
</td></tr></table></div>
<?
	cpfooter();
	exit;
}

function cpmsg($message, $url_forward = '', $msgtype = 'message', $extra = '', $cancelurl = '') {
	cpheader(0);
	if($msgtype == 'form') {
		$message = "$message$extra<br><br><form method=\"post\" action=\"$url_forward\">".
			"\n".
			"<input class=\"button\" type=\"submit\" name=\"confirmed\" value=\"确定\"> &nbsp; \n".
			"<input class=\"button\" type=\"button\" value=\"取消\" onClick=\"".
			($cancelurl == '' ? 'history.go(-1)' : 'location.href=\''.$cancelurl.'\'').
			";\"></form>";
	} else {
		if($url_forward) {
			$message .= "<br /><br /><a href=\"$url_forward\">如果您的浏览器没有自动跳转，请点击这里</a>";
			$message .= "<script>setTimeout(\"redirect('$url_forward');\", 2000);</script>";
		} elseif(strpos($message, "返回")) {
			$message .= "<br /><br /><a href=\"javascript:history.go(-1);\" class=\"mediumtxt\">[点击这里返回上一页]</a>";
		}
		$message = "<br />$message$extra<br />";
	}
?>
<div class="list-div" id="listDivs"> 
<table width="500" border="0" cellpadding="0" cellspacing="1" align="center">
<tr class="header"><th>系统提示</th></tr><tr><td class="altbg2"><div align="center">
<br /><br /><?=$message?></div><br />
</td></tr></table>
</div>
<?
	cpfooter();
	exit;
}

function cpheader($showmenu = 1){
	extract($GLOBALS, EXTR_SKIP);
	if($db){
		$query = $db->query("select database()");
		$title = "PNBAKMYSQL备份/恢复 - ".$db->result($query, 0);
	}else{
		$title = "PNBAKMYSQL备份/恢复";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<title>管理中心-数据备份恢复</title> 
<meta name="robots" content="noindex, nofollow"> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="../styles/general.css" rel="stylesheet" type="text/css" /> 
<link href="../styles/main.css" rel="stylesheet" type="text/css" /> 
<script language="javascript" type="text/javascript" src="pnbak.js"></script>
<script language="javascript" type="text/javascript" src="../js/utils.js"></script>
</head>
<body> 
<h1> 
<span class="action-span"> 

</span> 
<span class="action-span1"><a href="admin_index.php">管理中心</a> - 数据库<?=$action=='import'?"恢复":"备份"?> </span> 
<div style="clear:both"></div> 
</h1> 
<?php
}

function cpfooter(){
?>
<br />
<?
include_once("../foot.php");
?>
</body>
</html>
<?php
}
?>