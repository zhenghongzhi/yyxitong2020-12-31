<?
header('Content-Type: text/html;charset=utf-8');
header('Access-Control-Allow-Origin:*'); // *代表允许任何网址请求
header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); // 允许请求的类型

header('Access-Control-Allow-Headers: Content-Type,Content-Length,Accept-Encoding,X-Requested-with, Origin ,X-TOKEN, Accept, Authorization'); // 设置允许自定义请求头的字段
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');

	// 分类列表
	$usersql = "select u.kehu_number,u.username,u.phone,ut.subject from yasa_kehu as u left join yasa_kehu_type as ut on u.pid = ut.id";
	$user = $db->query($usersql);
	while($a=$db->fetch_array($user)){
		$userlist[] = $a;
	}


	

	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle('1');
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	$objPHPExcel->getActiveSheet()->setCellValue('A1', '客户编号');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', '类别');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', '姓名');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', '手机');
	$i = 2;

	foreach ($userlist as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $value[kehu_number]);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[subject]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[username]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[phone]);
		$i += 1;
	}
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="用户(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	$filename = '用户表';
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);

 ?>