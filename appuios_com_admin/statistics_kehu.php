<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?

session_cache_limiter('private, must-revalidate');
include_once("../include/common.ini.php");

$start = isset($_GET["start"]) ? $_GET["start"] : null;
$end = isset($_GET["end"]) ? $_GET["end"] : null;

if($start && $end) {

	$start = strtotime($start);
	$end = strtotime($end);
	$end = $end + (24*60*60);
	// echo $start;
	echo "<br>";
	// echo $end;

	$result = $db->query("SELECT SUM(type1) AS total FROM yasa_order WHERE addtime >= $start AND addtime <= $end");
	$row = $db->fetch_array($result);
	$total = (int)$row["total"];
	// echo $total;

	$db->query("SET NAMES UTF8");
	$result = $db->query("SELECT SUM(type1) AS money, (SELECT kehu_number FROM yasa_kehu WHERE id = yasa_order.pid1) AS kehu_number FROM yasa_order WHERE addtime >= $start AND addtime <= $end GROUP BY pid1 ORDER BY money DESC");
	
}

?>

<head>
	<style type="text/css">
		.main { text-align: center; }
		table { margin: auto; }
		table tr th { width: 200px; }
	</style>
</head>

<div class="main">

	<form action="">
		开始日期：<input type="date" name="start">
		结束日期：<input type="date" name="end">
		<input type="submit" name="提交">
	</form>
	<br>

	<? if($start && $end){ ?>
	<table border="1">
		<caption>客户统计</caption>
		<thead>
	      <tr>
	        <th>客户编号</th>
	        <th>金额</th>
	        <th>占比</th>
	      </tr>
	    </thead>
		<tbody>
	    	<? 
	    		while($row = $db->fetch_array($result)) {
		    		echo "<tr>";
		    		echo "<td>";
		    		echo $row["kehu_number"];
		    		echo "</td>";
		    		echo "<td>";
					echo (int)$row["money"];
		    		echo "</td>";
		    		echo "<td>";
					echo round($row["money"]/$total*100,2)."％<br />";
		    		echo "</td>";
		    		echo "</tr>";
	    		}
	    	?>
	    </tbody>
	</table>
	<?php } ?>
</div>
