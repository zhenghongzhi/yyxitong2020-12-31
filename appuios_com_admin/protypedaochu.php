<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');

	// 分类列表
	$protypelist0sql = "select * from  yasa_product_type order by number*1 asc";
	$protypelist0 = $db->query($protypelist0sql);
	while($a=$db->fetch_array($protypelist0)){
		$protypelist[] = $a;
	}


	$prolist = genTree($protypelist);

	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle('1');
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'pid');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', '编号');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', '名字');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', '等级');
	$i = 2;

	foreach ($prolist as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $value[pid]);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, implode('-',getfather($value[id],$protypelist)));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[name]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[level]);
		$i += 1;
		if ($value['list']) {
			foreach ($value['list'] as $ke => $val) {
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val[pid]);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, implode('-',getfather($val[id],$protypelist)));
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val[name]);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val[level]);
				$i += 1;
				if ($val['list']) {
					foreach ($val['list'] as $k => $v) {
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $v[pid]);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, implode('-',getfather($v[id],$protypelist)));
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $v[name]);
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $v[level]);
						$i += 1;
						if ($v['list']) {
							foreach ($v['list'] as $ks => $vs) {
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $vs[pid]);
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, implode('-',getfather($vs[id],$protypelist)));
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $vs[name]);
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $vs[level]);
								$i +=1;
							}
						}
					}

				}
			}
		}
	}
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="订单汇总表(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	$filename = '产品分类';
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);

 ?>