<?
	session_cache_limiter('private, must-revalidate');
	include_once("../include/common.ini.php");
	include_once('api/PHPExcel.php');
	include_once('api/PHPExcel/Writer/Excel2007.php');

	
	$where = "1 and w.status = 2";
	if ($k_khtype) {
		$where .= " and w.khtype = ".$k_khtype;	
	}
	if ($k_kehu_number) {
		$where .= " and w.userid like '%".$k_kehu_number."%'";
	}
	if ($k_fyid) {
		$where .= " and w.fyid = '$k_fyid'";
		$fy = $db->get_one("select * from yasa_fangyi_type where id = $k_fyid");
		$taitou = $fy[subject];
	}else{
		$taitou = '迪拜仓库库存表（未派送）';
	}


	$sql2 = $db->query("select w.userid,sum(w.count) as count,sum(w.weight) as weight,sum(w.volume) as volume,sum(w.money) as money,w.fahuohao,k.subject,group_concat(f.subject) as subject1,group_concat(w.remake) as remake from yasa_dibaiwarehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype 
		left join yasa_fangyi_type as f on f.id = w.fyid
		where $where group by w.userid");
	
	
	$arr = array();
	while ($a = $db->fetch_array($sql2)) {
		
		$qianshou = $db->get_one("select sum(count) as count, sum(weight) as weight,sum(volume) as volume from yasa_qianshou where fahuohao = '$a[fahuohao]' and userid = '$a[userid]'");
		$a[count] = $a[count]-$qianshou[count];
		$a[weight] = $a[weight]-$qianshou[weight];
		$a[volume] = $a[volume]-$qianshou[volume];
		$a[subject] = $a[subject];
		$arr[] = $a;
	}




	$filename = '迪拜未派送订单(' . date('Ymd-His') . ')';



	$objPHPExcel = new PHPExcel();
	// 标题
	$objPHPExcel->getProperties()->setTitle('未派送订单');
	// 设置当前的sheet
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
	$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
	$objPHPExcel->getActiveSheet()->setCellValue('A1', $taitou);

	$objPHPExcel->getActiveSheet()->setCellValue('A2', '序号');
	$objPHPExcel->getActiveSheet()->setCellValue('B2', '公司名称');
	$objPHPExcel->getActiveSheet()->setCellValue('C2', '客户编号');
	$objPHPExcel->getActiveSheet()->setCellValue('D2', '数量');
	$objPHPExcel->getActiveSheet()->setCellValue('E2', '立方');
	$objPHPExcel->getActiveSheet()->setCellValue('F2', '摘要');
	$objPHPExcel->getActiveSheet()->setCellValue('G2', '归属');




	$i = 3;
	foreach ($arr as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $key+1);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $value[subject1]);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $value[userid]);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $value[count]);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $value[volume]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value[remake]);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $value[subject]);
		$i++;
	}
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_start();//清除缓冲区,避免乱码
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="迪拜未派送订单(' . date('Ymd-His') . ').xls"');
	header('Cache-Control: max-age=0');
  	$objWriter->save('php://output');

  	$xlsData = ob_get_contents();
  	ob_end_clean();
	
	$data[filename] = $filename;
	$data[file] = "data:application/vnd.ms-excel;base64,".base64_encode($xlsData);

  	echo json_encode($data);
 ?>