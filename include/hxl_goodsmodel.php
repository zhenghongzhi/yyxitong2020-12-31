<?php
	// add方法,下订单
    function add($data,$db) {
        if($db->autoExecute($data,'order_info')) {
            return $db->last_id();
        } else {
            return false;
        }
    }

    // 插入订单对应的商品,操作的order_goods表
     function addGoods($data,$db) {
        if($db->autoExecute($data,'order_goods')) {
            return $db->last_id();
        } else {
            return false;
        }
    }

    // 撤消订单
     function invoke($order_id,$db) {
        $sql = 'delete from order_info where order_id = '. $order_id;
        $db->query($sql);

        $sql = 'delete from order_goods where order_id = '. $order_id;
        $db->query($sql);
    }


?>