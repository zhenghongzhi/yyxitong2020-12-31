<? 
/****
session加单例完成购物车类


添加商品
删除商品
修改商品数量
判断某商品是否存在
商品数量加1
商品数量减1
商品列表
计算商品的种类
计算商品的个数
计算总价格
清空购物车
****/
//网站就不用seesion_start,有统一的开启
//session_start();
class cart {
    static protected $ins = null;
	//每种商品都放在$basket中
    protected $basket = array();
    
    protected function __construct() {
    }

    static protected function getIns() {
        if(self::$ins == null) {
            self::$ins = new self();
        }

        return self::$ins;
    }

    static public function getCart() {
        // 如果session['cart']不存在,或者存在但不是cart类的实例
  if(!isset($_SESSION['cart']) || !($_SESSION['cart'] instanceof self)) {
            $_SESSION['cart'] = self::getIns();
        }

        return $_SESSION['cart'];
    }

    // 添加商品到购物车
    // 需要那些栏目:商品id,有商品名称,商品本店价格,购买数量
	//产品名称,商品id,商品名称,,商品本店价格,选购数量,产品编号,产品颜色,产品码数
	//subject
    public function addItem($id,$name,$shop_price,$num,$use_info,$picurl,$size) {
	//下面不合理买个N95溜达一下,再买一个,怎么修改basket
	//['id'=>3,'name'=>'n95',shop_pice=>1123,num=>1]
	
        if(!$this->hasItem($id)) {
		//判断id是否存在$this->hasItem($id)是做成的函数array_key_exists($id,$this->basket);
		//把id作为键放在basket中和商品类别一样,最好.
		//如要找3号商品只需用 array_key_exists();
            $this->basket[$id] = array('name'=>$name,
			'shop_price'=>$shop_price,
			'num'=>$num,
			'use_info'=>$use_info,
			'picurl'=>$picurl,
			'size'=>$size
			);
        } else {//有此商品那就在原基础上加 
            $this->basket[$id]['num'] += $num;
        }
    }

    // 删除商品
    public function delItem($id) {
        if($this->hasItem($id)) {
            unset($this->basket[$id]);
        }
    }

    // 通过id判断某商品是否存在
    public function hasItem($id) {
        return array_key_exists($id,$this->basket);
    }

    // 修改商品数量
    public function modItem($id,$num) {
        if($num == 0) {
            $this->delItem($id);
            return;
        }

        if($this->hasItem($id)) {
            $this->basket[$id]['num'] = $num;
        }
    }

    // 商品加1
    public function incItem($id) {
        if($this->hasItem($id)) {
            $this->basket[$id]['num'] += 1;
        }
    }

    // 商品-1
    public function decItem() {
        if($this->hasItem($id)) {
            $this->basket[$id]['num'] -= 1;
        }
        
        // 要是减到0了,则删掉
        if($this->basket[$id]['num'] == 0) {
            $this->delItem($id);
        }
    }

    // 计算商品种类
    public function cnt() {
        return count($this->basket);
    }
    
    // 返回商品列表
    public function items() {
        return $this->basket;
    }

    // 计算商品的个数,不好算
    public function getCount() {
        if($this->cnt() == 0) {
            return 0;
        }

        $count = 0;
        foreach($this->basket as $v) {
            $count += $v['num'];
        }

        return $count;
    }

    // 计算商品的总价格
    public function getPrice() {
        if($this->cnt() == 0) {
            return 0;
        }

        $price = 0;
        foreach($this->basket as $v) {
            $price += $v['num'] * $v['shop_price'];
        }

        return $price;
    }

    // 清空购物车,最好办
    public function clear() {
        $this->basket = array();
    }
}
/****测试模块?act=show

    $cart = cart::getCart();

    if($_GET['act'] == 'add') {
        $id = rand(1,10);
        $name = '诺基亚N' . $id;
        $shop_price = 100;
		$bianhao='N' . $id;
		$picurl='黑色';
		$size='4寸';
		
        $num = rand(1,5);
//传参数顺序要对应!
        $cart->addItem($id,$name,$shop_price,$num,$bianhao,$picurl,$size);
        echo 'OK';
    }

    if($_GET['act'] == 'show') {
        print_r($cart->items());
        echo '<hr />';
        echo '你买了',$cart->cnt(),'种商品,共',$cart->getCount(),'个';
        echo '<br /> ,花了',$cart->getPrice(),'元';
    }
    if($_GET['act'] == 'clear') {
        $cart->clear();
        echo '清空成功';
    }

?>
*/