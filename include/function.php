<?
	//get提交
	function curl_get_https($url){
	    $curl = curl_init(); // 启动一个CURL会话
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
	    $tmpInfo = curl_exec($curl);     //返回api的json对象
	    //关闭URL请求
	    curl_close($curl);
	    return $tmpInfo;    //返回json对象
	}
	// post提交
	function curl_post_https($url,$data=''){ // 模拟提交数据函数
		// print_r($data);die;
	    $curl = curl_init(); // 启动一个CURL会话
	    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加密算法是否存在
	    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
	    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
	    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
	    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
	    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
	    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
	    $tmpInfo = curl_exec($curl); // 执行操作
	    if (curl_errno($curl)) {
	        echo 'Errno'.curl_error($curl);//捕抓异常
	    }
	    curl_close($curl); // 关闭CURL会话
	    return $tmpInfo; // 返回数据，json格式
	}

	// 获取token保存
	function getToken($corpid,$secret){
		$content = file_get_contents('./qywx_access_token.txt');
		$tokendata = explode(',',$content);
		$getTokenurl = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid='.$corpid.'&corpsecret='.$secret;//获取token
		// echo $getTokenurl;die;
		if (!$tokendata[0]) {
			$token = curl_get_https($getTokenurl);
			$token = json_decode($token);
			// print_r($token);die;
			if ($token->errcode == 0) {
				$time = time()+7200;
				$con = $token->access_token.','.$time;
				file_put_contents('./qywx_access_token.txt',$con);
				$msg[status] = 0;
				$msg[access_token] = $token->access_token;
				
			}else{
				$msg[status] = $token->errcode;
				$msg[msg] = $token->errmsg;
			}
		}else{
			if ($tokendata[1] > time()) { //判断token是否过期 没过期
				$msg[status] = 0;
				$msg[access_token] = $tokendata[0];
			}else{
				$token = curl_get_https($getTokenurl);
				$token = json_decode($token);
				// print_r($token);die;
				if ($token->errcode == 0) {
					$time = time()+7200;
					$con = $token->access_token.','.$time;
					file_put_contents('./qywx_access_token.txt',$con);
					$msg[status] = 0;
					$msg[access_token] = $token->access_token;
					
				}else{
					$msg[status] = $token->errcode;
					$msg[msg] = $token->errmsg;
				}
			}
		}
		return $msg;

	}

	function getzdToken($corpid,$secret){
		$content = file_get_contents('./qywx_access_token.txt');
		$tokendata = explode(',',$content);
		$getTokenurl = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid='.$corpid.'&corpsecret='.$secret;//获取token
		// echo $getTokenurl;die;
		if (!$tokendata[0]) {
			$token = curl_get_https($getTokenurl);
			$token = json_decode($token);
			// print_r($token);die;
			if ($token->errcode == 0) {
				$time = time()+7200;
				$con = $token->access_token.','.$time;
				file_put_contents('./qywx_reply_access_token.txt',$con);
				$msg[status] = 0;
				$msg[access_token] = $token->access_token;
				
			}else{
				$msg[status] = $token->errcode;
				$msg[msg] = $token->errmsg;
			}
		}else{
			if ($tokendata[1] > time()) { //判断token是否过期 没过期
				$msg[status] = 0;
				$msg[access_token] = $tokendata[0];
			}else{
				$token = curl_get_https($getTokenurl);
				$token = json_decode($token);
				// print_r($token);die;
				if ($token->errcode == 0) {
					$time = time()+7200;
					$con = $token->access_token.','.$time;
					file_put_contents('./qywx_reply_access_token.txt',$con);
					$msg[status] = 0;
					$msg[access_token] = $token->access_token;
					
				}else{
					$msg[status] = $token->errcode;
					$msg[msg] = $token->errmsg;
				}
			}
		}
		return $msg;

	}

	
	
	// 获取模板详情
	function getApproval($access_token,$template_id){
		$getApprovalurl = 'https://qyapi.weixin.qq.com/cgi-bin/oa/gettemplatedetail?access_token='.$access_token;//获取审批模板详情
		$Approvaldata['template_id'] = $template_id;
		$a = json_encode($Approvaldata);
		$res = curl_post_https($getApprovalurl,$a);
		$res = json_decode($res);
		return $res;

	}


	// 审批内容转换
	// $corl 控制器
	// $itemid 控件id
	// $content 内容
	// 选择框是否多选 single单选 multi多选
	function zh($corl,$itemid,$content,$seltype){
		if ($corl == 'Text') {
			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][text] = $content;
		}elseif ($corl == 'Money') {
			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][new_money] = $content;
		}elseif ($corl == 'Number') {
			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][new_number] = $content;
		}elseif ($corl == "Selector") {

			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][selector][type] = $seltype;
			if ($seltype == 'multi') {
				foreach ($content as $key => $value) {
					$data[value][selector][options][$key][key] = $value;
				}
			}else{
				// echo $corl.$itemid.$content.$seltype;
				$data[value][selector][options][0][key] = $content;
			}
		}elseif ($corl == 'Textarea') {
			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][text] = $content;
		}elseif($corl == 'Date'){
			$data[control] = $corl;
			$data[id] = $itemid;
			$data[value][date][type] = 'day';
			$data[value][date][s_timestamp] = strtotime($content);
		}
		// print_r($data);
		return $data;
	}
	// 无限级数据列表
	function genTree($list, $pk = 'id', $pid = 'pid', $child = 'list', $root = 0){
		//创建Tree存放结果的数组
		$tree = array();
		if (is_array($list)) {
		    //创建基于主键的数组引用
		    $refer = array();
		    #把数组下标改成1开头的  多处维持相同的数据使用&
		    foreach ($list as $key => $data) {
		        $refer[$data[$pk]] = &$list[$key];
		    }
		    foreach ($list as $k => $data) {
		        //查看所有的pid
		        $parantId = $data[$pid];
		        if ($root == $parantId) {
		            #当pid=0的时候 相应的数组内容存到$tree数组内
		            $tree[] = &$list[$k];
		        } else {
		            #pid不为0的情况下 进入$refer  parent的数据来自refer refer来自list一直在改变list的数据
		            if (isset($refer[$parantId])) {
		                $parent = &$refer[$parantId];
		                $parent[$child][] = &$list[$k];
		            }
		        }
		    }
		}
		return $tree;
	}
	// 查找所有父类
	function getfather($id,$data){
	    $arr = [];
	    foreach($data as $v){
	        //从小到大 排列
	        if($v['id'] == $id){
	            $arr[] = $v['number'];
	            if($v['pid'] > 0){
	                $arr = array_merge(getfather($v['pid'],$data), $arr);
	            }
	        }
	    }
	 
	    return $arr;
	 
	}
	

	function uploadMedia($url,$data)
	{
	    $result = http_post($url, $data, true); //构造访问企业微信的API发送指定文本
	    $rs = json_decode($result,true); //将返回的json串转成数组
	    return $rs;
	}


	function http_post($url,$param,$post_file=false){
        $oCurl = curl_init(); //初始化curl

        if(stripos($url,"http://")!==FALSE){ //判断$url中是否存在"https://"；如果存在的话就执行以下代码
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE); //关闭CURLOPT_SSL_VERIFYPEER服务-禁用后cURL将终止从服务端进行验证
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false); //不设置CURLOPT_SSL_VERIFYPEER服务-禁用后cURL将终止从服务端进行验证
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //使用的SSL版本
        }
        if(PHP_VERSION_ID >= 50500 && class_exists('\CURLFile')){
            $is_curlFile = true;
        }else {
            $is_curlFile = false;
            if (defined('CURLOPT_SAFE_UPLOAD')) {
                curl_setopt($oCurl, CURLOPT_SAFE_UPLOAD, false);
            }
        }
        
        if($post_file) {
            if($is_curlFile) {
                foreach ($param as $key => $val) {
                    if(isset($val["tmp_name"])){
                        $param[$key] = new \CURLFile(realpath($val["tmp_name"]),$val["type"],$val["name"]);
                    }else if(substr($val, 0, 1) == '@'){
                        $param[$key] = new \CURLFile(realpath(substr($val,1)));
                    }
                }
            }
            $strPOST = $param;
        }else{
            $strPOST = json_encode($param); //将数值转换成json数据存储格式
        }
        // print_r($strPOST);die;	
        curl_setopt($oCurl, CURLOPT_URL, $url); //获取的URL地址
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 ); //在启用CURLOPT_RETURNTRANSFER的时候，返回原生的（Raw）输出。
        curl_setopt($oCurl, CURLOPT_POST,true); //发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样。
        curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST); //全部数据使用HTTP协议中的"POST"操作来发送。
        curl_setopt($oCurl, CURLOPT_VERBOSE, 1); //汇报所有的信息，存放在STDERR或指定的CURLOPT_STDERR中

        //curl_setopt($oCurl, CURLOPT_HEADER,  array( 'Expect:' ));  //将其删掉返回的为json串
        $sContent = curl_exec($oCurl); //执行一个cURL会话
        curl_close($oCurl); //关闭一个cURL会话

        return $sContent; //返回结果
    }


    function imagename($imagepath)
    {
    	// echo $imagepath;die;
    	$imgarr = explode('/',$imagepath);

    	$imgarr1 = explode('.',$imgarr[count($imgarr)-1]);
    	unset($imgarr[count($imgarr)-1]);

    	$name = $imgarr1[0].'_min.'.$imgarr1[1];
    	$imgarr[] = $name;

    	$namepath = implode('/',$imgarr);
    	
    	return $namepath;
    }

    function get_CateId_by_UId($UId){	//根据UId取得CateId
		$arr_UId=@explode(',', $UId);
		$CateId=$arr_UId[count($arr_UId)-2];
		return $CateId;
	}
	//select 
	// function selectmo($arr,$proid = ''){
	// 	$str = '<select name="pidarr[]" class="proname"><option value="0">请选择</option>';

	// 	foreach ($arr as $key => $value) {
	// 		$selectd = $value['id'] == $proid?'selected':'';
	// 		$str .= '<option value="'.$value['id'].'" '.$selectd.'>'.$value['name'].'（'.$value['xinghao'].'）</option>';
	// 	}
	// 	$str .= '</select>';
	// 	return $str;
	// }
	function selectmo($arr,$proid = ''){
		$str = '<select name="pidarr[]" class="proname"><option value="0">请选择</option>';
		// echo '<pre>';
		// print_r($arr);die;
		foreach ($arr as $key => $value) {
			$selectd = $value['id'] == $proid?'selected':'';
			$str .= '<option value="'.$value['id'].'" '.$selectd.'>|'.$value['name'].'</option>';
			if ($value['son']) {
				foreach ($value['son'] as $k => $val) {
					$selectd1 = $val['id'] == $proid?'selected':'';
					$str .= '<option value="'.$val['id'].'" '.$selectd1.'>|┝'.$val['name'].'</option>';
				}
			}
		}
		$str .= '</select>';
		return $str;
	}

	function base64_image_content($base64_image_content,$path){
        //匹配出图片的格式
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
            $type = $result[2];
            $new_file = $path."/".date('Ymd',time())."/";
            if(!file_exists($new_file)){
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($new_file, 0700);
            }
            $new_file = $new_file.time().rand(1000,9999).".{$type}";
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
                return $new_file;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    //add by zhz start
    function zhzcurl($url,$data,$post="POST"){
    	
       	$heads = array (
			'Accept: application/json, text/plain, */*',
			'Content-Type: application/json;charset=UTF-8',
			'X-Token:59136090-d702-11ea-b1e0-ff9e8fcdb84f'
		);
  		if ($post != 'GET') {
			$ch = curl_init ();
			// 设置选项，包括URL
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $heads);
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ( $ch, CURLOPT_BINARYTRANSFER, true);

			curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, $post);
			if ($data) {
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data ); // 把post的变量加上
			}
			$result = curl_exec ( $ch );
			// print_r($result);die;
			curl_close ( $ch ); 
  		}else{
  			$ch = curl_init();
		    //设置抓取的url
		    curl_setopt($ch, CURLOPT_URL, $url);
		    //设置头文件的信息作为数据流输出
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    // 超时设置,以秒为单位
		    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		 
		    // 超时设置，以毫秒为单位
		    // curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);
		 
		    // 设置请求头
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $heads);
		    //设置获取的信息以文件流的形式返回，而不是直接输出。
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    //执行命令
		    $result = curl_exec ( $ch );
		 
		    // 显示错误信息
		    if (curl_error($ch)) {
		        print "Error: " . curl_error($ch);
		    } else {
		        // 打印返回的内容
		        // var_dump($result);
		        curl_close($ch);
		    }
  		}
        return $result;
    }
    //订单统计
    function jisuan($db1,$starttime,$endtime,$type=1){
    	$p = 'Date-1589522781023';
		$p1 = 'item-1494251203122';
		$p2 = 'Selector-1575249886295';
		//订单量 订单总和 客户付款 佣金
		$dingdan = $db1->get_one("select count(*) as count,sum(o.type1) as money,sum(o.type5) as kehuyifu from yasa_order as o 
			left join yasa_kehu as k on k.id = o.pid1 where o.is_delete = 1 and k.kehu_number like 'YY-%' and o.addtime >= $starttime and o.addtime <= $endtime");

		if ($type == 1 || $type == 3) {
			//未交易单量/金额
			$daichulisql = $db1->query("select o.* from yasa_order as o 
				left join yasa_kehu as k on k.id = o.pid1 
				where o.is_delete = 1 and k.kehu_number like 'YY-%' and o.addtime >= 1577808000 and o.addtime >= $starttime and o.addtime <= $endtime and (o.isyingshou = 1 or o.isyingshou = 0)");
			while ($daichuli = $db1->fetch_array($daichulisql)) {
				$caiwuurl1 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$daichuli[order_id];
				$caiwu1 = curl_get_https($caiwuurl1);
				$caiwu1 = json_decode($caiwu1);
				$cangkujine = $db1->get_one("SELECT sum(money) as money from yasa_warehouse where orderid = '$daichuli[order_id]' and is_del = 1 and status != 10");
				//成交金额
				if ($cangkujine[money] > ($daichuli[type5]+$caiwu1->deposit+$caiwu1->balance)) {
					$chengjiaomoney += $cangkujine[money];
				}else{
					$chengjiaomoney += $daichuli[type5]+$caiwu1->deposit+$caiwu1->balance;
				}

				if (($daichuli[type5]+$caiwu1->deposit+$caiwu1->balance)==0 && !$cangkujine['money']) {
					$weijiaomoney += $daichuli[type1];
					$weijiaocount++;
					$weijiaomoneyid[] = $daichuli[id];

				}
				//应收未收
				if (round($caiwu1->deposit+$caiwu1->balance+$daichuli[type5] - $cangkujine[money]) > 0) {
					$yingshouweishou += round($caiwu1->deposit+$caiwu1->balance+$daichuli[type5] - $cangkujine[money]);
					$yingshouweishouid[] = $daichuli[id];
				}

				//应收佣金
				$ysyj += round($daichuli[type1]*$daichuli[yongjin]/100);

				//超期未收
				$l = $db1->get_one("select content from yasa_shenpi where oid = ".$daichuli['id']." and moudle = 'pay' and status not in(3,4) order by addtime desc limit 1");
				$m = json_decode($l[content]);
				if (strtotime($m->$p) <= strtotime(date("Y-m-d")) && $m->$p) {
					$chaoqiweishou++;
					$chaoqiweishouid[] = $daichuli[id];
				}

				//佣金异常
				$ssyj = '';
				$bi = $daichuli[yongjin];
				if ($cangkujine[money] >= ($caiwu1->deposit+$caiwu1->balance+$daichuli[type5])) {
					$ssyj = $cangkujine[money] * $bi /100;
					$ssyj = round($ssyj);
				}else{
					$ssyj = ($caiwu1->deposit+$caiwu1->balance+$daichuli[type5]) * $bi/100;
					$ssyj = round($ssyj);
				}

				$fktimeurl = "http://47.106.88.138:8080/api/lemonyy1/queryOrderByNumber?number=".$daichuli[order_id];
				$fktimeurl = curl_get_https($fktimeurl);
				// echo '<pre>';
				$stime = 0;
				$fktime = json_decode($fktimeurl);
				$expendmoney = 0;
				foreach ($fktime->list as $key => $value) {
					if ($value->expendType == '支付订金' || $value->expendType=='支付余款') {
						if (strtotime($stime) < strtotime($value->operateTime)) {
							$stime = $value->operateTime;
						}
					}
					if ($value->expendType == '佣金') {
						$expendmoney += $value->expend;
						$ysyongjin +=$value->expend;
					}
					
				}
				if (abs($expendmoney-$ssyj) > 1) {
					$yongjinyichang ++;
					$yongjinyichangid[] = $daichuli[id];
				}

				//睡眠单（没有入仓金额 没有支付金额且下单时间过了30天  有支付时间且支付时间过了30天）
				if ($type == 3) {
					if ($cangkujine[money] == 0 && ($caiwu1->deposit+$caiwu1->balance+$daichuli[type5]) == 0 && (strtotime(date("Y-m-d"))-strtotime(date("Y-m-d",$daichuli[addtime]))) > (60*60*24*30)) {
						$shuimiandan ++;
						$shuimiandanid[] = $daichuli[id];
					}
					if ((strtotime(date("Y-m-d")) - strtotime($stime)) > (60*60*24*30) && $stime) {
						$shuimiandan ++;
						$shuimiandanid[] = $daichuli[id];
					}
					if ($cangkujine[money] == 0 && ($caiwu1->deposit+$caiwu1->balance+$daichuli[type5]) != 0 && !$stime && (strtotime(date("Y-m-d"))-strtotime(date("Y-m-d",$daichuli[addtime]))) > (60*60*24*30)) {
						$shuimiandan ++;
						$shuimiandanid[] = $daichuli[id];
					}
				}
			}

			//成交总额
			$yichulisql = $db1->query("select o.* from yasa_order as o 
				left join yasa_kehu as k on k.id = o.pid1 
				where o.is_delete = 1 and k.kehu_number like 'YY-%' and o.addtime >= 1577808000 and o.addtime >= $starttime and o.addtime <= $endtime and o.isyingshou = 2 ");
			while ($yichuli = $db1->fetch_array($yichulisql)) {
				$caiwuurl2 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$yichuli[order_id];
				$caiwu2 = curl_get_https($caiwuurl2);
				$caiwu2 = json_decode($caiwu2);
				$cangkujine2 = $db1->get_one("SELECT sum(money) as money from yasa_warehouse where orderid = '$yichuli[order_id]' and is_del = 1 and status != 10");
				if ($cangkujine2[money] > ($yichuli[type5]+$caiwu2->deposit+$caiwu2->balance)) {
					$chengjiaomoney += $cangkujine2[money];
				}else{
					$chengjiaomoney += $yichuli[type5]+$caiwu2->deposit+$caiwu2->balance;
				}
				// $z1['cangkujine'] = $cangkujine2[money];
				// $z1['fukuan'] = $yichuli[type5]+$caiwu2->deposit+$caiwu2->balance;
				// $z[] =$z1;
				//应收佣金
				$ysyj += round($yichuli[type1]*$yichuli[yongjin]/100);

				$l = $db1->get_one("select content from yasa_shenpi where oid = ".$yichuli['id']." and moudle = 'pay' and status not in(3,4) order by addtime desc limit 1");
				$m = json_decode($l[content]);
				//佣金异常
				
				$ssyj = '';
				$bi = $yichuli[yongjin];
				if ($cangkujine2[money] >= ($caiwu2->deposit+$caiwu2->balance+$yichuli[type5])) {
					$ssyj = $cangkujine2[money] * $bi /100;
					$ssyj = round($ssyj);
				}else{
					$ssyj = ($caiwu2->deposit+$caiwu2->balance+$yichuli[type5]) * $bi/100;
					$ssyj = round($ssyj);
				}


				$fktimeurl = "http://47.106.88.138:8080/api/lemonyy1/queryOrderByNumber?number=".$yichuli[order_id];
				$fktimeurl = curl_get_https($fktimeurl);
				// echo '<pre>';
				$stime = 0;
				$fktime = json_decode($fktimeurl);
				$expendmoney = 0;
				foreach ($fktime->list as $key => $value) {
					// if ($value->expendType == '支付订金' || $value->expendType=='支付余款') {
					// 	if (strtotime($stime) < strtotime($value->operateTime)) {
					// 		$stime = $value->operateTime;
					// 	}
					// }
					if ($value->expendType == '佣金') {
						$expendmoney += $value->expend;
						$ysyongjin +=$value->expend;
					}
					
				}
				if (abs($expendmoney-$ssyj) > 1) {
					$yongjinyichang ++;
					$yongjinyichangid[] = $yichuli[id];
				}
			}

			//完成率
			$yichulicount = $db1->get_one("select count(*) as count from yasa_order as o 
				left join yasa_kehu as k on k.id = o.pid1 
				where o.is_delete = 1 and k.kehu_number like 'YY-%' and o.isyingshou = 2 and o.addtime >= 1577808000 and o.addtime >= $starttime and o.addtime <= $endtime");
			$wanchenglv = $yichulicount[count]/$dingdan[count]*100;

			$wei = $db1->get_one("select count(*) as count from yasa_order as o 
				left join yasa_kehu as k on k.id = o.pid1 
				where o.is_delete = 1 and k.kehu_number like 'YY-%' and (o.isyingshou = 1 or o.isyingshou = 0) and o.addtime >= 1577808000 and o.addtime >= $starttime and o.addtime <= $endtime");
		}

		$data['count']    = $dingdan[count];//订单数
		$data['money']    = $dingdan[money];//订单金额
		$data['kehuyifu'] = $dingdan[kehuyifu];//客户付款
		$data['yongjin']  = round($ysyj);//应收佣金
		$data['ysyongjin']  = round($ysyongjin);//已收佣金
		$data['weijiaomoney']  = round($weijiaomoney,0);//未交易金额
		$data['weijiaocount']  = round($weijiaocount,0);//未交易订单数
		$data['weijiaomoneyid']  = implode(',',$weijiaomoneyid);//未交易金额id


		$data['jiaoyizhong']  = round($wei[count]-$weijiaocount,0);//未交易订单数
		$data['chengjiaomoney']  = round($chengjiaomoney,0);//成交总额
		$data['wanchenglv']  = round($wanchenglv,2).'%';//完成率
		
		$data['yingshouweishou']  = round($yingshouweishou,2);//应收未收
		$data['yingshouweishouid']  = implode(',',$yingshouweishouid);//应收未收id

		$data['chaoqiweishou'] = $chaoqiweishou;//超期未收
		$data['chaoqiweishouid']  = implode(',',$chaoqiweishouid);//超期未收id

		$data['yongjinyichang'] = $yongjinyichang;//佣金异常
		$data['yongjinyichangid']  = implode(',',$yongjinyichangid);//佣金异常id

		$data['shuimiandan'] = $shuimiandan;//睡眠单
		$data['shuimiandanid']  = implode(',',$shuimiandanid);//睡眠单id

		$data['yichulicount'] = $yichulicount[count];//已完成数
		// $data['z1'] = $z1;

		return $data;
	}

	
	//物流统计 
	function wuliu($db1,$starttime,$endtime,$type=1){
		//入仓金额
		$rkje = $db1->get_one("select sum(money) as money from yasa_warehouse where is_del = 1 and rukutime BETWEEN '$starttime' and '$endtime'");
		//发货量//区分海空
		$fhlhai = $db1->get_one("select sum(volume) as volume from yasa_warehouse where is_del = 1 and status = 3 and fahuo = '海运' and chucangtime BETWEEN '$starttime' and '$endtime'");
		$fhlkong = $db1->get_one("select sum(weight) as weight from yasa_warehouse where is_del = 1 and status = 3 and fahuo = '空运' and chucangtime BETWEEN '$starttime' and '$endtime'");

		//已发货（柜/票）
		$yfhkysql = $db1->get_one("select count(*) as count from yasa_yunfeidan where is_del = 1 and fahuo = '空运' and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
		$yfhky = $yfhkysql[count];

		$yfhhysql = $db1->get_one("select count(*) as count from yasa_yunfeidan where is_del = 1 and fahuo = '海运' and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
		$yfhhy = $yfhhysql[count];

		if ($type == 2) {
			//超期未发货
			// $cqwfh = $db1->get_one("SELECT count(*) as count FROM yasa_warehouse WHERE status != 3 and status != 10 and is_del = 1 and  DATE_ADD(FRom_UNIXTIME(rukutime),INTERVAL 5 DAY) < NOW() and rukutime BETWEEN '$starttime' and '$endtime'");
			$cqwfhky = 0;
			$cqwfhhy = 0;
			$cqwfhsql = $db1->query("SELECT rukutime,chucangtime,fahuo,weight,volume,id FROM yasa_warehouse WHERE is_del = 1 and status != 10 and rukutime BETWEEN '$starttime' and '$endtime'");
			

			while ($cqwfh = $db1->fetch_array($cqwfhsql)) {
				if ((strtotime($cqwfh[chucangtime]) - strtotime($cqwfh[rukutime])) > (60*60*24*5) ) {
					if ($cqwfh[fahuo] == '海运') {
						// $zzz['海运'][strtotime($cqwfh[chucangtime]) - strtotime($cqwfh[rukutime])][] = $cqwfh[volume];
						$cqwfhhy += $cqwfh[volume];
						$cqwfhhyid[] = $cqwfh[id];
					}else{
						$cqwfhky += $cqwfh[weight];
						// $zzz['海运'][strtotime($cqwfh[chucangtime]) - strtotime($cqwfh[rukutime])][] = $cqwfh[volume];
						$cqwfhkyid[] = $cqwfh[id];
						
					}
				}
			}
			// print_r($c);die;


			//超期未到港
			$cqwdghysql = $db1->query("SELECT * FROM yasa_dibaiwarehouse WHERE fahuo = '海运' and DATE_ADD(FRom_UNIXTIME(addtime),INTERVAL 30 DAY) < NOW() and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59")." group by fahuohao");

			$cqwdghy = $db1->num_rows($cqwdghysql);
			// echo "SELECT count(*) as count FROM yasa_dibaiwarehouse WHERE fahuo = '海运' and DATE_ADD(FRom_UNIXTIME(addtime),INTERVAL 30 DAY) < NOW() and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59")." group by fahuohao";die;
			$cqwdgkysql = $db1->query("SELECT count(*) as count FROM yasa_dibaiwarehouse WHERE fahuo = '空运' and DATE_ADD(FRom_UNIXTIME(addtime),INTERVAL 10 DAY) < NOW() and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59")." group by fahuohao");
			$cqwdgky = $db1->num_rows($cqwdgkysql);
			

			//超期未派送迪拜
			$cqwpsdbsql = $db1->query("SELECT * FROM yasa_dibaiwarehouse WHERE addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
			$cqwpsdb = 0;
			while ($cqwpsdb1 = $db1->fetch_array($cqwpsdbsql)) {
				$qianshoudb = $db1->get_one("select * from yasa_qianshou where userid = '$cqwpsdb1[userid]' and fahuohao = '$cqwpsdb1[fahuohao]' order by id desc");
				if ($qianshoudb) { //如果签收单存在
					if (($qianshoudb[addtime]-$cqwpsdb1[dgtime]) > (60*60*24*3)) {
						$cqwpsdb++;
					}
				}else{
					if (($cqwpsdb1[dgtime]-$cqwpsdb1[addtime]) > (60*60*24*3)) {
						$cqwpsdb++;
					}
				}
			}

			//超期未派送伊朗
			$cqwpsylsql = $db1->query("SELECT * FROM yasa_ylwarehouse WHERE addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
			$cqwpsyl = 0;
			while ($cqwpsyl1 = $db1->fetch_array($cqwpsylsql)) {
				if ($cqwpsyl1[id]) {
					$qianshouyl = $db1->get_one("select * from yasa_ylqianshou where ylid = $cqwpsyl1[id] order by id desc");
					if (($qianshouyl[addtime]-$cqwpsyl1[dgtime]) > (60*60*24*3)) {
						$cqwpsyl++;
					}
				}else{
					if (($cqwpsyl1[dgtime]-$cqwpsyl1[addtime]) > (60*60*24*3)) {
						$cqwpsyl++;
					}
				}
			}
		}

		$data['rkje']    	= $rkje[money]?$rkje[money]:0;//入仓金额
		$data['fhlhai']  	= $fhlhai[volume]?$fhlhai[volume]:0;//发货量海运
		$data['fhlkong']	= $fhlkong[weight]?$fhlkong[weight]:0;//发货量空运
		$data['yfhky']     	= $yfhky;//已发货空运（柜/票）
		$data['yfhhy']     	= $yfhhy;//已发货海运（柜/票）
		$data['cqwfh']     	= $cqwfh[count]?$cqwfh[count]:0;//超期未发货
		$data['cqwdghy']    = $cqwdghy;//超期未到港海运
		$data['cqwdgky']    = $cqwdgky;//超期未到港空运
		$data['cqwpsdb']    = $cqwpsdb;//超期未派送迪拜
		$data['cqwpsyl']     	= $cqwpsyl;//超期未派送伊朗
		$data['gnccsx'] 	= $gnccsx;//国内出仓时效
		$data['dbdgsx'] 	= $dbdgsx;//迪拜到港时效
		$data['yldgsx'] 	= $yldgsx;//伊朗到港时效
		$data['cqwfhhy'] 	= $cqwfhhy;//超期未发货海运
		$data['cqwfhky'] 	= $cqwfhky;//超期未发货空运


		$data['cqwfhhyid']  = implode(',',$cqwfhhyid);//超期未发货海运id
		$data['cqwfhkyid']  = implode(',',$cqwfhkyid);//超期未发货空运id
		return $data;
	}

	function dangxiawuliu($db1){
		//库存量//区分海空  库存货值
		$kclhai = $db1->get_one("select sum(volume) as volume,sum(money) as money from yasa_warehouse where is_del = 1 and status != 3 and status != 10 and fahuo = '海运'");
		$kclkong = $db1->get_one("select sum(weight) as weight,sum(money) as money from yasa_warehouse where is_del = 1 and status != 3 and status != 10 and fahuo = '空运'");

		//库存货客户数
		$kchkhssql = $db1->query("select userid from yasa_warehouse where is_del = 1 and status != 3 and status != 10 and khtype = 20 group by userid");
		$kchkhs = $db1->num_rows($kchkhssql);

		$kchkhssql1 = $db1->query("select khtype from yasa_warehouse where is_del = 1 and status != 3 and status != 10 and khtype != 20 group by khtype");
		$kchkhs1 = $db1->num_rows($kchkhssql1);

		//已发货欠款客户
		$yfhqkkhsql = $db1->query("select userid from yasa_warehouse where is_del = 1 and status = 3 and khtype = 20 and chucangtime >= '".date("Y-m-d",strtotime('-20 days'))."' group by userid");
		$yfhqkkh = 0;
		while ($yfhqkkhs = $db1->fetch_array($yfhqkkhsql)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$yfhqkkhs[userid];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (floor($caiwu->balance) < 0) {
				$yfhqkkh ++;
			}
		}
		// echo date("Y-m-d",strtotime('-20 days'));
		$yfhqkkhsql = $db1->query("select k.subject from yasa_warehouse as w 
		left join yasa_kehutype as k on k.id = w.khtype where w.is_del = 1 and w.status = 3 and w.khtype != 20 and w.chucangtime >= '".date("Y-m-d",strtotime('-20 days'))."' group by w.khtype");
		while ($yfhqkkhs = $db1->fetch_array($yfhqkkhsql)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$yfhqkkhs[subject];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (floor($caiwu->balance) < 0) {
				$yfhqkkh ++;
			}
		}

		//待到港（柜/票）
		$daidaoganghysql = $db1->query("select fahuohao from yasa_dibaiwarehouse where status = 1 and fahuo = '海运' group by fahuohao");
		$daidaoganghy = $db1->num_rows($daidaoganghysql);

		$daidaogangkysql = $db1->query("select fahuohao from yasa_dibaiwarehouse where status = 1 and fahuo = '空运' group by fahuohao");
		$daidaogangky = $db1->num_rows($daidaogangkysql);

		//待派送量（一程）
		// $dpslyc = $db1->get_one("select sum(count) as count, sum(volume) as volume,sum(weight) as weight from yasa_dibaiwarehouse where status = 2");
		$dpsylcsql = $db1->query("select userid,sum(count) as count,sum(weight) as weight,sum(volume) as volume,fahuohao from yasa_dibaiwarehouse where status = 2 group by userid,fahuohao");
		while ($ddd = $db1->fetch_array($dpsylcsql)) {
			$qianshou = $db1->get_one("select sum(count) as count, sum(weight) as weight,sum(volume) as volume from yasa_qianshou where fahuohao = '$ddd[fahuohao]' and userid = '$ddd[userid]'");
			$allcount += $ddd[count]-$qianshou[count];
			$allweight += $ddd[weight]-$qianshou[weight];
			$allvolume += $ddd[volume]-$qianshou[volume];
		}
		$dpslyc[count] = $allcount;
		$dpslyc[weight] = $allweight;
		$dpslyc[volume] = $allvolume;

		//待派送量（二程）
		$dpslec = $db1->get_one("select sum(count) as count, sum(volume) as volume,sum(weight) as weight from yasa_ylwarehouse where status = 2 and is_del = 1");

		//欠款未派送客户数
		//迪拜yy客户
		$daibaikucunsql = $db1->query("select userid from yasa_dibaiwarehouse where status != 3 and khtype = 20 group by userid");
		$dbykwpskh = 0;
		while ($daibaikucun = $db1->fetch_array($daibaikucunsql)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$daibaikucun[userid];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (ceil($caiwu->balance) < 0) {
				$dbykwpskh ++;
				$kehu[] = $daibaikucun[userid];
			}
		}

		//迪拜非yy客户
		$daibaikucunsqlf = $db1->query("select k.subject from yasa_dibaiwarehouse as d 
			left join yasa_kehutype as k on k.id = d.khtype
			where d.status != 3 and d.khtype != 20 group by d.khtype");
		while ($daibaikucun = $db1->fetch_array($daibaikucunsqlf)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$daibaikucun[subject];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (ceil($caiwu->balance) < 0) {
				$dbykwpskh ++;
				$kehu[] = $daibaikucun[subject];
			}
		}

		//伊朗yy客户
		$yilangkucunsql = $db1->query("select userid from yasa_ylwarehouse where status = 2 and is_del = 1 and khtype = 20 group by userid");
		$ylykwpskh = 0;
		while ($yilangkucun = $db1->fetch_array($yilangkucunsql)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$yilangkucun[userid];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (ceil($caiwu->balance) < 0) {
				$ylykwpskh ++;
			}
		}

		//伊朗非yy客户
		$yilangkucunsqlf = $db1->query("select k.subject from yasa_ylwarehouse as d 
			left join yasa_kehutype as k on k.id = d.khtype
			where d.status = 2 and d.khtype != 20 group by d.khtype");
		while ($yilangkucun = $db1->fetch_array($yilangkucunsqlf)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$yilangkucun[subject];
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			if (ceil($caiwu->balance) < 0) {
				$ylykwpskh ++;
			}
		}

		//超期未发货
		// $cqwfh = $db1->get_one("SELECT count(*) as count FROM yasa_warehouse WHERE status != 3 and status != 10 and is_del = 1 and  DATE_ADD(rukutime,INTERVAL 5 DAY) > NOW()");
		// $cqwfh = $db1->get_one("SELECT min(rukutime) as rukutime,max(chucangtime) as chucangtime,fahuo,sum(weight) as weight,sum(volume) as volume FROM yasa_warehouse WHERE status != 3 and status != 10 and is_del = 1");
		$cqwfhky = 0;
		$cqwfhhy = 0;
		$cqwfhsql = $db1->query("SELECT * FROM yasa_warehouse WHERE status != 3 and status != 10 and is_del = 1");
		$c = $db1->num_rows($cqwfhsql);
		while ($cqwfh = $db1->fetch_array($cqwfhsql)) {
			if ((time() - strtotime($cqwfh[rukutime])) > (60*60*24*5) ) {
				$kuling = (strtotime(date("Y-m-d"))-strtotime($cqwfh['rukutime']))/(60*60*24);
				$cqwfh[kuling] = $kuling;
				if ($cqwfh[fahuo] == '海运') {
					// $zzz['海运'][strtotime($cqwfh[chucangtime]) - strtotime($cqwfh[rukutime])][] = $cqwfh[volume];
					$cqwfhhyarr[] = $cqwfh;
					$cqwfhhy += $cqwfh[volume];
				}else{
					$cqwfhky += $cqwfh[weight];
					$cqwfhkyarr[] = $cqwfh;
					// $zzz['海运'][strtotime($cqwfh[chucangtime]) - strtotime($cqwfh[rukutime])][] = $cqwfh[volume];
				}
			}
		}
		// echo $cqwfhky;die;


		//超期未到港
		$cqwdghysql = $db1->query("SELECT count(*) as count FROM yasa_dibaiwarehouse WHERE status = 1 and fahuo = '海运' and DATE_ADD(FRom_UNIXTIME(addtime),INTERVAL 30 DAY) < NOW() group by fahuohao");
		$cqwdghy = $db1->num_rows($cqwdghysql);
		$cqwdgkysql = $db1->query("SELECT count(*) as count FROM yasa_dibaiwarehouse WHERE status = 1 and fahuo = '空运' and DATE_ADD(FRom_UNIXTIME(addtime),INTERVAL 10 DAY) < NOW() group by fahuohao");
		$cqwdgky = $db1->num_rows($cqwdgkysql);



		//超期未派送迪拜
		$cqwpsdbsql = $db1->query("SELECT * FROM yasa_dibaiwarehouse WHERE addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
		$cqwpsdb = 0;
		while ($cqwpsdb1 = $db1->fetch_array($cqwpsdbsql)) {
			$qianshoudb = $db1->get_one("select * from yasa_qianshou where userid = '$cqwpsdb1[userid]' and fahuohao = '$cqwpsdb1[fahuohao]' order by id desc");
			if ($qianshoudb) { //如果签收单存在
				if (($qianshoudb[addtime]-$cqwpsdb1[dgtime]) > (60*60*24*3)) {
					$cqwpsdb++;
				}
			}else{
				if (($cqwpsdb1[dgtime]-$cqwpsdb1[addtime]) > (60*60*24*3)) {
					$cqwpsdb++;
				}
			}
		}

		//超期未派送伊朗
		$cqwpsylsql = $db1->query("SELECT * FROM yasa_ylwarehouse WHERE addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
		$cqwpsyl = 0;
		while ($cqwpsyl1 = $db1->fetch_array($cqwpsylsql)) {
			if ($cqwpsyl1[id]) {
				$qianshouyl = $db1->get_one("select * from yasa_ylqianshou where ylid = $cqwpsyl1[id] order by id desc");
				if (($qianshouyl[addtime]-$cqwpsyl1[dgtime]) > (60*60*24*3)) {
					$cqwpsyl++;
				}
			}else{
				if (($cqwpsyl1[dgtime]-$cqwpsyl1[addtime]) > (60*60*24*3)) {
					$cqwpsyl++;
				}
			}
		}

		$data['kclhai']  	= $kclhai[volume]?$kclhai[volume]:0;//库存量海运
		$data['kclkong'] 	= $kclkong[weight]?$kclkong[weight]:0;//库存量空运
		$data['kchz']    	= $kclhai[money]+$kclkong[money];//库存货值
		$data['kchkhs']  	= $kchkhs+$kchkhs1;//库存货客户数
		$data['yfhqkkh'] 	= $yfhqkkh;//已发货欠款客户
		$data['daidaogangky'] = $daidaogangky;//待到港（柜/票）空运
		$data['daidaoganghy'] = $daidaoganghy;//待到港（柜/票）海运
		$data['dpslyc'] 	= $dpslyc;//待派送量（一程）
		$data['dpslec'] 	= $dpslec;//待派送量（二程）
		$data['dbykwpskh'] 	= $dbykwpskh;//迪拜欠款客户
		$data['ylykwpskh'] 	= $ylykwpskh;//伊朗欠款客户


		$data['cqwfhhy'] 	= $cqwfhhy;//超期未发货海运
		$data['cqwfhhyarr'] = $cqwfhhyarr;//超期未发货海运
		$data['cqwfhky'] 	= $cqwfhky;//超期未发货海运
		$data['cqwfhkyarr'] = $cqwfhkyarr;//超期未发货海运

		$data['cqwdghy'] 	= $cqwdghy;//超期未到港海运
		$data['cqwdgky'] 	= $cqwdgky;//超期未到港海运


		$data['cqwdg'] 	    = $cqwdg[count]?$cqwdg[count]:0;//超期未到港
		$data['cqwpsdb'] 	= $cqwpsdb;//超期未派送迪拜
		$data['cqwpsyl'] 	= $cqwpsyl;//超期未派送伊朗
		$data['kehu'] = $kehu;//待到港（柜/票）空运
		return $data;
	}

	function shixiao($db1,$starttime,$endtime,$fahuo = '海运'){
		if ($fahuo) {
			$where = ' and fahuo = "'.$fahuo.'"';
		}
		if ($fahuo == '海运') {
			$filed = ',volume as count';
			$filed1 = ',sum(volume) as count';
		}else{
			$filed = ',weight as count';
			$filed1 = ',sum(weight) as count';
		}
		//国内出仓时效
		$gnccsxsql = $db1->query("select TIMESTAMPDIFF(DAY,rukutime,chucangtime) as time $filed from yasa_warehouse where is_del = 1 and status = 3 and chucangtime is not null and rukutime BETWEEN '$starttime' and '$endtime' ".$where);
		$time1 = 0;
		$time2 = 0;
		$time3 = 0;
		$time4 = 0;
		$time5 = 0;
		$time6 = 0;
		$time7 = 0;
		$time8 = 0;
		$time9 = 0;

		$count1 = 0;
		$count2 = 0;
		$count3 = 0;
		$count4 = 0;
		$count5 = 0;
		$count6 = 0;
		$count7 = 0;
		$count8 = 0;
		$count9 = 0;
		$gntian = array('1天', '2天', '3天', '4天', '5天临界值', '6~10天', '11~20天', '21~30天', '30天以上');
		while ($gnccsx = $db1->fetch_array($gnccsxsql)) {
			if ($gnccsx[time] == 1 || $gnccsx[time] == 0) {
				$time1++;
				$count1 += $gnccsx[count];
			}elseif ($gnccsx[time] == 2) {
				$time2++;
				$count2 += $gnccsx[count];
			}elseif ($gnccsx[time] == 3) {
				$time3++;
				$count3 += $gnccsx[count];
			}elseif ($gnccsx[time] == 4) {
				$time4++;
				$count4 += $gnccsx[count];
			}elseif ($gnccsx[time] == 5) {
				$time5++;
				$count5 += $gnccsx[count];
			}elseif ($gnccsx[time] >5 && $gnccsx[time] <= 10) {
				$time6++;
				$count6 += $gnccsx[count];			
			}elseif ($gnccsx[time] >10 && $gnccsx[time] <= 20) {
				$time7++;
				$count7 += $gnccsx[count];		
			}elseif ($gnccsx[time] >20 && $gnccsx[time] <= 30) {
				$time8++;
				$count8 += $gnccsx[count];	
			}elseif ($gnccsx[time] >30) {
				$time9++;
				$count9 += $gnccsx[count];
			}
			$a[] = $gnccsx[time];
		}
		$gnarr = array($time1,$time2,$time3,$time4,$time5,$time6,$time7,$time8,$time9);
		$gnarr1 = array($count1,$count2,$count3,$count4,$count5,$count6,$count7,$count8,$count9);
		//迪拜
		if ($fahuo == '海运') {
			$dbtian = array('0~20天','20~25天','26~30天临界值','31~35天','36~40天','41~45天','45天以上');
		}else{
			$dbtian = array('1~5天','6天','7天','8天','9天','10天临界值','11天','12天','13天','14天','15天','15~20天','20天以上');
		}
		if ($fahuo) {
			$where1 = ' and w.fahuo = "'.$fahuo.'"';
		}
		$dbdgsxsql = $db1->query("select y.fahuotime,w.fahuohao,min(w.dgtime) as time $filed from yasa_dibaiwarehouse as w left join yasa_yunfeidan as y on y.fahuohao = w.fahuohao where w.status != 1 and y.is_del = 1 and y.fahuotime BETWEEN '$starttime' and '$endtime' ".$where1." group by w.fahuohao,w.fahuo");
		// echo "select (min(dgtime)-max(addtime)) as time from yasa_dibaiwarehouse where status != 1 and (dgtime-addtime) > 0 and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59").$where." group by fahuohao";die;
		if ($fahuo == '海运') {
			$dbtime1 = 0;
			$dbtime2 = 0;
			$dbtime3 = 0;
			$dbtime4 = 0;
			$dbtime5 = 0;
			$dbtime6 = 0;
			$dbtime7 = 0;

			$dbcount1 = 0;
			$dbcount2 = 0;
			$dbcount3 = 0;
			$dbcount4 = 0;
			$dbcount5 = 0;
			$dbcount6 = 0;
			$dbcount7 = 0;
		}else{
			$dbtime1 = 0;
			$dbtime2 = 0;
			$dbtime3 = 0;
			$dbtime4 = 0;
			$dbtime5 = 0;
			$dbtime6 = 0;
			$dbtime7 = 0;
			$dbtime8 = 0;
			$dbtime9 = 0;
			$dbtime10 = 0;
			$dbtime11 = 0;
			$dbtime12 = 0;
			$dbtime13 = 0;

			$dbcount1 = 0;
			$dbcount2 = 0;
			$dbcount3 = 0;
			$dbcount4 = 0;
			$dbcount5 = 0;
			$dbcount6 = 0;
			$dbcount7 = 0;
			$dbcount8 = 0;
			$dbcount9 = 0;
			$dbcount10 = 0;
			$dbcount11 = 0;
			$dbcount12 = 0;
			$dbcount13 = 0;
		}
		while ($dbdgsx = $db1->fetch_array($dbdgsxsql)) {
			if ($fahuo == '海运') {
				if (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>= 0 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 20) {
					$dbtime1++;
					$dbcount1 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>20 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 25) {
					$dbtime2++;
					$dbcount2 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>25 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 30) {
					$dbtime3++;
					$dbcount3 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>30 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 35) {
					$dbtime4++;
					$dbcount4 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>35 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 40) {
					$dbtime5++;
					$dbcount5 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>40 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 45) {
					$dbtime6++;
					$dbcount6 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>45) {
					$dbtime7++;
					$dbcount7 += $dbdgsx[count];
				}
				$b[] = ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24));
			}else{
				if (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>= 1 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 5) {
					$dbtime1++;
					$dbcount1 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==6) {
					$dbtime2++;
					$dbcount2 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==7) {
					$dbtime3++;
					$dbcount3 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==8) {
					$dbtime4++;
					$dbcount4 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==9) {
					$dbtime5++;
					$dbcount5 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==10) {
					$dbtime6++;
					$dbcount6 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==11) {
					$dbtime7++;
					$dbcount7 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==12) {
					$dbtime8++;
					$dbcount8 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==13) {
					$dbtime9++;
					$dbcount9 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==14) {
					$dbtime10++;
					$dbcount10 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))==15) {
					$dbtime11++;
					$dbcount11 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>15 && ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24)) <= 20) {
					$dbtime12++;
					$dbcount12 += $dbdgsx[count];
				}elseif (ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24))>20) {
					$dbtime13++;
					$dbcount13 += $dbdgsx[count];
				}
				$b[] = ceil(($dbdgsx[time]-strtotime($dbdgsx[fahuotime]))/(60*60*24));
			}
		}
		if ($fahuo == '海运') {
			$dbarr = array($dbtime1,$dbtime2,$dbtime3,$dbtime4,$dbtime5,$dbtime6,$dbtime7);
			$dbarr1 = array($dbcount1,$dbcount2,$dbcount3,$dbcount4,$dbcount5,$dbcount6,$dbcount7);
		}else{
			$dbarr = array($dbtime1,$dbtime2,$dbtime3,$dbtime4,$dbtime5,$dbtime6,$dbtime7,$dbtime8,$dbtime9,$dbtime10,$dbtime11,$dbtime12,$dbtime13);
			$dbarr1 = array($dbcount1,$dbcount2,$dbcount3,$dbcount4,$dbcount5,$dbcount6,$dbcount7,$dbcount8,$dbcount9,$dbcount10,$dbcount11,$dbcount12,$dbcount13);
		}
		//伊朗到港时效
		$yldgsxsql = $db1->query("select (dgtime-addtime) as time from yasa_ylwarehouse where is_del = 1 and status != 1 and (dgtime-addtime) > 0 and addtime >= ".strtotime($starttime.' 0:0:0')." and addtime <= ".strtotime($endtime." 23:59:59"));
		$yltian = array("1~10天","11~20天","20~30天临界值","31~45天","45天以上");
		$yltime1 = 0;
		$yltime2 = 0;
		$yltime3 = 0;
		$yltime4 = 0;
		$yltime5 = 0;
		while ($yldgsx = $db1->fetch_array($yldgsxsql)) {
			if (ceil($yldgsx[time]/(60*60*24))>=1 && ceil($ysldgsx[time]/(60*60*24)) <= 10) {//10天以下
				$yltime1++;
			}elseif (ceil($yldgsx[time]/(60*60*24))>10 && ceil($yldgsx[time]/(60*60*24)) <= 20) {//11~20天
				$yltime2++;
			}elseif (ceil($yldgsx[time]/(60*60*24))>20 && ceil($yldgsx[time]/(60*60*24)) <= 30) {//21~30天
				$yltime3++;
			}elseif (ceil($yldgsx[time]/(60*60*24))>30 && ceil($yldgsx[time]/(60*60*24)) <= 45) {//31~40天
				$yltime4++;
			}elseif (ceil($yldgsx[time]/(60*60*24))>45) {//31~40天
				$yltime5++;
			}
			$c[] = ceil($yldgsx[time]/(60*60*24));
		}
		$ylarr = array($yltime1,$yltime2,$yltime3,$yltime4,$yltime5);

		$data[gntian] = $gntian;
		$data[gnarr] = $gnarr;
		$data[gnarr1] = $gnarr1;
		$data[dbtian] = $dbtian;
		$data[dbarr] = $dbarr;
		$data[dbarr1] = $dbarr1;
		$data[yltian] = $yltian;
		$data[ylarr] = $ylarr;
		$data[a] = $a;
		$data[b] = $b;
		$data[c] = $c;
		return $data;
	}

	//客户统计
	function kehu($db1,$starttime,$endtime,$type=1){
		//新客户
		$xinkehu = $db1->get_one("select count(*) as count from yasa_kehu where is_delete = 1 and addtime >= $starttime and addtime <= $endtime");
		$xinkehusql = $db1->query("select * from yasa_kehu where is_delete = 1 and addtime >= $starttime and addtime <= $endtime");
		while ($xinkehuarr = $db1->fetch_array($xinkehusql)) {
			$xinkehuarrs[kehu_number] = $xinkehuarr[kehu_number];
			$xinkehuarrs[username] = $xinkehuarr[username];
			$xinkehuarrs[phone] = $xinkehuarr[phone];
			$xinkehuarrs[value] = date("Y-m-d",$xinkehuarr[addtime]);
			$xinkehuid[] = $xinkehuarrs;
		}

		if ($type == 2) {
			//大额交易客户数（金额>30w） 
			$dejykhs = 0;
			$dejykhssql = $db1->query("select sum(o.type1) as money,o.pid1,k.kehu_number,k.username,k.phone from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where o.is_delete = 1 and o.addtime >= $starttime and o.addtime <= $endtime group by o.pid1");
			while ($dejykhsz = $db1->fetch_array($dejykhssql)) {
				if ($dejykhsz[money] >= 300000) {
					$dejykhs ++;
					$dearr[pid1] = $dejykhsz[pid1];
					$dearr[kehu_number] = $dejykhsz[kehu_number];
					$dearr[phone] = $dejykhsz[phone];
					$dearr[username] = $dejykhsz[username];
					$dearr[value] = $dejykhsz[money];
					$dejykhsid[] = $dearr;
				}
			}

			//大额佣金客户（佣金1w）
			$deyjkh = 0;
			$deyjkhsql = $db1->query("select o.order_id,o.pid1,k.kehu_number,k.username,k.phone from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where o.is_delete = 1 and o.pid1 != '' and o.addtime >= $starttime and o.addtime <= $endtime");
			while ($deyjkh1 = $db1->fetch_array($deyjkhsql)) {
				$fktimeurl = "http://47.106.88.138:8080/api/lemonyy1/queryOrderByNumber?number=".$deyjkh1[order_id];
				$fktimeurl = curl_get_https($fktimeurl);
				// echo '<pre>';
				$stime = 0;
				$fktime = json_decode($fktimeurl);
				$expendmoney = 0;
				foreach ($fktime->list as $key => $value) {
					// if ($value->expendType == '支付订金' || $value->expendType=='支付余款') {
					// 	if (strtotime($stime) < strtotime($value->operateTime)) {
					// 		$stime = $value->operateTime;
					// 	}
					// }
					if ($value->expendType == '佣金') {
						$expendmoney += $value->expend;	
					}
				}
				$kehu[$deyjkh1[pid1]]['value'] += $expendmoney;
				$kehu[$deyjkh1[pid1]]['kehu_number'] = $deyjkh1[kehu_number];
				$kehu[$deyjkh1[pid1]]['username'] = $deyjkh1[username];
				$kehu[$deyjkh1[pid1]]['phone'] = $deyjkh1[phone];
				$kehu[$deyjkh1[pid1]]['pid1'] = $deyjkh1[pid1];
			}
			// print_r($kehu);die;
			foreach ($kehu as $key => $value) {
				if ($value[value] >= 5000) {
					$deyjkh ++;
					$deyjkhid[] = $value;
				}
			}

			//大货量运输客户 海运/ 空运
			$dhlyskhhy = 0;
			$dhlyskhky = 0;
			$dhlyskhhysql = $db1->query("select sum(w.volume) as volume,w.userid,k.kehu_number,k.username,k.phone  from yasa_warehouse as w left join yasa_kehu as k on k.kehu_number = w.userid where w.is_del = 1 and w.fahuo = '海运' and w.chucangtime BETWEEN '".date('Y-m-d',$starttime)."' and '".date("Y-m-d",$endtime)."' group by w.userid");
			while ($dhlyskhhy1 = $db1->fetch_array($dhlyskhhysql)) {
				if ($dhlyskhhy1[volume] >= 40) {
					$dhlyskhhy ++;
					$dharr[kehu_number] = $dhlyskhhy1[kehu_number];
					$dharr[username] = $dhlyskhhy1[username];
					$dharr[phone] = $dhlyskhhy1[phone];
					$dharr[value] = round($dhlyskhhy1[volume],2);

					$dhlyskhhyid[] = $dharr;
				}
			}

			$dhlyskhkysql = $db1->query("select sum(w.volume) as volume,w.userid,k.kehu_number,k.username,k.phone  from yasa_warehouse as w left join yasa_kehu as k on k.kehu_number = w.userid where w.is_del = 1 and w.fahuo = '空运' and w.chucangtime BETWEEN '".date('Y-m-d',$starttime)."' and '".date("Y-m-d",$endtime)."' group by w.userid");
			while ($dhlyskhky1 = $db1->fetch_array($dhlyskhkysql)) {
				if ($dhlyskhky1[weight] >= 100) {
					$dhlyskhky ++;
					$dharr[kehu_number] = $dhlyskhky1[kehu_number];
					$dharr[username] = $dhlyskhky1[username];
					$dharr[phone] = $dhlyskhky1[phone];
					$dharr[value] = round($dhlyskhky1[weight],2);
					$dhlyskhkyid[] = $dharr;
				}
			}

		}


		$data[xinkehu]        = $xinkehu[count];//新客户
		$data[xinkehuid]      = $xinkehuid;

		$data[dejykhs]        = $dejykhs;//大额交易客户数（金额>50w）
		$data[dejykhsid]      = $dejykhsid;

		$data[deyjkh]         = $deyjkh;//大额佣金客户（佣金1w）
		$data[deyjkhid]       = $deyjkhid;

		$data[dhlyskhhy]      = $dhlyskhhy;//大货量运输客户 海运 68
		$data[dhlyskhhyid]    = $dhlyskhhyid;

		$data[dhlyskhky]      = $dhlyskhky;//大货量运输客户 空运 1000
		$data[dhlyskhkyid]    = $dhlyskhkyid;

		

		return $data;
	}
	function dangqiankehu($db1,$starttime,$endtime){
		//有存款客户数/金额总计 and 大额存款客户（余额>5w） and 有欠款客户数/金额总计 and 大额欠款客户（欠款>5w） and 结账客户数（余额=0）
		$ckkhs = 0;//存款客户数
		$ckkhmoneysum = 0;//存款客户 金额总计
		$deckkh = 0;//大额存款客户
		$qkkhs = 0;//有欠款客户数
		$qkkhmoneysum = 0;//欠款客户 金额总计
		$deqkkh = 0;//大额欠款客户
		$jzkhs = 0;//结账客户数

		//睡眠客户数
		$smkhs = 0;
		$jskhs = 0;


		$kehusql = $db1->query("select id,kehu_number,phone,username from yasa_kehu where is_delete = 1 and kehu_number like 'YY%'");

		while ($kehu = $db1->fetch_array($kehusql)) {
			$caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".urlencode($kehu[kehu_number]);
			$caiwu = curl_get_https($caiwuurl);
			$caiwu = json_decode($caiwu);
			
			if ($caiwu->balance !== null) {
				if (floor($caiwu->balance) < 0) {
					$qkkhs ++;
					$qkkhmoneysum += $caiwu->balance;
					$qkkhsarr[phone] = $kehu[phone];
					$qkkhsarr[username] = $kehu[username];
					$qkkhsarr[kehu_number] = $kehu[kehu_number];
					$qkkhsarr[value] = $caiwu->balance;
					$qkkhsid[] = $qkkhsarr;
				}elseif (floor($caiwu->balance) > 0) {
					$ckkhs ++;
					$ckkhmoneysum += $caiwu->balance;

					$ckkhsarr[phone] = $kehu[phone];
					$ckkhsarr[username] = $kehu[username];
					$ckkhsarr[kehu_number] = $kehu[kehu_number];
					$ckkhsarr[value] = $caiwu->balance;
					$ckkhsid[] = $ckkhsarr;

				}elseif (floor($caiwu->balance) == 0) {
					$jzkhs ++;

					$jzkhsarr[phone] = $kehu[phone];
					$jzkhsarr[username] = $kehu[username];
					$jzkhsarr[kehu_number] = $kehu[kehu_number];
					$jzkhsarr[value] = $caiwu->balance;
					$jzkhsid[] = $jzkhsarr;
				}
				//大额存款客户
				if (floor($caiwu->balance) > 10000) {
					$deckkh ++;
				
					$deckkharr[phone] = $kehu[phone];
					$deckkharr[username] = $kehu[username];
					$deckkharr[kehu_number] = $kehu[kehu_number];
					$deckkharr[value] = $caiwu->balance;
					$deckkhid[] = $deckkharr;
				}
				//大额欠款客户
				if (floor($caiwu->balance) < -20000) {
					$deqkkh ++;

					$deqkkharr[phone] = $kehu[phone];
					$deqkkharr[username] = $kehu[username];
					$deqkkharr[kehu_number] = $kehu[kehu_number];
					$deqkkharr[value] = $caiwu->balance;
					$deqkkhid[] = $deqkkharr;
				}
			}

			$shuimiankehu = $db1->get_one("select addtime from yasa_order where is_delete = 1 and pid1 = '$kehu[id]' order by addtime desc limit 1");
			if ($shuimiankehu) {
				if ($shuimiankehu[addtime] <= strtotime("- 6 months") && $shuimiankehu[addtime] >= strtotime("- 1 years")) {
					$smkhs ++;

					$smkhsarr[phone] = $kehu[phone];
					$smkhsarr[username] = $kehu[username];
					$smkhsarr[kehu_number] = $kehu[kehu_number];
					$smkhsarr[value] = date("Y-m-d",$shuimiankehu[addtime]);
					$smkhsid[] = $smkhsarr;
				}elseif ($shuimiankehu[addtime] < strtotime("- 1 years")) {
					$jskhs++;

					$jskhsarr[phone] = $kehu[phone];
					$jskhsarr[username] = $kehu[username];
					$jskhsarr[kehu_number] = $kehu[kehu_number];
					$jskhsarr[value] = date("Y-m-d",$shuimiankehu[addtime]);
					$jskhsid[] = $jskhsarr;
				}
			}else{
				$jskhs++;

				$jskhsarr[phone] = $kehu[phone];
				$jskhsarr[username] = $kehu[username];
				$jskhsarr[kehu_number] = $kehu[kehu_number];
				$jskhsarr[value] = '没下单';
				$jskhsid[] = $jskhsarr;
			}

		}

		
		// $shuimiankehusql = $db1->query("select max(o.addtime) as time,k.kehu_number,k.phone,k.username from yasa_order as o left join yasa_kehu as k on k.id = o.pid1 where o.is_delete = 1 and o.pid1 != '' group by o.pid1");
		// while ($shuimiankehushu1 = $db1->fetch_array($shuimiankehusql)) {
		// 	if ($shuimiankehushu1[time] <= strtotime("- 6 months") && $shuimiankehushu1[time] >= strtotime("- 1 years")) {
		// 		$smkhs ++;

		// 		$smkhsarr[phone] = $shuimiankehushu1[phone];
		// 		$smkhsarr[username] = $shuimiankehushu1[username];
		// 		$smkhsarr[kehu_number] = $shuimiankehushu1[kehu_number];
		// 		$smkhsarr[value] = date("Y-m-d",$shuimiankehushu1[time]);
		// 		$smkhsid[] = $smkhsarr;
		// 	}elseif ($shuimiankehushu1[time] < strtotime("- 1 years")) {
		// 		$jskhs++;

		// 		$jskhsarr[phone] = $shuimiankehushu1[phone];
		// 		$jskhsarr[username] = $shuimiankehushu1[username];
		// 		$jskhsarr[kehu_number] = $shuimiankehushu1[kehu_number];
		// 		$jskhsarr[value] = date("Y-m-d",$shuimiankehushu1[time]);
		// 		$jskhsid[] = $jskhsarr;
		// 	}
		// }
		


		$data[ckkhs]          = $ckkhs;//存款客户数
		$data[ckkhsid]        = $ckkhsid;

		$data[ckkhmoneysum]   = $ckkhmoneysum;//存款客户 金额总计

		$data[deckkh]         = $deckkh;//大额存款客户
		$data[deckkhid]       = $deckkhid;

		$data[qkkhs]          = $qkkhs;//有欠款客户数
		$data[qkkhsid]        = $qkkhsid;

		$data[qkkhmoneysum]   = $qkkhmoneysum;//欠款客户 金额总计
		// $data[qkkhmoneysumid] = $qkkhmoneysumid;

		$data[deqkkh]         = $deqkkh;//大额欠款客户
		$data[deqkkhid]       = $deqkkhid;

		$data[jzkhs]          = $jzkhs;//结账客户数
		$data[jzkhsid]        = $jzkhsid;

		$data[smkhs]          = $smkhs;//睡眠客户数
		$data[smkhsid]        = $smkhsid;

		$data[jskhs]          = $jskhs;//僵尸客户数
		$data[jskhsid]        = $jskhsid;


		return $data;
	}

	function getMonth($date){
		$firstday = date("Y-m-01",strtotime($date));
		$lastday = date("Y-m-d",strtotime("$firstday +1 month -1 day"));
		return array($firstday,$lastday);
	}

	function getlastMonthDays($date){
	    $timestamp=strtotime($date);
	    $firstday=date('Y-m-01',strtotime(date('Y',$timestamp).'-'.(date('m',$timestamp)-1).'-01'));
	    $lastday=date('Y-m-d',strtotime("$firstday +1 month -1 day"));
	    return array($firstday,$lastday);
 	}

    //add by zhz end
   	
    
	
?>