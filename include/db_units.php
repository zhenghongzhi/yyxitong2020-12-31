<?PHP


function sendPostData($url, $post){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post));
		return curl_exec($ch);
	
}	
/**
 * 系统提示信息
 *
 * @access      public
 * @param       string      msg_detail      消息内容
 * @param       int         msg_type        消息类型， 0消息，1错误，2询问
 * @param       array       links           可选的链接
 * @param       boolen      $auto_redirect  是否需要自动跳转
 * @return      void
 */
function er($msg,$msgtype,$links=array()){
	global $base_url;
	if (count($links) == 0)
    {
        $links[0]['text'] = "返回";
        $links[0]['href'] = 'javascript:history.go(-1)';
    }
	$urls = "";
	foreach($links as $k=>$v){
		$urls.="<li><a href='".$links[$k]['href']."'>".$links[$k]['text']."</a></li>";
	}
	$first_url = $links[0]['href'];
	if($msgtype == 0 ){
		$infoimg = "information";
	}elseif($msgtype == 2){
		$infoimg = "warning";
	}else{
		$infoimg = "confirm";
	}
	$msgfile = @file_get_contents($base_url."/msg.php");
	$msgfile = str_replace('{$msg}',$msg,$msgfile);
	$msgfile = str_replace('{$infoimg}',$infoimg,$msgfile);
	$msgfile = str_replace('{$firsturl}',$first_url,$msgfile);
	$msgfile = str_replace('{$urls}',$urls,$msgfile);
	exit($msgfile);
	
}


function gethtm($t,$id){
	global $db;
	$a=$db->get_one("select * from $t where id='$id'");
	if($a['id']){
		if($a['contents']) return char_cv($a['contents']);
		else return $a['content'];
	}
}

	


function cp($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$page = isset($page)?(int)$page:1;
	$getpage = new Page($page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA的日志
function cp2($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$gz_page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$gz_page = isset($gz_page)?(int)$gz_page:1;
	$getpage = new gz_Page($gz_page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA的日志
//TA的关注
function cp3($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$rz_page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$rz_page = isset($rz_page)?(int)$rz_page:1;
	$getpage = new rz_page($rz_page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA的关注
//TA关注的机会
function cp4($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$jh_page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$jh_page = isset($jh_page)?(int)$jh_page:1;
	$getpage = new jh_Page($jh_page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA关注的机会
//TA发布的机会
function cp5($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$fb_page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$fb_page = isset($fb_page)?(int)$fb_page:1;
	$getpage = new fb_Page($fb_page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA发布的机会
//TA参与的活动
function cp6($sql,$sql2,$pagesize,$baselink,&$pages,&$counts,$pagestyle=10){
	global $db,$hd_page;
	$c=$db->get_one($sql);
	$counts = $c['c'];
	$hd_page = isset($hd_page)?(int)$hd_page:1;
	$getpage = new hd_page($hd_page,$counts,$pagesize,$pagestyle);
	$pages = $getpage->genHtml($baselink);
	$sql2.=" LIMIT ".$getpage->start().", ".$pagesize;
	$query = $db->query($sql2);
	return $query;
	
}
//TA参与的活动

function fck($content,$tname='content',$w='100%',$h='400'){
	include_once("../include/fckeditor/fckeditor.php") ;
	$sBasePath = '../include/fckeditor/';
	$oFCKeditor = new FCKeditor($tname) ;
	$oFCKeditor->Width = $w;
	$oFCKeditor->Height = $h;
	$oFCKeditor->BasePath	= $sBasePath ;
	$oFCKeditor->Value		= $content;
	$oFCKeditor->Create();
}

function fck2($content,$tname='content',$w='100%',$h='400'){
	include_once("include/fckeditor/fckeditor.php") ;
	$sBasePath = 'include/fckeditor/';
	$oFCKeditor = new FCKeditor($tname) ;
	$oFCKeditor->Width = $w;
	$oFCKeditor->Height = $h;
	$oFCKeditor->BasePath	= $sBasePath ;
	$oFCKeditor->Value		= $content;
	$oFCKeditor->Create();
}


/*
表单提交检查

邮箱验证：$cstr[0]=array("值","邮箱","e","不能超过长度");
普通标题：$cstr[1]=array("值","标题","s","255");
数字检查：$cstr[2]=array("值","排序号",'n','10');

*/
function upc($cstr){
	$msg="";
	foreach($cstr as $k => $v){
		if($v[2]=='s'){
			if(empty($v[0])){
				$msg.= $v[1]."不能为空白<br>";
			}
		}elseif($v[2]=='e'){
			if(!is_email($v[0])){
				$msg.= "$v[1]的格式不对<br>";
			}
		}elseif($v[2]=='n'){
			if(!is_numeric($v[0])){
				$msg.="$v[1]只能填写数字<br>";
			}
		}elseif($v[2]=='p'){
			if($v[0]=="选择省份"){
				$msg.= $v[1]."不能为空白<br>";
			}
		}elseif($v[2]=='q'){
			if($v[0]=="选择城市"){
				$msg.= $v[1]."不能为空白<br>";
			}
		}elseif($v[2]=='r'){
			if($v[0]=="选择地区"){
				$msg.= $v[1]."不能为空白<br>";
			}
		}
		if($v[3]>0 && strlen($v[0])>$v[3]){
			$msg.=$v[1]."的长度不能超过".$v[3]."字节<br>";
		}
	}
	
	return $msg;
	
}



function insert_sql($arr,$table){
		$sql="insert into $table (";
		$sql1="";
		$sql2="";
		if(is_array($arr)){
			foreach($arr as $key => $value){
				if(is_array($value)){
					$sql1.=empty($sql1)?$value[0]:",".$value[0];
					$sql2.=empty($sql2)?"'$".$value[1]."'":",'$".$value[1]."'";
				}else{
					$sql1.=empty($sql1)?$value:",".$value;
					$sql2.=empty($sql2)?"'$".$value."'":",'$".$value."'";
				}
			}
		$sql=$sql.$sql1.') values ( '.$sql2.' )';
		return $sql;	
		}
		
	}
	
function get_cname($table,$needid=''){
global $db;
	$sql="SHOW COLUMNS FROM $table";

	$query=$db->query($sql);
	$colums = '';
	$i=0;
	while($a=$db->fetch_array($query)){
		$colums_=$a['Field'];
		if($needid==1){
			$colums.=empty($colums)?$colums_:",".$colums_;
		}else{
			if($i>0){
				$colums.=empty($colums)?"$colums_":",".$colums_."";
			}
		}
		$i++;	
	}
	$arr=explode(",",$colums);
	return  insert_sql($arr,$table);
	
}



/*
插入附件
*/
function uf($upload_dir,$max_size,$file_name,$attach_id,$pid=0,$m=0,$w=200,$h=200){
	global $db,$img_subject,$img_orders;
	if($m){
		include_once("little.pic.class.php");
	}	
	$fname = $_FILES[$file_name];
	
	if(($fc=count($fname['name']))>0){
		$arr = 1;
	}


	if($arr){
	
		for($i=0;$i<$fc;$i++){
	
			$fids=$i+1;
			if(!empty($fname['name'][$i])){

				if(!empty($img_subject[$i])){
					$filename_ = $img_subject[$i];
				}else{
					$filename_ = $fname['name'][$i];
				}
				$file_size = $fname['size'][$i];
				$file_url = 'upfiles/'.upload_file($file_name,$upload_dir,$max_size,$fids);								
				$file_ext = substr($fname['name'][$i],strrpos($fname['name'][$i],"."));
				if($m && ($file_ext == ".jpg" ||$file_ext == ".jpeg" ||$file_ext == ".gif" || $file_ext == ".png")){
					$resize_url='';
					$file_url1 = split("\.",$file_url);
					$newurl = $file_url1[0]."_small".".".$file_url1[1];
	
					$resizeimage = new resizeimage("../".$file_url, $w, $h, "0"); 
					$file_url = $resizeimage->dstimg;
					$file_url = str_replace("../","",$file_url);	
	
				}
				$db->query("insert into attach_file (filename,file_url,news_id,file_ext,file_size,subject,orders,pid) 
				values ('$filename_','$file_url','$attach_id','$file_ext','$file_size','$img_subject[$i]','$img_orders[$i]',$pid)");
			}
		}
	}else{
		if(!empty($fname['name'])){
			if(!empty($img_subject[0])){
				$filename_ = $img_subject[0];
			}else{
				$filename_ = $fname['name'];
			}
			$file_size = $fname['size'];
			$file_url = 'upfiles/'.upload_file($file_name,$upload_dir,$max_size);
			$file_ext = substr($fname['name'][$i],strrpos($fname['name'],"."));
			$db->query("insert into attach_file (filename,file_url,news_id,file_ext,file_size,subject,orders,pid) 
			values ('$filename_','$file_url','$attach_id','$file_ext','$file_size''$img_subject[0]','$img_orders[0]','$pid')");
			return $file_url;
		}
	}
	
	return $file_url;
}


function ufv($upload_dir,$max_size,$file_name,$img_subject,$img_orders){
	global $db;
	

	$upload_dirs = str_replace("../","",$upload_dir);
	$fname = $_FILES[$file_name];
	
	if(($fc=count($fname['name']))>0){
		$arr = 1;
	}
	
	$rs = array();

	if($arr){
	
	
		for($i=0;$i<$fc;$i++){

	
			$fids=$i+1;
			if(!empty($fname['name'][$i])){

				if(!empty($img_subject[$i])){
					$filename_ = $img_subject[$i];
				}else{
					$filename_ = $fname['name'][$i];
				}
				$rs[$i]['file_size'] = $fname['size'][$i];
				$rs[$i]['file_url'] = $upload_dirs.upload_file($file_name,$upload_dir,$max_size,$fids);								
				$rs[$i]['file_ext'] = substr($fname['name'][$i],strrpos($fname['name'][$i],"."));
				$rs[$i]['file_name'] = $filename_;
				$rs[$i]['orders'] = $img_orders[$i];
			}
		}
	}
	
	return $rs;
}

function gc($table,$cd,$subject=''){
	global $db;
	if(empty($subject)){
		$a=$db->get_one("select count(*) as c from $table $cd ");
	}else{
		$a=$db->get_one("select $subject as c from $table $cd ");
	}	
	return $a['c'];
}

function gcs($table,$cd){
	global $db;
	return $db->get_one("select * from $table $cd limit 0,1");
}

function getkeywords($content,$flag=0){
	global $db;
	$query=$db->query("select * from cuiniao_my_keywords");
	$k = array();
	$url = array();
	while($a=$db->fetch_array($query)){
		$k[]=$a['subject'];
		$url[]=$a['url'];
	}
	$kstr=array();
	foreach($k as $key=>$v){
		if(ereg($v,$content)){
			if($flag){
				if(ereg("http",$url[$key])){
					$kstr[]="<a href='$url[$key]' target='_blank'>".$v."</a>";
				}else{
					$kstr[]="<a href='/$url[$key]'>".$v."</a>";
				}	
			}else{
				$kstr[] = $v;
			}
		}
	}
	return implode(',',$kstr);

}

function getkeyurl($kstr,$content){
	global $db;
	$arr_rep = explode(",",$kstr);
	$kstr1 = strip_tags($kstr);
	$arr_find = explode(",",$kstr1);
	return str_replace($arr_find,$arr_rep,$content);	
}



function getart($img,$pid,$limit,$text){
	global $db,$base_url;
	$str='<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="12%" height="5"></td>
            <td width="88%"></td>
          </tr>
          <tr>
            <td height="18" align="left" style="padding-left:8px;"><img src="'.$base_url.'/images/'.$img.'.gif" width="9" height="9" /></td>
            <td height="22" class="artt"><a href="'.$base_url.'/article_pid_'.$pid.'_1.html">'.$text.'</a></td>
          </tr>';
	
		  
		  $query=$db->query("select * from news where pid='$pid' and isvalid=1 order by istop desc,orders desc,id desc limit 0,$limit");
		  while($a=$db->fetch_array($query)){
		  
          $str.='<tr>
            <td height="22" colspan="2" class="arttext" style="padding-left:8px;"><a href="'.$base_url.'/article/article_detail_'.$a['id'].'.html">'.substrs($a['subject'],30).'</a></td>
            </tr>';
		
		  }
		 $str.='
          <tr>
            <td  height="5"></td>
            <td ></td>
          </tr>
        </table>';
		return $str;
}

function checkuser($username){
	global $db;
	$flag = true;
	$a=$db->get_one("select count(*) as c  from pure.uc_members where username='$username'");
	if($a['c']>0){
		$flag = false;
	}
	return $flag;
	
}

function logs($msg){
	global $db;
	if($_SESSION['userid']){
		return $db->query("insert into editlog (subject,addtime,ifsee,userid) values ('$msg','".date("Y-m-d H:i:s")."',0,'$_SESSION[userid]')");
	}	
}

function getart_lijian($img,$pid,$limit,$text){
	global $db,$base_url;
	$str='<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="12%" height="5"></td>
            <td width="88%"></td>
          </tr>
          <tr>
            <td height="18" align="left" style="padding-left:8px;"><img src="'.$base_url.'/images/'.$img.'.gif" width="9" height="9" /></td>
            <td height="22" class="artt"><a href="'.$base_url.'/lijiang_article_pid_'.$pid.'_1.html">'.$text.'</a></td>
          </tr>';
	
		  
		  $query=$db->query("select * from news where pid='$pid' and isvalid=1 order by istop desc,orders desc,id desc limit 0,$limit");
		  while($a=$db->fetch_array($query)){
		  
          $str.='<tr>
            <td height="22" colspan="2" class="arttext" style="padding-left:8px;"><a href="'.$base_url.'/article/lijiang_article_detail_'.$a['id'].'.html">'.substrs($a['subject'],30).'</a></td>
            </tr>';
		
		  }
		 $str.='
          <tr>
            <td  height="5"></td>
            <td ></td>
          </tr>
        </table>';
		return $str;
}

function checkorder(){
	global $db;
	$a=$db->get_one("select A.*,B.subject from orders A left join photo_price B on A.pid=B.id where account='$_SESSION[ljname]' order by A.id desc limit 0,1");
	if($a){
		$restr = "<br>您最新预约的套系是：<br>$a[subject]<br>支付状态：";
		if($a['ispay']==0){
			$restr .= "未支付定金";
		}else{
			$mstr = $a['ispay']==1?"已付定金":"已全部支付";
			if($a['photo_date']>date("Y-m-d")){
				$restr .= "$mstr";
			}else{
				$restr = "<br>";
			}
		}
	}
	return $restr;	
}

function getphotodate(){
	global $db;
	if($_SESSION['isvip']){
		$a=gcs("orders"," where uid='$_SESSION[userid]' order by id desc ");
	}	
	return $a['photo_date'];
}

function getbbsjf(){
	global $db;
	$a=gcs("pure.cdb_members"," where username='$_SESSION[ljname]'");
	return "<b>".$a['credits']."</b>&nbsp;贝壳";
}

function chtm($fromurl,$tofile,$newfold="",$rep="",$repto=""){
	global $base_url;
	$something = @file_get_contents($fromurl);
	if($something){
		if($newfold){
	
			$something = str_replace("images/",$base_url."/images/",$something);
			$something = str_replace("css/",$base_url."/css/",$something);
			$something = str_replace("upfiles/",$base_url."/upfiles/",$something);
		}
		if($rep){	
			$something=preg_replace($rep,$repto,$something);
		}
		
		$handle = @fopen($tofile,w);
		@fwrite($handle,$something);
		@fclose($handle);
	}else{
		//chtm($fromurl,$tofile,$newfold="",$rep="",$repto="");
	}
}

function wl($subject){
	global $db;
	$optime = date("Y-m-d H:i:s");
	return $db->query("insert into order_log(subject,oper,optime) values ('$subject','$_SESSION[Admin_Name]','$optime')");	
}

function cur_pos(){
	global $db,$id,$pid;
	//当前位置:<a href="index.html">首页</a> > 网站推广
	$page = end(explode("/",$_SERVER['PHP_SELF']));
	$qstr = $_SERVER['QUERY_STRING'];
	if($qstr && $id){
		$page.="?";	
	}	
	$pos = "当前位置:";
	switch($page){
		case 'index1.php':
			$pos.="<a href='/index.html'>首页</a>";break;
		case 'about_us.php':
			$pos.="<a href='/index.html'>首页</a> > 关于我们";break;
		case 'contact_us.php':
			$pos.="<a href='/index.html'>首页</a> > 联系我们";break;
		case 'case.php':
			$pos.="<a href='/index.html'>首页</a> > 成功案例";break;
		case 'case.php?':
			$s = gcs("my_case_type"," where id='$id'");
			$subjects = $s['subject'];
			$pos.="<a href='/index.html'>首页</a> > <a href='/case.html'>成功案例</a> > $subjects";break;
		case 'seo.php':
			$pos.="<a href='/index.html'>首页</a> > 网站推广";break;
		case 'publicity_website_design.php':
			$pos.="<a href='/index.html'>首页</a> > 网站套餐";break;
		case 'publicity_website_design_detail.php?':
			$s = gcs("my_taocan"," where id='$id'");
			$subjects = $s['subject'];
			$pos.="<a href='/index.html'>首页</a> > <a href='/publicity_website_design.html'>网站套餐</a> > $subjects";break;
		case 'solutions.php':
			$pos.="<a href='/index.html'>首页</a> > 建站方案";break;
		case 'solutions.php?':
			$s = gcs("my_news"," where id='$id'");
			$subjects = $s['subject'];
			$pos.="<a href='/index.html'>首页</a> > <a href='/solutions.html'>建站方案</a> > $subjects";break;
		case 'module_price.php':
			$pos.="<a href='/index.html'>首页</a> > 功能报价";break;
		case 'website_knowledge.php':
			$pos.="<a href='/index.html'>首页</a> > 建站知识";break;
		case 'website_knowledge.php?':
			$s = gcs("my_news_type"," where id='$id'");
			if($s['pid']!=0){
				$s1 = gcs("my_news_type"," where id='$s[pid]'");
				$subjects = '<a href="/website_knowledge_'.$s1['id'].'_1.html">'.$s1['subject'].'<a> > '.$s['subject'];
			}else{
				$subjects = $s['subject'];
			}	
			$pos.="<a href='/index.html'>首页</a> > <a href='/website_knowledge.html'>建站知识</a> > $subjects";break;
		case 'website_knowledge_detail.php?':
			$a = gcs("my_news"," where id='$id'");
			$ids = $a['pid'];
			$s = gcs("my_news_type"," where id='$ids'");
			if($s['pid']!=0){
				$s1 = gcs("my_news_type"," where id='$s[pid]'");
				$subjects = '<a href="/website_knowledge_'.$s1['id'].'_1.html">'.$s1['subject'].'<a> > <a href="/website_knowledge_'.$s['id'].'_1.html">'.$s['subject'].'</a> > '.$a['subject'];
			}else{
				$subjects = $s['subject'];
			}	
			$pos.="<a href='/index.html'>首页</a> > <a href='/website_knowledge.html'>建站知识</a> > $subjects";break;
			
		case 'website_process.php':
			$pos.="<a href='/index.html'>首页</a> > 建站流程";break;
		case 'news.php':
			$pos.="<a href='/index.html'>首页</a> > 我们动态";break;
		case 'news_detail.php?':
			$a = gcs("my_news"," where id='$id'");
			$subjects = $a['subject'];
			$pos.="<a href='/index.html'>首页</a> > <a href='/news.html'>我们动态</a> > $subjects";break;
		case 'templates.php' || 'templages.php?':
			$pos.="<a href='/index.html'>首页</a> > 网站模板";break;
		default:
			$pos.="<a href='/index.html'>首页</a> > 在线试用";break;
	}
	return $pos;
}

function time2string($second){
	$day = floor($second/(3600*24));
	$second = $second%(3600*24);//除去整天之后剩余的时间
	$hour = floor($second/3600);
	$second = $second%3600;//除去整小时之后剩余的时间 
	$minute = floor($second/60);
	$second = $second%60;//除去整分钟之后剩余的时间 
	//返回字符串
	return $day.'天'.$hour.'时'.$minute.'分'.$second.'秒';
}



//各种通过手机提交的文本信息存在表情处理
/*
 * $type 1 为入库处理 2 为出库处理
*/
function content_handle($content,$type)
{
	if($type == 1)
	{
		$content = preg_replace_callback('/[\xf0-\xf7].{3}/', function($r) { return '@E' . base64_encode($r[0]);}, $content);
	}
	else
	{
		$content = preg_replace_callback('/@E(.{6}==)/', function($r) {return base64_decode($r[1]);}, $content);
	}
	
	return $content;
	
}

?>