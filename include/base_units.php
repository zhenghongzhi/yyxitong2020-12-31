<?PHP

/**
 * 图片加水印（适用于png/jpg/gif格式）
 * 
 * @author flynetcn
 *
 * @param $srcImg 原图片
 * @param $waterImg 水印图片
 * @param $savepath 保存路径
 * @param $savename 保存名字
 * @param $positon 水印位置 
 * 1:顶部居左, 2:顶部居右, 3:居中, 4:底部局左, 5:底部居右 
 * @param $alpha 透明度 -- 0:完全透明, 100:完全不透明
 * 
 * @return 成功 -- 加水印后的新图片地址
 *          失败 -- -1:原文件不存在, -2:水印图片不存在, -3:原文件图像对象建立失败
 *          -4:水印文件图像对象建立失败 -5:加水印后的新图片保存失败
 */
function img_water_mark($srcImg, $waterImg, $savepath=null, $savename=null, $positon=3, $alpha=30)
{
    $temp = pathinfo($srcImg);
    $name = $temp['basename'];
    $path = $temp['dirname'];
    $exte = $temp['extension'];
    $savename = $savename ? $savename : $name;
    $savepath = $savepath ? $savepath : $path;
    $savefile = $savepath .'/'. $savename;
    $srcinfo = @getimagesize($srcImg);
    if (!$srcinfo) {
        return -1; //原文件不存在
    }
    $waterinfo = @getimagesize($waterImg);
    if (!$waterinfo) {
        return -2; //水印图片不存在
    }
    $srcImgObj = image_create_from_ext($srcImg);
    if (!$srcImgObj) {
        return -3; //原文件图像对象建立失败
    }
    $waterImgObj = image_create_from_ext($waterImg);
    if (!$waterImgObj) {
        return -4; //水印文件图像对象建立失败
    }
    switch ($positon) {
    //1顶部居左
    case 1: $x=$y=0; break;
    //2顶部居右
    case 2: $x = $srcinfo[0]-$waterinfo[0]; $y = 0; break;
    //3居中
    case 3: $x = ($srcinfo[0]-$waterinfo[0])/2; $y = ($srcinfo[1]-$waterinfo[1])/2; break;
    //4底部居左
    case 4: $x = 0; $y = $srcinfo[1]-$waterinfo[1]; break;
    //5底部居右
    case 5: $x = $srcinfo[0]-$waterinfo[0]; $y = $srcinfo[1]-$waterinfo[1]; break;
    default: $x=$y=0;
    }
    imagecopymerge($srcImgObj, $waterImgObj, $x, $y, 0, 0, $waterinfo[0], $waterinfo[1], $alpha);
    switch ($srcinfo[2]) {
    case 1: imagegif($srcImgObj, $savefile); break;
    case 2: imagejpeg($srcImgObj, $savefile); break;
    case 3: imagepng($srcImgObj, $savefile); break;
    default: return -5; //保存失败
    }
    imagedestroy($srcImgObj);
    imagedestroy($waterImgObj);
    return $savefile;
}


function image_create_from_ext($imgfile)
{
    $info = getimagesize($imgfile);
    $im = null;
    switch ($info[2]) {
    case 1: $im=imagecreatefromgif($imgfile); break;
    case 2: $im=imagecreatefromjpeg($imgfile); break;
    case 3: $im=imagecreatefrompng($imgfile); break;
    }
    return $im;
}
/*
函数功能：对要输出的字符串进行转换
*/
function Char_cv($msg){
	$msg = str_replace('&amp;','&',$msg);
	$msg = str_replace('&nbsp;',' ',$msg);
	$msg = str_replace('"','&quot;',$msg);
	$msg = str_replace("'",'&#39;',$msg);
	$msg = str_replace("<","&lt;",$msg);
	$msg = str_replace(">","&gt;",$msg);
	$msg = str_replace("\t","   &nbsp;  &nbsp;",$msg);
	$msg = str_replace("\r","",$msg);
	$msg = str_replace("   "," &nbsp; ",$msg);
	$msg = str_replace("\n","<br>",$msg);
	$msg = str_replace("\n","<br />",$msg);
	return $msg;
}

/*
函数功能：对要输出的字符串进行反转换
 */
function Char_rev($msg){
	$msg = str_replace('&','&amp;',$msg);
	$msg = str_replace(' ','&nbsp;',$msg);
	$msg = str_replace('&quot;','"',$msg);
	$msg = str_replace("&#39;","'",$msg);
	$msg = str_replace("&lt;","<",$msg);
	$msg = str_replace("&gt;",">",$msg);
	$msg = str_replace("   &nbsp;  &nbsp;","\t",$msg);
	$msg = str_replace("","\r",$msg);
	$msg = str_replace(" &nbsp; ","   ",$msg);
	$msg = str_replace("<br>","\n",$msg);
	$msg = str_replace("<br />","\n",$msg);
	return $msg;
}


/*
函数功能：上传文件
参数描述：$files_name-上传文件域名称,如：upfile1；
			$upload_dir-上传目录,相对于upfile目录，如：parties/200604/35/；
			$max_size-大小限制,以KB为单位，默认值为0；
			返回值格式，如：2006041203272511502.gif
*/
function upload_file($file_name,$upload_dir,$max_size,$ids=0){
	
	
	$max_size*=1024;//把充许上传最大容量的单位转换成M
	$files_ext=array('.gif','.JPG','.jpg','.jpeg','.bmp','.mp3','.mid','.wav','.wmv');//充许上传的文件格式
	if($ids>0){
		$files_size=$_FILES[$file_name]['size'][$ids-1];
		$files_name=$_FILES[$file_name]['name'][$ids-1];
		$files_tempname = $_FILES[$file_name]['tmp_name'][$ids-1];
	}else{
		$files_size=$_FILES[$file_name]['size'];
		$files_name=$_FILES[$file_name]['name'];
		$files_tempname = $_FILES[$file_name]['tmp_name'];
	}
	
	//创建多级文件夹
	$array_dir=explode("/",$upload_dir);//把多级目录分别放到数组中
	for($i=0;$i<count($array_dir)-1;$i++){
		$path .= $array_dir[$i]."/";
		if(!file_exists($path)){
			mkdir($path);
		}
	}
	

	$file_ext=substr($files_name,strrpos($files_name,"."));	
	$file_ext=strtolower($file_ext);
	
	if($file_ext == 'js' || $file_ext == 'php' || $file_ext == 'html'){
		er("受限制的文件",2);
	}
	
	/*取出文件后缀名，strrpos()从标记开始前字节个数(不算标记),substr()显示从第strrpos()之后的字符*/

	if($files_size>$max_size){//检查文件大小
		exit("文件大小不能超过 ".$max_size." KB!");
	}
	
	$files_name=date("YmdHis").rand().$file_ext;
	$upload_file = $upload_dir. $files_name;//上传后文件的路径及文件名

	if (move_uploaded_file($files_tempname, $upload_file)) {
		return $files_name;//返回文件名
	} else {
		print "上传错误! 以下是上传的信息:\n";
		echo "<script>alert('上传文件有误！')</script>";
		//print_r($_FILES);
	}


}
function upload_file1($file_name,$upload_dir,$max_size,$ids=0){
	
	
	$max_size*=1024;//把充许上传最大容量的单位转换成M
	$files_ext=array('.gif','.JPG','.jpg','.jpeg','.bmp','.mp3','.mid','.wav','.wmv');//充许上传的文件格式
	if($ids>0){
		$files_size=$_FILES[$file_name]['size'][$ids-1];
		$files_name=$_FILES[$file_name]['name'][$ids-1];
		$files_tempname = $_FILES[$file_name]['tmp_name'][$ids-1];
	}else{
		$files_size=$_FILES[$file_name]['size'];
		$files_name=$_FILES[$file_name]['name'];
		$files_tempname = $_FILES[$file_name]['tmp_name'];
	}
	
	//创建多级文件夹
	$array_dir=explode("/",$upload_dir);//把多级目录分别放到数组中
	for($i=0;$i<count($array_dir)-1;$i++){
		$path .= $array_dir[$i]."/";
		if(!file_exists($path)){
			mkdir($path);
		}
	}

	$file_ext=substr($files_name,strrpos($files_name,"."));	
	$file_ext=strtolower($file_ext);
	
	if($file_ext == 'js' || $file_ext == 'php' || $file_ext == 'html'){
		er("受限制的文件",2);
	}
	
	/*取出文件后缀名，strrpos()从标记开始前字节个数(不算标记),substr()显示从第strrpos()之后的字符*/

	if($files_size>$max_size){//检查文件大小
		exit("文件大小不能超过 ".$max_size." KB!");
	}
	
	$files_name=date("YmdHis").rand().$file_ext;
	$upload_file = $upload_dir. $files_name;//上传后文件的路径及文件名
	if (move_uploaded_file($files_tempname, $upload_file)) {
		return $files_name;//返回文件名
	} else {
		print "上传错误! 以下是上传的信息:\n";
		echo "<script>alert('上传文件有误！')</script>";
		//print_r($_FILES);
	}


}
function upload_file_chinese($file_name,$upload_dir,$max_size,$ids=0,$rename){
	//$file_chinese_name=$rename;
	if($rename){
		$file_chinese_name=$rename;
	}else{
	$file_chinese_name=substr($_FILES[$file_name]['name'],0,strrpos($_FILES[$file_name]['name'], '.'));
	}
	$file_chinese_name=$rename;
	$max_size*=1024;//把充许上传最大容量的单位转换成M
	$files_ext=array('.gif','.jpg','.ppt','.pptx','.xls','.xlsx','.doc','.docx','.jpeg','.bmp','.mp3','.mid','.wav','.wmv');//充许上传的文件格式
	if($ids>0){
		$files_size=$_FILES[$file_name]['size'][$ids-1];
		$files_name=$_FILES[$file_name]['name'][$ids-1];
		$files_tempname = $_FILES[$file_name]['tmp_name'][$ids-1];
	}else{
		$files_size=$_FILES[$file_name]['size'];
		$files_name=$_FILES[$file_name]['name'];
		$files_tempname = $_FILES[$file_name]['tmp_name'];
	}

	//创建多级文件夹
	$array_dir=explode("/",$upload_dir);//把多级目录分别放到数组中
	for($i=0;$i<count($array_dir)-1;$i++){
		$path .= $array_dir[$i]."/";
		if(!file_exists($path)){
			//$res=mkdir(iconv("UTF-8", "GBK", $path),0777,true); 
			mkdir($path);
		}
	}
	

	$file_ext=substr($files_name,strrpos($files_name,"."));	
	$file_ext=strtolower($file_ext);
	
	if($file_ext == 'js' || $file_ext == 'php' || $file_ext == 'html'){
		er("受限制的文件",2);
	}
	
	/*取出文件后缀名，strrpos()从标记开始前字节个数(不算标记),substr()显示从第strrpos()之后的字符*/

	if($files_size>$max_size){//检查文件大小
		//exit("文件大小不能超过 ".$max_size." KB!");
	}
	
	$files_name=$file_chinese_name.$file_ext;//文件名称
	$upload_file = $upload_dir. $files_name;//上传后文件的路径及文件名

	if (move_uploaded_file($files_tempname, $upload_file)) {
		return $files_name;//返回文件名
	} else {
		print "上传错误! 以下是上传的信息:\n";
		echo "<script>alert('上传文件有误！')</script>";
		//print_r($_FILES);
	}


}


function upload_file_chinese_safe($file_name,$upload_dir,$max_size,$ids=0,$rename){
	//$file_chinese_name=$rename;
	if($rename){
		$file_chinese_name=$rename;
	}else{
	$file_chinese_name=substr($_FILES[$file_name]['name'],0,strrpos($_FILES[$file_name]['name'], '.'));
	}
	$file_chinese_name=$rename;
	$max_size*=1024;//把充许上传最大容量的单位转换成M
	$files_ext=array('.gif','.jpg','.ppt','.pptx','.xls','.xlsx','.doc','.docx','.jpeg','.bmp','.mp3','.mid','.wav','.wmv');//充许上传的文件格式
	if($ids>0){
		$files_size=$_FILES[$file_name]['size'][$ids-1];
		$files_name=$_FILES[$file_name]['name'][$ids-1];
		$files_tempname = $_FILES[$file_name]['tmp_name'][$ids-1];
	}else{
		$files_size=$_FILES[$file_name]['size'];
		$files_name=$_FILES[$file_name]['name'];
		$files_tempname = $_FILES[$file_name]['tmp_name'];
	}

	//创建多级文件夹
	$array_dir=explode("/",$upload_dir);//把多级目录分别放到数组中
	for($i=0;$i<count($array_dir)-1;$i++){
		$path .= $array_dir[$i]."/";
		if(!file_exists($path)){
			//$res=mkdir(iconv("UTF-8", "GBK", $path),0777,true); 
			mkdir($path);
		}
	}
	

	$file_ext=substr($files_name,strrpos($files_name,"."));	
	$file_ext=strtolower($file_ext);
	
	if($file_ext == 'js' || $file_ext == 'php' || $file_ext == 'html'){
		er("受限制的文件",2);
	}
	
	/*取出文件后缀名，strrpos()从标记开始前字节个数(不算标记),substr()显示从第strrpos()之后的字符*/

	if($files_size>$max_size){//检查文件大小
		//exit("文件大小不能超过 ".$max_size." KB!");
	}
	
	$files_name=$file_chinese_name.date("YmdHis").rand().$file_ext;//文件名称
	$upload_file = $upload_dir. $files_name;//上传后文件的路径及文件名

	if (move_uploaded_file($files_tempname, $upload_file)) {
		return $files_name;//返回文件名
	} else {
		print "上传错误! 以下是上传的信息:\n";
		echo "<script>alert('上传文件有误！')</script>";
		//print_r($_FILES);
	}


}
//字符串截取函数
function substrs($content,$length) {
	if($length && strlen($content)>$length){
		$num=0;
		for($i=0;$i<$length-3;$i++) {
			if(ord($content[$i])>127){
				$num++;
			}
		}
		$num%2==1 ? $content=substr($content,0,$length-4):$content=substr($content,0,$length-3);
		$content.='..';
	}
	return $content;
}

function cut_str($string, $sublen, $start = 0, $code = 'UTF-8') 
{ 
if($code == 'UTF-8') 
{ 
$pa ="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/"; 
preg_match_all($pa, $string, $t_string); if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen))."..."; 
return join('', array_slice($t_string[0], $start, $sublen)); 
} 
else 
{ 
$start = $start*2; 
$sublen = $sublen*2; 
$strlen = strlen($string); 
$tmpstr = ''; for($i=0; $i<$strlen; $i++) 
{ 
if($i>=$start && $i<($start+$sublen)) 
{ 
if(ord(substr($string, $i, 1))>129) 
{ 
$tmpstr.= substr($string, $i, 2); 
} 
else 
{ 
$tmpstr.= substr($string, $i, 1); 
} 
} 
if(ord(substr($string, $i, 1))>129) $i++; 
} 
if(strlen($tmpstr)<$strlen ) $tmpstr.= "..."; 
return $tmpstr; 
} 
}

/*
设置COOKIE
*/
function get_date($timestamp,$timeformat=''){
	global $db_datefm,$_datefm,$_timedf;
	$date_show=$timeformat ? $timeformat : ($_datefm ? $_datefm : $db_datefm);
	return date($date_show,$timestamp+$_timedf*60);
}

//16进制互相转换函数
function SingleDecToHex($dec)
{
	$tmp="";
	$dec=$dec%16;
	if($dec<10)
		return $tmp.$dec;
	$arr=array("a","b","c","d","e","f");
	$k=$dec-10;
	return $tmp.$arr[$k];
}

function SingleHexToDec($hex){
	$v=Ord($hex);
	if(47<$v&&$v<58)
		return $v-48;
	if(96<$v&&$v<103)
		return $v-87;
}

function SetToHexString($str){ //转16进制
	if(!$str)returnfalse;
		$tmp="";
	for($i=0;$i<strlen($str);$i++){
		$ord=Ord($str[$i]);
		$tmp.=SingleDecToHex(($ord-$ord%16)/16);
		$tmp.=SingleDecToHex($ord%16);
	}
	return $tmp;
}

function UnsetFromHexString($str){ //逆传16进制
	if(!$str)returnfalse;
		$tmp="";
	for($i=0;$i<strlen($str);$i+=2){
		$tmp.=chr(SingleHexToDec(substr($str,$i,1))*16+SingleHexToDec(substr($str,$i+1,1)));
	}
	return $tmp;
}

/*
函数功能：读并输出图片文件流
参数描述：$name-文件域名称:ext:../../images/test.jpg；
*/
function readfiles($name,$base_url,$pic_ext){
	$name=UnsetFromHexString($name);
	$a=$base_url."/".$name;
	echo $a;
	exit;
	$fp = fopen($a, 'rb');
	header("Content-Type: image/jpeg");
	fpassthru($fp);
	exit;
}




		
function get_image_size($pic_url,$limit_w,$limit_h){
	list($width, $height, $type, $attr) =@getimagesize($pic_url);
	if($height>=$limit_h && $width>=$limit_w){
		
		$k=$width / $height;
		$k1=$limit_w / $limit_h;
		if($k>$k1){ //宽度优先
			$width=$limit_w;
			$height=$limit_w / $k;
		}else{ //高度优先
			$height=$limit_h;
			$width=$limit_h * $k;
		}
	}else{
		if($height>=$limit_h && $width<=$limit_w){
			$heigth1=$height;
			$height=$limit_h;
			$width=$width * ($limit_h /$heigth1);
		}
		if($width>=$limit_w && $height<$limit_h){
			$width1=$width;
			$width=$limit_w;
			$height=$height * ($limit_w /$width1);
		}
	}
	return $width."-".$height;
}		


/*
返回包含一个日期的参数，这一日期还加上了一段时间间隔。
$interval：y-年;q-季;m-月;d-日;w-周;
$number：数值表达式，是要加上的时间间隔的数目。其数值可以为正数（得到未来的日期），也可以为负数（得到过去的日期）。
$date：非必填，表示日期的文字。空表示当前日期。
返回值：YYYY-MM-DD
*/
function date_add1($interval, $number, $date=''){
	if($date=='') $date=date('Y-m-d');
	
	$year=substr($date,0,4);
	$month=substr($date,5,2);
	$day=substr($date,8,2);
	
	switch ($interval) { 
		case "y": $year +=$number; break; 
		case "q": $month +=($number*3); break; 
		case "m": $month +=$number; break;  
		case "d": $day+=$number; break; 
		case "w": $day+=($number*7); break; 
	}
	$timestamp = mktime(0 ,0, 0, $month,$day, $year); 
	return date('Y-m-d',$timestamp);
} 




function random($length, $numeric = 0) {
	$numeric=1;
	PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
	if($numeric) {
		$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
	} else {
		$hash = '';
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		$max = strlen($chars) - 1;
		for($i = 0; $i < $length; $i++) {
			$hash .= $chars[mt_rand(0, $max)];
		}
	}
	return $hash;
}

function is_email($str){
	return preg_match("/^(([0-9a-zA-Z]+)|([0-9a-zA-Z]+[_.0-9a-zA-Z-]*[0-9a-zA-Z-]+))@([a-zA-Z0-9-]+[.])+([a-zA-Z]|net|NET|asia|ASIA|com|COM|gov|GOV|mil|MIL|org|ORG|edu|EDU|int|INT|cn|CN|cc|CC)$/", $str);
}

function is_url($str){
	return preg_match("/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/", $str);
}

function base_url(){
	$base_url = "http://".$_SERVER['HTTP_HOST']; 
	$scs = explode("/",$_SERVER['SCRIPT_NAME']);
	for($i=0;$i<count($scs)-1;$i++){
		if(!empty($scs[$i])){
			$base_url.="/".$scs[$i];	
		}	
	}
	return $base_url;
}

/**
 * 创建一个JSON格式的数据
 *
 * @access  public
 * @param   string      $content
 * @param   integer     $error
 * @param   string      $message
 * @param   array       $append
 * @return  void
 */
function make_json_response($content='', $error="0", $message='', $append=array())
{
    include_once('cls_json.php');

    $json = new JSON;

    $res = array('error' => $error, 'message' => $message, 'content' => $content);

    if (!empty($append))
    {
        foreach ($append AS $key => $val)
        {
            $res[$key] = $val;
        }
    }

    $val = $json->encode($res);

    exit($val);
}

/**
 *
 *
 * @access  public
 * @param
 * @return  void
 */
function make_json_result($content, $message='', $append=array())
{
    make_json_response($content, 0, $message, $append);
}

/**
 * 创建一个JSON格式的错误信息
 *
 * @access  public
 * @param   string  $msg
 * @return  void
 */
function make_json_error($msg)
{
    make_json_response('', 1, $msg);
}

function showday($par,$date_format='%Y-%m-%d'){
	return ' onclick="showCalendar(\''.$par.'\', \''.$date_format.'\', false, false,\''.$par.'\');" ';
}

function fav($url,$name){
  return "onclick='window.external.AddFavorite(\"$url\",\"$name\")' target='_self'";
}

function home($url){
	return "onclick='this.style.behavior=&quot;url(#default#homepage)&quot;;this.setHomePage(\"".$url."\")' target='_self'";
}

function select($a,$b){
	if($a==$b){
	return "selected";
	}else{
	return "";
	}
}


function radio($a,$b){
	if($a==$b){
	return "checked";
	}else{
	return "";
	}
}

function qi(){
	return "";
	return "TEXT-JUSTIFY: inter-ideograph; TEXT-ALIGN: justify;WORD-WRAP: break-word;TABLE-LAYOUT: fixed;word-break:break-all;";
}

function err($msg,$url=''){
	global $base_url;
	if($url){
		$str = file_get_contents($base_url."/err_tpl_url.php");
		$str = str_replace("{url}",$url,$str);
	}else{
		$str = file_get_contents($base_url."/err_tpl.php");
	}	
	$str = str_replace("{errmsg}",$msg,$str);
	echo $str;
	exit;
}
function quescrypt($questionid, $answer) {
	return $questionid > 0 && $answer != '' ? substr(md5($answer.md5($questionid)), 16, 8) : '';
}

function ok($msg,$url='history.go(-1);',$okstr='返 回',$getstr=''){
	global $base_url;
	$str = file_get_contents($base_url."/ok_tpl.php");
	$str = str_replace("{url}",$url,$str);
	$str = str_replace("{okmsg}",$msg,$str);
	$str = str_replace("{okstr}",$okstr,$str);
	$str = str_replace("{getstr}",$getstr,$str);
	echo $str;
	exit;
}

function debug($filename,$str,$mode){
	$handle = fopen($filename,$mode);
	fwrite($handle,$str);
	fclose($handle);
}

function ResizeImage($im,$maxwidth,$maxheight,$name){ 
$width = imagesx($im); 
$height = imagesy($im); 
if(($maxwidth && $width > $maxwidth) || ($maxheight && $height > $maxheight)){ 
if($maxwidth && $width > $maxwidth){ 
$widthratio = $maxwidth/$width; 
$RESIZEWIDTH=true; 
} 
if($maxheight && $height > $maxheight){ 
$heightratio = $maxheight/$height; 
$RESIZEHEIGHT=true; 
} 
if($RESIZEWIDTH && $RESIZEHEIGHT){ 
if($widthratio <$heightratio){ 
$ratio = $widthratio; 
}else{ 
$ratio = $heightratio; 
} 
}elseif($RESIZEWIDTH){ 
$ratio = $widthratio; 
}elseif($RESIZEHEIGHT){ 
$ratio = $heightratio; 
} 
$newwidth = $width * $ratio; 
$newheight = $height * $ratio; 
if(function_exists("imagecopyresampled")){ 
$newim = imagecreatetruecolor($newwidth, $newheight); 
imagecopyresampled($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
}else{ 
$newim = imagecreate($newwidth, $newheight); 
imagecopyresized($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
} 
ImageJpeg ($newim,$name . ".jpg"); 
ImageDestroy ($newim); 
}else{ 
ImageJpeg ($im,$name . ".jpg"); 
} 
} 



?>