<?PHP
//分页的类
class timer
	{ 
	    var $StartTime = 0;
	    var $StopTime = 0;
	    var $TimeSpent = 0;
	
		function start(){
			$this->StartTime = microtime();
		}
		function stop(){
			$this->StopTime = microtime();
		}
		function spent(){
			if ($this->TimeSpent) {
				return $this->TimeSpent;
			} else {
				$StartMicro = (float)substr($this->StartTime,0,10);
				$StartSecond = (int)substr($this->StartTime,11,10);
				$StopMicro = (float)substr($this->StopTime,0,10);
				$StopSecond= (int)substr($this->StopTime,11,10);
				$start = doubleval($StartMicro)+$StartSecond;
				$stop=doubleval($StopMicro)+$StopSecond;
				$this->TimeSpent=$stop-$start;
				return substr($this->TimeSpent,0,8)."秒";
			}
		}
	}

class Page {
	var $m_page; //当前页面
	var $m_dbcount; //数据总数
	var $m_pagecount; //页面总数
	var $m_size; //每个页面数据量
	var $m_start; //当前页面开始记录数
	var $with_back;//是否带返回选项
	var $backpage; //返回到哪里

	function page($v_page, $v_dbcount, $v_size,$back=0,$back_page='photos.php')
	{
		if(empty($v_page))
			$this->m_page=1;
		else
			$this->m_page = $v_page;
		$this->m_dbcount = $v_dbcount;
		$this->m_size = $v_size;
		$this->m_pagecount = ceil($this->m_dbcount / $this->m_size);
		$this->m_start = ($this->m_page - 1) * $this->m_size;
		$this->with_back = $back;
		$this->backpage = $back_page;
		
		if($v_page > $this->m_pagecount)
			$this->m_page = 1;
		
	}

	function start()
	{
		return $this->m_start;
	}
	function getpage()
	{
		return $this->m_page;
	}
	function count()
	{
		return $this->m_pagecount;
	}
	function begin()
	{
		return 1;
	}
	function end()
	{
		if($this->m_pagecount==0)
			$end = 1;
		else
			$end = $this->m_pagecount;
		return $end;
	}
	function pre()
	{

		if($this->m_pagecount==0 || $this->m_page == 1 )
			$pre = 1;
		else
			$pre = $this->m_page - 1;
		return $pre;
	}
	function next()
	{
		if($this->m_pagecount==0)
			$next = 1;
		elseif($this->m_pagecount==$this->m_page)
			$next = $this->m_pagecount;
		else
			$next = $this->m_page + 1;
		return $next;
	}
	function genMysqlLimit() {
		return " LIMIT ".$this->start().", ".$this->m_size;
	}
	function genHtml($baseLink, $pageFont="") {

		$firstop = strpos($baseLink, "?")<0?"?":"&";
		
		if($this->with_back == 10){
		
			$pages = "";
			
			$pages.="第 ".$this->m_page."/".$this->m_pagecount." 页（共 ".$this->m_dbcount." 条）&nbsp;每页<input type='text' id='pagesize' value='".$this->m_size."' size=3 onchange=\"if(parseInt(this.value)>0){window.location='".$baseLink.$firstop."pagesizes='+this.value;}else{alert('每页数量必需填写数字，并且要大于0');}\">条&nbsp;&nbsp;";
			$pages.= "<a href=".$baseLink.$firstop."page=".$this->begin()." title=\"首页\">首页</a>&nbsp;<a href=".$baseLink.$firstop."page=".$this->pre()." title=\"上 ".$this->m_size." 页\">上一页</a>";
			if($this->count() < 10) {
				$k = 1;
				$j = $this->count();
			} else {
				$m = intval(($this->getpage()-1)/10);
				$k = $m*10+1;
				$j = $m*10 + 10;
			}
	
			for($i = $k; $i <= $j && $i<=$this->count(); $i++) {
				if($i==$this->m_page ) {
					$pages.= "&nbsp;<a href=".$baseLink.$firstop."page=".$i."><font color=\"red\"><b>$i</b></font></a>";
				} else {
					$pages.= "&nbsp;<a href=".$baseLink.$firstop."page=".$i.">$i</a>";
				}
			}
			$pages.= "&nbsp;<a href=".$baseLink.$firstop."page=".$this->next()." title=\"下 ".$this->m_size." 页\">下一页</a>&nbsp;<a href=".$baseLink.$firstop."page=".$this->end()." title=\"尾页\">尾页</a>";
			
			return $pages;
		}elseif($this->with_back == 1){
		    
			//伪静态 $firstop='_';
			$firstop='&';
			$pages = "";
			//中文应用伪静态
			//$pages.= "共".$this->m_pagecount."页&nbsp;<a href=".$baseLink.$firstop."page".$this->begin().".html title=\"首页\">首页</a>&nbsp;<a href=".$baseLink.$firstop."page".$this->pre().".html title=\"上一页\">上一页</a>";
			//英文伪静态
			//$pages.="<a href=".$baseLink.$firstop."page".$this->pre().".html title=\"Pre\" class=\"page_button page0\" >Previous</a>";
			
			$pages.="<a href=".$baseLink.$firstop."&page=".$this->pre()." title=\"上一页\" class=\"page_button page0\" >上一页</a>";
			if($this->count() < 5) {
				$k = 1;
				$j = $this->count();
			} else {
				$m = intval(($this->getpage()-1)/5);
				$k = $m*5+1;
				$j = $m*5 + 5;
			}
			
			//伪静态 $firstop='_';
			
			$firstop='&';
			for($i = $k; $i <= $j && $i<=$this->count(); $i++) {
				if($i==$this->m_page ) {
					//选中样式
		
					$pages.= "&nbsp;<a   class=\"curr\" href=".$baseLink.$firstop."page=".$i.">$i</a>";
					 
				} else {
					
					//没选的样式
					$pages.= "&nbsp;<a href=".$baseLink.$firstop."page=".$i.">$i</a>";
				}
			}
			//中文应用伪静态
		//	$pages.= "&nbsp;<a href=".$baseLink.$firstop."page".$this->next().".html title=\"下一页\">下一页</a>&nbsp;<a href=".$baseLink.$firstop."page".$this->end().".html title=\"尾页\">尾页</a>";		
			//英文伪静态
			//$pages.= "&nbsp;<a href=".$baseLink.$firstop."page".$this->next().".html title=\"Next\" class=\"page_button page1\" >Next</a>";		
			
			$pages.= "&nbsp;<a href=".$baseLink.$firstop."page=".$this->next()." title=\"下一页\" class=\"page_button page1\" >下一页</a>";
			

			return $pages;
		}

		return $pages;
		
	}
}

class getpageseo{
	var $arr;
	function getpageseo($page,$db){
		$this->arr=$db->get_one("select * From cuiniao_seo where pageurl='$page'");
	}
	
	function titles(){
		return $this->arr['titles']?$this->arr['titles']:"hxl";
	}
	function keywords(){
		return $this->arr['keywords']?$this->arr['keywords']:"hxl";
	}
	function descs(){
		return $this->arr['descs']?$this->arr['descs']:"hxl";
	}
	
	
}

?>